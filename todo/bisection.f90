function bisection(func, param, x1, x2, xacc) result(root)

!*******************************************************************************
! Copyright (C) 2009 Wojciech Lipinski
!
! This file is part of the Thermal Science Library (TSL).
!
! TSL is free software: you can redistribute it and/or modify
! it under the terms of the GNU Lesser General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! TSL is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU Lesser General Public License for more details.
!
! You should have received a copy of the GNU Lesser General Public License
! along with TSL.  If not, see <http://www.gnu.org/licenses/>.
!*******************************************************************************

!*******************************************************************************
! Description:
!    Acquire the root of a function in the region [x1, x2] by using the
!    bisection method. The function muse be monotonous in the region. 
!    In addition, you should make sure that there is a root during the region.
!
! Input:
!    func    The name of the defined function, written by the user
!            and transferd to the program
!    param   The coefficients array or the parameters of the function.
!            It is determined by the function type
!    x1      The left boundary of the region
!    x2      The right boundary of the region
!    xacc    The tolerance in the bisection method
! Output:
!    root    The root in the region [x1, x2]
!*******************************************************************************

!*******************************************************************************
! 'use' and 'implicit none' statements
!*******************************************************************************

   use tsl_basics_kinds,     only: k_i32, k_r64
   use tsl_basics_utils,     only: tsl_error
   implicit none

!*******************************************************************************
! Dummy argument declaration constructs
!*******************************************************************************

   interface
      function func(x, param)
         use tsl_basics_kinds,     only: k_r64
         implicit none
         real(k_r64),               intent(in) :: x
         real(k_r64), dimension(:), intent(in) :: param
         real(k_r64) :: func
      end function func
   end interface

   real(k_r64), dimension(:), intent(in) :: param
   real(k_r64),               intent(in) :: x1, x2, xacc
   real(k_r64) :: root

!*******************************************************************************
! Local declaration constructs
!*******************************************************************************

   integer(k_i32), parameter :: MAXIT = 40
   integer(k_i32)            :: j
   real(k_r64)                :: dx, f, fmid, xmid

!*******************************************************************************
! Execution part
!*******************************************************************************

   fmid = func(x2, param)
   f = func(x1, param)
   if (f*fmid >= 0.0) call tsl_error('bisect: root must be bracketed')
   if (f < 0.0) then
      root = x1
      dx   = x2-x1
   else
      root = x2
      dx   = x1-x2
   end if
   do j = 1, MAXIT
      dx   = dx*0.5_k_r64
      xmid = root+dx
      fmid = func(xmid, param)
      if (fmid <= 0.0) root = xmid
      if (abs(dx) < xacc .or. fmid == 0.0) return 
   end do
   call tsl_error('bisection: too many bisections')
   return

!*******************************************************************************
! 'end function' statement
!*******************************************************************************

end function bisection
