subroutine intersection_line_paraboloid(point_line, dir, c1, c2, r, &
                                      surface, points)

!*******************************************************************************
! Copyright (C) 2009 Zheng Liang
! Copyright (C) 2009 Wojciech Lipinski
!
! This file is part of the Thermal Science Library (TSL).
!
! TSL is free software: you can redistribute it and/or modify
! it under the terms of the GNU Lesser General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! TSL is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU Lesser General Public License for more details.
!
! You should have received a copy of the GNU Lesser General Public License
! along with TSL.  If not, see <http://www.gnu.org/licenses/>.
!*******************************************************************************

!*******************************************************************************
! Description:
!    Returns coordinates of an intersection point between an infinite line
!    and a finite-height elliptic paraboloid, whose standard equation is 
!    z = a(x**2 + y**2), consisting of one disk and a lateral wall.
!
! Input:
!    point_line   Point on the line
!    dir          Direction of the line (does not need to be normalized)
!    c1           Centers of the upper disk
!    c2           The origin point of the paraboloid
!    r            Radius of the upper disk
!
! Output:
!    surface      Vector containing indices of the surfaces with the intersection
!                 points. The indices can take the following values:
!                 0 = no intersection
!                 1 = disk containing c1
!                 2 = origin point
!                 3 = lateral wall
!                 Either both surface(1) and surface(2) are equal to 0 or
!                 both surface(1) and surface(2) are different from 0.
!                 The pathological cases of only one intersection point or an
!                 infinit number of intersection points are reported as
!                 "no intersection", i.e. surface(1) and surface(2)
!                 are equal to 0.
!    points       Coordinates of the intersection points ordered
!                 along direction dir
!*******************************************************************************

!*******************************************************************************
! 'use' and 'implicit none' statements
!*******************************************************************************

   use tsl_basics_kinds,     only: k_i16, k_r64, k_lg
   use tsl_math_polynomials, only: solve_quadratic
   use tsl_math_vector_algebra, only: vector_euclidean_norm
   use tsl_consants_math,   only: c_ux, c_uy, c_uz
   implicit none

!*******************************************************************************
! Dummy argument declaration constructs
!*******************************************************************************

   real(k_r64),    dimension(3),   intent(in) :: point_line, dir, c1, c2
   real(k_r64),                    intent(in) :: r
   integer(k_i16), dimension(2),   intent(out):: surface
   real(k_r64),    dimension(2,3), intent(out):: points

   interface
      pure function transform_coordinates(o1, x1, y1, z1, o2, x2, y2, z2, p1,& 
           td) result(p2)
         use tsl_basics_kinds, only: k_i8, k_r64, k_lg
         implicit none
         real(k_r64), dimension(3), intent(in) :: o1,x1, y1, z1, o2, x2, y2, &
                                                     z2, p1
         logical(k_lg),              intent(in), optional :: td
         real(k_r64), dimension(3)             :: p2
      end function transform_coordinates
   end interface

!*******************************************************************************
! Local declaration constructs
!*******************************************************************************

   real(k_r64), dimension(3) :: pbmain, pbx, pby
   real(k_r64), dimension(3) :: op, vp, pnew, vpnew, dirnew, c2new
   real(k_r64)                 :: dis, a, sa, sb, sc
   real(k_r64), dimension(2,3):: pointnew
   real(k_r64), dimension(2) :: lam, tt
   integer(k_i16)            :: i
   logical(k_lg)             :: flag

!*******************************************************************************
! Execution part
!*******************************************************************************
   
   pbmain = c1 - c2  !The main axis of the paraboloid, new z-axis
   dis = vector_euclidean_norm (pbmain)  ! The distance between c1 and c2
   pbmain = pbmain / dis
   call tripod(pbmain, pbx, pby)  !Calculate other axes in the new coordinate
   op = (/0,0,0/)         !The origin point
   vp = point_line + dir  !The other point on the line
   pnew = transform_coordinates(op, c_ux, c_uy, c_uz, c2, pbx, pby, pbmain, &
          point_line, .true.)   !The point on the line in the new coordinate
   vpnew = transform_coordinates(op, c_ux, c_uy, c_uz, c2, pbx, pby, pbmain, &
          vp, .true.)       !The other point on the line in the new coordinate
   dirnew = vpnew - pnew  !The direction vector in the new coordinate
   
   a = dis / (r**2)  !The coefficient a
   ! The parameter equation of the line x = x0 + v *t is used
   ! The variable tt is the parameter "t" in the line parameter equation
   ! The coefficients in the quadratic equation
   sa = a*dirnew(1)**2 + a*dirnew(2)**2
   sb = 2*a*pnew(1)*dirnew(1) + 2*a*pnew(2)*dirnew(2) - dirnew(3)
   sc = a*pnew(1)**2 + a*pnew(2)**2 - pnew(3)
   !The point on the disk
   c2new = (/0,0,0/)
   c2new(3) = dis
   !The case of one intersecting point with the paraboloid and one with the disk
   if (abs(sa) < 1e-8) then
      tt(1) = -sc/sb
      pointnew (1,:) = pnew + dirnew * tt(1)
      if (abs(pointnew(1,3)) < 1e-8) then
         surface (1) = 2
      else
         surface (1) = 3
      end if
      call intersection_line_plane(pnew, dirnew, c2new, c_uz, &
                               pointnew (2,:), flag)
      surface (2) = 1
   else
      ! Calculate the roots of tt
      call solve_quadratic(sa, sb, sc, tt, flag)
      if ( flag .eqv. .false.) then  !No intersection
         surface (1) = 0
         surface (2) = 0
      else if(abs(pointnew(1,3) - pointnew(2,3)) < 1e-8 .and. &
         abs(pointnew(1,3) - dis) < 1e-8 ) then  !Infinite
         surface (1) = 0
         surface (2) = 0   
      else
         pointnew (1,:) = pnew + dirnew * tt(1)
         pointnew (2,:) = pnew + dirnew * tt(2)
         if (abs(pointnew(1,3)) < 1e-8) then  !Origin point
               surface (1) = 2
         else if (abs(pointnew(1,3)) > dis) then !Upper disk
            call intersection_line_plane(pnew, dirnew, c2new, c_uz, &
                               pointnew (1,:), flag)
            surface (1) = 1
         else
            surface (1) = 3
         end if
         if (abs(pointnew(2,3)) < 1e-8) then  !Origin point
            surface (2) = 2
         else if (abs(pointnew(2,3)) > dis) then  !Upper disk
            call intersection_line_plane(pnew, dirnew, c2new, c_uz, &
                               pointnew (2,:), flag)
            surface (2) = 1
         else
            surface (2) = 3
         end if
     end if
  end if
  !Transform the intersecting points in the new coordinate to 
  !the original coordinate
  points(1,:) = transform_coordinates(c2, pbx, pby, pbmain, op, c_ux, c_uy, c_uz, &
          pointnew(1,:), .false.)  
  points(2,:) = transform_coordinates(c2, pbx, pby, pbmain, op, c_ux, c_uy, c_uz, &
          pointnew(2,:), .false.)  
   
  return

!*******************************************************************************
! 'end subroutine' statement
!*******************************************************************************

end subroutine intersection_line_paraboloid
