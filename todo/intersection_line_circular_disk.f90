subroutine intersection_line_circular_disk(point, dir, cp, cnormal, r, points&
                                             , flag)
!*******************************************************************************
! Copyright (C) 2009 Zheng Liang
! Copyright (C) 2009 Wojciech Lipinski
!
! This file is part of the Thermal Science Library (TSL).
!
! TSL is free software: you can redistribute it and/or modify
! it under the terms of the GNU Lesser General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! TSL is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU Lesser General Public License for more details.
!
! You should have received a copy of the GNU Lesser General Public License
! along with TSL.  If not, see <http://www.gnu.org/licenses/>.
!*******************************************************************************

!*******************************************************************************
! Description:
!    Returns coordinates of an intersection point between an infinite line
!    and a circular disk.
!
! Input:
!    point      Point on the line
!    dir        Direction of the line (does not need to be normalized)
!    cp         The center of the circular disk
!    cnormal    Normal basis vector of the disk surface, 
!    r          Radius of the circular disk
!
! Output:
!    points     Coordinates of the intersection point
!    flag       Logical flag taking the following values:
!               flag = .true. if the intersection point exists
!               flag = .false. if the intersection point does not exist
!                              or the line is on the disk surface
!*******************************************************************************

!*******************************************************************************
! 'use' and 'implicit none' statements
!*******************************************************************************

   use tsl_basics_kinds,     only: k_r64, k_lg
   use tsl_consants_math,   only: c_ux, c_uy, c_uz
   implicit none

!*******************************************************************************
! Dummy argument declaration constructs
!*******************************************************************************

   real(k_r64),    dimension(3),   intent(in) :: point, dir, cp, cnormal
   real(k_r64),                    intent(in) :: r
   real(k_r64),    dimension(3),  intent(out):: points
   logical(k_lg),                   intent(out), optional :: flag

   interface
      pure function transform_coordinates(o1, x1, y1, z1, o2, x2, y2, z2, p1,& 
           td) result(p2)
         use tsl_basics_kinds, only: k_i8, k_r64, k_lg
         implicit none
         real(k_r64), dimension(3), intent(in) :: o1,x1, y1, z1, o2, x2, y2, &
                                                     z2, p1
         logical(k_lg),              intent(in), optional :: td
         real(k_r64), dimension(3)             :: p2
      end function transform_coordinates
   end interface

!*******************************************************************************
! Local declaration constructs
!*******************************************************************************

   real(k_r64), dimension(3) :: pbx, pby
   real(k_r64), dimension(3) :: op, vp, pnew, vpnew, dirnew
   real(k_r64)                 :: tt
   real(k_r64), dimension(3):: pointnew
      
!*******************************************************************************
! Execution part
!*******************************************************************************

   call tripod(cnormal, pbx, pby)  !Calculate other axes in the new coordinate
   op = (/0,0,0/)          !The origin point
   vp = point + dir        !The other point on the line
   pnew = transform_coordinates(op, c_ux, c_uy, c_uz, cp, pbx, pby, cnormal, &
          point, .true.)    !The point on the line in the new coordinate
   vpnew = transform_coordinates(op, c_ux, c_uy, c_uz, cp, pbx, pby, cnormal, &
          vp, .true.)      !The other point on the line in the new coordinate
   dirnew = vpnew - pnew   !The direction vector in the new coordinate
   ! The parameter equation of the line x = x0 + v *t is used
   ! The variable tt is the parameter "t" in the line parameter equation
   if (abs(dirnew(3)) < 1e-8) then
      ! dirnew(3)=0 (z-direction component of the line direction vector
      ! In that case, the line is on the plane z=0
      ! We treat that case as no intersection
      flag = .false.
   else
      tt = -pnew(3) / dirnew(3)
      pointnew (:) = pnew + dirnew * tt
      ! Judge whether the intersecting point is inside the disk
      if ( (pointnew(1)**2 + pointnew(2)**2) > (r**2)) then !Outside
         flag = .false.
      else
         flag = .true.
      end if
   end if
   !Transform the intersecting points in the new coordinate to 
   !the original coordinate
   points(:) = transform_coordinates(cp, pbx, pby, cnormal, op, c_ux, c_uy, c_uz, &
             pointnew(:), .false.)  
   return
   
!*******************************************************************************
! 'end subroutine' statement
!*******************************************************************************

end subroutine intersection_line_circular_disk