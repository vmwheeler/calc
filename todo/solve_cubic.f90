pure subroutine solve_cubic(a, b, c, roots, flag)

!*******************************************************************************
! Copyright (C) 2007 Brian Gough
! Copyright (C) 2009 Zheng Liang
! Copyright (C) 2009 Wojciech Lipinski
!
! This file is part of the Thermal Science Library (TSL).
!
! TSL is free software: you can redistribute it and/or modify
! it under the terms of the GNU Lesser General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! TSL is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU Lesser General Public License for more details.
!
! You should have received a copy of the GNU Lesser General Public License
! along with TSL.  If not, see <http://www.gnu.org/licenses/>.
!*******************************************************************************

!*******************************************************************************
! Description:
!    Computes real roots of the cubic equation.
!
! Input:
!    a, b, c   Coefficients of the equation x**3 + a*x**2 + b*x + c = 0
!
! Output:
!    roots     Roots of the cubic equation
!    flag      Integer flag indicating the number of real roots (1 or 3):
!
! References:
!    [1] W.H. Press, S.A. Teukolsky, W.T. Vetterling, and B.P. Flannery.
!        Numerical Recipes in C++. Cambridge University Press, 2002. Page 191.
!*******************************************************************************

!*******************************************************************************
! 'use' and 'implicit none' statements
!*******************************************************************************

   use tsl_basics_kinds, only: k_r64, k_i32
   use tsl_consants_math, only: c_pi
   implicit none

!*******************************************************************************
! Dummy argument declaration constructs
!*******************************************************************************

   real(k_r64),                 intent(in)            :: a, b, c
   real(k_r64),   dimension(3), intent(out)           :: roots
   integer(k_i32),               intent(out)         :: flag

!*******************************************************************************
! Local declaration constructs
!*******************************************************************************

   real(k_r64) :: Q, R, Q3, R2
   real(k_r64) :: theta, norm
   real(k_r64) :: AA, BB

!*******************************************************************************
! Execution part
!*******************************************************************************

   Q = (a * a - 3 * b) / 9
   R = (2 * a * a * a - 9 * a * b + 27 * c) / 54
   Q3 = Q * Q * Q
   R2 = R * R
   if(abs(R) < 1E-16 .and. abs(Q) < 1E-16)  then  !Three same real roots
      roots(1) = - a / 3
      roots(2) = - a / 3
      roots(3) = - a / 3
      flag = 3
   else if(abs(R2 - Q3) < 1E-16) then
      if(R > 0) then
         roots(1) = -2 * sqrt(Q)  - a / 3
         roots(2) = sqrt(Q) - a / 3
         roots(3) = sqrt(Q) - a / 3
      else
         roots(1) = -sqrt(Q) - a / 3
         roots(2) = -sqrt(Q) - a / 3
         roots(3) = 2 * sqrt(Q)  - a / 3
      end if
      flag = 3
   else if(R2 < Q3) then  !Three real roots
      theta = acos (R / sqrt(Q**3))
      norm = -2 * sqrt(Q)
      roots(1) = norm * cos (theta / 3) - a / 3
      roots(2) = norm * cos ((theta + 2.0 * c_pi) / 3) - a / 3
      roots(3) = norm * cos ((theta - 2.0 * c_pi) / 3) - a / 3
      flag = 3
   else  !One real root
      AA = -sign(1._k_r64, R) * (abs (R) + sqrt (R2 - Q3)) ** (1.0/3.0)
      BB = Q / AA
      roots(1) = AA + BB - a / 3
      roots(2) = 0
      roots(3) = 0
      flag =1    !Only roots(1) is available
   end if
   
   return

!*******************************************************************************
! 'end subroutine' statement
!*******************************************************************************

end subroutine solve_cubic