function bisect(func, param, x1, x2, xacc) result(root)

!*******************************************************************************
! Copyright (C) 2008 Wojciech Lipinski
!
! This file is part of Thermal Science Library (TSL).
!
! TSL is free software: you can redistribute it and/or modify
! it under the terms of the GNU Lesser General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! TSL is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU Lesser General Public License for more details.
!
! You should have received a copy of the GNU Lesser General Public License
! along with TSL.  If not, see <http://www.gnu.org/licenses/>.
!*******************************************************************************

!*******************************************************************************
! Description:
!*******************************************************************************

!*******************************************************************************
! Use and implicit none statements
!*******************************************************************************

  use tsl_type, only: I4B, DP
  use tsl_util, only : tsl_error
  implicit none

!*******************************************************************************
! Dummy argument declaration constructs
!*******************************************************************************

  interface
    function func(x, param)
      use tsl_type
      implicit none
      real(DP), intent(in) :: x
      real(DP), dimension(:), intent(in) :: param
      real(DP) :: func
    end function func
  end interface
  real(DP), dimension(:), intent(in) :: param
  real(DP), intent(in) :: x1, x2, xacc
  real(DP) :: root

!*******************************************************************************
! Local declaration constructs
!*******************************************************************************

  integer(I4B), parameter :: MAXIT = 40
  integer(I4B) :: j
  real(DP) :: dx, f, fmid, xmid

!*******************************************************************************
! Execution part
!*******************************************************************************

  fmid = func(x2, param)
  f = func(x1, param)
  if(f*fmid >= 0.0) call tsl_error('bisect: root must be bracketed')
  if(f < 0.0) then
    root = x1
    dx = x2-x1
  else
    root = x2
    dx = x1-x2
  end if
  do j = 1,MAXIT
    dx = dx*0.5_DP
    xmid = root+dx
    fmid = func(xmid, param)
    if (fmid <= 0.0) root=xmid
    if (abs(dx) < xacc .or. fmid == 0.0) return 
  end do
  call tsl_error('bisect: too many bisects')
  return

!*******************************************************************************
! End function statement
!*******************************************************************************

end function bisect
