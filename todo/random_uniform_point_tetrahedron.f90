pure function random_uniform_point_tetrahedron(rn1, rn2, rn3, vertices) &
     result(point)

!*******************************************************************************
! Copyright (C) 2009 Wojciech Lipinski
!
! This file is part of the Thermal Science Library (TSL).
!
! TSL is free software: you can redistribute it and/or modify
! it under the terms of the GNU Lesser General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! TSL is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU Lesser General Public License for more details.
!
! You should have received a copy of the GNU Lesser General Public License
! along with TSL.  If not, see <http://www.gnu.org/licenses/>.
!*******************************************************************************

!*******************************************************************************
! Description:
!    Returns coordinates of a uniformly-distributed random point
!    in a tetrahedron.
!
! Input:
!    rn1, rn2, rn3    Three random numbers
!    vertices         The four vertices of the tetrahedron, 1*3 array
!
! Output:
!    point            The random point inside the tetrahedron
!
! References:
!    [1] http://vcg.isti.cnr.it/activities/geometryegraphics/
!        pointintetraedro.html
!*******************************************************************************

!*******************************************************************************
! 'use' and 'implicit none' statements
!*******************************************************************************

   use tsl_basics_kinds, only: k_r64
   implicit none

!*******************************************************************************
! Dummy argument declaration constructs
!*******************************************************************************

   real(k_r64),                 intent(in) :: rn1, rn2, rn3
   real(k_r64), dimension(4,3), intent(in) :: vertices
   real(k_r64), dimension(3)               :: point

!*******************************************************************************
! Local declaration constructs
!*******************************************************************************

   real(k_r64) :: s, t, u, tmp

!*******************************************************************************
! Execution part
!*******************************************************************************

   s = rn1
   t = rn2
   u = rn3
   if (s+t > 1._k_r64) then
      s = 1._k_r64-s
      t = 1._k_r64-t
   end if
   if (t+u > 1._k_r64) then
      tmp = u
      u   = 1._k_r64-s-t
      t   = 1._k_r64-tmp;
   else if (s+t+u > 1._k_r64) then
      tmp = u
      u   = s+t+u-1._k_r64
      s   = 1._k_r64-t-tmp
   end if
   point = (1._k_r64-s-t-u)*vertices(1,:)+ &
           s*vertices(2,:)+             &
           t*vertices(3,:)+             &
           u*vertices(4,:)
   return

!*******************************************************************************
! 'end function' statement
!*******************************************************************************

end function random_uniform_point_tetrahedron
