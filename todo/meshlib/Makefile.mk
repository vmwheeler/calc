#*******************************************************************************
# Copyright 2008 Wojciech Lipinski
#
# This file is part of Thermal Science Library (TSL).
#
# TSL is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Foobar is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with TSL.  If not, see <http://www.gnu.org/licenses/>.
#*******************************************************************************
# Path variables
#*******************************************************************************

SUBDIRS =

#*******************************************************************************
# Object and module files
#*******************************************************************************

MOD = $(addsuffix .mod, $(basename $(wildcard $(NAME)*.f90)))
OBJ = $(addsuffix .o, $(basename $(wildcard *.f90)))

#*******************************************************************************
# General rules
#*******************************************************************************

all: $(MOD) $(OBJ)
 
$(MOD) $(OBJ): $(SUBDIRS)

$(OBJ): $(MOD)

$(SUBDIRS):
	$(MAKE) -C $@ -f $(SUBMAKEFILE)

%.mod %.o: %.f90
	$(FC) $(FCFLAGS) $(INCLUDES) -c $<

#*******************************************************************************
# Specific rules
#*******************************************************************************

$(NAME)_meshlib.mod: $(NAME)_meshgeo.mod

#*******************************************************************************
# Clean and phony targets
#*******************************************************************************

clean:
	@for dir in $(SUBDIRS); do \
	  $(MAKE) -C $$dir -f $(SUBMAKEFILE) clean;\
	done; \
 	$(RM) *.o *.mod *.a
 
.PHONY: all $(SUBDIRS) clean
