module MeshGeo
   implicit none; private

!SUMMARY OF PUBLIC PROCEDURES

   public:: geoOpen     ! open mesh file
   public:: geoScale    ! scale coordinates according to ICEM CFD units
   public:: geoUseSurf  ! make surface infomation accessible
   public:: geoUseClSu  ! same as geoUseSurf, but with "clustering"
   public:: geoClose    ! deallocate memory allocated by geoOpen

   public:: geoFirst    ! set initial cursor position
   public:: geoNextB    ! move cursor on a line, stop at mesh boundary
   public:: geoNextF    ! move cursor on a line, stop at next cell face

   public:: geoInCell   ! indicate whether a point is inside a given cell
   public:: geoVolCell  ! compute the volume of a cell
   public:: geoBbCell   ! compute the bounding box of a cell
   public:: geoTriVert  ! get vertices of a cell face

   public:: geoImpact
   public:: geoAreaSE   ! compute the area of a boundary surface element
   public:: geoCenterSE ! compute the center of a boundary surface element

   public:: geoUnusedSE ! count unused surface elements

   public:: geoSmooth   ! smooth cluster boundaries

   public:: geoWriClst  ! write a mesh file, resolve surfaces into clusters
   public:: geoWriArch  ! write a mesh file, resolve surfaces into "archipels"

   public:: geoNSE      ! provide neighbours of a given surface element

   public:: OldSqF,Solve3

!PUBLIC VARIABLES

!1. Vertices
   integer(4),public:: nVertices ! number of vertices in the mesh

   real(8),allocatable,public:: VrtXYZ(:,:)
   ! Vrt(1:3,j) = coordinates of vertex j

!2. List of Cells (i.e. volume elements)

!   Each cell is represented by one list entry containing entries for ...
!   ... the cell type (tetrahedron, pyramid, etc)
!   ... the vertex indices
!   ... the domain to which the cell belongs
!   ... information about what is adjacent to each face of the cell
!       (neighbouring cell, or boundary surface element).

   integer(4),public:: nCells      ! total number of cells in the mesh

   integer(2),allocatable,public:: CellTyp(:)
   ! CellTyp(i) = 4 for tetrahedra,
   !              5 for pyramids,
   !              6 for wedges, and
   !              8 for hexahedra.
   ! i.e. the cell type is identical with the number of its vertices

   integer(4),allocatable,public:: CellVrt(:,:)
   ! CellVrt(1:Typ,i) = vertex indices of cell i, where Typ=CellTyp(i)

   integer(2),allocatable,public:: CellDom(:)
   ! CellDom(i) = index of the domain to which cell i belongs

   integer(4),allocatable,public:: CellNgb(:,:)
   ! CellNgb(f,j) indicates what is adjacent to face f of cell j. In particular:
   ! - A positive value is equal to the index of the neighbouring cell.
   ! - A value of 0 indicates that the face is on a unused boundary surface
   !   (i.e. not introduced by geoUseSurf).
   ! - A negative value indicates that the face is on a used boundary surface.
   !   In this case, the magnitude of CellNgb(f,i) is equal to the index of the
   !   element in the list of boundary surface elements (see below).

!3a. List of original surfaces
   character(16),allocatable,public:: origLab(:)

!3b. List of used boundary surfaces

!   A boundary surface is considered to be "used" after it has been introduced
!   by "call geoUseSurf(...)".

   integer(2),public:: nUsedSurf
   ! number of surfaces that have been indroduced by geoUseSurf.

   integer(4),allocatable,public::    Surf_WI(:) ! "Wojciech Indices"
   integer(4),allocatable,public::    Surf_nC(:)
   character(16),allocatable,public:: Surf_Lab(:)

!3c. List of boundary surface elements
!   The list of boundary surface elements contains all elements of used
!   boundary surfaces.
!   The first entries of the list are occupied by elements of surface #1.
!   They are followed by elements of surface #2 etc.

   integer(4),public:: nUsedSE
   ! number of used surface elements, i.e. number of surface elements belonging
   ! to used surfaces (see above).

   integer(4),allocatable,public:: SE_Cell(:)
   ! SE_Cell(i) = index of the cell adjacent to surface element i

   integer(2),allocatable,public:: SE_Face(:)
   ! SE_Face(i) = index of the face of the cell adjacent to surface element i

   integer(2),allocatable,public:: SE_Surf(:)
   ! SE_Surf(i) = index of the surface to which element i belongs

   integer(4),allocatable,public:: SE_clst(:)

!3d. Clusters
   integer(4),public::             nClst
   integer(4),allocatable,public:: Clst_WI(:)

!4. Cursor position

!   MeshGeo implements the concept of an imaginary "cursor". The purpose of
!   this concept is to facilite ray tracing, to construct random walks inside
!   the mesh region, to compute stream lines etc.

!   There are two types of cursor operations:
!   - moving the cursor to a "starting" position (subroutine geoFirst)
!   - moving from the previous cursor position on a straight line to a new
!     position (subroutines geoNextB, geoNextF).

   real(8),public::  CursorXYZ(3)
   ! Cartesian coordinates of the current cursor position.

   integer(4),public:: CursorCell
   ! index of the cell containing CursorXYZ.
   ! If CursorCell=0, the cursor position is considered to be undefined.

!5. Miscellaneous data

   integer(2),public:: nDomains    ! number of domains in the mesh

   integer(2),public:: nOrigSurf
   ! number of "original" surfaces of the mesh, i.e. surfaces specified in the
   ! mesh file (in contrast to used surfaces).

   integer(4),public:: nOrigSE     ! total number of original" surface elements

   real(8),public:: BbLow(3), BbHigh(3)
   ! "Bounding Box" of the mesh, i.e. minimal and maximal values of
   ! vertex coordinates

   real(8),public:: BbDiam         ! diameter of the bounding box of the mesh

   character(32):: MshFilNam

   integer(4):: nlps ! number of lines preceding surface definitions

!PRIVATE VARIABLES OF MeshGeo

   logical SurfAlloc

   integer(4) BadFaces
   ! Number of 2D elements belonging to more than two cells. This pathological
   ! situation has actually been observed in meshes produced by ICEM_CFD,
   ! and confirmed by the "check mesh" tool of ICEM_CFD.

   real(8), private:: inEltEps

   integer(2),parameter,private:: NF(4:8)=[4,5,5,-1,6]

   integer(4),private:: DimFct

   integer(4),parameter,private::   DimHas = 2_4**21 - 1
   integer(4),allocatable,private:: Hash1First(:), Hash1Next(:)
   logical,private::                Hash1Busy

   integer(4) srfCC(5)
   integer(2) srfSS(5), srfMM, srfU
contains

!PUBLIC PROCEDURES
!-----------------------------------------------------------------------------
subroutine geoSmooth
   integer(4),allocatable:: nn(:)
   logical(1),allocatable:: moved(:)
   integer(4) e,e1,e2,c,c1,c2,c3,ngb(3),m,n,i,nce,cell,clst
   integer(2) u,face, s,s1,s2,s3,surf
   real(8) a(3)

   print*, 'geoSmooth/beg'
   e1=Clst_WI(0)+1
   e2=Clst_WI(nClst)
   nce=e2-e1+1 ! number of "clustered" surface elements
   !print*, 'e1,e2,nce=',e1,e2,nce

   allocate(moved(e1:e2)); moved(:)=.false.
   do
      m=0
      do e=e1,e2
         if(.not.moved(e)) then
            c =SE_clst(e)
            s =SE_surf(e)

            call geoNSE3(ngb,a,e)
            c1=SE_clst(ngb(1))
            c2=SE_clst(ngb(2))
            c3=SE_clst(ngb(3))
            s1=SE_surf(ngb(1))
            s2=SE_surf(ngb(2))
            s3=SE_surf(ngb(3))

            if(    isMoved(c,s, c1,c2, s1,s2, a(1),a(2))) then
               m=m+1; moved(e)=.true.
            elseif(isMoved(c,s, c2,c3, s2,s3, a(2),a(3))) then
               m=m+1; moved(e)=.true.
            elseif(isMoved(c,s, c3,c1, s3,s1, a(3),a(1))) then
               m=m+1; moved(e)=.true.
            endif
            SE_clst(e)=c
         endif
      enddo
      print*,'moved elements:',m
      if(m.eq.0) exit
   enddo
   deallocate(moved)

   print*, 'before removing empty clusters: nClst=',nClst
   allocate(nn(nClst))
   !beg{Remove empty clusters; redefine cluster indices jj(e) correspondingly;
   !    redefine nW as the total number of non-empty clusters.}
      call makenn ! nn(i) <- number of elements of cluster i

      n=0
      do i=1,nClst
         if(nn(i).ne.0) then
            n=n+1
            nn(i)=n !  = new cluster index as a function of old index i
         endif
      enddo
      do e=e1,e2
         SE_clst(e)=nn(SE_clst(e))
      enddo
      nClst=n
   !end{Remove empty clusters; redefine cluster indices jj(e); redefine nW}
   print*, 'after removing empty clusters:  nClst=',nClst

   !beg{Establish new Wojciech indices of clusters}
      call makenn ! nn(i) <- number of elements of cluster i
      do i=1,nClst
         Clst_WI(i)=Clst_WI(i-1)+nn(i)
      enddo
   !end{Establish Wojciech indices of the new clusters}
   deallocate(nn)

   allocate(nn(nce))
   !beg{Sort clustered elements, with cluster index as sorting criterion;
   !    Let nn(1) be the (old) index of the first element after sorting etc.
      do i=1,nce
         nn(i)=e1+i-1
      enddo
      call mysort(nn, SE_clst, nce)
   !end{sort elements}
   do i=1,nce-1
      if(SE_clst(nn(i)).gt.SE_clst(nn(i+1))) goto 91
   enddo

   call GetUnit(u)
   open(UNIT  =u,             &
   &    ACCESS='SEQUENTIAL',  &
   &    FORM  ='UNFORMATTED', &
   &    STATUS='SCRATCH')

   do i=1,nce
      e=nn(i) ! e = old index
      cell=SE_cell(e)
      face=SE_face(e)
      clst=SE_clst(e)
      surf=SE_surf(e)
      if(CellNgb(face,cell).ne.-e) goto 92
      write(u) cell, face, surf, clst
   enddo
   deallocate(nn)
   rewind(u)
   do e=e1,e2
      read(u)  cell, face, surf, clst
      if(CellNgb(face,cell).ge.0) goto 93
      CellNgb(face,cell)=-e
      SE_cell(e)=cell
      SE_face(e)=face
      SE_clst(e)=clst
      if(SE_surf(e).ne.surf) goto 94
   enddo
   close(u)
   print*, 'geoSmooth/end'
   return
! * * * * *
91 call Crash('geoSmo/91')
92 call Crash('geoSmo/92')
93 call Crash('geoSmo/93')
94 call Crash('geoSmo/94')

contains

   subroutine makenn
      integer(4) c,e
      nn=0
      do e=e1,e2
         c=SE_clst(e)
         if(c.le.0 .or. c.gt.nClst) call Crash('makenn')
         nn(c)=nn(c)+1
      enddo
   end subroutine makenn
end subroutine geoSmooth
!-----------------------------------------------------------------------------
function isMoved(             c, s, c1, c2, s1,s2, a1,a2) result(moved)
   integer(4),intent(inout):: c
   integer(4),intent(in)::          c1, c2
   integer(2),intent(in)::       s,         s1,s2
   real(8),intent(in)::                            a1,a2
   logical::                                                     moved

   integer(4) oldc
! * * * *
   oldc=c
   if(s.eq.s1 .and. s.eq.s2) then
      if(c1.eq.c2 .and. c.ne.c1) then
         c=c1
      elseif(c.ne.c1 .and. c.ne.c2 .and. c1.ne.c2) then
         if(    a1 .gt. 1.3*a2) then
            c = c1
         elseif(a2 .gt. 1.3*a1) then
            c = c2
         endif
      endif
   endif
   moved=oldc.ne.c
end function isMoved
!-----------------------------------------------------------------------------
function geoNSE(          e) result(nn)
   integer(4),intent(in)::e
   integer(4)::                     nn(3)

   integer(4) c,c0,B(4),V(3),on,B1,B2
   integer(2) f,j,g
! * * * * * * * * *
   if(e.le.0 .or. e.gt.nUsedSE) goto 91
   c0=SE_Cell(e)
   f=SE_Face(e)
   if(c0.le.0 .or. c0.gt.nCells .or. f.le.0 .or. f.gt.4) goto 92
   B(1:3)=geoTriVert(c0,f)
   B(4)=B(1)
   do j=1,3
      B1=B(j)
      B2=B(j+1)
      on=-e ! "old neighbour", i.e. indicates from where we are coming
      c=c0
      do while(c.gt.0)
         g=0
         do f=1,4
            V=geoTriVert(c,f)
            if((B1.eq.V(1) .or. B1.eq.V(2) .or. B1.eq.V(3)) .and. &
               (B2.eq.V(1) .or. B2.eq.V(2) .or. B2.eq.V(3)) .and. &
               CellNgb(f,c).ne.on)                                &
            then
               if(g.ne.0) goto 93
               g=f
            endif
         enddo
         if(g.eq.0) goto 94
         on=c
         c=CellNgb(g,c)
      enddo
      nn(j)=-c
   enddo
   return
! * * * *
91 call Crash('geoNSE1')
92 call Crash('geoNSE2')
93 call Crash('geoNSE3')
94 call Crash('geoNSE4')
end function geoNSE
!-----------------------------------------------------------------------------
subroutine geoNSE3(         nn,  ll,  e)
   integer(4),intent(out):: nn(3)
   real(8),intent(out)::         ll(3)
   integer(4),intent(in)::            e

   integer(4) c,c0,B(4),V(3),on,B1,B2
   integer(2) f,j,g
! * * * * * * * * *
   if(e.le.0 .or. e.gt.nUsedSE) goto 91
   c0=SE_Cell(e)
   f=SE_Face(e)
   if(c0.le.0 .or. c0.gt.nCells .or. f.le.0 .or. f.gt.4) goto 92
   B(1:3)=geoTriVert(c0,f)
   B(4)=B(1)
   do j=1,3
      B1=B(j)
      B2=B(j+1)
      on=-e ! "old neighbour", i.e. indicates from where we are coming
      c=c0
      do while(c.gt.0)
         g=0
         do f=1,4
            V=geoTriVert(c,f)
            if((B1.eq.V(1) .or. B1.eq.V(2) .or. B1.eq.V(3)) .and. &
               (B2.eq.V(1) .or. B2.eq.V(2) .or. B2.eq.V(3)) .and. &
               CellNgb(f,c).ne.on)                                &
            then
               if(g.ne.0) goto 93
               g=f
            endif
         enddo
         if(g.eq.0) goto 94
         on=c
         c=CellNgb(g,c)
      enddo
      nn(j)=-c
      ll(j)=SQRT( SUM( (VrtXYZ(:,B2)-VrtXYZ(:,B1))**2 ) )
   enddo
   return
! * * * *
91 call Crash('geoNSE1')
92 call Crash('geoNSE2')
93 call Crash('geoNSE3')
94 call Crash('geoNSE4')
end subroutine geoNSE3
!-----------------------------------------------------------------------------
subroutine geoOpen(         meshFile)
   character(*),intent(in)::meshFile
!Purpose:
!  Make the data encoded in a mesh file accessible to the MeshGeo user.
!
!input:
!  meshFile    name of the mesh file, in the same format as that produced by
!              Ansys ICEM CFD with Option "Ansys CFX"
!- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
   integer(2),parameter:: Typ_Shp(4) = [4, 6, 8, 5]

   real(8) CriErr, XYZ(3), Vol
   integer(4) J,N,N0,JJ(10)
   integer(4) Anz_Shp(4)
   integer(2) U1, U2, Dom, Shp, I, M, Typ, XFE, XVE, FF(10), S
   character(20) C20
   character(16) C16
! * * * * * * * * *
   !print*,'begin{geoOpen}'
   mshFilNam=meshFile
   call OldSqF(U1, meshFile)

   read(U1,'(a20)') C20
   if(C20.ne.'1128683573') goto 90
   read(U1,'(a20)') C20
   if(C20.ne.'Version number: 5.6D' .and. &
      C20.ne.'Version number: 5.6S') goto 91

   read(U1,*) nVertices, Anz_Shp, nDomains, nOrigSurf
   nlps = 3

   !print 78,'Vertices',   nVertices
   !print 78,'Tetrahedra', Anz_Shp(1)
   !print 78,'Wedges',     Anz_Shp(2)
   !print 78,'Hexahedra',  Anz_Shp(3)
   !print 78,'Pyramids',   Anz_Shp(4)
   !print 78,'Domains',    nDomains
   !print 78,'Surfaces',   nOrigSurf

   nCells=SUM(Anz_Shp)
   DimFct =5*nCells

   XVE=4 ! max number of vertices per element
   XFE=4 ! max number of facets per element
   if(Anz_Shp(4).ne.0) then ! pyramids exist
      XVE=5
      XFE=5
   endif
   if(Anz_Shp(2).ne.0) then ! wedges exist
      XVE=6
      XFE=5
   endif
   if(Anz_Shp(3).ne.0) then ! hexas exist
      XVE=8
      XFE=6
   endif

   allocate(VrtXYZ(3,  nVertices))

   allocate(CellVrt(XVE,nCells))
   allocate(CellDom(    nCells))
   allocate(CellTyp(    nCells))

   BbLow=HUGE(BbLow)
   BbHigh=-BbLow

   do J=1,nVertices
      read(U1,*) XYZ; nlps=nlps+1
      BbLow=MIN(BbLow, XYZ)
      BbHigh=MAX(BbHigh, XYZ)
      VrtXYZ(:,J)=XYZ
   enddo

   J=0
   do Shp=1,4
      Typ=Typ_Shp(Shp)
      do N=1,Anz_Shp(Shp)
         J=J+1
         read(U1,*) CellVrt(1:Typ,J); nlps=nlps+1
         CellTyp(J)=Typ
      enddo
   enddo

   CellDom(:)=0
   Vol=0
   do Dom=1,nDomains
      read(U1,*) N0,C16; nlps=nlps+1
      N=N0
      do while(N.ne.0)
         M=MIN(N,10_4)
         read(U1,*) JJ(1:M); nlps=nlps+1
         do I=1,M
            J=JJ(I)
            if(J.le.0 .or. J.gt.nCells .or. CellDom(J).ne.0) goto 93
            CellDom(J)=Dom
            Vol=Vol+geoVolCell(J)
         enddo
         N=N-M
      enddo
      i=INDEX(C16, ' ')-1
      !print 79, C16(1:i), N0, ABS(Vol)
   enddo

   allocate(origLab(nOrigSurf))
   nOrigSE=0
   do S=1,nOrigSurf
      read(U1,*) N,C16
      !print*, N,C16
      origLab(S)=C16
      nOrigSE=nOrigSE+N

      i=INDEX(C16, ' ')-1
      call NewSqF(U2, C16(1:i)//'.tmp')
      write(U2,*) N
      do while(N.ne.0)
         M=MIN(N,10_4)
         read(U1,*) (JJ(i),FF(i),i=1,M)
         do i=1,M
            write(U2,*) JJ(i), FF(i)
         enddo
         N=N-M
      enddo
      close(U2)
   enddo
   close(U1)

   BadFaces=0
   call NgbOpn(XFE)
   if(BadFaces.ne.0) goto 95

   if(geoUnusedSE().ne.nOrigSE) goto 96

   CriErr=SUM(MAX(ABS(BbLow),ABS(BbHigh)))*1.0E-6
   inEltEps=CriErr
   if(KIND(VrtXYZ).eq.8) inEltEps=inEltEps**2
   BbDiam=SQRT(SUM((BbHigh(:)-BbLow(:))**2))
   nUsedSurf=0
   nUsedSE=0
   nClst=0
   SurfAlloc=.false.

   CursorCell=0
   CursorXYZ=0
   !print*,'end{geoOpen}'
   return
! * * * *
!78 format(1x,a20,':',i8)
!79 format(1x,a20,':',i8, ' elements, volume=',1p,e15.7)
!80 format(1x,a20,':',i8, ' elements,   area=',1p,e15.7)

90 call Crash('geoOpen/90')
91 call Crash('geoOpen/91')
93 call Crash('geoOpen/93')
95 call Crash('geoOpen/95')
96 call Crash('geoOpen/96')
end subroutine geoOpen
!-------------------------------------------------------------------------------
subroutine geoUseSurf(      label)
   character(*),intent(in)::label
!Purpose:
!  To make the information about a specified boundary surface accessible
!  to the MeshGeo user.
!  Association of surface labels with indices is determined by the sequence of
!  "call geoUseSurf" statements. Example: The code sequence

!     call geoUseSurf('InnerWall')
!     call geoUseSurf('OuterWall')

!  associates 'InnerWall' with index 1, and 'OuterWall" with index 2.
! - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

   call UseSurf2(label, 0.0d0)
end subroutine geoUseSurf
!-------------------------------------------------------------------------------
subroutine geoUseClSu(      label, CluDia)
   character(*),intent(in)::label
   real(8),intent(in)::           CluDia

   call UseSurf2(label, CluDia)
end subroutine geoUseClSu
!-------------------------------------------------------------------------------
subroutine UseSurf2(        lab_d,CluDia)
   character(*),intent(in)::lab_d
   real(8),     intent(in)::      CluDia

   integer(2)    iOrig,k

   integer(4) i0, nC, nCtot
   character(16) lab
! * * * * * * * * * *
   !print*, 'UseSurf2/beg lab_d=',lab_d, ' CluDia=',CluDia
   if(.not. SurfAlloc) then
      allocate(Surf_WI(0:nOrigSurf))
               Surf_WI=0

      allocate(Surf_lab(nOrigSurf))
               Surf_lab=' '

      allocate(Surf_nC(nOrigSurf))
               Surf_nC=0

      allocate(SE_Surf(nOrigSE))
               SE_Surf=0

      allocate(SE_Cell(nOrigSE))
               SE_Cell=0

      allocate(SE_Face(nOrigSE))
               SE_Face=0

      allocate(SE_Clst(nOrigSE))
               SE_Clst=0

      allocate(Clst_WI(0:nOrigSE))
               Clst_WI=0
      SurfAlloc=.true.
   endif

   if(CluDia.ge.0 .and. nClst.eq.0) Clst_WI(0)=nUsedSE

   lab=lab_d; k=INDEX(lab,' ')-1

   nCtot=0
   do iOrig=1,nOrigSurf
      if(lab(1:k).eq.origLab(iOrig)(1:k)) then
         if(CluDia.gt.0) then
            call AppendSurf3(origLab(iOrig),CluDia,nC)
            nCtot=nCtot+nC
         else
            call AppendSurf1(origLab(iOrig))
         endif
      endif
   enddo
   i0=Surf_WI(nUsedSurf) ! cardinal number of SEs before call useSurf2
   if(i0.eq.nUsedSE) goto 90
   nUsedSurf=nUsedSurf+1
   Surf_lab(nUsedSurf)=lab
   Surf_WI(nUsedSurf)=nUsedSE
   Surf_nC(nUsedSurf)=nCtot
   SE_Surf(i0+1:nUsedSE)=nUsedSurf
   !print*,    'UseSurf2/end nUsedSurf=',nUsedSurf
   return
! * * * * *
90 print*, 'The surface "',lab_d, '" does not exist in the mesh.'
   call Crash('UseSurf2')
end subroutine UseSurf2
!------------------------------------------------------------------------------
subroutine AppendSurf1(     OrigLab)
   character(*),intent(in)::OrigLab
! - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
   integer(4):: e, c, nAE
   integer(2):: U,f,k
   character(16) lab
! * * * * * * * * *
   !print*, 'AppendSurf1/beg ',OrigLab
   lab=OrigLab; k=INDEX(lab,' ')-1
   call OldSqF(U, lab(1:k)//'.tmp')
   read(U,*) nAE ! number of Appended Elements
   do e=1,nAE
      read(U,*) c, f
      nUsedSE=nUsedSE+1
      SE_cell(nUsedSE)=c
      SE_face(nUsedSE)=f
      if(CellNgb(f,c).ne.0) goto 90
      CellNgb(f,c)=-nUsedSE
   enddo
   close(U)
   !print*, 'AppendSurf1/end ',OrigLab
   return
90 call Crash('AppendSurf1')
end subroutine AppendSurf1
!------------------------------------------------------------------------------
subroutine AppendSurf3(      OrigLab, CluDia, nC)
   character(*),intent(in):: OrigLab
   real(8),     intent(in)::          CluDia
   integer(4),  intent(out)::                 nC
! - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
   integer(2),parameter:: criterion=1
   ! Associate a surface triangle with the Voronoi center which is ...
   ! 1: closest to the center of the triangle
   ! 2: closest to any point of the triangle
   real(8),allocatable:: WW(:,:)
   integer(4),allocatable:: Cell(:), jj(:), nn(:), VV(:,:)
   integer(2),allocatable:: Face(:)
   real(8):: xyz(3), BbL(3), BbH(3),Dx,Dy,Dz,Dp,Dq,pi,r,a,b,p,q,cdp,cdq, &
             x0(3), x1(3), x2(3), x3(3), W(3), D4, B4L, B4H
   integer(4):: i,c, V(3), e, np, nq, nW, n, j, nAE
   integer(2):: U,f,k
   character(16) lab
! * * * * * * * * *
   print*, 'AppendSurf3/beg ',OrigLab
   if(CluDia.le.0) goto 10
   cdp=CluDia
   cdq=CluDia*SQRT(0.75d0)
   pi=4.0d0*ATAN(1.0d0)
   lab=OrigLab; k=INDEX(lab,' ')-1
   call OldSqF(U, lab(1:k)//'.tmp')
   read(U,*) nAE ! number of Appended Elements
   allocate(VV(3,nAE))
   allocate(Cell(nAE))
   allocate(Face(nAE))
   allocate(jj(nAE))

   B4L=HUGE(B4L)
   B4H=-B4L

   BbL=HUGE(BbL)
   BbH=-BbL

   do e=1,nAE
      read(U,*) c, f
      V=geoTriVert(c,f)
      do k=1,3
         xyz(:)=VrtXYZ(:,V(k))
         BbL = MIN(BbL, xyz)
         BbH = MAX(BbH, xyz)

         B4L = MIN(B4L, xyz(1)+xyz(2))
         B4H = MAX(B4H, xyz(1)+xyz(2))
      enddo
      VV(:,e)=V

      Cell(e)=c
      Face(e)=f
   enddo
   !print*, 'BbL=', REAL(BbL)
   !print*, 'BbH=', REAL(BbH)
   close(U)

   Dx=BbH(1)-BbL(1)
   Dy=BbH(2)-BbL(2)
   Dz=BbH(3)-BbL(3)
   D4=(B4H-B4L)/SQRT(2.0d0)
   !print*, 'Dx=',Dx
   !print*, 'Dy=',Dy
   !print*, 'Dz=',Dz
   !print*, 'D4=',D4

   nW=0

   if(Dz .gt. 0.01*MAX(Dx,Dy)) then
      if(approxeq(Dx,Dy)) then
         if(approxeq(Dx+Dy, 2*D4)) then
            print*, 'full cylinder,Dx/Dy=', REAL(Dx/Dy)
            r=MAX(Dx,Dy)/2
            Dp=2*pi*r
            Dq=Dz
            nq=nDI(Dq, cdq)
            if(nq.ne.0) then
               np=MAX(nDI(Dp, cdp), 1_4)
               n=(nq+1)*np
               allocate(WW(3,n))
               do j=0,nq
                  q=j*(Dq/nq)
                  do i=0,np-1
                     p=(i+0.5*IAND(j,1))*(Dp/np)
                     a=2*pi*p/Dp
                     nW=nW+1
                     WW(1,nW) = (BbL(1) + BbH(1))/2 + r*COS(a)
                     WW(2,nW) = (BbL(2) + BbH(2))/2 + r*SIN(a)
                     WW(3,nW) = BbL(3) + q
                  enddo
               enddo
            endif
         elseif(approxeq(D4/((Dx+Dy)/2), 1.0d0 - SQRT(2.0d0)/2)) then
            print*, 'quarter cylinder, (2*D4)/(Dx+Dy)=', REAL((2*D4)/(Dx+Dy))
            print*, '                     1-SQRT(2)/2=', 1.0-SQRT(2.0)/2
            r=MAX(Dx,Dy)
            Dp=(pi/2)*r
            Dq=Dz
            np=nDI(Dp,cdp)
            nq=nDI(Dq,cdq)
            if(np.ne.0 .and. nq.ne.0) then
               n=(np+1)*(nq+1)
               allocate(WW(3,n))
               do j=0,nq
                  q=j*(Dq/nq)
                  do i=0,np
                     p=(i+0.5*IAND(j,1))*(Dp/np)
                     a=(pi/2)*(p/Dp)
                     nW=nW+1
                     WW(1,nW) = BbL(1) + r*COS(a)
                     WW(2,nW) = BbL(2) + r*SIN(a)
                     WW(3,nW) = BbL(3) + q
                  enddo
               enddo
            endif
         else
            goto 20
         endif
      elseif(approxeq(2*Dx,Dy)) then
         print*, 'cyl right half xy plane, Dx/Dy=', REAL(Dx/Dy)
         r=MAX(Dx,Dy/2)
         Dp=pi*r
         Dq=Dz
         np=nDI(Dp,cdp)
         nq=nDI(Dq,cdq)
         if(np.ne.0 .and. nq.ne.0) then
            n=(np+1)*(nq+1)
            allocate(WW(3,n))
            do j=0,nq
               q=j*(Dq/nq)
               do i=0,np
                  p=(i+0.5*IAND(j,1))*(Dp/np)
                  a=pi*(p-Dp/2)/Dp
                  nW=nW+1
                  WW(1,nW) = BbL(1)              + r*COS(a)
                  WW(2,nW) = (BbL(2) + BbH(2))/2 + r*SIN(a)
                  WW(3,nW) = BbL(3) + q
               enddo
            enddo
         endif
      elseif(approxeq(Dx,2*Dy)) then
         print*, 'cyl upper half xy plane, Dx/Dy=', REAL(Dx/Dy)
         r=MAX(Dx/2,Dy)
         Dp=pi*r
         Dq=Dz
         np=nDI(Dp,cdp)
         nq=nDI(Dq,cdq)
         if(np.ne.0 .and. nq.ne.0) then
            n=(np+1)*(nq+1)
            allocate(WW(3,n))
            do j=0,nq
               q=j*(Dq/nq)
               do i=0,np
                  p=(i+0.5*IAND(j,1))*(Dp/np)
                  a=pi * p/Dp
                  nW=nW+1
                  WW(1,nW) = (BbL(1) + BbH(1))/2 + r*COS(a)
                  WW(2,nW) = BbL(2)              + r*SIN(a)
                  WW(3,nW) = BbL(3) + q
               enddo
            enddo
         endif
      else
         goto 30
      endif
   else ! area parallel to xy plane, arbitrary shape
      print*, 'plane'
      Dp=Dx
      Dq=Dy
      np=nDI(Dp,cdp)
      nq=nDI(Dq,cdq)
      if(np.ne.0 .and. nq.ne.0) then
         n=(np+1)*(nq+1)
         allocate(WW(3,n))
         do j=0,nq
            q=j*(Dq/nq)
            do i=0,np
               p=(i+0.5*IAND(j,1))*(Dp/np)
               xyz(1)=BbL(1)+p
               xyz(2)=BbL(2)+q
               xyz(3)=(BbL(3)+BbH(3))/2
               nW=nW+1
               WW(:,nW)=xyz(:)
            enddo
         enddo
      endif
   endif

   if(nW.eq.0) then
      nW=1
      allocate(WW(3,nW))
      WW(:,1) = 0
   endif

   !beg{For each element e of the new surface to be appended,
   !    determine the index jj(e) of the nearest Voronoi center, according
   !    to the criterion of what means "nearest".}
      do e=1,nAE
         V(:)=VV(:,e)
         x1=VrtXYZ(:,V(1))
         x2=VrtXYZ(:,V(2))
         x3=VrtXYZ(:,V(3))
         x0=(x1+x2+x3)/3

         b=HUGE(b)
         do i=1,nW
            W=WW(:,i)
            if(criterion.eq.1_2) then
               a=SUM((W-x0)**2)
            else
               a=MIN(SUM((W-x0)**2), &
                     SUM((W-x1)**2), &
                     SUM((W-x2)**2), &
                     SUM((W-x3)**2))
            endif
            if(a.lt.b) then
               jj(e)=i ! = index of nearest Voronoi center of e
               b=a
            endif
         enddo
      enddo
   !end{For each element e find cluster index jj(e)}

   deallocate(WW)
   deallocate(VV)

   allocate(nn(nW))

   !beg{Remove empty clusters; redefine cluster indices jj(e) correspondingly;
   !    redefine nW as the total number of non-empty clusters.}
      call makenn ! nn(i) <- number of elements of cluster i
      n=0
      do i=1,nW
         if(nn(i).ne.0) then
            n=n+1
            nn(i)=n !  = new cluster index as a function of old index i
         endif
      enddo
      do e=1,nAE
         jj(e)=nn(jj(e))
      enddo
      nW=n
   !end{Remove empty clusters; redefine cluster indices jj(e); redefine nW}

   !beg{Establish Wojciech indices of the new clusters}
      call makenn ! nn(i) <- number of elements of cluster i
      do i=1,nW
         Clst_WI(nClst+i)=Clst_WI(nClst+i-1)+nn(i)
      enddo
   !end{Establish Wojciech indices of the new clusters}
   deallocate(nn)

   allocate(nn(nAE))
   !beg{Sort elements, with cluster index as sorting criterion;
   !    Let nn(e) be the ordinal number of element e after sorting.}
      do e=1,nAE
         nn(e)=e ! = ordinal number of e before sorting
      enddo
      call mysort(nn, jj, nAE) ! nn <- ordinal numbers after sorting
   !end{sort elements}

   !beg{Define SE_cell, SE_face, SE_clst for all new elements,
   !    and redefine nClst as the new total number of clusters.}
      do e=1,nAE
         n=nn(e)
         c=Cell(n)
         f=Face(n)

         nUsedSE=nUsedSE+1

         SE_cell(nUsedSE)=c
         SE_face(nUsedSE)=f

         if(CellNgb(f,c).ne.0) goto 40
         CellNgb(f,c)=-nUsedSE

         SE_clst(nUsedSE)=nClst+jj(n)

      enddo
      nClst=nClst+nW
   !end{define SE_cell, SE_face, SE_clst for all new elements; redefine nClst}

   deallocate(nn)

   deallocate(Cell)
   deallocate(Face)
   deallocate(jj)

   nC=nW
   print*, 'AppendSurf3/end'
   return
! * * * * *
10 call Crash('AppendSurf3/1')
20 call Crash('AppendSurf3/2')
30 call Crash('AppendSurf3/1')
40 call Crash('AppendSurf3/2')

contains
   subroutine makenn

   integer(4) c,e
   nn=0
   do e=1,nAE
      c=jj(e)
      nn(c)=nn(c)+1
   enddo
   end subroutine makenn

end subroutine AppendSurf3
!-------------------------------------------------------------------------------
logical function approxeq(x,y)
   real(8),intent(in)::   x,y

   approxeq = ABS(x/y - 1.0d0) .lt. 0.05d0
end function approxeq
!-------------------------------------------------------------------------------
integer(4) function nDI(a,b)
   real(8),intent(in):: a,b
   real(8) c

   c=99
   if(b.ne.0) c=MIN(c,a/b+0.5)
   NDI=c
end function nDI
!-------------------------------------------------------------------------------
subroutine geoWriClst(      cfx5)
   character(*),intent(in)::cfx5

   integer(2) k
   integer(4) e, e1, e2, c
   character(16) lab
! * * * * * * * * *
   !print*, 'geoWriClst/beg ',cfx5
   k=nClst
   call Header(cfx5, k)
   do c=1,nClst
      e1 = Clst_WI(c-1)+1
      e2 = Clst_WI(c)
      lab=Surf_lab(SE_surf(e1)); k = INDEX(lab, ' ')-1
      write(lab(k+1:k+5),'(i5.5)') c
      write(srfU,'(i8,1x,a)') e2-e1+1, lab(1:k+5)
      call SrfInit
      do e = e1,e2
         if(c.ne.SE_Clst(e)) call Crash('geoWriClst')
         call SrfElt(e)
      enddo
      call SrfFlush
   enddo
   close(srfU)
   !print*, 'geoWriClst/end'
end subroutine geoWriClst
!-------------------------------------------------------------------------------
subroutine geoWriArch(      cfx5)
   character(*),intent(in)::cfx5

   integer(2),parameter:: dimA=8
   integer(2):: k,maxA,i,a,s,nUCS
   integer(2),allocatable:: SE_a(:)
   integer(4) c, e, e1, e2, N, NN(3), count(dimA)
   logical:: used(0:dimA)
   character(16) lab
! * * * * * * * * *
   print*, 'geoWriArch/beg ',cfx5
   allocate(SE_a(nUsedSE))
   SE_a(:)=0
   maxA=0
   do c=1,nClst
      e1=Clst_WI(c-1)+1
      e2=Clst_WI(c)
      used(:)=.false.
      s=SE_surf(e1)
      do e=e1,e2
         NN=geoNSE(e)
         do i=1,3
            N=NN(i)
            if(N.ne.0) then
               !if(SE_Surf(N).eq.s) then
                  a=SE_a(N)
                  if(a.lt.0 .or. a.gt.dimA) goto 91
                  used(a)=.true.
               !endif
            endif
         enddo
      enddo

      do a=1,dimA
         if(.not.used(a)) exit
      enddo
      if(a.gt.dimA) goto 91
      SE_a(e1:e2)=a
      maxA=MAX(maxA,a)
   enddo

   !print*, 'maxA=',maxA


   nUCS=0
   do s=1,nUsedSurf
      if(Surf_nC(s).ne.0) nUCS=nUCS+1
   enddo

   !print*, 'nUCS=',nUCS
   call Header(cfx5, maxA*nUCS)

   do s=1,nUsedSurf
      if(Surf_nC(s).ne.0) then
         lab=Surf_lab(s); k=INDEX(lab,' ')-1
         e1=Surf_WI(s-1)+1
         e2=Surf_WI(s)
         Count(:)=0
         do e=e1,e2
            a=SE_a(e)
            if(a.le.0 .or. a.gt.maxA) goto 91
            Count(a)=Count(a)+1
         enddo
         do a=1,maxA
            write(srfU, '(i8,1x,a)') Count(a), lab(1:k)//CHAR(64+a)
            call SrfInit
            do e=e1,e2
               if(SE_a(e).eq.a) call SrfElt(e)
            enddo
            call SrfFlush
         enddo
      endif
   enddo
   close(srfU)
   deallocate(SE_a)
   print*, 'geoWriArch/end'
   return
91 call Crash('geoWriArch')
end subroutine geoWriArch
!-------------------------------------------------------------------------------
subroutine Header(          cfx5, nSurfaces)
   character(*),intent(in)::cfx5
   integer(2),  intent(in)::      nSurfaces

   integer(2) U1
   character(128) Buf

   integer(4) nV, A_S(4), n
   integer(2) nD
! * * * * * * * * *
   call OldSqF(U1, mshFilNam)
   call NewSqF(srfU, cfx5)
   read(U1,'(a128)') Buf ! 1128683573
   call MyWrite

   read(U1,'(a128)') Buf ! Version number: 5.6S
   call MyWrite

   read(U1,*)  nV, A_S, nD
   write(srfU,'(7(1x,i8))') nV, A_S, nD, nSurfaces

   do n=1,nlps-3
      read(U1,'(a128)') Buf
      call MyWrite
   enddo
   close(U1)

contains

   subroutine myWrite
      integer(2) i

      i=128
      do while(i.gt.0 .and. Buf(i:i).eq.' ')
         i=i-1
      enddo
      write(srfU, '(a)') Buf(1:i)
   end subroutine myWrite
end subroutine Header
!------------------------------------------------------------------------------
subroutine SrfInit
   srfMM=0
end subroutine SrfInit

subroutine SrfElt(       e)
   integer(4),intent(in)::e
! * * * * * * * *
   if(srfMM.eq.5) call SrfFlush
   srfMM=srfMM+1
   srfCC(srfMM)=SE_Cell(e)
   srfSS(srfMM)=SE_Face(e)
end subroutine SrfElt

subroutine SrfFlush
   integer(2) I
! * * * * * * * *
   if(srfMM.ne.0) write(srfU,'(5(2x,i8,1x,i1))') (srfCC(I),srfSS(I),I=1,srfMM)
   srfMM=0
end subroutine SrfFlush
!------------------------------------------------------------------------------
subroutine geoScale(   Scale)
   real(8),intent(in)::Scale
!Purpose:
!  Multiplay all coordinates and lengths by a given factor.

   VrtXYZ=VrtXYZ    *Scale
   BbLow   =BbLow   *Scale
   BbHigh   =BbHigh *Scale
   BbDiam=BbDiam    *Scale
end subroutine geoScale

function geoUnusedSE() result(N)
   integer(4)                 N
!result:
!  number of "unused" surface elements, i.e. number of surface elements
!  not belonging to a surface introduced by geoUseSurf.
! - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
   integer(4) J
   integer(2) F
! * * * * * * * * *
   N=0
   do J=1,nCells
      do F=1,CellTyp(J)
         if(CellNgb(F,J).eq.0) N=N+1
      enddo
   enddo
end function geounusedSE
!------------------------------------------------------------------------------
subroutine geoImpact(      xStart,    dir, iTar, xTar,    cosIA)
   real(8),   intent(in):: xStart(3), dir(3)
   integer(4),intent(in)::                 iTar
   real(8),   intent(out)::                      xTar(3), cosIA

   real(8) M(3,3), A1(3), A21(3), A31(3), X(3)
   integer(4) V(3)

   V=geoTriVert(SE_Cell(iTar),SE_Face(iTar))
   A1 =VrtXYZ(:,V(1))
   A21=VrtXYZ(:,V(2))-A1
   A31=VrtXYZ(:,V(3))-A1

   M(:,1)=A21
   M(:,2)=A31
   M(:,3)=-dir
   call Solve3(M,X,xStart-A1)
   if(X(3).ge.0 .and. X(1).ge.0 .and. X(2).ge.0 .and. X(1)+X(2).le.1) then
      xTar=xStart + X(3)*dir
      call CrsPrd(X, A21,A31)
      cosIA=SUM(X*dir)/SQRT(SUM(X**2)*SUM(dir**2))
   else
      xTar=0
      cosIA=HUGE(cosIA)
   endif
end subroutine geoImpact
!-------------------------------------------------------------------------------
real(8) function geoAreaSE(E)
   integer(4),intent(in):: E

!Input:
!  E   index of a surface element

!Result: area of the surface element
! - - - - - - - - - - - - - - - - - -
   integer(4) J,V(8),Z(3)
   integer(2) F, Typ, K
   real(8) A(3),B(3),C(3)

   J=SE_Cell(E)
   F=SE_Face(E)

   Typ=CellTyp(J)
   if(Typ.ne.4) call Crash('geoAreaSE only implemented for tetrahedra')
   V(1:Typ)=CellVrt(1:Typ,J)
   call LdFace(Z, K, V, F, Typ)

   A=VrtXYZ(:,Z(1))
   B=VrtXYZ(:,Z(2))-A
   C=VrtXYZ(:,Z(3))-A
   call CrsPrd(A, B, C)
   geoAreaSE=SQRT(SUM(A**2))/2
end function geoAreaSE
!-------------------------------------------------------------------------------
function geoCenterSE(     E) result(C)
   integer(4),intent(in)::E
   real(8)                          C(3)

!Input:
!  E   index of a surface element

!Result:
!  C   center of the surface element
! - - - - - - - - - - - - - - - - - -
   integer(4) J,V(8),Z(3)
   integer(2) F, Typ, K

   J=SE_Cell(E)
   F=SE_Face(E)

   Typ=CellTyp(J)
   if(Typ.ne.4) call Crash('geoCenterSE only implemented for tetrahedra')
   V(1:Typ)=CellVrt(1:Typ,J)
   call LdFace(Z, K, V, F, Typ)

   C=(VrtXYZ(:,Z(1)) + &
      VrtXYZ(:,Z(2)) + &
      VrtXYZ(:,Z(3)))/3
end function geoCenterSE
!-------------------------------------------------------------------------------
subroutine geoClose
!Purpose:
!   Deallocate memory allocated by geoOpen
! - - - - - - - - - - - - - - - - - - - - - - -
   call NgbClo
   deallocate(VrtXYZ)
   deallocate(CellVrt)
   deallocate(CellDom)
   deallocate(CellTyp)
   deallocate(origLab)

   if(SurfAlloc) then
      deallocate(Surf_WI)
      deallocate(Surf_lab)
      deallocate(Surf_nC)
      deallocate(SE_Surf)
      deallocate(SE_Cell)
      deallocate(SE_Face)
      deallocate(SE_Clst)
      deallocate(Clst_WI)
   endif
end subroutine geoClose
!-------------------------------------------------------------------------------
subroutine geoFirst(      xyz, Dom)
   real(8),   intent(in)::xyz(3)
   integer(2),intent(out)::    Dom

!Purpose:
!  Move the cursor to an arbitrary point in the mesh domain.

!Input:
!  xyz   coordinates of an arbitrary point

!Output:
!  Dom   If Dom>0, then Dom = index of the domain containing xyz.
!        If Dom=0, the xyz is outside the mesh region.

!Side effects:
!  If Dom>0, CursorXYZ becomes equal to xyz.
!  If Dom=0, CursorCell is set to 0, i.e. the cursor position becomes undefined.
! - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
   integer(4) J
! * * * * * * * * *
   do J=1,nCells
      if(geoInCell(xyz,J)) exit
   enddo
   if(J.le.nCells) then
      Dom=CellDom(J)
      CursorCell=J
      CursorXYZ=xyz
   else
      Dom=0
      CursorCell=0
   endif
end subroutine geoFirst
!-------------------------------------------------------------------------------
subroutine geoNextB(      P_tar,   f, OutNor)
   real(8),   intent(in)::P_tar(3)
   integer(2),intent(out)::        f
   real(8),   intent(out)::           OutNor(3)

!Purpose:
!  Move the cursor from the current position on a straight line to a specified
!  target position, but not farther than the nearest intersection with the mesh
!  boundary -- if there is any.

!Input:
!  P_tar target position

!Output:
!  f=0 means that there is no intersection with the mesh boundary.
!  f>0 means that the line from the old cursor position to P_tar intersects
!      the mesh boundary at least once. The first intersection point is on the
!      boundary of the cell pointed to by CursorCell after geoNextB returns,
!      and f indicates the face of that cell.

!  OutNor If f=0, then OutNor = [0, 0, 0]
!         If f>0, then OutNor = outward normal of the cell face containing the
!                               intersection point.
! - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
   real(8) Q(3),R(3),ON(3)

   real(8) lam,A1(3),A2(3),A3(3)
   integer(4) P(3),V(8)
   integer(2) k,n,g
! * * * * * * * * * *
   !!print*,'geoNext/beg CursorXYZ=',CursorXYZ, ' CursorCell=',CursorCell
   if(CursorCell.le.0 .or. CursorCell.gt.nCells) goto 90
   do
      f=0
      if(geoInCell(P_tar,CursorCell)) exit
      n=CellTyp(CursorCell)
      V(1:n)=CellVrt(1:n,CursorCell)
      R=P_tar - CursorXYZ
      lam=HUGE(lam)
      do g=1,NF(n)
         call LdFace(P,k,V,g,n)           ! k=1: auswaerts; k=2: einwaerts
         if(k.eq.2) call XchgJ(P(2),P(3)) ! Nun ist P nach auswaerts orientiert.
         A1=VrtXYZ(:,P(1))
         A2=VrtXYZ(:,P(2))
         A3=VrtXYZ(:,P(3))
         call NextFace(f, lam, Q, ON, R, A1,A2,A3, g)
      enddo
      if(f.eq.0) goto 91
      CursorXYZ=Q
      if(CellNgb(f,CursorCell).le.0) exit
      CursorCell=CellNgb(f,CursorCell)
   enddo
   if(f.ne.0) then ! Der Gebietsrand wurde erreicht.
      OutNor=ON/SQRT(SUM(ON**2))
   else            ! Der Gebietsrand ist nie angetroffen worden.
      CursorXYZ=P_tar
      OutNor=0
   endif
   return
! * * * * * * * * *
90 call Crash('geoNext/90')
91 call Crash('geoNext/91')
!92 call Crash('geoNext/92')
end subroutine geoNextB
!-------------------------------------------------------------------------------
subroutine geoNextF(       D,   f)
   real(8),   intent(in):: D(3)
   integer(2),intent(out)::     f

!Purpose:
!  Move the cursor in a given direction to the nearest cell boundary.
!  Leave CursorCell unchanged.

!Input:
!  D  direction vector, needs not to be normalized

!Output:
!  f  index of the cell face containing the new cursor position
! - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
   real(8) lam,A1(3),A2(3),A3(3),ON(3),Q(3)
   integer(4) P(3),V(8)
   integer(2) g,k,Typ
! * * * * * * * * * *
   if(CursorCell.le.0 .or. CursorCell.gt.nCells) goto 90
   f=0
   Typ=CellTyp(CursorCell)
   V(1:Typ)=CellVrt(1:Typ,CursorCell)
   lam=HUGE(lam)
   do g=1,NF(Typ)
      call LdFace(P,k,V,g,Typ)         ! k=1: auswaerts; k=2: einwaerts
      if(k.eq.2) call XchgJ(P(2),P(3)) ! Nun ist P nach auswaerts orientiert.
      A1=VrtXYZ(:,P(1))
      A2=VrtXYZ(:,P(2))
      A3=VrtXYZ(:,P(3))
      call NextFace(f, lam, Q, ON, D, A1,A2,A3, g)
   enddo
   if(f.eq.0) goto 91
   CursorXYZ=Q
   return
! * * * * * * * * *
90 call Crash('geoNext/90')
91 call Crash('geoNext/91')
end subroutine geoNextF
!-------------------------------------------------------------------------------
subroutine geoBbCell(       xyzmin,    xyzmax,  c)
   real(8),   intent(out):: xyzmin(3), xyzmax(3)
   integer(4),intent(in)::                      c
!Purpose:
!  Compute the bounding box of a specified cell.

!Input:
!   c      cell index

!Output:
!  xyzmin   mimima of x,y,z in the cell
!  xyzmax   maxima of x,y,z in the cell
! - - - - - - - - - - - - - - - - - - - - - - - -
   real(8) xyz(3)
   integer(4) V
   integer(2) i,Typ

   Typ=CellTyp(c)
   xyzmin=HUGE(xyzmin)
   xyzmax=-xyzmin

   do i=1,Typ
      V=CellVrt(i,c)
      xyz=VrtXYZ(:,V)
      xyzmin = MIN(xyzmin, xyz)
      xyzmax = MAX(xyzmax, xyz)
   enddo
end subroutine geoBbCell
!-------------------------------------------------------------------------------
logical function geoInCell(xyz, J)
   real(8),   intent(in):: xyz(3)
   integer(4),intent(in)::      J

!Input:
!  xyz  coordinates of an arbitrary point
!  J    index of a cell

!Result = .true. iff the point is inside the specified cell
! - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
   real(8) C(3),XY1(3)
   integer(4) V(8),P(3)
   integer(2) K,Typ,F
! * * * * * * * * *
   Typ=CellTyp(J)
   V(1:Typ)=CellVrt(1:Typ,J)
   do F=1,NF(Typ)
      call LdFace(P,K,V,F,Typ)         ! k=1: auswaerts; k=2: einwaerts
      if(K.eq.2) call XchgJ(P(2),P(3)) ! Nun ist P nach auswaerts orientiert.
      XY1(:)=VrtXYZ(:,P(1))
      call CrsPrd(C, VrtXYZ(:,P(2)) - XY1(:), &
                     VrtXYZ(:,P(3)) - XY1(:))
      if(SUM((xyz-XY1)*C)/SQRT(SUM(C**2)) .gt. inEltEps) exit
   enddo
   geoInCell=F.eq.NF(Typ)+1
end function geoInCell
!-------------------------------------------------------------------------------
function geoTriVert(       c, f) result(iV)
   integer(4),intent(in):: c
   integer(2),intent(in)::    f
   integer(4)::                         iV(3)
!Purpose:
!  Retrieve three vertices of a given cell face.

!Input:
!  c  cell index
!  f  face index

!Result:
!  iV If the cell face is a triangle, then iV contains the vertex indices of
!     the cell face.
!     If cell c is quadrilateral, then V contains the indices of three
!     "representative" vertices of face f.
!     In both cases, if V1,V2,V3 are the vertices pointed to by
!     iV(1),iV(2),iV(3),
!     then the cross product [V2-V1,V3-V1] is pointing outwards.
! - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
   integer(2) k,n

   n=CellTyp(c)
   call LdFace(iV, k, CellVrt(1:n,c), f, n)
   if(k.eq.2) call XchgJ(iV(2),iV(3)) ! Now, iV is outward oriented.
end function geoTriVert

!end of PUBLIC PROCEDURES

!beg CONNECTIVITY
subroutine NgbOpn(        XFE)
   integer(2),intent(in)::XFE  ! = maximal number of facets per cell

   integer(4),allocatable:: FctTPW(:,:)
   integer(2),allocatable:: FctFac(:,:)

   integer(4) AnzFac, J, TPW1, TPW2

   integer(2) Fac1,Fac2
! * * * * * * * * * *
   !!print*, 'NgbOpn/beg'
   allocate(CellNgb(XFE,nCells))
   CellNgb(:,:)=0
   allocate(FctTPW(2,DimFct))
   allocate(FctFac(2,DimFct))

   call Con(FctTPW,FctFac,AnzFac)

   do J=1,AnzFac
      TPW1=FctTPW(1,J)
      TPW2=FctTPW(2,J)
      Fac1=FctFac(1,J)
      Fac2=FctFac(2,J)
      if(TPW1.ne.0) then
         if(TPW1.le.0.or.TPW2.gt.nCells.or.Fac1.le.0.or.Fac1.gt.XFE) goto 91
         if(CellNgb(Fac1,TPW1).ne.0) goto 92
            CellNgb(Fac1,TPW1) = TPW2
      endif
      if(TPW2.ne.0) then
         if(TPW2.le.0.or.TPW2.gt.nCells.or.Fac2.le.0.or.Fac2.gt.XFE) goto 93
         if(CellNgb(Fac2,TPW2).ne.0) goto 94
            CellNgb(Fac2,TPW2) = TPW1
      endif
   enddo

   deallocate(FctFac)
   deallocate(FctTPW)
   !!print*, 'NgbOpn/end'
   return
! * * * * * * * * *
91 call Crash('NgbOpn/91')
92 call Crash('NgbOpn/92')
93 call Crash('NgbOpn/93')
94 call Crash('NgbOpn/94')
end subroutine NgbOpn
!-------------------------------------------------------------------------------
subroutine NgbClo
   deallocate(CellNgb)
   !print*,'NgbClo'
end subroutine NgbClo
!-------------------------------------------------------------------------------
subroutine Con(              FctTPW, FctFac,  AnzFac)
   integer(4),intent(out)::  FctTPW(2,*)
   integer(2),intent(out)::          FctFac(2,*)
   integer(4),intent(out)::                   AnzFac

   integer(4),allocatable:: FctVrt(:,:)

   integer(4) J,P(3),T,V(8),C,L
   integer(2) F,K,Typ
! * * * * * * * * * *
   !!print*, 'begin{connectivity}'
   allocate(FctVrt(3,DimFct))
   call Hash1Opn(DimFct)
   AnzFac=0

   do J=1,nCells
      Typ=CellTyp(J)
      if(Typ.lt.4 .or. Typ.gt.8) goto 91
      V(1:Typ)=CellVrt(1:Typ,J)
      do F=1,NF(Typ)
         call LdFace(P,K, V,F,Typ)
         call Fct_Lku(T, P, C, L, FctVrt)
         if(T.eq.0) call Fct_Ent(T, P, C, L, FctVrt,FctTPW,FctFac,AnzFac)
         if(FctTPW(K,T).eq.0) then
            FctTPW(K,T)=J
            FctFac(K,T)=F
         else
            print*, 'overlapping cells:',FctTPW(K,T),J
            BadFaces=BadFaces+1
         endif
      enddo
   enddo
   call Hash1Clo
   deallocate(FctVrt)
   !!print*, 'end{connectivity}'
   return
! * * * * *
91 call Crash('Con/91')
   call Crash('Con/92')
end subroutine Con
!-------------------------------------------------------------------------------
subroutine Fct_Lku(         T, P,    C, L, FctVrt)
   integer(4),intent(out):: T
   integer(4),intent(in)::     P(3)
   integer(4),intent(out)::          C, L
   integer(4),intent(in)::                 FctVrt(3,*)

   integer(4) N
   logical Found
! * * * * * * * * * *
   C=IAND(P(1)+P(2)+P(3),DimHas) ! hash code
   N=Hash1First(C)
   Found=.false.
   do while(N.ne.0 .and. .not.Found)
      Found=P(1).eq.FctVrt(1,N) .and.  &
      &     P(2).eq.FctVrt(2,N) .and.  &
      &     P(3).eq.FctVrt(3,N)
      L=N
      N=Hash1Next(N)
   enddo
   if(Found) then
      T=L
   else
      T=0
   endif
end subroutine Fct_Lku
!-------------------------------------------------------------------------------
subroutine Fct_Ent(        T,P,   C,L,  FctVrt,      FctTPW,FctFac, AnzFac)
   integer(4),intent(out)::T
   integer(4),intent(in)::   P(3),C,L
   integer(4),intent(out)::             FctVrt(3,*), FctTPW(2,*)
   integer(2),intent(out)::                                 FctFac(2,*)
   integer(4),intent(inout)::                                       AnzFac
! * * * * * * * * * *
   call NewEnt(T,AnzFac,DimFct,'FctEnt')
   if(Hash1First(C).eq.0) then
      Hash1First(C)=T
   else
      if(Hash1Next(L).ne.0) call Crash('FctEnt')
      Hash1Next(L)=T
   endif
   Hash1Next(T)=0
   FctVrt(:,T)=P(:)
   FctTPW(:,T)=0
   FctFac(:,T)=0
end subroutine Fct_Ent
!=== end CONNECTIVITY

!=== beg OPEN/CLOSE HASH TABLE
subroutine Hash1Opn(      Dim)
   integer(4),intent(in)::Dim
! * * * * *
   if(Hash1Busy) call Crash('Hash1Opn')
   allocate(Hash1First(0:DimHas))
   allocate(Hash1Next(Dim))
   Hash1First(:)=0
   Hash1Next(:)=123456789
   Hash1Busy=.true.
end subroutine Hash1Opn
!-------------------------------------------------------------------------------
subroutine Hash1Clo
! * * * * *
   if(.not.Hash1Busy) call Crash('Hash1Clo')
   deallocate(Hash1Next)
   deallocate(Hash1First)
   Hash1Busy=.false.
end subroutine Hash1Clo
!=== end OPEN/CLOSE HASH TABLE

!=== beg DIVERSE PROZEDUREN MIT ZUGRIFF AUF GLOBALE VARIABELN
subroutine NextFace(          fo,lam,Q,   N_d,   R,   A1,   A2,   A3,   fi)
   integer(2),intent(inout):: fo
   real(8),   intent(inout)::      lam
   real(8),   intent(out)::          Q(3),N_d(3)
   real(8),   intent(in)::                       R(3),A1(3),A2(3),A3(3)
   integer(2),intent(in)::                                              fi

   real(8) A21(3),A31(3),Y(3),AA(3,3),X(3),N(3)
! * * * * * * * * *
   A21=A2 - A1
   A31=A3 - A1

   call CrsPrd(N, A21,A31)
   if(SUM(N*R).gt.0) then
      AA(:,1)=A21
      AA(:,2)=A31
      AA(:,3)=-R
      Y=CursorXYZ-A1
      call Solve3(AA,X,Y)
      if(X(3).lt.lam) then
         lam=X(3)
         Q=CursorXYZ+lam*R
         N_d=N
         !!print*,'NextFace:',A1 + X(1)*A21 + X(2)*A31 - Q
         fo=fi
      endif
   endif
end subroutine NextFace
!-------------------------------------------------------------------------------
subroutine LdFace(         Z_d,   K_d, V_d,   F, Typ)
   integer(4),intent(out)::Z_d(3)
   integer(2),intent(out)::       K_d
   integer(4),intent(in)::             V_d(8)
   integer(2),intent(in)::                    F, Typ
! k=1: Z_d(1:3) ist auswaerts orientiert
! k=2: Z_d(1:3) ist einwaerts orientiert

   integer(2),parameter:: T1(4) = [1, 1, 2, 1], &
                          T2(4) = [2, 2, 3, 3], &
                          T3(4) = [3, 4, 4, 4], &
                          T4(4) = [4, 3, 1, 2], &

                          P1(4) = [1, 2, 1, 3], &
                          P2(4) = [4, 3, 2, 4], &
                          P3(4) = [5, 5, 5, 5], &
                          P4(4) = [2, 1, 3, 1], &
                          P5(4) = [3, 4, 4, 2], &

                          W1(5) = [1, 1, 2, 1, 4], &
                          W2(5) = [3, 2, 3, 2, 5], &
                          W3(5) = [4, 4, 5, 3, 6], &
                          W4(5) = [6, 5, 6, 4, 1], &
                          W5(5) = [2, 3, 1, 5, 2], &
                          W6(5) = [5, 6, 4, 6, 3], &

                          H1(6) = [1, 2, 1, 3, 1, 5], &
                          H2(6) = [3, 4, 2, 4, 2, 6], &
                          H3(6) = [5, 6, 5, 7, 3, 7], &
                          H4(6) = [7, 8, 6, 8, 4, 8], &
                          H5(6) = [2, 1, 3, 1, 5, 1], &
                          H6(6) = [4, 3, 4, 2, 6, 2], &
                          H7(6) = [6, 5, 7, 5, 7, 3], &
                          H8(6) = [8, 7, 8, 6, 8, 4]
   real(8) A,B,C,D
   integer(4) V(8),Z(8)
   integer(2) I,K
! * * * * * * * * * *
   V(1:Typ)=V_d(1:Typ)

   if(Typ.eq.4) then ! tetrahedron
      if(F.ge.1 .and. F.le.4) then
         Z(1)=V(T1(F))
         Z(2)=V(T2(F))
         Z(3)=V(T3(F))
         Z(4)=V(T4(F))

         if(Z(1).gt.Z(2)) call XChgJ(Z(1),Z(2))
         if(Z(1).gt.Z(3)) call XChgJ(Z(1),Z(3))
         if(Z(2).gt.Z(3)) call XChgJ(Z(2),Z(3))

         if(VolTetJ(Z(1),Z(2),Z(3),Z(4)).lt.0) then
            K=1
         else
            K=2
         endif
      else
         goto 90
      endif
   elseif(Typ.eq.5) then ! pyramid
      if(F.ge.1 .and. F.le.4) then
         Z(1)=V(P1(F))
         Z(2)=V(P2(F))
         Z(3)=V(P3(F))
         Z(4)=V(P4(F))
         Z(5)=V(P5(F))

         if(Z(1).gt.Z(2)) call XChgJ(Z(1),Z(2))
         if(Z(1).gt.Z(3)) call XChgJ(Z(1),Z(3))
         if(Z(2).gt.Z(3)) call XChgJ(Z(2),Z(3))

         A=VolTetJ(Z(1),Z(2),Z(3),Z(4))
         B=VolTetJ(Z(1),Z(2),Z(3),Z(5))
         if(A*B.le.0) goto 91
         if(A.lt.0) then
            K=1
         else
            K=2
         endif
      elseif(F.eq.5) then
         do I=1,5
            Z(I)=V(I)
         enddo

         do K=4,2,-1
            do I=2,K
               if(Z(I-1).gt.Z(I)) call XchgJ(Z(I-1),Z(I))
            enddo
         enddo

         if(VolTetJ(Z(1),Z(2),Z(3),Z(5)).lt.0) then
            K=1
         else
            K=2
         endif
      else
         goto 92
      endif
   elseif(Typ.eq.6) then ! wedge
      Z(1)=V(W1(F))
      Z(2)=V(W2(F))
      Z(3)=V(W3(F))
      Z(4)=V(W4(F))
      Z(5)=V(W5(F))
      Z(6)=V(W6(F))
      if(F.ge.1 .and. F.le.3) then
         do K=4,2,-1
            do I=2,K
               if(Z(I-1).gt.Z(I)) call XchgJ(Z(I-1),Z(I))
            enddo
         enddo

         A=VolTetJ(Z(1),Z(2),Z(3),Z(5))
         B=VolTetJ(Z(1),Z(2),Z(3),Z(6))
         if(A*B.le.0) goto 93
         if(A.lt.0) then
            K=1
         else
            K=2
         endif
      elseif(F.eq.4 .or. F.eq.5) then
         if(Z(1).gt.Z(2)) call XChgJ(Z(1),Z(2))
         if(Z(1).gt.Z(3)) call XChgJ(Z(1),Z(3))
         if(Z(2).gt.Z(3)) call XChgJ(Z(2),Z(3))

         A=VolTetJ(Z(1),Z(2),Z(3),Z(4))
         B=VolTetJ(Z(1),Z(2),Z(3),Z(5))
         C=VolTetJ(Z(1),Z(2),Z(3),Z(6))
         if(A*B.le.0 .or. A*C.le.0) goto 94
         if(A.lt.0) then
            K=1
         else
            K=2
         endif
      else
         goto 95
      endif
   elseif(Typ.eq.8) then ! hexahedron
      if(F.ge.1 .and. F.le.6) then
         Z(1)=V(H1(F))
         Z(2)=V(H2(F))
         Z(3)=V(H3(F))
         Z(4)=V(H4(F))
         Z(5)=V(H5(F))
         Z(6)=V(H6(F))
         Z(7)=V(H7(F))
         Z(8)=V(H8(F))

         do K=4,2,-1
            do I=2,K
               if(Z(I-1).gt.Z(I)) call XchgJ(Z(I-1),Z(I))
            enddo
         enddo

         A=VolTetJ(Z(1),Z(2),Z(3),Z(5))
         B=VolTetJ(Z(1),Z(2),Z(3),Z(6))
         C=VolTetJ(Z(1),Z(2),Z(3),Z(7))
         D=VolTetJ(Z(1),Z(2),Z(3),Z(8))

         if(A*B.le.0 .or. A*C.le.0 .or. A*D.le.0) goto 94
         if(A.lt.0) then
            K=1
         else
            K=2
         endif
      else
         goto 95
      endif
   else
      print*, 'Typ=',Typ
      goto 96
   endif

   Z_d(1)=Z(1)
   Z_d(2)=Z(2)
   Z_d(3)=Z(3)
   K_d   =K
   return
! * * * * *
90 call Crash('LdFace 90')
91 call Crash('LdFace 91')
92 call Crash('LdFace 92')
93 call Crash('LdFace 93')
94 call Crash('LdFace 94')
95 call Crash('LdFace 95')
96 call Crash('LdFace 96')
end subroutine LdFace
!-------------------------------------------------------------------------------
real(8) function VolTetJ( P1,P2,P3,P4)
   integer(4),intent(in)::P1,P2,P3,P4

   real(8) A(3),B(3),C(3),D(3)
! * * * * * * * * * *
   A(:)=VrtXYZ(:,P1)
   B(:)=VrtXYZ(:,P2)
   C(:)=VrtXYZ(:,P3)
   D(:)=VrtXYZ(:,P4)
   VolTetJ=VolTet4(A,B,C,D)
end function VolTetJ
!-------------------------------------------------------------------------------
real(8) function geoVolCell(J)
   integer(4),intent(in)::  J

   integer(4) I1,I2,I3,I4
   real(8) K1(3),K2(3),K3(3),K4(3)
! * * * * * * * * *
      if(CellTyp(J).eq.4) then
         I1=CellVrt(1,J)    ! Indizes der Tetraeder-Eckpunkte
         I2=CellVrt(2,J)
         I3=CellVrt(3,J)
         I4=CellVrt(4,J)

         K1=VrtXYZ(:,I1)   ! Koordinaten der Eckpunkte
         K2=VrtXYZ(:,I2)
         K3=VrtXYZ(:,I3)
         K4=VrtXYZ(:,I4)
         geoVolCell=ABS(VolTet4(K1,K2,K3,K4)) ! Volumen des Tetraeders
      else
         call Crash('geoVolCell implemented only for tetrahedra')
         geoVolCell=0
      endif
end function geoVolCell
!=== end DIVERSE PROZEDUREN MIT ZUGRIFF AUF GLOBALE VARIABELN
!-------------------------------------------------------------------------------
subroutine Solve3(       AA,     X,   M)
   real(8),intent(in)::  AA(3,3),     M(3)
   real(8),intent(out)::         X(3)

   real(8) A(3),B(3),C(3),D

   A=AA(:,1)
   B=AA(:,2)
   C=AA(:,3)

   D   =Det3(A,B,C)

   X(1)=Det3(M,B,C)/D
   X(2)=Det3(A,M,C)/D
   X(3)=Det3(A,B,M)/D
end subroutine Solve3
!-------------------------------------------------------------------------------
real(8) function Det3( A,   B,   C)
   real(8),intent(in)::A(3),B(3),C(3)

   Det3 =   A(1)*(B(2)*C(3) - B(3)*C(2)) &
          - A(2)*(B(1)*C(3) - B(3)*C(1)) &
          + A(3)*(B(1)*C(2) - B(2)*C(1))
end function Det3
!-------------------------------------------------------------------------------
real(8) function VolTet4(A0,   A1,   A2,   A3)
   real(8),intent(in)::  A0(3),A1(3),A2(3),A3(3)
!VolTet4 > 0 bedeutet, dass die Seitendreiecke folgendermassen orientiert sind:
!   (A0,A1,A2): einwaerts
!   (A0,A1,A3): auswaerts
!   (A0,A2,A3): einwaerts
!   (A1,A2,A3): auswaerts
!
! Das Dreieck (A0,A1,A2) heisst "einwaerts orientiert",
! wenn (A1-A0)x(A2-A0) in das Tetraeder hineinzeigt.
! * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *

   real(8) A10(3), C(3), A21(3), A31(3)
! * * * * * * * * * *
   A21=A2-A1
   A31=A3-A1
   A10=A1-A0
   call CrsPrd(C, A21, A31)
   VolTet4=SUM(A10*C)/6
end function VolTet4
!------------------------------------------------------------------------------
!function CrossProduct(  A,   B) result(AxB)
!   real(8),intent(in):: A(3),B(3)
!   real(8)::                           AxB(3)
!! * * * * * * * * * *
!   AxB(1) =  A(2)*B(3) - B(2)*A(3)
!   AxB(2) = -A(1)*B(3) + B(1)*A(3)
!   AxB(3) =  A(1)*B(2) - B(1)*A(2)
!end function CrossProduct
!-------------------------------------------------------------------------------
subroutine CrsPrd(       AxB,   A,   B)
   real(8),intent(out):: AxB(3)
   real(8),intent(in)::         A(3),B(3)
! * * * * * * * * * *
   AxB(1) =  A(2)*B(3) - B(2)*A(3)
   AxB(2) = -A(1)*B(3) + B(1)*A(3)
   AxB(3) =  A(1)*B(2) - B(1)*A(2)
end subroutine CrsPrd
!-------------------------------------------------------------------------------
subroutine XchgJ(             P1, P2)
   integer(4),intent(inout):: P1, P2

   integer(4) L
! * * * * *
   L=P2
   P2=P1
   P1=L
end subroutine XchgJ
!-------------------------------------------------------------------------------
subroutine NewEnt(             A,B,D,T)
   integer(4),  intent(out)::  A
   integer(4),  intent(inout)::  B
   integer(4),  intent(in)::       D
   character(*),intent(in)::         T
! * * * * *
   !print*,'NewEnt:',T,'B,D=',B,D
   B=B+1
   A=B
   if(A.le.D) return
! * * * * *
   print*, 'NewEnt:', T, ' D=',D
   call Crash('NewEnt')
end subroutine NewEnt
!-------------------------------------------------------------------------------
subroutine OldSqF(            U, Name)
   integer(2),  intent(out):: U
   character(*),intent(in)::     Name
! * * * * * * * * * *
   call GetUnit(U)
   open(unit=U, file=Name, access='SEQUENTIAL', form='FORMATTED', &
   &    status='OLD', err=90)
   return
! * * * * *
90 print*, 'file ',Name,' does not exist.'
   call Crash('OldSqF')
end subroutine OldSqF
!-------------------------------------------------------------------------------
subroutine NewSqF(            U, Name)
   integer(2),  intent(out):: U
   character(*),intent(in)::     Name
! * * * * * * * * *
   call GetUnit(U)
   call DelIfExi(U, Name)
   open(unit=U, file=Name, access='SEQUENTIAL', form='FORMATTED', &
   &    status='NEW')
end subroutine NewSqF
!-------------------------------------------------------------------------------
subroutine GetUnit(        U_d)
   integer(2),intent(out)::U_d

   integer(2) U
   logical isOpen
! * * * * * * * * * *
   do U=1,16
      inquire(unit=U, opened=isOpen)
      if(.not.isOpen) exit
   enddo
   if(U.gt.16) call Crash('GetUnit')
   U_d=U
end subroutine GetUnit
!-------------------------------------------------------------------------------
subroutine DelIfExi(        U, Name)
   integer(2),intent(in)::  U
   character(*),intent(in)::   Name

   logical x
! * * * * * * * * * *
   inquire(file=Name, exist=x)
   if(x) then
      open(unit=U, file=Name)
      close(unit=U, status='DELETE')
   endif
end subroutine DelIfExi
!-------------------------------------------------------------------------------
subroutine Crash(           Text)
   character(*),intent(in)::Text
   print*, 'crashing in MeshGeo:', Text
   stop
end subroutine Crash

subroutine MySort(           ilst,    sc,    anz)
   integer(4),intent(in)::                   anz
   integer(4),intent(inout)::ilst(anz)
   integer(4),intent(in)::            sc(anz)

   integer(4),allocatable:: Z(:)
   integer(4) G,H,J,K,N1,N2,N,I1,I2,V1,V2
! * * * * * * * * *
   allocate(Z(anz))
   !print*,'MySort/beg, anz=',anz
   H=1
   do while(H.lt.anz)
      !print*,'H=',H
      G=2*H
      do J=1,anz,G
         N =MIN(G,anz-J+1)
         N1=MIN(H,N)
         N2=N-N1

         I1=J
         I2=I1+N1
         do K=1,N
            if(N1.ne.0 .and. N2.ne.0) then
               V1=ilst(I1)
               V2=ilst(I2)
               if(sc(V1).le.sc(V2)) then
                  Z(K)=V1
                  I1=I1+1; N1=N1-1
               else
                  Z(K)=V2
                  I2=I2+1; N2=N2-1
               endif
            elseif(N1.ne.0) then
               Z(K)=ilst(I1)
               I1=I1+1; N1=N1-1
            else
               Z(K)=ilst(I2)
               I2=I2+1; N2=N2-1
            endif
         enddo
         ilst(J:J+N-1)=Z(1:N)
      enddo
      H=H*2
   enddo
   deallocate(Z)
   !print*,'MySort/end'
end subroutine MySort

end module MeshGeo
