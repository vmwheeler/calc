module MeshLib
! - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
!Purpose
!  To provide access to mesh data, in particular with respect to ray tracing.
!
!  The routines contained in this module can be considered as interfaces to
!  the low-level module meshGeo.
! - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
   implicit none; private

   public useSurf        ! introduce a non-clustered surface
   public useClst        ! introduce a clustered surface

   public getSurfNum     ! get total number of surfaces
   public getClstNum     ! get total number of clusters

   public getSurfElemNum ! get first/last element index in each surface
   public getClstElemNum ! get first/last element index in each cluster

   public smoothClst     ! smooth cluster boundaries

   public writeMeshClst  ! write a mesh file with surfaces resolved in clusters
   public writeMeshArch  ! write a mesh file with surfaces resolved in archipels

   public closeMesh      ! deallocate memory
   public FaceToSurfElem !
   public getArea        !
   public getBaryNorm    ! get barycenter and (average) normal of a surface
   public getFaceIsect
   public getFaceType
   public getImpact
   public getNextVolElem
   public getNormal
   public getSurfIndex
   public getSurfIsect
   public getSurfVert
   public getVol
   public getVolElemNum
   public getVolNum
   public getVolVert
   public openMesh
   public SurfElemToVolElem

contains
!------------------------------------------------------------------------------
subroutine useSurf(         label)
   use meshGeo,only: geoUseSurf
   character(*),intent(in)::label
! - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
!Purpose:
!  -- To "introduce" a non-clustered boundary surface,
!     i.e. to make it accessible to the MeshLib user;

!  -- to associate a surface index with the specified surface label

!  Association of labels with indices is determined by the sequence of
!  "call useSurf" and "call useClst" statements. Example: The code sequence

!     call UseSurf('aperture')
!     call UseClst('front', 0.1d0)
!     call UseClst('back',  0.1d0)

!  associates 'aperture' with index 1,
!             'front'    with index 2,
!             'back'     with index 3.
! - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
   call geoUseSurf(label)
end subroutine useSurf
!------------------------------------------------------------------------------
subroutine useClst(         label, ClusterDiameter)
   use meshGeo, only: geoUseClSu
   character(*),intent(in)::label
   real(8),     intent(in)::       ClusterDiameter
!Purpose:
!  -- To introduce a clustered boundary surface;
!  -- to associate a surface index with the specified surface label.

!Restriction:
!  All clustered surfaces must be introduced:
!  -- either before all non-clustered surfaces
!  -- or after all non-clusteded surfaces.
!  This implies that indices of non-clustered surfaces are all lower
!  than indices of clustered surfaces, or vice-versa.
! - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
   call geoUseClSu(label, ClusterDiameter)
end subroutine useClst
!------------------------------------------------------------------------------
function getSurfNum() result(n)
   use meshGeo, only: nUsedSurf
   integer(4)::              n
! - - - - - - - - - - - - - - - - - - - - - -
!Result:
!  n   number of boundary surfaces in the mesh (including clustered surfaces
!      as well as non-clustered surfaces)
! - - - - - - - - - - - - - - - - - - - - - -
   n=nUsedSurf
end function getSurfNum
!------------------------------------------------------------------------------
function getClstNum() result(n)
   use meshGeo, only: nClst
   integer(4)::              n
! - - - - - - - - - - - - - - - - - - - - - -
!Result:
!  n   total number of clusters (i.e. a sum over all clustered surfaces)
! - - - - - - - - - - - - - - - - - - - - - -
   n=nClst
end function getClstNum
!------------------------------------------------------------------------------
function getSurfElemNum() result(nn)
   use meshGeo, only: nUsedSE, nUsedSurf, nOrigSE, Surf_WI
   integer(4)                    nn(0:nUsedSurf)
! - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
!Result:
!    nn(1)   = index of last  element in surface #1

!    nn(1)+1 = index of first element in cluster #2
!    nn(2)   = index of last  element in cluster #2

!    etc

!An error termination occures if one or more boundary surfaces have not been
!  "introduced" by useSurf or useClst prior to calling getSurfElemNum.
!  Note that useSurf/useClst must be called exactly once
!  for ever boundary surface.
! - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
   if(nUsedSE.ne.nOrigSE) &
      call Crash('useSurf must be called exactly once for every surface.')
   nn(0:nUsedSurf)=Surf_WI(0:nUsedSurf)
end function getSurfElemNum
!------------------------------------------------------------------------------
function getClstElemNum() result(nn)
   use meshGeo, only: nClst, Clst_WI
   integer(4)                    nn(0:nClst)
! - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
!Result:
!    nn(0)+1 = index of first element in cluster #1
!    nn(1)   = index of last  element in cluster #1

!    nn(1)+1 = index of first element in cluster #2
!    nn(2)   = index of last  element in cluster #2
! - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
   nn(0:nClst)=Clst_WI(0:nClst)
end function getClstElemNum
!------------------------------------------------------------------------------
subroutine smoothClst()
   use meshGeo, only: geoSmooth
!Purpose:
!  To smooth the boundary between adjacent clusters.
! - - - - - - - - - - - - - - - - - - - - - - - - -
   call geoSmooth
end subroutine smoothClst
!------------------------------------------------------------------------------
subroutine WriteMeshClst(   filename)
   use meshGeo, only: geoWriClst
   character(*),intent(in)::filename
!Purpose:
!  To write a mesh file, where every clustered surfaces is represented by
!  a set of surfaces, with
!  -- prefixes identical to the respective surface names, and
!  -- suffixes identical to cluster indices.
   call geoWriClst(filename)
end subroutine WriteMeshClst
!------------------------------------------------------------------------------
subroutine WriteMeshArch(   filename)
   use meshGeo, only: geoWriArch
   character(*),intent(in)::filename

   call geoWriArch(filename)
end subroutine WriteMeshArch
!==============================================================================
subroutine closeMesh
! - - - - - - - - - - - - - - - - - - - - -
!Purpose:
!  Deallocate memory allocated by MeshOpen
! - - - - - - - - - - - - - - - - - - - - -
   use meshGeo, only: geoClose
   call geoClose
end subroutine closeMesh
!------------------------------------------------------------------------------
function FaceToSurfElem(  iVolElem, iFace) result(iSurfElem)
   use meshGeo, only: CellNgb, nCells
   integer(4),intent(in)::iVolElem, iFace
   integer(4)                                     iSurfElem
! - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
!Input:
!  iVolElem   index of a volume element
!  iFace      index of a face of that volume element

!Result:
!  iSurfElem  index of the surface element being identical with the face
!             defined by iVolElem and iFace

!Conditions of error termination
!1. Illegal values for iVolElem or iFace.
!2. The face spcified by iVolElem and iFace does not belong to a boundary
!   surface of the mesh.
! - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
   if(iFace.le.0    .or. iFace.gt.4 .or.     &
      iVolElem.le.0 .or. iVolElem.gt.nCells) &
      call Crash('FaceToSurfElem: input argument(s) out of range')
   iSurfElem=-CellNgb(iFace, iVolElem)
   if(iSurfElem.le.0) call Crash('FaceToSurfElem:face not on boundary surface')
end function FaceToSurfElem
!------------------------------------------------------------------------------
function getArea(         iSurfElem) result(area)
   use meshGeo, only: geoAreaSE, nUsedSE
   integer(4),intent(in)::iSurfElem
   real(8)                                  area

   if(iSurfElem.le.0 .or. iSurfElem.gt.nUsedSE) &
      call Crash('getArea: index of surface element out of range')
   area=geoAreaSE(iSurfElem)
end function getArea
!------------------------------------------------------------------------------
subroutine getBaryNorm(   iSurf, Bary,    Norm)
   use meshGeo, only: Surf_WI, nUsedSurf
   integer(4),intent(in)::iSurf
   real(8),intent(out)::         Bary(3), Norm(3)
! - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
!Purpose
!  Get BARYcenter & average inward NORMal vector of a given surface

!Input
!  iSurf  surface index
!
!Output
!  Bary   barycenter
!  Norm   inward normal vector
! - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
   real(8) A, totA, V(3,3)
   integer(4) i1, i2, i

   if(iSurf.le.0 .or. iSurf.gt.nUsedSurf) &
      call crash('getBaryNorm: iSurf out of range')
   i1=Surf_WI(iSurf-1)+1
   i2=Surf_WI(iSurf)

   Bary=0
   Norm=0
   totA=0
   do i=i1,i2
      A=GetArea(i)
      V=GetSurfVert(i)
      Bary=Bary + A*(V(1,:)+V(2,:)+V(3,:))
      Norm=Norm + A*GetNormal(i)
      totA=totA + A
   enddo
   Bary=Bary/(3*totA)
   Norm=Norm/totA
end subroutine getBaryNorm
!------------------------------------------------------------------------------
subroutine getFaceIsect(  iVolElem, xStart,   direct, iFace, xIsect)
   use meshGeo, only: geoNextF,geoInCell,CursorCell,CursorXYZ,nCells
   integer(4),intent(in)::iVolElem
   real(8),   intent(in)::          xStart(3),direct(3)
   integer(4),intent(out)::                           iFace
   real(8),   intent(out)::                                  xIsect(3)
! - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
!Purpose:
!  To find the point where a ray starting at a given point in a given volume
!  element intersects the surface of the volume element.

!Input:
!  iVolElem index of volume element containing the xStart
!  xStart   starting point
!  direct   ray direction, does not need to be normalized

!Output:
!  iFace    index of the face of the volume element where the ray reaches the
!           cell boundary. iFace is in the range 1:4 .
!  xIsect   intersection point

!Conditions of error termination:
!1. iStart < 0  or  iStart > total number of volume elements.
!2. xStart is outside the volume element iVolElem.
! - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
   integer(2) f

   CursorCell=iVolElem
   if(CursorCell.le.0 .or. CursorCell.gt.nCells) &
      call crash('getFaceIsect: index of volume element out of range')
   CursorXYZ=xStart
   if(.not.geoInCell(CursorXYZ, CursorCell)) &
      call crash('getFaceIsect: starting point not in volume element')
   call geoNextF(direct, f)
   iFace=f
   xIsect=CursorXYZ
end subroutine getFaceIsect
!------------------------------------------------------------------------------
function getFaceType(     iVolElem, iFace) result(FaceType)
   use meshGeo, only: nCells, CellNgb
   integer(4),intent(in)::iVolElem, iFace
   character(8)                                   FaceType

   if(iFace.le.0 .or. iFace.gt.4 .or. iVolElem.le.0 .or. iVolElem.gt.nCells) &
      call crash('getFaceType: argument(s) out of range')
   if(CellNgb(iFace, iVolElem).lt.0) then
      FaceType='boundary'
   else
      FaceType='internal'
   endif
end function getFaceType
!------------------------------------------------------------------------------
subroutine getImpact(     xStart,   dir, iSurf, iTarget, xTarget)
   use meshGeo, only: geoImpact, Surf_WI, nUsedSurf
   real(8),   intent(in)::xStart(3),dir(3)
   integer(4),intent(in)::               iSurf
   integer(4),intent(out)::                     iTarget
   real(8),   intent(out)::                              xTarget(3)
! - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
!Purpose:
!  Browse a given boundary surface to find the element which is intersected
!  by a given ray coming from outside the mesh region.

!Input:
!  xStart   starting point of ray
!  dir      direction of ray
!  iSurf    index of the surface to be browsed

!Ouput in case when an intersection exists:
!  iTarget     index of intersected surface element
!  xTarget     intersection point

!Ouput in the case when no intersection exists:
!  iTarget     0
!  xTarget     [0,0,0]
! - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
!The following two parameters can be used for testing, or for modifying
!the behaviour of getImpact. In particular, if both parameters are .false.,
!the ray is allowed to impinge "from both sides".

   logical,parameter:: MustImpingeFromInside  = .false. ! .true.
   logical,parameter:: MustImpingeFromOutside = .true.  ! .false.

   real(8) cosIA ! cosine of the Impinging Angle, negative if ray is
                 ! impinging from outside the mesh region
   integer(4) i1,i2

   if(iSurf.le.0 .or. iSurf.gt.nUsedSurf) &
      call crash('getImpact: iSurf out of range')
   i1=Surf_WI(iSurf-1)+1
   i2=Surf_WI(iSurf)
   do iTarget=i1,i2
      call geoImpact(xStart, dir, iTarget, xTarget, cosIA)
      if(MustImpingeFromOutSide .and. cosIA.lt.0 .or. &
         MustImpingeFromInside  .and. cosIA.gt.0 .and. cosIA.lt.2) exit
   enddo
   if(iTarget.gt.i2) then
      iTarget=0
      xTarget=0
   endif
end subroutine getImpact
!------------------------------------------------------------------------------
function getNextVolElem(  iVolElem, iFace) result(iNextVolElem)
   use meshGeo, only: nCells, CellNgb
   integer(4),intent(in)::iVolElem, iFace
   integer(4)::                                   iNextVolElem
! - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
!Input:
!  iVolElem      index of a volume element
!  iFace         index of a face of element iVolElem
!Result:
!  iNextVolElem  index of the volume element adjacent to the face defined by
!                the two input arguments.
!Conditions of error termination:
!1. An input argument is out of range.
!2. The specified face does not belong to a boundary surface of the mesh.
! - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
   if(iFace.le.0    .or. iFace.gt.4 .or.                 &
      iVolElem.le.0 .or. iVolElem.gt.nCells) &
      call Crash('getNextVolElem: argument(s) out of range')
   iNextVolElem=CellNgb(iFace,iVolElem)
   if(iNextVolElem.le.0) &
      call Crash('getNextVolElemt: face not on boundary surface')
end function getNextVolElem
!------------------------------------------------------------------------------
function getNormal(        iSurfElem) result(N)
   use meshGeo, only: geoTriVert, SE_Cell,SE_Face, VrtXYZ

   integer(4),intent(in):: iSurfElem
   real(8)                                   N(3)
! - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
!Input:
!   iSurfElem  index of a surface element

!Result:
!   N          inward normal vector of the specified surface element
! - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
   integer(4) V(3)
   real(8) A1(3),A2(3),A3(3)

   V=geoTriVert(SE_Cell(iSurfElem), SE_Face(iSurfElem))
   A1=VrtXYZ(:,V(1))
   A2=VrtXYZ(:,V(2))-A1
   A3=VrtXYZ(:,V(3))-A1
   N = CrossProduct(A3,A2)
   N = N/SQRT(SUM(N**2))
end function getNormal
!------------------------------------------------------------------------------
function getSurfIndex(     iSurfElem) result(iSurf)
   use meshGeo, only: SE_Surf
   integer(4),intent(in):: iSurfElem
   integer(4)::                              iSurf
! - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
!Input:
!  iSurfElem   index of a surface element

!Result:
!  iSurf       index of the surface containing the given surface element
! - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
   iSurf=SE_Surf(iSurfElem)
end function getSurfIndex
!------------------------------------------------------------------------------
subroutine getSurfIsect(   iStart, xStart,   direct, iIsect, xIsect)
   use meshGeo, only: geoFirst, geoNextB, geoInCell, &
                      BbDiam, nUsedSE, CursorCell, CursorXYZ, CellNgb, SE_Cell

   integer(4),intent(in):: iStart
   real(8),   intent(in)::         xStart(3),direct(3)
   integer(4),intent(out)::                          iIsect
   real(8),   intent(out)::                                  xIsect(3)
! - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
!Purpose:
!  To find the point where a ray starting from a given point and with a given
!  direction intersects the boundary surface of the mesh.

!Input:
!  iStart   index of the surface element containing iStart,
!           or 0 if xStart is not on the mesh boundary, or
!                if the surface element containing xStart is unknown.
!  xStart   starting point of ray to be traced
!  direct   direction of ray, to be specified as a unit vector

!Output:
!  iIsect   index of the surface element containing xIsect
!  xIsect   intersection point

!Conditions of error termination:
!1. xStart is outside the mesh region.
!2. iStart.ne.0 and xStart is not located (within a suitable tolerance)
!   in the volume element adjacent to the surface element iStart.
!3. iStart < 0  or  iStart > total number of surface elements
! - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
   real(8) OutNor(3)
   integer(2) s

   if(iStart.eq.0) then
      call geoFirst(xStart, s)
      if(s.le.0) call Crash('getSurfIsect: xStart outside mesh region')
   elseif(iStart.gt.0 .and. iStart.le.nUsedSE) then
      CursorCell=SE_Cell(iStart)
      CursorXYZ=xStart
      if(.not.geoInCell(CursorXYZ, CursorCell)) &
         call Crash('getSurfIsect: xStart not on surface element Istart')
   else
      call Crash('getSurfIsect: iStart out of range')
   endif
   call geoNextB(xstart+BbDiam*direct, s, OutNor)
   if(s.le.0 .or. s.gt.4) goto 99
   iIsect=-CellNgb(s,CursorCell)
   if(iIsect.le.0 .or. iIsect.gt.nUsedSE) goto 99
   !print*, getNormal(iIsect)+OutNor
   xIsect=CursorXYZ
   return
99 call Crash('getSurfIsect: internal error')
end subroutine getSurfIsect
!------------------------------------------------------------------------------
function getSurfVert(      iSurfElem) result(xyz)
   use meshGeo, only: geoTriVert, SE_Cell, SE_Face, VrtXYZ
   integer(4),intent(in):: iSurfElem
   real(8)                                   xyz(3,3)
! - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
!Input:
!  iSurfElem   index of a surface element

!Result:
!  xyz(i,1) = x coordinate of vertex i of the surface element, i=1,2,3
!  xyz(i,2) = y coordinate of vertex i of the surface element, i=1,2,3
!  xyz(i,3) = z coordinate of vertex i of the surface element, i=1,2,3
! - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
   integer(4) V(3)

   V=geoTriVert(SE_Cell(iSurfElem),SE_Face(iSurfElem))
   xyz(1,:)=VrtXYZ(:,V(1))
   xyz(2,:)=VrtXYZ(:,V(2))
   xyz(3,:)=VrtXYZ(:,V(3))
end function getSurfVert
!------------------------------------------------------------------------------
function getVol(           iVolElem) result(vol)
   use meshGeo,only: geoVolCell, nCells
   integer(4),intent(in):: iVolElem
   real(8)                                  vol
! - - - - - - - - - - - - - - - - - - - - - - - - -
!Input:
!  iVolElem  index of a volume element

!Result:
!  vol       volume of the specified volume element
! - - - - - - - - - - - - - - - - - - - - - - - - -
   if(iVolElem.le.0 .or. iVolElem.gt.nCells) &
      call Crash('getVol: cell index out of range')

   vol=geoVolCell(iVolElem)
end function getVol
!------------------------------------------------------------------------------
function getVolElemNum() result(nn)
   use meshGeo, only: nDomains, nCells
   integer(4)                   nn(nDomains)

   if(nDomains.ne.1) call Crash('number of domains > 1: not yet implemented')
   nn(1)=nCells
end function getVolElemNum
!------------------------------------------------------------------------------
function getVolNum() result(n)
   use meshGeo, only: nDomains
   integer(4)               n
! - - - - - - - - - - - - - - - - - -
!Result:
!  n   number of domains in the mesh
! - - - - - - - - - - - - - - - - - -
   n=nDomains
end function getVolNum
!------------------------------------------------------------------------------
function getVolVert(       iVol) result(xyz)
   use meshGeo, only: CellVrt,VrtXYZ
   integer(4),intent(in):: iVol
   real(8)                              xyz(4,3)
! - - - - - - - - - - - - - - - - - - - - - - - -
!Input:
!  iVol  index of a volume element

!Result:
!  xyz(v,1:3) = coordinates of vertex v, v=1,2,3,4
! - - - - - - - - - - - - - - - - - - - - - - - -
   xyz(1,:)=VrtXYZ(:,CellVrt(1,iVol))
   xyz(2,:)=VrtXYZ(:,CellVrt(2,iVol))
   xyz(3,:)=VrtXYZ(:,CellVrt(3,iVol))
   xyz(4,:)=VrtXYZ(:,CellVrt(4,iVol))
end function getVolVert
!------------------------------------------------------------------------------
subroutine openMesh(        FileName)
   use meshGeo, only: geoOpen
   character(*),intent(in)::FileName
!Purpose:
!  To make the data encoded in a mesh file accessible to the meshlib user.
!
!input:
!  FileName    name of the mesh file. The mesh file must be in the format
!              used by Ansys ICEM CFD with Option "Ansys CFX"
!- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
   call geoOpen(FileName)
end subroutine openMesh
!------------------------------------------------------------------------------
function SurfElemToVolElem(iSurfElem) result(iVolElem)
   use meshGeo, only: nUsedSE, SE_Cell
   integer(4),intent(in):: iSurfElem
   integer(4)                                iVolElem
! - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
!Input:
!  iSurfElem  index of a surface element

!Result:
!  iVolElem   index of the adjacent volume element
! - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
   if(iSurfElem.le.0 .or. iSurfElem.gt.nUsedSE) &
      call crash('SurfToVolElem: input parameter out of range')
   iVolElem=SE_Cell(iSurfElem)
end function SurfElemToVolElem
!------------------------------------------------------------------------------
function CrossProduct( A,   B) result(AxB)
   real(8),intent(in)::A(3),B(3)
   real(8)::                          AxB(3)
! * * * * * * * * * *
   AxB(1) =  A(2)*B(3) - B(2)*A(3)
   AxB(2) = -A(1)*B(3) + B(1)*A(3)
   AxB(3) =  A(1)*B(2) - B(1)*A(2)
end function CrossProduct
!------------------------------------------------------------------------------
subroutine Crash(           text)
   character(*),intent(in)::text

   print*, 'Crashing in module MeshLib. Error specification:'
   print*, text
   stop
end subroutine Crash
end module MeshLib
