pure function random_uniform_point_triangle(rn1, rn2, vertices) result(point)

!*******************************************************************************
! Copyright (C) 2009 Wojciech Lipinski
!
! This file is part of the Thermal Science Library (TSL).
!
! TSL is free software: you can redistribute it and/or modify
! it under the terms of the GNU Lesser General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! TSL is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU Lesser General Public License for more details.
!
! You should have received a copy of the GNU Lesser General Public License
! along with TSL.  If not, see <http://www.gnu.org/licenses/>.
!*******************************************************************************

!*******************************************************************************
! Description:
!    Returns coordinates of a uniformly distributed point
!    in a triangle.
!
! Input:
!    rn1, rn2    Two random numbers
!    vertices    The three vertices of the triangle, 1*3 array
!
! Output:
!    point       The random point inside the triangle
!
! References:
!    [1] http://mathworld.wolfram.com/TrianglePointPicking.html
!*******************************************************************************

!*******************************************************************************
! 'use' and 'implicit none' statements
!*******************************************************************************

   use tsl_basics_kinds, only: k_r64
   implicit none

!*******************************************************************************
! Dummy argument declaration constructs
!*******************************************************************************

   real(k_r64),                 intent(in) :: rn1, rn2
   real(k_r64), dimension(3,3), intent(in) :: vertices
   real(k_r64), dimension(3)               :: point

!*******************************************************************************
! Local declaration constructs
!*******************************************************************************

   real(k_r64) :: s, t

!*******************************************************************************
! Execution part
!*******************************************************************************

   s = rn1 
   t = rn2 
   if (s+t > 1._k_r64) then
      s = 1-s
      t = 1-t
   end if
   point = (1._k_r64-s-t)*vertices(1,:)+s*vertices(2,:)+t*vertices(3,:)
   return

!*******************************************************************************
! 'end function' statement
!*******************************************************************************

end function random_uniform_point_triangle
