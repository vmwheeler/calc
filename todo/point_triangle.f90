pure function point_triangle(ran1, ran2, vertices) result(point)

!*******************************************************************************
! Copyright (C) 2008 Wojciech Lipinski
!
! This file is part of Thermal Science Library (TSL).
!
! TSL is free software: you can redistribute it and/or modify
! it under the terms of the GNU Lesser General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! TSL is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU Lesser General Public License for more details.
!
! You should have received a copy of the GNU Lesser General Public License
! along with TSL.  If not, see <http://www.gnu.org/licenses/>.
!*******************************************************************************

!*******************************************************************************
! Description:
!*******************************************************************************

!*******************************************************************************
! Use and implicit none statements
!*******************************************************************************

  use tsl_type, only: DP
  implicit none

!*******************************************************************************
! Dummy argument declaration constructs
!*******************************************************************************

  real(DP), intent(in) :: ran1, ran2
  real(DP), dimension(3,3), intent(in) :: vertices
  real(DP), dimension(3) :: point

!*******************************************************************************
! Local declaration constructs
!*******************************************************************************

  real(DP) :: s, t

!*******************************************************************************
! Execution part
!*******************************************************************************

  s = ran1 
  t = ran2 
  if(s+t > 1._DP) then
    s = 1-s
    t = 1-t
  end if
  point = (1._DP-s-t)*vertices(1,:)+s*vertices(2,:)+t*vertices(3,:)
  return

!*******************************************************************************
! End function statement
!*******************************************************************************

end function point_triangle
