subroutine intersection_line_triangle(point, dir, vertices, points, flag)                                           
!*******************************************************************************
! Copyright (C) 2009 Zheng Liang
! Copyright (C) 2009 Wojciech Lipinski
!
! This file is part of the Thermal Science Library (TSL).
!
! TSL is free software: you can redistribute it and/or modify
! it under the terms of the GNU Lesser General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! TSL is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU Lesser General Public License for more details.
!
! You should have received a copy of the GNU Lesser General Public License
! along with TSL.  If not, see <http://www.gnu.org/licenses/>.
!*******************************************************************************

!*******************************************************************************
! Description:
!    Returns coordinates of an intersection point between an infinite line
!    and a triangle.
!
! Input:
!    point       Point on the line
!    dir         Direction of the line (does not need to be normalized)
!    vertices    The three vertices of the triangle, 1*3 array
!
! Output:
!    points      Coordinates of the intersection point
!    flag        Logical flag taking the following values:
!                flag = .true. if the intersection point exists
!                flag = .false. if the intersection point does not exist
!                              or the line is on the triangle surface
!*******************************************************************************

!*******************************************************************************
! 'use' and 'implicit none' statements
!*******************************************************************************

   use tsl_basics_kinds,     only: k_r64, k_lg
   use tsl_math_vector_algebra, only: cross_product, vector_euclidean_norm
   use tsl_consants_math,   only: c_ux, c_uy, c_uz
   implicit none

!*******************************************************************************
! Dummy argument declaration constructs
!*******************************************************************************

   real(k_r64),    dimension(3),   intent(in) :: point, dir
   real(k_r64),    dimension(3,3), intent(in) :: vertices
   real(k_r64),    dimension(3),  intent(out):: points
   logical(k_lg),                   intent(out), optional :: flag

   interface
      pure function transform_coordinates(o1, x1, y1, z1, o2, x2, y2, z2, p1,& 
           td) result(p2)
         use tsl_basics_kinds, only: k_i8, k_r64, k_lg
         implicit none
         real(k_r64), dimension(3), intent(in) :: o1,x1, y1, z1, o2, x2, y2, &
                                                     z2, p1
         logical(k_lg),              intent(in), optional :: td
         real(k_r64), dimension(3)             :: p2
      end function transform_coordinates
   end interface

!*******************************************************************************
! Local declaration constructs
!*******************************************************************************

   real(k_r64), dimension(3)  :: tnormal, pbx, pby, pbz
   real(k_r64), dimension(3)  :: op, vp, pnew, vpnew, dirnew
   real(k_r64)                  :: tt
   real(k_r64), dimension(3)  :: pointnew
   real(k_r64), dimension(3,3):: tvnew
   real(k_r64), dimension(3)  :: MM12, MM13
   real(k_r64)                  :: area12, area23, area31, area      
   
!*******************************************************************************
! Execution part
!*******************************************************************************

   !Calculate the normal vector of the triangle
   MM12 = vertices(2,:) - vertices(1,:)
   MM13 = vertices(3,:) - vertices(1,:)
   tnormal = cross_product(MM12, MM13)
   tnormal = tnormal / vector_euclidean_norm(tnormal)
   pbz = tnormal
   call tripod(pbz, pbx, pby)  !Calculate other axes in the new coordinate
   op = (/0,0,0/)          !The origin point
   vp = point + dir         !The other point on the line
   pnew = transform_coordinates(op, c_ux, c_uy, c_uz, vertices(1,:), pbx, pby, &
          pbz, point, .true.)   !The point on the line in the new coordinate
   vpnew = transform_coordinates(op, c_ux, c_uy, c_uz, vertices(1,:), pbx, pby, &
          pbz, vp, .true.) !The other point on the line in the new coordinate
   dirnew = vpnew - pnew   !The direction vector in the new coordinate
   !Calculate the triangle vertices position in the new coordinate
   tvnew (1,:) = (/0,0,0/)
   tvnew (2,:) = transform_coordinates(op, c_ux, c_uy, c_uz, vertices(1,:), pbx, &
                 pby, pbz, vertices(2,:), .true.)  
   tvnew (3,:) = transform_coordinates(op, c_ux, c_uy, c_uz, vertices(1,:), pbx, &
                 pby, pbz, vertices(3,:), .true.)  
   ! The parameter equation of the line x = x0 + v *t is used
   ! The variable tt is the parameter "t" in the line parameter equation
   if (abs(dirnew(3)) < 1e-8 ) then
      ! dirnew(3)=0 (z-direction component of the line direction vector
      ! In that case, the line is on the plane z=0
      ! We treat that case as no intersection
      flag = .false.
   else
      tt = -pnew(3) / dirnew(3)
      pointnew (:) = pnew + dirnew * tt
      ! Judge whether the intersecting point is inside the triangle
      ! Calculate the area among the intersecting point and vertices 1, 2 and 3
      area12 = abs((tvnew(1,1) * tvnew(2,2) + tvnew(2,1) * pointnew(2)+ &
               pointnew(1) * tvnew(1,2) - tvnew(1,1) * pointnew(2) - &
               tvnew(2,1) * tvnew(1,2) - pointnew(1) * tvnew(2,2)))/2   
      area23 = abs((pointnew(1) * tvnew(2,2) + tvnew(2,1) * tvnew(3,2)+&
               tvnew(3,1) * pointnew(2) - pointnew(1) * tvnew(3,2) - &
               tvnew(2,1) * pointnew(2) - tvnew(3,1) * tvnew(2,2)))/2   
      area31 = abs((tvnew(1,1) * pointnew(2) + pointnew(1) * tvnew(3,2)+&
               tvnew(3,1) * tvnew(1,2) - tvnew(1,1) * tvnew(3,2)- &
               pointnew(1) * tvnew(1,2) - tvnew(3,1) * pointnew(2)))/2
      ! Calculate the area of the triangle
      area = abs((tvnew(1,1) * tvnew(2,2) + tvnew(2,1) * tvnew(3,2)+ &
               tvnew(3,1) * tvnew(1,2) - tvnew(1,1) * tvnew(3,2) - &
               tvnew(2,1) * tvnew(1,2) - tvnew(3,1) * tvnew(2,2)))/2   
      if ( abs(area12 + area23 + area31 - area) < 1e-8) then !Inside
         flag = .true.
      else
         flag = .false.
      end if
   end if
   !Transform the intersecting points in the new coordinate to 
   !the original coordinate
   points(:) = transform_coordinates(vertices(1,:), pbx, pby, pbz, op, c_ux, &
             c_uy, c_uz, pointnew(:), .false.)  
   return
   
!*******************************************************************************
! 'end subroutine' statement
!*******************************************************************************

end subroutine intersection_line_triangle