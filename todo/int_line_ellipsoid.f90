pure subroutine int_line_ellipsoid(r1, s0, r2, semiaxes, ???, xi, flag)
!-------------------------------------------------------------------------------
! This file is part of the Computational Algorithm Library Collection (CALCC).
!
! Copyright 2012 The CALCC Team
!
! CALCC is free software: you can redistribute it and/or modify
! it under the terms of the GNU Lesser General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! CALCC is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU Lesser General Public License for more details.
!
! You should have received a copy of the GNU Lesser General Public License
! along with CALCC.  If not, see <http://www.gnu.org/licenses/>.
!-------------------------------------------------------------------------------

!-------------------------------------------------------------------------------
! Begin specification part
!-------------------------------------------------------------------------------

  ! -- Use association --

   use calc_types,  only: k_r64, k_lg
   use calc_conss, only: c_ux, c_uy, c_uz
   use calc_poly,   only: solve_quadratic

  ! -- Null mapping --

  implicit none

  ! -- Dummy argument declarations --

  real   (k_r64), dimension(3),   intent(in)  :: x, u, opn
  real   (k_r64),                 intent(in)  :: ra, rb, rc
  real   (k_r64), dimension(3),   intent(in)  :: xn, yn, zn
  real   (k_r64), dimension(2,3), intent(out) :: xi
  logical(k_lg),                  intent(out) :: ierr

   interface
      pure function transform_coordinates(o1, x1, y1, z1, o2, x2, y2, z2, p1,& 
           td) result(p2)
         use tsl_basics_kinds, only: k_i8, k_r64, k_lg
         implicit none
         real(k_r64), dimension(3), intent(in) :: o1,x1, y1, z1, o2, x2, y2, &
                                                     z2, p1
         logical(k_lg),              intent(in), optional :: td
         real(k_r64), dimension(3)             :: p2
      end function transform_coordinates
   end interface

  ! -- Local declarations --

   real(k_r64), dimension(3) :: op, vp, pnew, vpnew, dirnew
   real(k_r64)               :: sa, sb, sc
   real(k_r64), dimension(2) :: roots
   real(k_r64), dimension(2,3):: pointnew

!-------------------------------------------------------------------------------
! End specification part
!-------------------------------------------------------------------------------

!-------------------------------------------------------------------------------
! Begin execution part
!-------------------------------------------------------------------------------

  op = (/0,0,0/)         !The origin point
  vp = x + u             !The other point on the line
  pnew = transform_coordinates(op, c_ux, c_uy, c_uz, opn, xn, yn, zn, x, .true.)
                     !The point on the line in the new coordinate
  vpnew = transform_coordinates(op, c_ux, c_uy, c_uz, opn, xn, yn, zn, vp, .true.)
                     !The other point on the line in the new coordinate
  dirnew = vpnew - pnew  !The direction vector in the new coordinate

  ! The parameter equation of the line x = x0 + v *t is used
  ! The variable roots is the parameter "t" in the line parameter equation
  ! The coefficients in the quadratic equation
  sa = dirnew(1)**2/ra**2 + dirnew(2)**2/rb**2 + dirnew(3)**2/rc**2
  sb = 2*pnew(1)*dirnew(1)/ra**2 + 2*pnew(2)*dirnew(2)/rb**2 + &
       2*pnew(3)*dirnew(3)/rc**2
  sc = pnew(1)**2/ra**2 + pnew(2)**2/rb**2 + pnew(3)**2/rc**2 - 1 
  ! Calculate the roots
  call solve_quadratic(sa, sb, sc, roots, ierr)
  if (flag .eqv. .true.) then  !The intersection point exists
     pointnew(1,:) = pnew + roots(1) * dirnew
     pointnew(2,:) = pnew + roots(2) * dirnew
    !Transform the intersecting points in the new coordinate to 
    !the original coordinate
    xi(1,:) = transform_coordinates(opn, xn, yn, zn, op, c_ux, c_uy, c_uz, &
         pointnew(1,:), .false.) 
    xi(2,:) = transform_coordinates(opn, xn, yn, zn, op, c_ux, c_uy, c_uz, &
         pointnew(2,:), .false.) 
  end if
  return


!-------------------------------------------------------------------------------
! End execution part
!-------------------------------------------------------------------------------

!-------------------------------------------------------------------------------
! Begin subprogram part
!-------------------------------------------------------------------------------

!-------------------------------------------------------------------------------
! End subprogram part
!-------------------------------------------------------------------------------

end subroutine int_line_ellipsoid
