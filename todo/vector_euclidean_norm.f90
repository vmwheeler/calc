pure function vector_euclidean_norm(vector) result(norm)

!*******************************************************************************
! Copyright (C) 2009 Wojciech Lipinski
!
! This file is part of the Thermal Science Library (TSL).
!
! TSL is free software: you can redistribute it and/or modify
! it under the terms of the GNU Lesser General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! TSL is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU Lesser General Public License for more details.
!
! You should have received a copy of the GNU Lesser General Public License
! along with TSL.  If not, see <http://www.gnu.org/licenses/>.
!*******************************************************************************

!*******************************************************************************
! Description:
!    Compute the the intuitive notion of length of the vector 
!    x = [x1, x2, ..., xn] in Euclidean space
! Input:
!    vector   A vector one-dimensional arrary
! Output:
!    norm     The length of the vector (Euclidean norm)
!
! References: 
!    [1] Wikipedi, http://en.wikipedia.org/wiki/Euclidean_norm#Euclidean_norm
!*******************************************************************************

!*******************************************************************************
! 'use' and 'implicit none' statements
!*******************************************************************************

   use tsl_basics_kinds, only: k_r64
   implicit none

!*******************************************************************************
! Dummy argument declaration constructs
!*******************************************************************************

   real(k_r64), dimension(:), intent(in) :: vector
   real(k_r64)                             :: norm

!*******************************************************************************
! Local declaration constructs
!*******************************************************************************

!*******************************************************************************
! Execution part
!*******************************************************************************

   norm = sqrt(sum(vector**2))
   return

!*******************************************************************************
! End function statement
!*******************************************************************************

end function vector_euclidean_norm
