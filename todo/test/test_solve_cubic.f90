program test_solve_cubic

!*******************************************************************************
! Copyright (C) 2009 Zheng Liang
! Copyright (C) 2009 Wojciech Lipinski
!
! This file is part of the Thermal Science Library (TSL).
!
! TSL is free software: you can redistribute it and/or modify
! it under the terms of the GNU Lesser General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! TSL is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU Lesser General Public License for more details.
!
! You should have received a copy of the GNU Lesser General Public License
! along with TSL.  If not, see <http://www.gnu.org/licenses/>.
!*******************************************************************************

!*******************************************************************************
! Description:
!    This is a driver for solve_cubic.
!*******************************************************************************

!*******************************************************************************
! 'use' and 'implicit none' statements
!*******************************************************************************

   use tsl_basics_kinds, only: k_r64, k_i32
   use tsl_basics_utils, only: tsl_preamble
   use tsl_math_polynomials, only: solve_cubic
   !use tsl, only: k_r64, k_lg, tsl_preamble, solve_cubic
   implicit none

!*******************************************************************************
! Declaration constructs
!*******************************************************************************

   real(k_r64)                   :: a, b, c 
   real(k_r64),   dimension(3) :: roots
   integer(k_i32)               :: flag


!*******************************************************************************
! Execution part
!*******************************************************************************

   call tsl_preamble('test_solve_cubic', 'Zheng Liang', '2009')
   !a=3, b=3, c=1, three same real roots
   flag = .false.
   a = 3.0
   b = 3.0
   c = 1.0
   print '()'
   print '(2x, ''Solving  x**3 + '', f5.2, ''*x**2 + '', f5.2, ''*x + '', &
          f5.2, '' = 0'')', a, b, c
   print '(2x, ''Analytical solution:'')'
   print '(2x, ''Q = 0, R = 0'')'
   print '(2x, ''x(1) = -1'')'
   print '(2x, ''x(2) = -1'')'
   print '(2x, ''x(3) = -1'')'
   print '(2x, ''Solution by solve_cubic:'')'
   call solve_cubic(a, b, c, roots, flag)
   if (flag == 3) then
      print '(2x, ''x(1) = '', f7.4)', roots(1)
      print '(2x, ''x(2) = '', f7.4)', roots(2)
      print '(2x, ''x(3) = '', f7.4)', roots(3)
   else
      print '(2x, ''x = '', f7.4)', roots(1)
   end if
   print '()'
   !a=6, b=11, c=6, three real roots
   flag = .false.
   a = 6.0
   b = 11.0
   c = 6.0
   print '()'
   print '(2x, ''Solving  x**3 + '', f5.2, ''*x**2 + '', f5.2, ''*x + '', &
          f5.2, '' = 0'')', a, b, c
   print '(2x, ''Analytical solution:'')'
   print '(2x, ''Q = 1/3, R = 0'')'
   print '(2x, ''x(1) = -1'')'
   print '(2x, ''x(2) = -2'')'
   print '(2x, ''x(3) = -3'')'
   print '(2x, ''Solution by solve_cubic:'')'
   call solve_cubic(a, b, c, roots, flag)
   if (flag == 3) then
      print '(2x, ''x(1) = '', f7.4)', roots(1)
      print '(2x, ''x(2) = '', f7.4)', roots(2)
      print '(2x, ''x(3) = '', f7.4)', roots(3)
   else
      print '(2x, ''x = '', f7.4)', roots(1)
   end if
   print '()'   

   !a=4, b=9, c=10, one real root
   flag = .false.
   a = 4.0
   b = 9.0
   c = 10.0
   print '()'
   print '(2x, ''Solving  x**3 + '', f5.2, ''*x**2 + '', f5.2, ''*x + '', &
          f5.2, '' = 0'')', a, b, c
   print '(2x, ''Analytical solution:'')'
   print '(2x, ''Q = -11/9, R = 37/27'')'
   print '(2x, ''x = -2'')'
   print '(2x, ''Solution by solve_cubic:'')'
   call solve_cubic(a, b, c, roots, flag)
   if (flag == 3) then
      print '(2x, ''x(1) = '', f7.4)', roots(1)
      print '(2x, ''x(2) = '', f7.4)', roots(2)
      print '(2x, ''x(3) = '', f7.4)', roots(3)
   else
      print '(2x, ''x = '', f7.4)', roots(1)
   end if
   print '()'  
   
   !a=3, b=0, c=0, three real roots
   flag = .false.
   a = 3.0
   b = 0
   c = 0
   print '()'
   print '(2x, ''Solving  x**3 + '', f5.2, ''*x**2 + '', f5.2, ''*x + '', &
          f5.2, '' = 0'')', a, b, c
   print '(2x, ''Analytical solution:'')'
   print '(2x, ''Q = -11/9, R = 37/27'')'
   print '(2x, ''x(1) = -3'')'
   print '(2x, ''x(2) = 0'')'
   print '(2x, ''x(3) = 0'')'
   print '(2x, ''Solution by solve_cubic:'')'
   call solve_cubic(a, b, c, roots, flag)
   if (flag == 3) then
      print '(2x, ''x(1) = '', f7.4)', roots(1)
      print '(2x, ''x(2) = '', f7.4)', roots(2)
      print '(2x, ''x(3) = '', f7.4)', roots(3)
   else
      print '(2x, ''x = '', f7.4)', roots(1)
   end if
   print '()'   
      
   print '(2x, ''Done.'')' 
   print '()'    
   
   
   read (*,*)

!*******************************************************************************
! Subprogram part
!*******************************************************************************

!*******************************************************************************
! 'end program' statement
!*******************************************************************************
end program test_solve_cubic