program test_transform_coordinates_any_any

!*******************************************************************************
! Copyright (C) 2009 Zheng Liang
! Copyright (C) 2009 Wojciech Lipinski
!
! This file is part of the Thermal Science Library (TSL).
!
! TSL is free software: you can redistribute it and/or modify
! it under the terms of the GNU Lesser General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! TSL is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU Lesser General Public License for more details.
!
! You should have received a copy of the GNU Lesser General Public License
! along with TSL.  If not, see <http://www.gnu.org/licenses/>.
!*******************************************************************************

!*******************************************************************************
! Description:
!    This is a driver for transform_coordinates_any_any.
!*******************************************************************************

!*******************************************************************************
! 'use' and 'implicit none' statements
!*******************************************************************************

   use tsl_basics_kinds, only: k_r64
   use tsl_basics_utils, only: tsl_preamble
   use tsl_math_analyticalc_geommetry, only: transform_coordinates_any
   !use tsl, only: k_r64, tsl_preamble, transform_coordinates_any
   implicit none

!*******************************************************************************
! Declaration constructs
!*******************************************************************************

   real(k_r64), dimension(3)     :: o1, x1, y1, z1, o2, x2, y2, z2, p1
   real(k_r64), dimension(3)     :: p2


!*******************************************************************************
! Execution part
!*******************************************************************************

   call tsl_preamble('test_transform_coordinates_any', 'Zheng Liang', '2009')
   !The original coordinate is standard Cartesian coordinate
   !Orignal point is (0,0,0)
   !Basis vectors are (1,0,0), (0,1,0), (0,0,1)
   o1 = (/0,0,0/)
   x1 = (/1,0,0/)
   y1 = (/0,1,0/)
   z1 = (/0,0,1/)
   print '()'
   print '(2x, ''The origin point of the original coordinate: ('',\)'
   print '(f7.2\ )', o1
   print '(2x, '')'' )'
   print '(2x, ''The basis vectors of the original coordinate:'')'
   print '(2x, ''('',\)'
   print '(f7.2\ )', x1
   print '(2x, '')'' )'
   print '(2x, ''('',\)'
   print '(f7.2\ )', y1
   print '(2x, '')'' )'
   print '(2x, ''('',\)'
   print '(f7.2\ )', z1
   print '(2x, '')'' )'
   !The new coordinate is Cartesian coordinate
   !Orignal point is (2,2,2)
   !Basis vectors are (1,0,0), (0,1,0), (0,0,1)
   o2 = (/2,2,2/)
   x2 = (/1,0,0/)
   y2 = (/0,1,0/)
   z2 = (/0,0,1/)
   print '()'
   print '(2x, ''The origin point of the new coordinate: ('',\)'
   print '(f7.2\ )', o2
   print '(2x, '')'' )'
   print '(2x, ''The basis vectors of the new coordinate:'')'
   print '(2x, ''('',\)'
   print '(f7.2\ )', x2
   print '(2x, '')'' )'
   print '(2x, ''('',\)'
   print '(f7.2\ )', y2
   print '(2x, '')'' )'
   print '(2x, ''('',\)'
   print '(f7.2\ )', z2
   print '(2x, '')'' )'
   !The point in the original coordinate is (1,1,1)
   p1=(/1,1,1/)
   print '(2x, ''The point in the original coordinate: ('',\)'
   print '(f7.2\ )', p1
   print '(2x, '')'' )'
   print '(2x, ''Analytical solution:'')'
   print '(2x, ''The point in the new coordinate: (-1, -1, -1) '')'
   print '(2x, ''Solution by transform_coordinates_any:'')'
   p2=transform_coordinates_any(o1, x1, y1, z1, o2, x2, y2, z2, p1)
   print '(2x, ''The point in the new coordinate: ('',\)'
   print '(f9.4\ )', p2
   print '(2x, '')'' )'
   print '()'
   !The new coordinate is Cartesian coordinate
   !Orignal point is (2,2,2)
   !Basis vectors are (0.6,0,0.8), (0,1,0), (0.8,0,-0.6)
   o2 = (/2,2,2/)
   x2 = (/0.6,0,0.8/)
   y2 = (/0,1,0/)
   z2 = (/0.8,0,-0.6/)
   print '()'
   print '(2x, ''The origin point of the new coordinate: ('',\)'
   print '(f7.2\ )', o2
   print '(2x, '')'' )'
   print '(2x, ''The basis vectors of the new coordinate:'')'
   print '(2x, ''('',\)'
   print '(f7.2\ )', x2
   print '(2x, '')'' )'
   print '(2x, ''('',\)'
   print '(f7.2\ )', y2
   print '(2x, '')'' )'
   print '(2x, ''('',\)'
   print '(f7.2\ )', z2
   print '(2x, '')'' )'
   !The point in the original coordinate is (1,1,1)
   p1=(/1,1,1/)
   print '(2x, ''The point in the original coordinate: ('',\)'
   print '(f7.2\ )', p1
   print '(2x, '')'' )'
   print '(2x, ''Analytical solution:'')'
   print '(2x, ''The point in the new coordinate: (-1.4, -1, -0.2) '')'
   print '(2x, ''Solution by transform_coordinates_any:'')'
   p2=transform_coordinates_any(o1, x1, y1, z1, o2, x2, y2, z2, p1)
   print '(2x, ''The point in the new coordinate: ('',\)'
   print '(f9.4\ )', p2
   print '(2x, '')'' )'
   print '()'
   !The new coordinate is Cartesian coordinate
   !Orignal point is (2,2,2)
   !Basis vectors are (1,1,1), (0.71,-0.71,0), (0.41,0.41,-0.82)
   o2 = (/2,2,2/)
   x2 = (/1,1,1/)
   y2 = (/0.71,-0.71,0/)
   z2 = (/0.41,0.41,-0.82/)
   print '()'
   print '(2x, ''The origin point of the new coordinate: ('',\)'
   print '(f7.2\ )', o2
   print '(2x, '')'' )'
   print '(2x, ''The basis vectors of the new coordinate:'')'
   print '(2x, ''('',\)'
   print '(f7.2\ )', x2
   print '(2x, '')'' )'
   print '(2x, ''('',\)'
   print '(f7.2\ )', y2
   print '(2x, '')'' )'
   print '(2x, ''('',\)'
   print '(f7.2\ )', z2
   print '(2x, '')'' )'
   !The point in the original coordinate is (1,1,1)
   p1=(/1,1,1/)
   print '(2x, ''The point in the original coordinate: ('',\)'
   print '(f7.2\ )', p1
   print '(2x, '')'' )'
   print '(2x, ''Analytical solution:'')'
   print '(2x, ''The point in the new coordinate: (-1.73, 0, 0) '')'
   print '(2x, ''Solution by transform_coordinates_any:'')'
   p2=transform_coordinates_any(o1, x1, y1, z1, o2, x2, y2, z2, p1)
   print '(2x, ''The point in the new coordinate: ('',\)'
   print '(f9.4\ )', p2
   print '(2x, '')'' )'
   !The new coordinate is Cartesian coordinate
   !Orignal point is (2,2,0)
   !Basis vectors are (0,-1,0), (-1,0,0), (0,0,1)
   o2 = (/2,2,0/)
   x2 = (/0,-1,0/)
   y2 = (/-1,0,0/)
   z2 = (/0,0,1/)
   print '()'
   print '(2x, ''The origin point of the new coordinate: ('',\)'
   print '(f7.2\ )', o2
   print '(2x, '')'' )'
   print '(2x, ''The basis vectors of the new coordinate:'')'
   print '(2x, ''('',\)'
   print '(f7.2\ )', x2
   print '(2x, '')'' )'
   print '(2x, ''('',\)'
   print '(f7.2\ )', y2
   print '(2x, '')'' )'
   print '(2x, ''('',\)'
   print '(f7.2\ )', z2
   print '(2x, '')'' )'
   !The point in the original coordinate is (1,1,1)
   p1=(/1,1,0/)
   print '(2x, ''The point in the original coordinate: ('',\)'
   print '(f7.2\ )', p1
   print '(2x, '')'' )'
   print '(2x, ''Analytical solution:'')'
   print '(2x, ''The point in the new coordinate: (1, 1, 0) '')'
   print '(2x, ''Solution by transform_coordinates_any:'')'
   p2=transform_coordinates_any(o1, x1, y1, z1, o2, x2, y2, z2, p1)
   print '(2x, ''The point in the new coordinate: ('',\)'
   print '(f9.4\ )', p2
   print '(2x, '')'' )'
   print '()'

   !Orignal point is (2,0,0)
   !Basis vectors are (-1,0,0), (0,1,0), (0,0,1)
   o1 = (/2,0,0/)
   x1 = (/-1,0,0/)
   y1 = (/0,1,0/)
   z1 = (/0,0,1/)
   print '()'
   print '(2x, ''The origin point of the original coordinate: ('',\)'
   print '(f7.2\ )', o1
   print '(2x, '')'' )'
   print '(2x, ''The basis vectors of the original coordinate:'')'
   print '(2x, ''('',\)'
   print '(f7.2\ )', x1
   print '(2x, '')'' )'
   print '(2x, ''('',\)'
   print '(f7.2\ )', y1
   print '(2x, '')'' )'
   print '(2x, ''('',\)'
   print '(f7.2\ )', z1
   print '(2x, '')'' )'
   !The new coordinate is Cartesian coordinate
   !Orignal point is (0,0,0)
   !Basis vectors are (1,0,0), (0,1,0), (0,0,1)
   o2 = (/0,0,0/)
   x2 = (/1,0,0/)
   y2 = (/0,1,0/)
   z2 = (/0,0,1/)
   print '()'
   print '(2x, ''The origin point of the new coordinate: ('',\)'
   print '(f7.2\ )', o2
   print '(2x, '')'' )'
   print '(2x, ''The basis vectors of the new coordinate:'')'
   print '(2x, ''('',\)'
   print '(f7.2\ )', x2
   print '(2x, '')'' )'
   print '(2x, ''('',\)'
   print '(f7.2\ )', y2
   print '(2x, '')'' )'
   print '(2x, ''('',\)'
   print '(f7.2\ )', z2
   print '(2x, '')'' )'
   !The point in the original coordinate is (1,1,1)
   p1=(/1,1,0/)
   print '(2x, ''The point in the original coordinate: ('',\)'
   print '(f7.2\ )', p1
   print '(2x, '')'' )'
   print '(2x, ''Analytical solution:'')'
   print '(2x, ''The point in the new coordinate: (1, 1, 0) '')'
   print '(2x, ''Solution by transform_coordinates_any:'')'
   p2=transform_coordinates_any(o1, x1, y1, z1, o2, x2, y2, z2, p1)
   print '(2x, ''The point in the new coordinate: ('',\)'
   print '(f9.4\ )', p2
   print '(2x, '')'' )'
   print '()'
   
   print '(2x, ''Done.'')'
   print '()'  
   
   read (*,*)

!*******************************************************************************
! Subprogram part
!*******************************************************************************

!*******************************************************************************
! 'end program' statement
!*******************************************************************************
end program test_transform_coordinates_any_any