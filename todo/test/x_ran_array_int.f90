program x_ran_array_int

!-------------------------------------------------------------------------------
! Copyright 2009 Regents of the University of Minnesota. All rights reserved.
!
! This file is part of the Fortran Scientific Library (FSL).
!
! FSL is free software: you can redistribute it and/or modify
! it under the terms of the GNU Lesser General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! FSL is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU Lesser General Public License for more details.
!
! You should have received a copy of the GNU Lesser General Public License
! along with FSL.  If not, see <http://www.gnu.org/licenses/>.
!-------------------------------------------------------------------------------

!-------------------------------------------------------------------------------
! Description:
!    This is a driver for the ran_array_int random number generator.
!-------------------------------------------------------------------------------

!-------------------------------------------------------------------------------
! 'use' and 'implicit none' statements
!-------------------------------------------------------------------------------

   use fsl, only: k_i4
   use fsl, only: fsl_driver_head, fsl_driver_foot
   use fsl, only: ran_start_int, ran_array_int
   implicit none

!-------------------------------------------------------------------------------
! Declaration constructs
!-------------------------------------------------------------------------------

   integer(k_i4), allocatable, dimension(:) :: a
   integer(k_i4) :: i

!-------------------------------------------------------------------------------
! Execution part
!-------------------------------------------------------------------------------

   allocate(a(2009))
   call fsl_driver_head('ran_array_int')
   call ran_start_int(310952)
   do i = 1, 2010
      call ran_array_int(a, 1009)
   end do 
   print'(2x, ''First test:'')'
   print'(2x, ''The number should be 995235265:'')'
   print'(17x, i15)', a(1)
   print'()'
   call ran_start_int(310952)
   do i = 1, 1010
      call ran_array_int(a, 2009)
   end do
   print'(2x, ''Second test:'')'
   print'(2x, ''The number should be 995235265:'')'
   print'(17x, i15)', a(1)
   call fsl_driver_foot()
   deallocate(a)
   stop

!-------------------------------------------------------------------------------
! 'end program' statement
!-------------------------------------------------------------------------------

end program x_ran_array_int
