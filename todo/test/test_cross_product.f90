program test_cross_product

!*******************************************************************************
! Copyright (C) 2009 Zheng Liang
! Copyright (C) 2009 Wojciech Lipinski
!
! This file is part of the Thermal Science Library (TSL).
!
! TSL is free software: you can redistribute it and/or modify
! it under the terms of the GNU Lesser General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! TSL is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU Lesser General Public License for more details.
!
! You should have received a copy of the GNU Lesser General Public License
! along with TSL.  If not, see <http://www.gnu.org/licenses/>.
!*******************************************************************************

!*******************************************************************************
! Description:
!    This is a driver for cross_product.
!*******************************************************************************

!*******************************************************************************
! 'use' and 'implicit none' statements
!*******************************************************************************

   use tsl_basics_kinds, only: k_r64
   use tsl_basics_utils, only: tsl_preamble
   use tsl_math_vector_algebra, only: cross_product
   !use tsl, only: k_r64, tsl_preamble, cross_product
   implicit none

!*******************************************************************************
! Declaration constructs
!*******************************************************************************

   real(k_r64),  dimension(3) :: a,b,c 

!*******************************************************************************
! Execution part
!*******************************************************************************

   call tsl_preamble('test_cross_product', 'Zheng Liang', '2009')
   ! a=(1,1,1), b=(2,2,2)
   a=(/1.0,1.0,1.0/)
   b=(/2.0,2.0,2.0/)
   print '()'
   print '(2x, ''The vector a: ('',\)'
   print '(f7.2\ )', a
   print '(2x, '')'' )'
   print '(2x, ''The vector b: ('',\)'
   print '(f7.2\ )', b
   print '(2x, '')'' )'
   print '(2x, ''Analytical solution:'')'
   print '(2x, ''Cross product = (0, 0, 0)'')'
   print '(2x, ''Solution by cross_product:'')'
   c=cross_product(a, b)
   print '(2x, ''Cross product: ('',\)'
   print '(f7.2\ )', c
   print '(2x, '')'' )'   
   print '()'
   ! a=(1.2,5.5,15.4), b=(2.5,2.8,6.4)
   a=(/1.2,5.5,15.4/)
   b=(/2.5,2.8,6.4/)
   print '()'
   print '(2x, ''The vector a: ('',\)'
   print '(f7.2\ )', a
   print '(2x, '')'' )'
   print '(2x, ''The vector b: ('',\)'
   print '(f7.2\ )', b
   print '(2x, '')'' )'
   print '(2x, ''Analytical solution:'')'
   print '(2x, ''Cross product = (-7.92, 30.82, -10.39)'')'
   print '(2x, ''Solution by cross_product:'')'
   c=cross_product(a, b)
   print '(2x, ''Cross product: ('',\)'
   print '(f7.2\ )', c
   print '(2x, '')'' )'   
   print '(2x, ''Done.'')'
   print '()'



  
   
   read (*,*)

!*******************************************************************************
! Subprogram part
!*******************************************************************************

!*******************************************************************************
! 'end program' statement
!*******************************************************************************
end program test_cross_product