program test_search_vector

!*******************************************************************************
! Copyright (C) 2009 Wojciech Lipinski
! Copyright (C) 2009 Zheng Liang
!
! This file is part of the Thermal Science Library (TSL).
!
! TSL is free software: you can redistribute it and/or modify
! it under the terms of the GNU Lesser General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! TSL is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU Lesser General Public License for more details.
!
! You should have received a copy of the GNU Lesser General Public License
! along with TSL.  If not, see <http://www.gnu.org/licenses/>.
!*******************************************************************************

!*******************************************************************************
! Description:
!    This is a driver for ssearch_vector.
!*******************************************************************************

!*******************************************************************************
! 'use' and 'implicit none' statements
!*******************************************************************************

   use tsl_basics_kinds, only: k_i32, k_r64, k_lg
   use tsl_basics_utils, only: tsl_preamble
   use tsl_math_searching, only: search_vector
   !use tsl, only: k_i32, k_r64, k_lg, tsl_preamble, search_vector
   implicit none

!*******************************************************************************
! Declaration constructs
!*******************************************************************************

   real(k_r64), allocatable, dimension(:) :: xv
   real(k_r64)                               :: x
   integer(k_i32)                           :: ipos
   integer                                  :: status


!*******************************************************************************
! Execution part
!*******************************************************************************

   call tsl_preamble('test_search_vector', 'Zheng Liang', '2009')
   !xv= (1,2,3,4,5,6,7,8,9,10), x=5.5
   allocate (xv(10), stat=status)
   xv=(/1,2,3,4,5,6,7,8,9,10/)
   x=5.5
   print '()'
   print '(2x, ''The vector: ('',\)'
   print '(f7.2\ )', xv
   print '(2x, '')'' )'
   print '(2x, ''x is '', f7.2)', x
   print '(2x, ''Analytical solution:'')'
   print '(2x, ''Position is  5'')'
   print '(2x, ''Solution by search_vector:'')'
   ipos = search_vector(xv, x) 
   if (ipos >0 .and. ipos < SIZE(xv)) then
      print '(2x, ''Position is '', 2i2)', ipos
   else
      print '(2x, ''No found and x is out of range.'')'
   end if
   print '()'
   deallocate (xv,stat=status)
   !xv= (1,2,3,4,5,6,7,8,9,10), x=12
   allocate (xv(10), stat=status)
   xv=(/1,2,3,4,5,6,7,8,9,10/)
   x=12.0
   print '()'
   print '(2x, ''The vector: ('',\)'
   print '(f7.2\ )', xv
   print '(2x, '')'' )'
   print '(2x, ''x is '', f7.2)', x
   print '(2x, ''Analytical solution:'')'
   print '(2x, ''x is out of range'')'
   print '(2x, ''Solution by search_vector:'')'
   ipos = search_vector(xv, x) 
   if (ipos >0 .and. ipos < SIZE(xv)) then
      print '(2x, ''Position is '', 2i2)', ipos
   else
      print '(2x, ''No found and x is out of range.'')'
   end if
   print '(2x, ''Done.'')'
   print '()'
   deallocate (xv,stat=status)
 
   
   read (*,*)

!*******************************************************************************
! Subprogram part
!*******************************************************************************

!*******************************************************************************
! 'end program' statement
!*******************************************************************************
end program test_search_vector 