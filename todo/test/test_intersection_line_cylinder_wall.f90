program test_intersection_line_cylinder_wall

!*******************************************************************************
! Copyright (C) 2009 Zheng Liang
! Copyright (C) 2009 Wojciech Lipinski
!
! This file is part of the Thermal Science Library (TSL).
!
! TSL is free software: you can redistribute it and/or modify
! it under the terms of the GNU Lesser General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! TSL is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU Lesser General Public License for more details.
!
! You should have received a copy of the GNU Lesser General Public License
! along with TSL.  If not, see <http://www.gnu.org/licenses/>.
!*******************************************************************************

!*******************************************************************************
! Description:
!    This is a driver for intersection_line_cylinder_wall.
!*******************************************************************************

!*******************************************************************************
! 'use' and 'implicit none' statements
!*******************************************************************************

   use tsl_basics_kinds, only: k_r64, k_lg
   use tsl_basics_utils, only: tsl_preamble
   use tsl_math_analyticalc_geommetry, only: intersection_line_cylinder_wall
   !use tsl, only: k_i16, k_r64, tsl_preamble, intersection_line_cylinder_wall
   implicit none

!*******************************************************************************
! Declaration constructs
!*******************************************************************************

   real(k_r64),    dimension(3)   :: point, dir, cp, cdir
   real(k_r64)                      :: r
   real(k_r64),    dimension(2,3)  :: points
   logical(k_lg)                    :: flag


!*******************************************************************************
! Execution part
!*******************************************************************************

   call tsl_preamble('test_intersection_line_cylinder_wall', 'Zheng Liang', '2009')
   !No tangent case will be tested in the following codes
   !Center of cylinder is (0,0,0)
   !Cylinder axis direction vector is (0,0,1)
   !The radius is 5
   cdir = (/0,0,1/)
   cp = (/0,0,0/)
   r = 5
   print '()'
   print '(2x, ''The center of cylinder: ('',\)'
   print '(f7.2\ )', cp
   print '(2x, '')'' )'
   print '(2x, ''The cylinder axis direction vector: ('',\)'
   print '(f7.2\ )', cdir
   print '(2x, '')'' )'
   print '(2x, ''The radius of cylinder: '', f7.2)',r
   print '()'
   !The point on the line is (1,1,-1), and direction vector is (0,0,1)
   !The intersecting points are on the top and bottom surface
   point = (/1,1,-1/)
   dir = (/0,0,1/)
   print '(2x, ''The point on the line: ('',\)'
   print '(f7.2\ )', point
   print '(2x, '')'' )'
   print '(2x, ''The direction vector: ('',\)'
   print '(f7.2\ )', dir
   print '(2x, '')'' )'
   print '(2x, ''Analytical solution:'')'
   print '(2x, ''No intersection'')'
   print '(2x, ''Solution by intersection_line_cylinder_wall:'')'
   print '(2x, ''The intersecting points are on'')'
   call  intersection_line_cylinder_wall(point, dir, cp, cdir, r, points, flag)
   if(flag ==.false.) then
      print '(2x, ''No intersection'')'
   else
      print '(2x, ''The intersecting points: '')'
      print '(2x, ''('', \)'
      print '(f9.4\ )', points(1,:)
      print '(2x, '')'' )'
      print '(2x, ''('', \)'
      print '(f9.4\ )', points(2,:)
      print '(2x, '')'' )'
   end if
   print '()'
   !The point on the line is (1,1,-1), and direction vector is (1,1,1)
   !The intersecting points are on the top and bottom surface
   point = (/1,1,-1/)
   dir = (/1,1,1/)
   print '(2x, ''The point on the line: ('',\)'
   print '(f7.2\ )', point
   print '(2x, '')'' )'
   print '(2x, ''The direction vector: ('',\)'
   print '(f7.2\ )', dir
   print '(2x, '')'' )'
   print '(2x, ''Analytical solution:'')'
   print '(2x, ''The intersecting points: (-3.54,-3.54,-5.54)  (3.54,3.54,1.54)'')'
   print '(2x, ''Solution by intersection_line_cylinder_wall:'')'
   print '(2x, ''The intersecting points are on'')'
   call  intersection_line_cylinder_wall(point, dir, cp, cdir, r, points, flag)
   if(flag ==.false.) then
      print '(2x, ''No intersection'')'
   else
      print '(2x, ''The intersecting points: '')'
      print '(2x, ''('', \)'
      print '(f9.4\ )', points(1,:)
      print '(2x, '')'' )'
      print '(2x, ''('', \)'
      print '(f9.4\ )', points(2,:)
      print '(2x, '')'' )'
   end if
   print '()'
   !Center of cylinder is (0,0,0)
   !Cylinder axis direction vector is (1,0,0)
   !The radius is 5
   cdir = (/1,0,0/)
   cp = (/0,0,0/)
   r = 5
   print '()'
   print '(2x, ''The center of cylinder: ('',\)'
   print '(f7.2\ )', cp
   print '(2x, '')'' )'
   print '(2x, ''The cylinder axis direction vector: ('',\)'
   print '(f7.2\ )', cdir
   print '(2x, '')'' )'
   print '(2x, ''The radius of cylinder: '', f7.2)',r
   print '()'
   !The point on the line is (1,1,-1), and direction vector is (0,0,1)
   !The intersecting points are on the top and bottom surface
   point = (/1,1,-1/)
   dir = (/0,0,1/)
   print '(2x, ''The point on the line: ('',\)'
   print '(f7.2\ )', point
   print '(2x, '')'' )'
   print '(2x, ''The direction vector: ('',\)'
   print '(f7.2\ )', dir
   print '(2x, '')'' )'
   print '(2x, ''Analytical solution:'')'
   print '(2x, ''The intersecting points: (1,1,-4.9)  (1,1,4.9)'')'
   print '(2x, ''Solution by intersection_line_cylinder_wall:'')'
   print '(2x, ''The intersecting points are on'')'
   call  intersection_line_cylinder_wall(point, dir, cp, cdir, r, points, flag)
   if(flag ==.false.) then
      print '(2x, ''No intersection'')'
   else
      print '(2x, ''The intersecting points: '')'
      print '(2x, ''('', \)'
      print '(f9.4\ )', points(1,:)
      print '(2x, '')'' )'
      print '(2x, ''('', \)'
      print '(f9.4\ )', points(2,:)
      print '(2x, '')'' )'
   end if
   print '()'
      
   print '(2x, ''Done.'')'
              
   read (*,*)

!*******************************************************************************
! Subprogram part
!*******************************************************************************

!*******************************************************************************
! 'end program' statement
!*******************************************************************************
end program test_intersection_line_cylinder_wall