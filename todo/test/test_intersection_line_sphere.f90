program test_intersection_line_sphere

!*******************************************************************************
! Copyright (C) 2009 Zheng Liang
! Copyright (C) 2009 Wojciech Lipinski
!
! This file is part of the Thermal Science Library (TSL).
!
! TSL is free software: you can redistribute it and/or modify
! it under the terms of the GNU Lesser General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! TSL is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU Lesser General Public License for more details.
!
! You should have received a copy of the GNU Lesser General Public License
! along with TSL.  If not, see <http://www.gnu.org/licenses/>.
!*******************************************************************************

!*******************************************************************************
! Description:
!    This is a driver for intersection_line_sphere.
!*******************************************************************************

!*******************************************************************************
! 'use' and 'implicit none' statements
!*******************************************************************************

   use tsl_basics_kinds, only: k_r64, k_lg
   use tsl_basics_utils, only: tsl_preamble
   use tsl_math_analyticalc_geommetry, only: intersection_line_sphere
   !use tsl, only: k_r64, k_lg, tsl_preamble, intersection_line_sphere
   implicit none

!*******************************************************************************
! Declaration constructs
!*******************************************************************************

   real(k_r64), dimension(3)            :: x, dir, point
   real(k_r64)                            :: r
   real(k_r64), dimension(2,3)           :: xi
   logical(k_lg)                          :: flag

!*******************************************************************************
! Execution part
!*******************************************************************************

   call tsl_preamble('test_intersection_line_sphere', 'Zheng Liang', '2009')
   !No tangent cases will be tested in this program
   !The center of the sphere is (0,0,0)
   !The radius of the sphere is 5
   point = (/0,0,0/)
   r=5
   print '()'
   print '(2x, ''The center of the sphere: ('',\)'
   print '(f7.2\ )', x
   print '(2x, '')'' )'
   print '(2x, ''The radius of the sphere: '',f7.2)',r
   print '()'
   !The point on the line is (1,1,-6), and direction vector is (0,0,1)
   x = (/1,1,-6/)
   dir = (/0,0,1/)
   print '(2x, ''The point on the line: ('',\)'
   print '(f7.2\ )', x
   print '(2x, '')'' )'
   print '(2x, ''The direction vector: ('',\)'
   print '(f7.2\ )', dir
   print '(2x, '')'' )'
   print '(2x, ''Analytical solution:'')'
   print '(2x, ''The intersecting points: (1,1,-4.8)  (1,1,4.8)'')'
   print '(2x, ''Solution by intersection_line_sphere:'')'
   call intersection_line_sphere(x, dir, point, r, xi, flag)
   if(flag ==.false.) then
      print '(2x, ''No intersection'')'
   else
      print '(2x, ''The intersecting points: '')'
      print '(2x, ''('', \)'
      print '(f9.4\ )', xi(1,:)
      print '(2x, '')'' )'
      print '(2x, ''('', \)'
      print '(f9.4\ )', xi(2,:)
      print '(2x, '')'' )'
   end if
   print '()'
   !The point on the line is (1,1,-6), and direction vector is (1,1,1)
   x = (/1,1,-6/)
   dir = (/1,1,1/)
   print '(2x, ''The point on the line: ('',\)'
   print '(f7.2\ )', x
   print '(2x, '')'' )'
   print '(2x, ''The direction vector: ('',\)'
   print '(f7.2\ )', dir
   print '(2x, '')'' )'
   print '(2x, ''Analytical solution:'')'
   print '(2x, ''No intersection'')'
   print '(2x, ''Solution by intersection_line_sphere:'')'
   call intersection_line_sphere(x, dir, point, r, xi, flag)
   if(flag ==.false.) then
      print '(2x, ''No intersection'')'
   else
      print '(2x, ''The intersecting points: '')'
      print '(2x, ''('', \)'
      print '(f9.4\ )', xi(1,:)
      print '(2x, '')'' )'
      print '(2x, ''('', \)'
      print '(f9.4\ )', xi(2,:)
      print '(2x, '')'' )'
   end if
   print '()'
   !The point on the line is (1,1,-6), and direction vector is (1,4,5)
   x = (/1,1,-6/)
   dir = (/1,4,5/)
   print '(2x, ''The point on the line: ('',\)'
   print '(f7.2\ )', x
   print '(2x, '')'' )'
   print '(2x, ''The direction vector: ('',\)'
   print '(f7.2\ )', dir
   print '(2x, '')'' )'
   print '(2x, ''Analytical solution:'')'
   print '(2x, ''The intersecting points: (1.38,2.53,-4.08)  (1.81,4.23,-1.97)'')'
   print '(2x, ''Solution by intersection_line_sphere:'')'
   call intersection_line_sphere(x, dir, point, r, xi, flag)
   if(flag ==.false.) then
      print '(2x, ''No intersection'')'
   else
      print '(2x, ''The intersecting points: '')'
      print '(2x, ''('', \)'
      print '(f9.4\ )', xi(1,:)
      print '(2x, '')'' )'
      print '(2x, ''('', \)'
      print '(f9.4\ )', xi(2,:)
      print '(2x, '')'' )'
   end if
   print '(2x, ''Done.'')'
                     
   read (*,*)

!*******************************************************************************
! Subprogram part
!*******************************************************************************

!*******************************************************************************
! 'end program' statement
!*******************************************************************************
end program test_intersection_line_sphere