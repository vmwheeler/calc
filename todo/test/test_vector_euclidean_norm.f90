program test_vector_euclidean_norm

!*******************************************************************************
! Copyright (C) 2009 Zheng Liang
! Copyright (C) 2009 Wojciech Lipinski
!
! This file is part of the Thermal Science Library (TSL).
!
! TSL is free software: you can redistribute it and/or modify
! it under the terms of the GNU Lesser General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! TSL is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU Lesser General Public License for more details.
!
! You should have received a copy of the GNU Lesser General Public License
! along with TSL.  If not, see <http://www.gnu.org/licenses/>.
!*******************************************************************************

!*******************************************************************************
! Description:
!    This is a driver for vector_euclidean_norm.
!*******************************************************************************

!*******************************************************************************
! 'use' and 'implicit none' statements
!*******************************************************************************

   use tsl_basics_kinds, only: k_i64, k_r64
   use tsl_basics_utils, only: tsl_preamble
   use tsl_math_vector_algebra, only: vector_euclidean_norm
   !use tsl, only: k_i64, k_r64, tsl_preamble, vector_euclidean_norm
   implicit none

!*******************************************************************************
! Declaration constructs
!*******************************************************************************

   real(k_r64), allocatable, dimension(:) :: vector 
   real(k_r64)                               :: norm 
   integer(k_i64)                           :: dime
   integer(k_i64)                           :: status


!*******************************************************************************
! Execution part
!*******************************************************************************

   call tsl_preamble('test_vector_euclidean_norm', 'Zheng Liang', '2009')
   !dime = 2, vector = (4,1)
   dime=2
   allocate (vector(dime), stat=status)
   vector(1)=4
   vector(2)=1
   print '()'
   print '(2x, ''The vector: ('',\)'
   print '(f7.2\ )', vector
   print '(2x, '')'' )'
   print '(2x, ''Analytical solution:'')'
   print '(2x, ''Vector length = sqrt(17)'')'
   print '(2x, ''Solution by vector_euclidean_norm:'')'
   norm=vector_euclidean_norm(vector)
   print '(2x, ''Vector length = '',f7.4)',norm
   print '()'
   deallocate (vector,stat=status)
   !dime = 3, vector = (3,4,5)
   dime=3
   allocate (vector(dime), stat=status)
   vector(1)=3
   vector(2)=4
   vector(3)=5
   print '()'
   print '(2x, ''The vector: ('',\)'
   print '(f7.2\ )', vector
   print '(2x, '')'' )'
   print '(2x, ''Analytical solution:'')'
   print '(2x, ''Vector length = 5*sqrt(2)'')'
   print '(2x, ''Solution by vector_euclidean_norm:'')'
   norm=vector_euclidean_norm(vector)
   print '(2x, ''Vector length = '',f7.4)',norm
   print '(2x, ''Done.'')'
   print '()'
   deallocate (vector,stat=status)

  
   
   read (*,*)

!*******************************************************************************
! Subprogram part
!*******************************************************************************

!*******************************************************************************
! 'end program' statement
!*******************************************************************************
end program test_vector_euclidean_norm