program test_solve_quadratic

!*******************************************************************************
! Copyright (C) 2009 Wojciech Lipinski
! Copyright (C) 2009 Zheng Liang
!
! This file is part of the Thermal Science Library (TSL).
!
! TSL is free software: you can redistribute it and/or modify
! it under the terms of the GNU Lesser General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! TSL is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU Lesser General Public License for more details.
!
! You should have received a copy of the GNU Lesser General Public License
! along with TSL.  If not, see <http://www.gnu.org/licenses/>.
!*******************************************************************************

!*******************************************************************************
! Description:
!    This is a driver for solve_quadratic.
!*******************************************************************************

!*******************************************************************************
! 'use' and 'implicit none' statements
!*******************************************************************************

   use tsl_basics_kinds, only: k_r64, k_lg
   use tsl_basics_utils, only: tsl_preamble
   use tsl_math_polynomials, only: solve_quadratic
   !use tsl, only: k_r64, k_lg, tsl_preamble, solve_quadratic
   implicit none

!*******************************************************************************
! Declaration constructs
!*******************************************************************************

   real(k_r64)                   :: a, b, c 
   real(k_r64),   dimension(2) :: roots
   logical(k_lg)                 :: flag


!*******************************************************************************
! Execution part
!*******************************************************************************

   call tsl_preamble('test_solve_quadratic', 'Wojciech Lipinski &
                      and Zheng Liang', '2009')
   !a=1, b=4, c=2, two different real roots
   flag = .false.
   a = 1.0
   b = 4.0
   c = 2.0
   print '()'
   print '(2x, ''Solving  '', f4.2, ''*x**2 + '', f4.2, ''*x + '', &
          f4.2, '' = 0'')', a, b, c
   print '(2x, ''Analytical solution:'')'
   print '(2x, ''Delta = 2*sqrt(2)'')'
   print '(2x, ''x(1) = -3.4142'')'
   print '(2x, ''x(2) = -0.5858'')'
   print '(2x, ''Solution by solve_quadratic:'')'
   call solve_quadratic(a, b, c, roots, flag)
   if (flag == .true.) then
      print '(2x, ''x(1) = '', f7.4)', roots(1)
      print '(2x, ''x(2) = '', f7.4)', roots(2)
   else
      print '(2x, ''No real roots found.'')'
   end if
   print '()'
   !a=1, b=4, c=0, two different real roots
   flag = .false.
   a = 1.0
   b = 4.0
   c = 0.0
   print '()'
   print '(2x, ''Solving  '', f4.2, ''*x**2 + '', f4.2, ''*x + '', &
          f4.2, '' = 0'')', a, b, c
   print '(2x, ''Analytical solution:'')'
   print '(2x, ''Delta = 4'')'
   print '(2x, ''x(1) = -4.0000'')'
   print '(2x, ''x(2) = 0.0000'')'
   print '(2x, ''Solution by solve_quadratic:'')'
   call solve_quadratic(a, b, c, roots, flag)
   if (flag == .true.) then
      print '(2x, ''x(1) = '', f7.4)', roots(1)
      print '(2x, ''x(2) = '', f7.4)', roots(2)
   else
      print '(2x, ''No real roots found.'')'
   end if
   print '()'   
   !a=1, b=4, c=4, two same real roots
   flag = .false.
   a = 1.0
   b = 4.0
   c = 4.0
   print '()'
   print '(2x, ''Solving  '', f4.2, ''*x**2 + '', f4.2, ''*x + '', &
          f4.2, '' = 0'')', a, b, c
   print '(2x, ''Analytical solution:'')'
   print '(2x, ''Delta = 0'')'
   print '(2x, ''x(1) = -2.0000'')'
   print '(2x, ''x(2) = -2.0000'')'
   print '(2x, ''Solution by solve_quadratic:'')'
   call solve_quadratic(a, b, c, roots, flag)
   if (flag == .true.) then
      print '(2x, ''x(1) = '', f7.4)', roots(1)
      print '(2x, ''x(2) = '', f7.4)', roots(2)
   else
      print '(2x, ''No real roots found.'')'
   end if
   print '()'    
   !a=1, b=4, c=5, no real root
   flag = .false.
   a = 1.0
   b = 4.0
   c = 5.0
   print '()'
   print '(2x, ''Solving  '', f4.2, ''*x**2 + '', f4.2, ''*x + '', &
          f4.2, '' = 0'')', a, b, c
   print '(2x, ''Analytical solution:'')'
   print '(2x, ''Delta = -4'')'
   print '(2x, ''No real root'')'
   print '(2x, ''Solution by solve_quadratic:'')'
   call solve_quadratic(a, b, c, roots, flag)
   if (flag == .true.) then
      print '(2x, ''x(1) = '', f7.4)', roots(1)
      print '(2x, ''x(2) = '', f7.4)', roots(2)
   else
      print '(2x, ''No real roots found.'')'
   end if
   print '(2x, ''Done.'')'
   print '()'    
   
   
   read (*,*)

!*******************************************************************************
! Subprogram part
!*******************************************************************************

!*******************************************************************************
! 'end program' statement
!*******************************************************************************
end program test_solve_quadratic 