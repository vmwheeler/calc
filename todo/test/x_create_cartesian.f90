program x_create_cartesian

!-------------------------------------------------------------------------------
! Copyright 2009 Regents of the University of Minnesota. All rights reserved.
!
! This file is part of the Fortran Scientific Library (FSL).
!
! FSL is free software: you can redistribute it and/or modify
! it under the terms of the GNU Lesser General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! FSL is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU Lesser General Public License for more details.
!
! You should have received a copy of the GNU Lesser General Public License
! along with FSL.  If not, see <http://www.gnu.org/licenses/>.
!-------------------------------------------------------------------------------

!-------------------------------------------------------------------------------
! Description:
!    This is a driver for the subroutine create_cartesian.
!-------------------------------------------------------------------------------

!-------------------------------------------------------------------------------
! 'use' and 'implicit none' statements
!-------------------------------------------------------------------------------

   use fsl, only: k_i1, k_r8
   use fsl, only: c_small
   use fsl, only: fsl_driver_head, fsl_driver_foot
   use fsl, only: create_cartesian, vector_length
   implicit none

!-------------------------------------------------------------------------------
! Declaration constructs
!-------------------------------------------------------------------------------

   real(k_r8), dimension(3) :: a
   integer(k_i1)            :: iaxis
   real(k_r8), dimension(3) :: ex, ey, ez
   integer(k_i1)            :: ierr

!-------------------------------------------------------------------------------
! Execution part
!-------------------------------------------------------------------------------

   call fsl_driver_head('create_cartesian')
   print'(2x, ''Give the vector'')'
   read(*,*) a
   print'(2x, ''Give the index of the axis:'')'
   read(*,*) iaxis
   print'()'
   call create_cartesian(a, iaxis, ex, ey, ez, ierr)
   print'(2x, ''ex   ='', 3(1x, f15.12))', ex
   print'(2x, ''ey   ='', 3(1x, f15.12))', ey
   print'(2x, ''ez   ='', 3(1x, f15.12))', ez
   print'(2x, ''ierr ='', 1x, i2)', ierr
   call fsl_driver_foot()

!-------------------------------------------------------------------------------
! 'end program' statement
!-------------------------------------------------------------------------------

end program x_create_cartesian
