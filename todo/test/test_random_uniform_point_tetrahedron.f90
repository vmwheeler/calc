program test_random_uniform_point_tetrahedron

!*******************************************************************************
! Copyright (C) 2009 Zheng Liang
! Copyright (C) 2009 Wojciech Lipinski
!
! This file is part of the Thermal Science Library (TSL).
!
! TSL is free software: you can redistribute it and/or modify
! it under the terms of the GNU Lesser General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! TSL is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU Lesser General Public License for more details.
!
! You should have received a copy of the GNU Lesser General Public License
! along with TSL.  If not, see <http://www.gnu.org/licenses/>.
!*******************************************************************************

!*******************************************************************************
! Description:
!    This is a driver for random_uniform_point_tetrahedron.
!*******************************************************************************

!*******************************************************************************
! 'use' and 'implicit none' statements
!*******************************************************************************

   use tsl_basics_kinds, only: k_i32, k_r64
   use tsl_basics_utils, only: tsl_preamble
   use tsl_math_analyticalc_geommetry, only: random_uniform_point_tetrahedron
   use tsl_math_random_uniform_maclaren, only: sdprnd, dprand
   use tsl_math_vector_algebra, only: cross_product
   !use tsl, only: k_i32, k_r64, tsl_preamble, random_uniform_point_tetrahedron
   implicit none

!*******************************************************************************
! Declaration constructs
!*******************************************************************************

   real(k_r64)                               :: rn1, rn2, rn3
   real(k_r64), dimension(4,3)             :: vertices
   real(k_r64), dimension(3)               :: point


   integer                                  :: n 
   character (len=12), dimension(3)       :: real_clock 
   integer(k_i32), dimension(8)           ::  date_time
   integer(k_i32)                           :: timeseed

   integer(k_i32), dimension(4)           ::  num_ran
   integer                                  :: i
   real(k_r64), dimension(3)               :: aa, bb, cc
   real(k_r64)                               :: vol123, vol124, vol134, vol234

!*******************************************************************************
! Execution part
!*******************************************************************************

   call tsl_preamble('test_random_uniform_point_tetrahedron', &
                     'Zheng Liang', '2009')
   !This program will test the uniformity of the random points generated in the 
   !tetrahedron. The uniformity is judged by the volume among the random point  
   !and any other three vertices. If this volume is less than other three 
   !volumes, we say that this random point is near that face constituted by the
   !three vertices. In the program, the number of random points near each face  
   !is counted
   !The vertices of the test tetrahedron are (2,0,0), (-2,0,0), (0,4,0), (0,0,3)
   vertices(1,:) = (/2,0,0/)
   vertices(2,:) = (/-2,0,0/)
   vertices(3,:) = (/0.0,4,0.0/)
   vertices(4,:) = (/0.0,0.0,3/)
   print '()'
   print '(2x, ''The vertices of the tetrahedron:'')'
   print '(2x, ''('',\)'
   print '(f7.2\ )', vertices(1,:)
   print '(2x, '')'' )'
   print '(2x, ''('',\)'
   print '(f7.2\ )', vertices(2,:)
   print '(2x, '')'' )'
   print '(2x, ''('',\)'
   print '(f7.2\ )', vertices(3,:)
   print '(2x, '')'' )'
   print '(2x, ''('',\)'
   print '(f7.2\ )', vertices(4,:)
   print '(2x, '')'' )'
   n = 100000
   !Initialize the array num_ran (counters)
   do i = 1, 4
      num_ran(i) = 0
   end do
   !Generate n random pointss and put them into different groups
   do i = 1,n
      !Calculate the time seed
      call date_and_time(real_clock(1), real_clock(2), real_clock(3), date_time)
      timeseed = date_time(8) + date_time(7) * 1000+ date_time(6) * 1000 * 60 &
                 +date_time(6) * 1000 * 60 * 60
      !Generate the random number
      call sdprnd(timeseed)
      rn1 = dprand()
       !Calculate the time seed
      call date_and_time(real_clock(1), real_clock(2), real_clock(3), date_time)
      timeseed = date_time(8) + date_time(7) * 1000+ date_time(6) * 1000 * 60 &
                 +date_time(6) * 1000 * 60 * 60
      !Generate the random number
      call sdprnd(timeseed)
      rn2 =dprand()
       !Calculate the time seed
      call date_and_time(real_clock(1), real_clock(2), real_clock(3), date_time)
      timeseed = date_time(8) + date_time(7) * 1000+ date_time(6) * 1000 * 60 &
                 +date_time(6) * 1000 * 60 * 60
      !Generate the random number
      call sdprnd(timeseed)
      rn3 = dprand()
      !Generate the random point
      point=random_uniform_point_tetrahedron(rn1, rn2, rn3, vertices)
      vol123 = abs(dot_product((point-vertices(3,:)),cross_product( &
                   (point-vertices(1,:)),(point-vertices(2,:)) )))/6
      vol124 = abs(dot_product((point-vertices(4,:)),cross_product( &
                   (point-vertices(1,:)),(point-vertices(2,:)) )))/6
      vol134 = abs(dot_product((point-vertices(4,:)),cross_product( &
                   (point-vertices(1,:)),(point-vertices(3,:)) )))/6
      vol234 = abs(dot_product(point-vertices(4,:),cross_product( &
                   (point-vertices(3,:)),(point-vertices(2,:)) )))/6
      !Put it into suitable group
      if (vol123<vol124 .and. vol123<vol134 .and. vol123<vol234) then
         num_ran(1)=num_ran(1)+1
      else if (vol124<vol123 .and. vol124<vol134 .and. vol124<vol234) then
         num_ran(2)=num_ran(2)+1
      else if (vol134<vol123 .and. vol134<vol124 .and. vol134<vol234) then
         num_ran(3)=num_ran(3)+1
      else if (vol234<vol123 .and. vol234<vol134 .and. vol234<vol134) then
         num_ran(4)=num_ran(4)+1
      endif
   end do
   print '(2x, ''Solution by random_uniform_point_tetrahedron:'')'
   print '(2x, ''Number of random points near the face:'')'
   print '(2x, ''Face by '')'
   print '(2x, ''('',\)'
   print '(f7.2\ )', vertices(1,:)
   print '(2x, '')'' )'
   print '(2x, ''('',\)'
   print '(f7.2\ )', vertices(2,:)
   print '(2x, '')'' )'
   print '(2x, ''('',\)'
   print '(f7.2\ )', vertices(3,:)
   print '(2x, '')'' )'
   print '(2x, ''Number: '',2i5 )',num_ran(1)
   print '(2x, ''Face by '')'
   print '(2x, ''('',\)'
   print '(f7.2\ )', vertices(1,:)
   print '(2x, '')'' )'
   print '(2x, ''('',\)'
   print '(f7.2\ )', vertices(2,:)
   print '(2x, '')'' )'
   print '(2x, ''('',\)'
   print '(f7.2\ )', vertices(4,:)
   print '(2x, '')'' )'
   print '(2x, ''Number: '',2i5 )',num_ran(2)
   print '(2x, ''Face by '')'
   print '(2x, ''('',\)'
   print '(f7.2\ )', vertices(1,:)
   print '(2x, '')'' )'
   print '(2x, ''('',\)'
   print '(f7.2\ )', vertices(3,:)
   print '(2x, '')'' )'
   print '(2x, ''('',\)'
   print '(f7.2\ )', vertices(4,:)
   print '(2x, '')'' )'
   print '(2x, ''Number: '',2i5 )',num_ran(3)
   print '(2x, ''Face by '')'
   print '(2x, ''('',\)'
   print '(f7.2\ )', vertices(2,:)
   print '(2x, '')'' )'
   print '(2x, ''('',\)'
   print '(f7.2\ )', vertices(3,:)
   print '(2x, '')'' )'
   print '(2x, ''('',\)'
   print '(f7.2\ )', vertices(4,:)
   print '(2x, '')'' )'
   print '(2x, ''Number: '',2i5 )',num_ran(4)

   print '(2x, ''Done.'')'
   print '()'

  
   
   read (*,*)

!*******************************************************************************
! Subprogram part
!*******************************************************************************

!*******************************************************************************
! 'end program' statement
!*******************************************************************************
end program test_random_uniform_point_tetrahedron