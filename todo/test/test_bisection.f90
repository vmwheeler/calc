program test_bisection

!*******************************************************************************
! Copyright (C) 2009 Zheng Liang
! Copyright (C) 2009 Wojciech Lipinski
!
! This file is part of the Thermal Science Library (TSL).
!
! TSL is free software: you can redistribute it and/or modify
! it under the terms of the GNU Lesser General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! TSL is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU Lesser General Public License for more details.
!
! You should have received a copy of the GNU Lesser General Public License
! along with TSL.  If not, see <http://www.gnu.org/licenses/>.
!*******************************************************************************

!*******************************************************************************
! Description:
!    This is a driver for bisection.
!*******************************************************************************

!*******************************************************************************
! 'use' and 'implicit none' statements
!*******************************************************************************

   use tsl, only: k_i32, k_r64
   use tsl, only: tsl_preamble
   use tsl, only: bisection
   implicit none

!*******************************************************************************
! Declaration constructs
!*******************************************************************************
   !Polynomial function
   interface
      function func_poly(x, param)
         use tsl_basics_kinds,     only: k_r64
         implicit none
         real(k_r64),               intent(in) :: x
         real(k_r64), dimension(:), intent(in) :: param
         real(k_r64) :: func_poly
      end function func_poly
   end interface

   real(k_r64), allocatable, dimension(:) :: param
   real(k_r64)                               :: x1, x2, xacc
   real(k_r64)                               :: root
   integer(k_i32)                           :: status


!*******************************************************************************
! Execution part
!*******************************************************************************

   call tsl_preamble('test_bisection', 'Zheng Liang', '2009')
   !There are no cases about no bisection and too many bisections tested here
   !Polynomial function
   !func = x**2 + 4*x + 2, [-2,0]
   allocate (param(3), stat=status)
   param = (/2.0,4.0,1.0/)
   x1 = -2.0
   x2 = 0
   xacc = 0.0001
   print '()'
   print '(2x, ''The function: f(x) = x**2 + 4*x + 2'' )'
   print '(2x, ''The calculation region: [ '',f5.2, '','' ,f5.2, '']'' )',x1,x2
   print '(2x, ''The tolerance: '',f7.4 )',xacc
   print '(2x, ''Analytical solution:'')'
   print '(2x, ''Root = -0.5858'')'
   print '(2x, ''Solution by bisection:'')'
   root = bisection(func_poly, param, x1, x2, xacc)
   print '(2x, ''Root = '',f7.4)', root
   print '()'
   deallocate (param,stat=status)
   !func = x**3 + 6*x**2 + 13*x + 10, [-5,5]
   allocate (param(4), stat=status)
   param = (/10.0,13.0,6.0,1.0/)
   x1 = -5.0
   x2 = 5.0
   xacc = 0.0001
   print '()'
   print '(2x, ''The function: f(x) = x**3 + 6*x**2 + 13*x + 10'' )'
   print '(2x, ''The calculation region: [ '',f5.2, '','' ,f5.2, '']'' )',x1,x2
   print '(2x, ''The tolerance: '',f7.4 )',xacc
   print '(2x, ''Analytical solution:'')'
   print '(2x, ''Root = -2.0'')'
   print '(2x, ''Solution by bisection:'')'
   root = bisection(func_poly, param, x1, x2, xacc)
   print '(2x, ''Root = '',f7.4)', root
   print '(2x, ''Done.'')'
   print '()'
   deallocate (param,stat=status)

  
   
   read (*,*)

!*******************************************************************************
! 'end program' statement
!*******************************************************************************
end program test_bisection

!*******************************************************************************
! Subprogram part
!*******************************************************************************
function func_poly(x, param)
!*******************************************************************************
! Dummy argument declaration constructs
!******************************************************************************* 
   use tsl_basics_kinds,     only: k_r64
   implicit none
   real(k_r64),               intent(in) :: x
   real(k_r64), dimension(:), intent(in) :: param
   real(k_r64) :: func_poly
!*******************************************************************************
! Local declaration constructs
!******************************************************************************* 
   integer                     :: n  !The size of the parameters array
   integer                     :: i  
   real(k_r64)                  :: fres !The result
!*******************************************************************************
! Execution part
!******************************************************************************* 
   n = size(param)  
   fres = 0
   do i = 1, n
      fres = fres+param(i)*x**(i-1)
   end do
   func_poly = fres
   return
end function func_poly
