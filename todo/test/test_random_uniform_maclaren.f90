program test_random_uniform_maclaren

!*******************************************************************************
! Copyright (C) 2009 Zheng Liang
! Copyright (C) 2009 Wojciech Lipinski
!
! This file is part of the Thermal Science Library (TSL).
!
! TSL is free software: you can redistribute it and/or modify
! it under the terms of the GNU Lesser General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! TSL is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU Lesser General Public License for more details.
!
! You should have received a copy of the GNU Lesser General Public License
! along with TSL.  If not, see <http://www.gnu.org/licenses/>.
!*******************************************************************************

!*******************************************************************************
! Description:
!    This is a driver for random_uniform_maclaren.
!*******************************************************************************

!*******************************************************************************
! 'use' and 'implicit none' statements
!*******************************************************************************

   use tsl_basics_kinds, only: k_i32, k_r64
   use tsl_basics_utils, only: tsl_preamble
   use tsl_math_random_uniform_maclaren, only: sdprnd, dprand
   !use tsl, only: k_i32, k_r64, tsl_preamble
   implicit none

!*******************************************************************************
! Declaration constructs
!*******************************************************************************
   integer(k_i32)                               :: n 
   real(k_r64), allocatable, dimension(:)     :: aa
   integer          :: status
   character (len=12), dimension(3)           :: real_clock 
   integer(k_i32), dimension(8)               ::  date_time
   integer(k_i32)                               :: timeseed

   integer(k_i32), dimension(10)               ::  num_ran
   integer :: i

!*******************************************************************************
! Execution part
!*******************************************************************************

   call tsl_preamble('test_random_uniform_maclaren', 'Zheng Liang', '2009')
   !This program will generate a large amount of random numbers and put them
   !into different uniform group ([0,0.1], (0.1,0.2], ..., (0.9,1]) and then
   !check whether each group has nearly the same number
   n = 100000
   allocate (aa(n), stat = status)
   !Initialize the array num_ran (counters)
   do i = 1, 10
      num_ran(i)=0
   end do
   !Generate n random numbers and put them into different groups
   do i = 1, n
      !Calculate the time seed
      call date_and_time (real_clock(1), real_clock(2), real_clock(3), &
                          date_time)
      timeseed= date_time(8) + date_time(7) * 1000 + date_time(6) *1000 *60 + &
                date_time(6) * 1000 * 60 * 60
      !Generate the random number
      call sdprnd(timeseed)
      aa(i) = dprand()
      !Put it into suitable group
      if (aa(i) >0 .and. aa(i) <=0.1) then
         num_ran(1)=num_ran(1)+1
      else if (aa(i) >0.1 .and. aa(i) <=0.2) then
         num_ran(2)=num_ran(2)+1
      else if (aa(i) >0.2 .and. aa(i) <=0.3) then
         num_ran(3)=num_ran(3)+1
      else if (aa(i) >0.3 .and. aa(i) <=0.4) then
         num_ran(4)=num_ran(4)+1
      else if (aa(i) >0.4 .and. aa(i) <=0.5) then
         num_ran(5)=num_ran(5)+1
      else if (aa(i) >0.5 .and. aa(i) <=0.6) then
         num_ran(6)=num_ran(6)+1
      else if (aa(i) >0.6 .and. aa(i) <=0.7) then
         num_ran(7)=num_ran(7)+1
      else if (aa(i) >0.7 .and. aa(i) <=0.8) then
         num_ran(8)=num_ran(8)+1
      else if (aa(i) >0.8 .and. aa(i) <=0.9) then
         num_ran(9)=num_ran(9)+1
      else if (aa(i) >0.9 .and. aa(i) <=1.0) then
         num_ran(10)=num_ran(10)+1
      endif
   end do
   print '()'
   print '(2x, ''The random numbers are divided into 10 groups'' )'
   print '(2x, ''The following is the statistics of the number that'' )'
   print '(2x, ''the random numbers fall into each group'' )'
   print '(2x, ''The group [0, 0.1]: '',3i7 )',num_ran(1)
   print '(2x, ''The group (0.1, 0.2]: '',3i7 )',num_ran(2)
   print '(2x, ''The group (0.2, 0.3]: '',3i7 )',num_ran(3)
   print '(2x, ''The group (0.3, 0.4]: '',3i7 )',num_ran(4)
   print '(2x, ''The group (0.4, 0.5]: '',3i7 )',num_ran(5)
   print '(2x, ''The group (0.5, 0.6]: '',3i7 )',num_ran(6)
   print '(2x, ''The group (0.6, 0.7]: '',3i7 )',num_ran(7)
   print '(2x, ''The group (0.7, 0.8]: '',3i7 )',num_ran(8)
   print '(2x, ''The group (0.8, 0.9]: '',3i7 )',num_ran(9)
   print '(2x, ''The group (0.9, 1.0]: '',3i7 )',num_ran(10)
   print '(2x, ''Done.'')'
   print '()'
   deallocate (aa,stat = status)
 
   
   read (*,*)

!*******************************************************************************
! 'end program' statement
!*******************************************************************************
end program test_random_uniform_maclaren

