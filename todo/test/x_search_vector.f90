program x_search_vector

!-------------------------------------------------------------------------------
! Copyright 2009 Regents of the University of Minnesota. All rights reserved.
!
! This file is part of the Fortran Scientific Library (FSL).
!
! FSL is free software: you can redistribute it and/or modify
! it under the terms of the GNU Lesser General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! FSL is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU Lesser General Public License for more details.
!
! You should have received a copy of the GNU Lesser General Public License
! along with FSL.  If not, see <http://www.gnu.org/licenses/>.
!-------------------------------------------------------------------------------

!-------------------------------------------------------------------------------
! Description:
!    This is a driver for the search_vector function.
!-------------------------------------------------------------------------------

!-------------------------------------------------------------------------------
! 'use' and 'implicit none' statements
!-------------------------------------------------------------------------------

   use fsl, only: k_i4, k_r8
   use fsl, only: fsl_driver_head, fsl_driver_foot
   use fsl, only: search_vector
   implicit none

!-------------------------------------------------------------------------------
! Declaration constructs
!-------------------------------------------------------------------------------

   real(k_r8), allocatable, dimension(:) :: vector
   real(k_r8)                            :: r, r1, r2
   integer(k_i4)                         :: i, ipos, n

!-------------------------------------------------------------------------------
! Execution part
!-------------------------------------------------------------------------------

   call fsl_driver_head('search_vector')
   print'(2x, ''Give the size of the vector:'')'
   read(*,*) n
   print'(2x, ''Give the smallest number in the vector'')'
   read(*,*) r1
   print'()'
   print'(2x, ''Give the largest number in the vector:'')'
   read (*,*) r2
   print'()'
   print'(2x, ''Give a real number:'')'
   read(*,*) r
   print'()'
   allocate(vector(n))
   vector(1) = r1
   vector(n) = r2
   do i = 2, n-1
      vector(i) = vector(i-1) + (vector(n) - vector(i)) / n
   end do
   ipos = search_vector(vector, r)
   print'(2x, ''ipos ='', 1x, i10)', ipos
   deallocate(vector)
   call fsl_driver_foot()

!-------------------------------------------------------------------------------
! 'end program' statement
!-------------------------------------------------------------------------------

end program x_search_vector
