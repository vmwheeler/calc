program test_intersection_line_cylinder

!*******************************************************************************
! Copyright (C) 2009 Zheng Liang
! Copyright (C) 2009 Wojciech Lipinski
!
! This file is part of the Thermal Science Library (TSL).
!
! TSL is free software: you can redistribute it and/or modify
! it under the terms of the GNU Lesser General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! TSL is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU Lesser General Public License for more details.
!
! You should have received a copy of the GNU Lesser General Public License
! along with TSL.  If not, see <http://www.gnu.org/licenses/>.
!*******************************************************************************

!*******************************************************************************
! Description:
!    This is a driver for intersection_line_cylinder.
!*******************************************************************************

!*******************************************************************************
! 'use' and 'implicit none' statements
!*******************************************************************************

   use tsl_basics_kinds, only: k_i16, k_r64
   use tsl_basics_utils, only: tsl_preamble
   use tsl_math_analyticalc_geommetry, only: intersection_line_cylinder
   !use tsl, only: k_i16, k_r64, tsl_preamble, intersection_line_cylinder
   implicit none

!*******************************************************************************
! Declaration constructs
!*******************************************************************************

   real(k_r64),    dimension(3)   :: point, dir, c1, c2
   real(k_r64)                      :: r
   integer(k_i16), dimension(2)  :: surface
   real(k_r64),    dimension(2,3) :: points


!*******************************************************************************
! Execution part
!*******************************************************************************

   call tsl_preamble('test_intersection_line_cylinder', 'Zheng Liang', '2009')
   !No tangent case will be tested in the following codes
   !No infinite case will be tested in the following codes
   !The cylinder is located in the center of the coordinate
   !Center of top surface is (0,0,10)
   !Center of bottom surface is (0,0,0)
   !The radius is 5
   c1 = (/0,0,10/)
   c2 = (/0,0,0/)
   r = 5
   print '()'
   print '(2x, ''The center of top surface: ('',\)'
   print '(f7.2\ )', c1
   print '(2x, '')'' )'
   print '(2x, ''The center of bottom surface: ('',\)'
   print '(f7.2\ )', c2
   print '(2x, '')'' )'
   print '(2x, ''The radius of cylinder: '', f7.2)',r
   print '()'
   !The point on the line is (1,1,-1), and direction vector is (0,0,1)
   !The intersecting points are on the top and bottom surface
   point = (/1,1,-1/)
   dir = (/0,0,1/)
   print '(2x, ''The point on the line: ('',\)'
   print '(f7.2\ )', point
   print '(2x, '')'' )'
   print '(2x, ''The direction vector: ('',\)'
   print '(f7.2\ )', dir
   print '(2x, '')'' )'
   print '(2x, ''Analytical solution:'')'
   print '(2x, ''The intersecting points are on'')'
   print '(2x, ''Top surface: (1, 1, 10) '')'
   print '(2x, ''Bottom surface: (1, 1, 0) '')'
   print '(2x, ''Solution by intersection_line_cylinder:'')'
   print '(2x, ''The intersecting points are on'')'
   call  intersection_line_cylinder(point, dir, c1, c2, r, surface, points)
   if (surface(1)==0 .and. surface(2)==0) then
      print '(2x, ''No intersection or infinite intersection '')'
   else
      if (surface(1)==1) then
         print '(2x, ''Top surface:: ('',\)'
         print '(f9.4\ )', points(1,:)
         print '(2x, '')'' )'
      else if (surface(1)==2) then
         print '(2x, ''Bottom surface:: ('',\)'
         print '(f9.4\ )', points(1,:)
         print '(2x, '')'' )'
      else
         print '(2x, ''Lateral surface:: ('',\)'
         print '(f9.4\ )', points(1,:)
         print '(2x, '')'' )'
      end if
      if (surface(2)==1) then
         print '(2x, ''Top surface:: ('',\)'
         print '(f9.4\ )', points(2,:)
         print '(2x, '')'' )'
      else if (surface(2)==2) then
         print '(2x, ''Bottom surface:: ('',\)'
         print '(f9.4\ )', points(2,:)
         print '(2x, '')'' )'
      else
         print '(2x, ''Lateral surface:: ('',\)'
         print '(f9.4\ )', points(2,:)
         print '(2x, '')'' )'
      end if
   end if
   print '()'
   !The point on the line is (1,1,-1), and direction vector is (1,1,1)
   !The intersecting points are on the lateral and bottom surface
   point = (/1,1,-1/)
   dir = (/1,1,1/)
   print '(2x, ''The point on the line: ('',\)'
   print '(f7.2\ )', point
   print '(2x, '')'' )'
   print '(2x, ''The direction vector: ('',\)'
   print '(f7.2\ )', dir
   print '(2x, '')'' )'
   print '(2x, ''Analytical solution:'')'
   print '(2x, ''The intersecting points are on'')'
   print '(2x, ''Lateral surface: (3.54, 3.54, 1.54) '')'
   print '(2x, ''Bottom surface: (2, 2, 0) '')'
   print '(2x, ''Solution by intersection_line_cylinder:'')'
   print '(2x, ''The intersecting points are on'')'
   call  intersection_line_cylinder(point, dir, c1, c2, r, surface, points)
   if (surface(1)==0 .and. surface(2)==0) then
      print '(2x, ''No intersection or infinite intersection '')'
   else
      if (surface(1)==1) then
         print '(2x, ''Top surface:: ('',\)'
         print '(f9.4\ )', points(1,:)
         print '(2x, '')'' )'
      else if (surface(1)==2) then
         print '(2x, ''Bottom surface:: ('',\)'
         print '(f9.4\ )', points(1,:)
         print '(2x, '')'' )'
      else
         print '(2x, ''Lateral surface:: ('',\)'
         print '(f9.4\ )', points(1,:)
         print '(2x, '')'' )'
      end if
      if (surface(2)==1) then
         print '(2x, ''Top surface:: ('',\)'
         print '(f9.4\ )', points(2,:)
         print '(2x, '')'' )'
      else if (surface(2)==2) then
         print '(2x, ''Bottom surface:: ('',\)'
         print '(f9.4\ )', points(2,:)
         print '(2x, '')'' )'
      else
         print '(2x, ''Lateral surface:: ('',\)'
         print '(f9.4\ )', points(2,:)
         print '(2x, '')'' )'
      end if
   end if
   print '()'
   !The point on the line is (1,1,-1), and direction vector is (1,1,0)
   !No intersecting points
   point = (/1,1,-1/)
   dir = (/1,1,0/)
   print '(2x, ''The point on the line: ('',\)'
   print '(f7.2\ )', point
   print '(2x, '')'' )'
   print '(2x, ''The direction vector: ('',\)'
   print '(f7.2\ )', dir
   print '(2x, '')'' )'
   print '(2x, ''Analytical solution:'')'
   print '(2x, ''The intersecting points are on'')'
   print '(2x, ''No intersection '')'
   print '(2x, ''Solution by intersection_line_cylinder:'')'
   print '(2x, ''The intersecting points are on'')'
   call  intersection_line_cylinder(point, dir, c1, c2, r, surface, points)
   if (surface(1)==0 .and. surface(2)==0) then
      print '(2x, ''No intersection or infinite intersection '')'
   else
      if (surface(1)==1) then
         print '(2x, ''Top surface:: ('',\)'
         print '(f9.4\ )', points(1,:)
         print '(2x, '')'' )'
      else if (surface(1)==2) then
         print '(2x, ''Bottom surface:: ('',\)'
         print '(f9.4\ )', points(1,:)
         print '(2x, '')'' )'
      else
         print '(2x, ''Lateral surface:: ('',\)'
         print '(f9.4\ )', points(1,:)
         print '(2x, '')'' )'
      end if
      if (surface(2)==1) then
         print '(2x, ''Top surface:: ('',\)'
         print '(f9.4\ )', points(2,:)
         print '(2x, '')'' )'
      else if (surface(2)==2) then
         print '(2x, ''Bottom surface:: ('',\)'
         print '(f9.4\ )', points(2,:)
         print '(2x, '')'' )'
      else
         print '(2x, ''Lateral surface:: ('',\)'
         print '(f9.4\ )', points(2,:)
         print '(2x, '')'' )'
      end if
   end if
   print '()'
   !The point on the line is (1,1,-1), and direction vector is (1,0,1)
   !The intersecting points are on the lateral and bottom surface
   point = (/1,1,-1/)
   dir = (/1,0,1/)
   print '(2x, ''The point on the line: ('',\)'
   print '(f7.2\ )', point
   print '(2x, '')'' )'
   print '(2x, ''The direction vector: ('',\)'
   print '(f7.2\ )', dir
   print '(2x, '')'' )'
   print '(2x, ''Analytical solution:'')'
   print '(2x, ''The intersecting points are on'')'
   print '(2x, ''Lateral surface: (4.90, 1, 2.90) '')'
   print '(2x, ''Bottom surface: (2, 1, 0) '')'
   print '(2x, ''Solution by intersection_line_cylinder:'')'
   print '(2x, ''The intersecting points are on'')'
   call  intersection_line_cylinder(point, dir, c1, c2, r, surface, points)
   if (surface(1)==0 .and. surface(2)==0) then
      print '(2x, ''No intersection or infinite intersection '')'
   else
      if (surface(1)==1) then
         print '(2x, ''Top surface:: ('',\)'
         print '(f9.4\ )', points(1,:)
         print '(2x, '')'' )'
      else if (surface(1)==2) then
         print '(2x, ''Bottom surface:: ('',\)'
         print '(f9.4\ )', points(1,:)
         print '(2x, '')'' )'
      else
         print '(2x, ''Lateral surface:: ('',\)'
         print '(f9.4\ )', points(1,:)
         print '(2x, '')'' )'
      end if
      if (surface(2)==1) then
         print '(2x, ''Top surface:: ('',\)'
         print '(f9.4\ )', points(2,:)
         print '(2x, '')'' )'
      else if (surface(2)==2) then
         print '(2x, ''Bottom surface:: ('',\)'
         print '(f9.4\ )', points(2,:)
         print '(2x, '')'' )'
      else
         print '(2x, ''Lateral surface:: ('',\)'
         print '(f9.4\ )', points(2,:)
         print '(2x, '')'' )'
      end if
   end if   
   print '()'
   !The point on the line is (8,1,12), and direction vector is (1,1,1)
   !The intersecting points are on the lateral surfaces
   point = (/8,1,12/)
   dir = (/1,1,1/)
   print '(2x, ''The point on the line: ('',\)'
   print '(f7.2\ )', point
   print '(2x, '')'' )'
   print '(2x, ''The direction vector: ('',\)'
   print '(f7.2\ )', dir
   print '(2x, '')'' )'
   print '(2x, ''Analytical solution:'')'
   print '(2x, ''The intersecting points are on'')'
   print '(2x, ''Lateral surface: (3, -4, 7) '')'
   print '(2x, ''Lateral surface: (4, -3, 8) '')'
   print '(2x, ''Solution by intersection_line_cylinder:'')'
   print '(2x, ''The intersecting points are on'')'
   call  intersection_line_cylinder(point, dir, c1, c2, r, surface, points)
   if (surface(1)==0 .and. surface(2)==0) then
      print '(2x, ''No intersection or infinite intersection '')'
   else
      if (surface(1)==1) then
         print '(2x, ''Top surface:: ('',\)'
         print '(f9.4\ )', points(1,:)
         print '(2x, '')'' )'
      else if (surface(1)==2) then
         print '(2x, ''Bottom surface:: ('',\)'
         print '(f9.4\ )', points(1,:)
         print '(2x, '')'' )'
      else
         print '(2x, ''Lateral surface:: ('',\)'
         print '(f9.4\ )', points(1,:)
         print '(2x, '')'' )'
      end if
      if (surface(2)==1) then
         print '(2x, ''Top surface:: ('',\)'
         print '(f9.4\ )', points(2,:)
         print '(2x, '')'' )'
      else if (surface(2)==2) then
         print '(2x, ''Bottom surface:: ('',\)'
         print '(f9.4\ )', points(2,:)
         print '(2x, '')'' )'
      else
         print '(2x, ''Lateral surface:: ('',\)'
         print '(f9.4\ )', points(2,:)
         print '(2x, '')'' )'
      end if
   end if   
   print '()'
   !The point on the line is (1,1,12), and direction vector is (1,1,1)
   !The intersecting points are on the top and lateral surface
   point = (/1,1,12/)
   dir = (/1,1,1/)
   print '(2x, ''The point on the line: ('',\)'
   print '(f7.2\ )', point
   print '(2x, '')'' )'
   print '(2x, ''The direction vector: ('',\)'
   print '(f7.2\ )', dir
   print '(2x, '')'' )'
   print '(2x, ''Analytical solution:'')'
   print '(2x, ''The intersecting points are on'')'
   print '(2x, ''Top surface: (-1, -1, 10) '')'
   print '(2x, ''Lateral surface: (-3.54, -3.54, 7.46) '')'
   print '(2x, ''Solution by intersection_line_cylinder:'')'
   print '(2x, ''The intersecting points are on'')'
   call  intersection_line_cylinder(point, dir, c1, c2, r, surface, points)
   if (surface(1)==0 .and. surface(2)==0) then
      print '(2x, ''No intersection or infinite intersection '')'
   else
      if (surface(1)==1) then
         print '(2x, ''Top surface:: ('',\)'
         print '(f9.4\ )', points(1,:)
         print '(2x, '')'' )'
      else if (surface(1)==2) then
         print '(2x, ''Bottom surface:: ('',\)'
         print '(f9.4\ )', points(1,:)
         print '(2x, '')'' )'
      else
         print '(2x, ''Lateral surface:: ('',\)'
         print '(f9.4\ )', points(1,:)
         print '(2x, '')'' )'
      end if
      if (surface(2)==1) then
         print '(2x, ''Top surface:: ('',\)'
         print '(f9.4\ )', points(2,:)
         print '(2x, '')'' )'
      else if (surface(2)==2) then
         print '(2x, ''Bottom surface:: ('',\)'
         print '(f9.4\ )', points(2,:)
         print '(2x, '')'' )'
      else
         print '(2x, ''Lateral surface:: ('',\)'
         print '(f9.4\ )', points(2,:)
         print '(2x, '')'' )'
      end if
   end if
   print '(2x, ''Done.'')'
              
   read (*,*)

!*******************************************************************************
! Subprogram part
!*******************************************************************************

!*******************************************************************************
! 'end program' statement
!*******************************************************************************
end program test_intersection_line_cylinder