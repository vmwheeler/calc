program test_intersection_line_ellipsoid

!*******************************************************************************
! Copyright (C) 2009 Zheng Liang
! Copyright (C) 2009 Wojciech Lipinski
!
! This file is part of the Thermal Science Library (TSL).
!
! TSL is free software: you can redistribute it and/or modify
! it under the terms of the GNU Lesser General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! TSL is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU Lesser General Public License for more details.
!
! You should have received a copy of the GNU Lesser General Public License
! along with TSL.  If not, see <http://www.gnu.org/licenses/>.
!*******************************************************************************

!*******************************************************************************
! Description:
!    This is a driver for intersection_line_ellipsoid.
!*******************************************************************************

!*******************************************************************************
! 'use' and 'implicit none' statements
!*******************************************************************************

   use tsl_basics_kinds, only: k_r64, k_lg
   use tsl_basics_utils, only: tsl_preamble
   use tsl_math_analyticalc_geommetry, only: intersection_line_ellipsoid
   !use tsl, only: k_r64, k_lg, tsl_preamble, intersection_line_ellipsoid
   implicit none

!*******************************************************************************
! Declaration constructs
!*******************************************************************************

   real(k_r64), dimension(3)           :: x, u, opn
   real(k_r64)                          :: ra, rb, rc
   real(k_r64), dimension(3)          :: xn, yn, zn
   real(k_r64), dimension(2,3)        :: xi
   logical(k_lg)                        :: flag
 
!*******************************************************************************
! Execution part
!*******************************************************************************

   call tsl_preamble('test_intersection_line_ellipsoid', 'Zheng Liang', '2009')
   !No tangent cases will be tested in this program
   !The center of the ellipsoid is (0,0,0)
   !The axes of the ellipsoid are (1,0,0), (0,1,0), (0,0,1)
   !The semiaxis length of the ellipsoid is 10,5,5
   opn = (/0,0,0/)
   ra = 10
   rb = 5
   rc = 5
   xn = (/1,0,0/)
   yn = (/0,1,0/)
   zn = (/0,0,1/)
   print '()'
   print '(2x, ''The center of the ellipsoid: ('',\)'
   print '(f7.2\ )', x
   print '(2x, '')'' )'
   print '(2x, ''The x-direction vector: ('',\)'
   print '(f7.2\ )', xn
   print '(2x, '')'' )'
   print '(2x, ''The y-direction vector: ('',\)'
   print '(f7.2\ )', yn
   print '(2x, '')'' )'
   print '(2x, ''The z-direction vector: ('',\)'
   print '(f7.2\ )', zn
   print '(2x, '')'' )'
   print '(2x, ''The x-direction semiaxis length: '',f7.2)',ra
   print '(2x, ''The y-direction semiaxis length: '',f7.2)',rb
   print '(2x, ''The z-direction semiaxis length: '',f7.2)',rc
   print '()'
   !The point on the line is (1,1,-6), and direction vector is (0,0,1)
   x = (/1,1,-6/)
   u = (/0,0,1/)
   print '(2x, ''The point on the line: ('',\)'
   print '(f7.2\ )', x
   print '(2x, '')'' )'
   print '(2x, ''The direction vector: ('',\)'
   print '(f7.2\ )', u
   print '(2x, '')'' )'
   print '(2x, ''Analytical solution:'')'
   print '(2x, ''The intersecting points: (1,1,-4.87)  (0,0,4.87)'')'
   print '(2x, ''Solution by intersection_line_ellipsoid:'')'
   call intersection_line_ellipsoid(x, u, opn, ra, rb, rc, xn, yn, zn, xi, flag)
   if(flag ==.false.) then
      print '(2x, ''No intersection'')'
   else
      print '(2x, ''The intersecting points: '')'
      print '(2x, ''('', \)'
      print '(f9.4\ )', xi(1,:)
      print '(2x, '')'' )'
      print '(2x, ''('', \)'
      print '(f9.4\ )', xi(2,:)
      print '(2x, '')'' )'
   end if
   print '()'
   !The point on the line is (1,1,-6), and direction vector is (1,1,1)
   x = (/1,1,-6/)
   u = (/1,1,1/)
   print '(2x, ''The point on the line: ('',\)'
   print '(f7.2\ )', x
   print '(2x, '')'' )'
   print '(2x, ''The direction vector: ('',\)'
   print '(f7.2\ )', u
   print '(2x, '')'' )'
   print '(2x, ''Analytical solution:'')'
   print '(2x, ''No intersection'')'
   print '(2x, ''Solution by intersection_line_ellipsoid:'')'
   call intersection_line_ellipsoid(x, u, opn, ra, rb, rc, xn, yn, zn, xi, flag)
   if(flag ==.false.) then
      print '(2x, ''No intersection'')'
   else
      print '(2x, ''The intersecting points: '')'
      print '(2x, ''('', \)'
      print '(f9.4\ )', xi(1,:)
      print '(2x, '')'' )'
      print '(2x, ''('', \)'
      print '(f9.4\ )', xi(2,:)
      print '(2x, '')'' )'
   end if
   print '()'
   !The point on the line is (1,1,-6), and direction vector is (1,4,5)
   x = (/1,1,-6/)
   u = (/1,4,5/)
   print '(2x, ''The point on the line: ('',\)'
   print '(f7.2\ )', x
   print '(2x, '')'' )'
   print '(2x, ''The direction vector: ('',\)'
   print '(f7.2\ )', u
   print '(2x, '')'' )'
   print '(2x, ''Analytical solution:'')'
   print '(2x, ''The intersecting points: (1.32,2.28,-4.40)  (1.93,4.71,-1.36)'')'
   print '(2x, ''Solution by intersection_line_ellipsoid:'')'
   call intersection_line_ellipsoid(x, u, opn, ra, rb, rc, xn, yn, zn, xi, flag)
   if(flag ==.false.) then
      print '(2x, ''No intersection'')'
   else
      print '(2x, ''The intersecting points: '')'
      print '(2x, ''('', \)'
      print '(f9.4\ )', xi(1,:)
      print '(2x, '')'' )'
      print '(2x, ''('', \)'
      print '(f9.4\ )', xi(2,:)
      print '(2x, '')'' )'
   end if
   print '()'
   
   print '(2x, ''Done.'')'
                     
   read (*,*)

!*******************************************************************************
! Subprogram part
!*******************************************************************************

!*******************************************************************************
! 'end program' statement
!*******************************************************************************
end program test_intersection_line_ellipsoid