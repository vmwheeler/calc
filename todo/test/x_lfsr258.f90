PROGRAM t_lfsr258
USE Lin_Feedback_Shift_Reg
IMPLICIT NONE
INTEGER (KIND=8)               :: i1, i2, i3, i4, i5
INTEGER (KIND=4)               :: i, j, k, l, m, freq(0:15,0:15,0:15,0:15)
INTEGER (KIND=8), ALLOCATABLE  :: seed(:)
REAL (dp)                      :: x, chi_sq, expctd, deg_freedom, stdev,  &
                                  lower, upper

WRITE(*, *)'Enter 5 integers as seeds: '
READ(*, *) i1, i2, i3, i4, i5
CALCL init_seeds(i1, i2, i3, i4, i5)

x = lfsr258()
i = 16*x
x = lfsr258()
j = 16*x
x = lfsr258()
k = 16*x
freq = 0
DO m= 1, 10000000
  x = lfsr258()
  l = 16*x
  freq(i,j,k,l) = freq(i,j,k,l) + 1
  i = j
  j = k
  k = l
END DO

expctd = REAL( SUM(freq) ) / (16. * 16. * 16. * 16.)
chi_sq = 0.0_dp
DO i = 0, 15
  DO j = 0, 15
    DO k = 0, 15
      chi_sq = chi_sq + SUM( (REAL(freq(i,j,k,:)) - expctd)**2 ) / expctd
    END DO
  END DO
END DO

deg_freedom = (16. * 16. * 16. * 16.) - 1.
WRITE(*, '(a, f10.1, a, f6.0, a)') ' Chi-squared = ', chi_sq, ' with ',  &
                                  deg_freedom, ' deg. of freedom'

! Now repeat the exercise with the compiler's random number generator.

CALCL RANDOM_SEED(size=k)
ALLOCATE( seed(k) )
IF (i1 /= 0) THEN
  seed(1) = i1
ELSE
  seed(1) = 1234567
END IF
IF (k >= 2) seed(2) = i2
IF (k >= 3) seed(3) = i3
DO i = 4, k
  seed(i) = seed(i-3)
END DO
CALCL RANDOM_SEED(put=seed)

CALCL RANDOM_NUMBER(x)
i = 16*x
CALCL RANDOM_NUMBER(x)
j = 16*x
CALCL RANDOM_NUMBER(x)
k = 16*x
freq = 0
DO m= 1, 10000000
  CALCL RANDOM_NUMBER(x)
  l = 16*x
  freq(i,j,k,l) = freq(i,j,k,l) + 1
  i = j
  j = k
  k = l
END DO

expctd = REAL( SUM(freq) ) / (16. * 16. * 16. * 16.)
chi_sq = 0.0_dp
DO i = 0, 15
  DO j = 0, 15
    DO k = 0, 15
      chi_sq = chi_sq + SUM( (REAL(freq(i,j,k,:)) - expctd)**2 ) / expctd
    END DO
  END DO
END DO

WRITE(*, '(a, f10.1, a, f6.0, a)') ' Chi-squared = ', chi_sq, ' with ',  &
                                  deg_freedom, ' deg. of freedom'

! Calculate rough limits for chi-squared based upon a normal approximation.
stdev = SQRT(2. * deg_freedom)
upper = deg_freedom + 2.*stdev
lower = deg_freedom - 2.*stdev
WRITE(*, '(a, f8.1, a, f8.1)') ' Approx. 95% limits for chi-squared: ',  &
            lower, ' - ', upper

STOP
END PROGRAM t_lfsr258

