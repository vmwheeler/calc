program test_random_uniform_knuth

!*******************************************************************************
! Copyright (C) 2009 Zheng Liang
! Copyright (C) 2009 Wojciech Lipinski
!
! This file is part of the Thermal Science Library (TSL).
!
! TSL is free software: you can redistribute it and/or modify
! it under the terms of the GNU Lesser General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! TSL is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU Lesser General Public License for more details.
!
! You should have received a copy of the GNU Lesser General Public License
! along with TSL.  If not, see <http://www.gnu.org/licenses/>.
!*******************************************************************************

!*******************************************************************************
! Description:
!    This is a driver for random_uniform_knuth.
!*******************************************************************************

!*******************************************************************************
! 'use' and 'implicit none' statements
!*******************************************************************************

   use tsl_basics_kinds, only: k_i32, k_r64
   use tsl_basics_utils, only: tsl_preamble
   use tsl_math_random_uniform_knuth, only: ran_start_int, ran_start_dp, &
        ran_array_int, ran_array_dp
   !use tsl, only: k_i32, k_r64, tsl_preamble
   implicit none

!*******************************************************************************
! Declaration constructs
!*******************************************************************************
   integer(k_i32)                               :: n
   integer(k_i32), allocatable, dimension(:) :: aa
   real(k_r64), allocatable, dimension(:)     :: bb
   integer          :: status

   character (len=12), dimension(3)           :: real_clock 
   integer(k_i32), dimension(8)               ::  date_time
   integer(k_i32)                               :: timeseed

   integer(k_i32), dimension(10)              ::  num_ran
   integer :: i

!*******************************************************************************
! Execution part
!*******************************************************************************

   call tsl_preamble('test_random_uniform_knuth', 'Zheng Liang', '2009')
   !This program will generate a large amount of integer random numbers
   !in the array bb and put them into different uniform group
   !([0,0.1*2**30], (0.1*2**30, 0.2*2**30], ..., ((0.9*2**30,2**30]). 
   !Then it will check whether each group has nearly the same number
   n = 100000
   allocate (aa(n), stat = status)
   !Initialize the array num_ran (counters)
   do i = 1, 10
      num_ran(i) = 0
   end do
   !Generate the random number array aa and put them into different groups
   !Calculate the time seed
   call date_and_time (real_clock(1), real_clock(2), real_clock(3), date_time)
   timeseed = date_time(8) + date_time(7) * 1000 + date_time(6) * 1000 * 60 + &
             date_time(6) * 1000 * 60 * 60
   !Generate the random number array
   call ran_start_int(timeseed)
   call ran_array_int(aa, n)
   do i = 1, n
      !Put the random numbers in the array aa into suitable group
      if (aa(i) >0 .and. aa(i) <=0.1*2**30) then
         num_ran(1)=num_ran(1)+1
      else if (aa(i) >0.1*2**30 .and. aa(i) <=0.2*2**30) then
         num_ran(2)=num_ran(2)+1
      else if (aa(i) >0.2*2**30 .and. aa(i) <=0.3*2**30) then
         num_ran(3)=num_ran(3)+1
      else if (aa(i) >0.3*2**30 .and. aa(i) <=0.4*2**30) then
         num_ran(4)=num_ran(4)+1
      else if (aa(i) >0.4*2**30 .and. aa(i) <=0.5*2**30) then
         num_ran(5)=num_ran(5)+1
      else if (aa(i) >0.5*2**30 .and. aa(i) <=0.6*2**30) then
         num_ran(6)=num_ran(6)+1
      else if (aa(i) >0.6*2**30 .and. aa(i) <=0.7*2**30) then
         num_ran(7)=num_ran(7)+1
      else if (aa(i) >0.7*2**30 .and. aa(i) <=0.8*2**30) then
         num_ran(8)=num_ran(8)+1
      else if (aa(i) >0.8*2**30 .and. aa(i) <=0.9*2**30) then
         num_ran(9)=num_ran(9)+1
      else if (aa(i) >0.9*2**30 .and. aa(i) <=2**30) then
         num_ran(10)=num_ran(10)+1
      endif
   end do      
   print '()'
   print '(2x, ''Test of the double precision random numbers generator'' )'
   print '(2x, ''The random numbers are divided into 10 groups'' )'
   print '(2x, ''The following is the statistics of the number that'' )'
   print '(2x, ''the random numbers fall into each group'' )'
   print '(2x, ''The group [0, 0.1*2**30]: '',3i7 )',num_ran(1)
   print '(2x, ''The group (0.1*2**30, 0.2*2**30]: '',3i7 )',num_ran(2)
   print '(2x, ''The group (0.2*2**30, 0.3*2**30]: '',3i7 )',num_ran(3)
   print '(2x, ''The group (0.3*2**30, 0.4*2**30]: '',3i7 )',num_ran(4)
   print '(2x, ''The group (0.4*2**30, 0.5*2**30]: '',3i7 )',num_ran(5)
   print '(2x, ''The group (0.5*2**30, 0.6*2**30]: '',3i7 )',num_ran(6)
   print '(2x, ''The group (0.6*2**30, 0.7*2**30]: '',3i7 )',num_ran(7)
   print '(2x, ''The group (0.7*2**30, 0.8*2**30]: '',3i7 )',num_ran(8)
   print '(2x, ''The group (0.8*2**30, 0.9*2**30]: '',3i7 )',num_ran(9)
   print '(2x, ''The group (0.9*2**30,2**30]: '',3i7 )',num_ran(10)
   print '()'
   deallocate (aa,stat = status)
   !This program will generate a large amount of double precision random numbers
   !in the array bb and put them into different uniform group
   !([0,0.1], (0.1,0.2], ..., (0.9,1]). Then it will check whether each group
   !has nearly the same number
   n = 100000
   allocate (bb(n), stat = status)
   !Initialize the array num_ran (counters)
   do i = 1, 10
      num_ran(i) = 0
   end do
   !Generate the random number array bb and put them into different groups
   !Calculate the time seed
   call date_and_time (real_clock(1), real_clock(2), real_clock(3), date_time)
   timeseed = date_time(8) + date_time(7)*1000 + date_time(6) * 1000*60 + &
              date_time(6) * 1000 * 60 * 60
   !Generate the random number array
   call ran_start_dp(timeseed)
   call ran_array_dp(bb, n)   
   do i = 1, n
      !Put the random numbers in the array bb into suitable group
      if (bb(i) >0 .and. bb(i) <=0.1) then
         num_ran(1)=num_ran(1)+1
      else if (bb(i) >0.1 .and. bb(i) <=0.2) then
         num_ran(2)=num_ran(2)+1
      else if (bb(i) >0.2 .and. bb(i) <=0.3) then
         num_ran(3)=num_ran(3)+1
      else if (bb(i) >0.3 .and. bb(i) <=0.4) then
         num_ran(4)=num_ran(4)+1
      else if (bb(i) >0.4 .and. bb(i) <=0.5) then
         num_ran(5)=num_ran(5)+1
      else if (bb(i) >0.5 .and. bb(i) <=0.6) then
         num_ran(6)=num_ran(6)+1
      else if (bb(i) >0.6 .and. bb(i) <=0.7) then
         num_ran(7)=num_ran(7)+1
      else if (bb(i) >0.7 .and. bb(i) <=0.8) then
         num_ran(8)=num_ran(8)+1
      else if (bb(i) >0.8 .and. bb(i) <=0.9) then
         num_ran(9)=num_ran(9)+1
      else if (bb(i) >0.9 .and. bb(i) <=1.0) then
         num_ran(10)=num_ran(10)+1
      endif
   end do      
   print '()'
   print '(2x, ''Test of the double precision random numbers generator'' )'
   print '(2x, ''The random numbers are divided into 10 groups'' )'
   print '(2x, ''The following is the statistics of the number that'' )'
   print '(2x, ''the random numbers fall into each group'' )'
   print '(2x, ''The group [0, 0.1]: '',3i7 )',num_ran(1)
   print '(2x, ''The group (0.1, 0.2]: '',3i7 )',num_ran(2)
   print '(2x, ''The group (0.2, 0.3]: '',3i7 )',num_ran(3)
   print '(2x, ''The group (0.3, 0.4]: '',3i7 )',num_ran(4)
   print '(2x, ''The group (0.4, 0.5]: '',3i7 )',num_ran(5)
   print '(2x, ''The group (0.5, 0.6]: '',3i7 )',num_ran(6)
   print '(2x, ''The group (0.6, 0.7]: '',3i7 )',num_ran(7)
   print '(2x, ''The group (0.7, 0.8]: '',3i7 )',num_ran(8)
   print '(2x, ''The group (0.8, 0.9]: '',3i7 )',num_ran(9)
   print '(2x, ''The group (0.9, 1.0]: '',3i7 )',num_ran(10)
   print '()'
   deallocate (bb,stat = status)
   print '(2x, ''Done.'')'
 
   
   read (*,*)

!*******************************************************************************
! 'end program' statement
!*******************************************************************************
end program test_random_uniform_knuth

