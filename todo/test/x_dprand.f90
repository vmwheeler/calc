program x_dprand

!-------------------------------------------------------------------------------
! Copyright 2009 Regents of the University of Minnesota. All rights reserved.
!
! This file is part of the Fortran Scientific Library (FSL).
!
! FSL is free software: you can redistribute it and/or modify
! it under the terms of the GNU Lesser General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! FSL is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU Lesser General Public License for more details.
!
! You should have received a copy of the GNU Lesser General Public License
! along with FSL.  If not, see <http://www.gnu.org/licenses/>.
!-------------------------------------------------------------------------------

!-------------------------------------------------------------------------------
! Description:
!    This is a driver for the dprand random number generator.
!-------------------------------------------------------------------------------

!-------------------------------------------------------------------------------
! 'use' and 'implicit none' statements
!-------------------------------------------------------------------------------

   use tsl, only: k_i4, k_r8
   use tsl, only: fsl_driver_head, fsl_driver_foot
   use tsl, only: sdprnd, dprand
   use tsl, only: search_vector
   implicit none

!-------------------------------------------------------------------------------
! Declaration constructs
!-------------------------------------------------------------------------------

   real(k_r8), allocatable, dimension(:)    :: limits
   real(k_r8)                               :: mean, rand, sum
   integer(k_i4), allocatable, dimension(:) :: bins
   integer(k_i4)                            :: i, iseed, j, nbins, nrand

!-------------------------------------------------------------------------------
! Execution part
!-------------------------------------------------------------------------------

   call fsl_driver_head('dprand')
   print'(2x, ''Give the seed (an integer between 1 and 9999):'')'
   read(*,*) iseed
   print'()'
   call sdprnd(iseed)
   print'(2x, ''Give the number of random numbers:'')'
   read (*,*) nrand
   print'()'
   print'(2x, ''Give the number of bins:'')'
   read(*,*) nbins
   print'()'
   allocate(bins(nbins))
   allocate(limits(nbins+1))
   do i = 1, nbins+1
      limits(i) = real(i-1, k_r8) / real(nbins, k_r8)
   end do
   sum  = 0
   bins = 0
   do i = 1, nrand
      rand = dprand()
      sum  = sum + rand
      j = search_vector(limits, rand)
      bins(j) = bins(j) + 1
   end do
   mean = sum / nrand
   print'(7x, ''Bin'', 6x, ''Number of random numbers'')'
   do i = 1, nbins
      print'(2x, i8, 11x, i10)', i, bins(i)
   end do
   print'()'
   print'(2x, ''Mean:'', 1x, f22.20)', mean
   deallocate(bins)
   deallocate(limits)
   call fsl_driver_foot()
   stop

!-------------------------------------------------------------------------------
! 'end program' statement
!-------------------------------------------------------------------------------

end program x_dprand
