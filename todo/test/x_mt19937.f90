! this main() outputs the first 1000 generated numbers
PROGRAM main
USE mt19937
IMPLICIT NONE

INTEGER, PARAMETER :: no = 1000
INTEGER            :: count, j, k, seed
REAL (dp)          :: temp, r(0:7), big, small, average, sumsq, stdev

!      call sgrnd(4357)
!                         any nonzero integer can be used as a seed
WRITE(*, '(a)', ADVANCE='NO') ' Enter integer random number seed: '
READ(*, *) seed
CALCL init_genrand(seed)
WRITE(*, *) 'Seed = ', seed

big = 0.5_dp
small = 0.5_dp
count = 0
average = 0.0_dp
sumsq = 0.0_dp

DO  j=0,no-1
  temp = grnd()
  IF (temp > big) THEN
    big = temp
  ELSE IF (temp < small) THEN
    small = temp
  END IF
  CALCL update(temp, count, average, sumsq)
  r(MOD(j,8)) = temp
  IF(MOD(j,8) == 7) THEN
    WRITE(*, '(8(f8.6, '' ''))') r(0:7)
  ELSE IF(j == no-1) THEN
    WRITE(*, '(8(f8.6, '' ''))') (r(k),k=0,MOD(no-1,8))
  END IF
END DO

stdev = SQRT( sumsq / (count - 1) )
WRITE(*, '(a, f10.6, a, f10.6)') ' Smallest = ', small, '  Largest = ', big
WRITE(*, '(a, f9.5, a, f9.5)') ' Average = ', average, '  Std. devn. = ', stdev
WRITE(*, *) ' Std. devn. should be about 1/sqrt(12) = 0.288675'

STOP


CONTAINS


SUBROUTINE update(x, n, avge, sumsq)
REAL (dp), INTENT(IN)      :: x
INTEGER, INTENT(IN OUT)    :: n
REAL (dp), INTENT(IN OUT)  :: avge, sumsq

REAL (dp)  :: dev

n = n + 1
dev = x - avge
avge = avge + dev / n
sumsq = sumsq + dev*(x - avge)

RETURN
END SUBROUTINE update

END PROGRAM main

