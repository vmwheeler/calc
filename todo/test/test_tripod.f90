program test_tripod

!*******************************************************************************
! Copyright (C) 2009 Zheng Liang
! Copyright (C) 2009 Wojciech Lipinski
!
! This file is part of the Thermal Science Library (TSL).
!
! TSL is free software: you can redistribute it and/or modify
! it under the terms of the GNU Lesser General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! TSL is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU Lesser General Public License for more details.
!
! You should have received a copy of the GNU Lesser General Public License
! along with TSL.  If not, see <http://www.gnu.org/licenses/>.
!*******************************************************************************

!*******************************************************************************
! Description:
!    This is a driver for tripod.
!*******************************************************************************

!*******************************************************************************
! 'use' and 'implicit none' statements
!*******************************************************************************

   use tsl, only: k_r64
   use tsl, only: tsl_preamble
   use tsl, only: tripod
   !use tsl, only: k_r64, tsl_preamble, tripod
   implicit none

!*******************************************************************************
! Declaration constructs
!*******************************************************************************

   real(k_r64), dimension(3) :: n
   real(k_r64), dimension(3) :: t1, t2


!*******************************************************************************
! Execution part
!*******************************************************************************

   call tsl_preamble('test_tripod', 'Zheng Liang', '2009')
   !The first basis vector is (1,0,0)
   n = (/1,0,0/)
   print '()'
   print '(2x, ''The first basis vector: ('',\)'
   print '(f7.2\ )', n
   print '(2x, '')'' )'
   print '(2x, ''Analytical solution:'')'
   print '(2x, ''The second basis vector: (0, 1, 0) '')'
   print '(2x, ''The third basis vector: (0, 0, 1) '')'
   print '(2x, ''Solution by tripod:'')'
   call tripod(n, t1, t2)
   print '(2x, ''The second basis vector: ('',\)'
   print '(f9.4\ )', t1
   print '(2x, '')'' )'
   print '(2x, ''The third basis vector: ('',\)'
   print '(f9.4\ )', t2
   print '(2x, '')'' )'
   print '()'
   !The first basis vector is (3,0,4)
   n = (/3,0,4/)
   print '()'
   print '(2x, ''The first basis vector: ('',\)'
   print '(f7.2\ )', n
   print '(2x, '')'' )'
   print '(2x, ''Analytical solution:'')'
   print '(2x, ''The second basis vector: (0, 1, 0) '')'
   print '(2x, ''The third basis vector: (0.8, 0, -0.6) '')'
   print '(2x, ''Solution by tripod:'')'
   call tripod(n, t1, t2)
   print '(2x, ''The second basis vector: ('',\)'
   print '(f9.4\ )', t1
   print '(2x, '')'' )'
   print '(2x, ''The third basis vector: ('',\)'
   print '(f9.4\ )', t2
   print '(2x, '')'' )'
   print '()'
   !The first basis vector is (1,1,1)
   n = (/1,1,1/)
   print '()'
   print '(2x, ''The first basis vector: ('',\)'
   print '(f7.2\ )', n
   print '(2x, '')'' )'
   print '(2x, ''Analytical solution:'')'
   print '(2x, ''The second basis vector: (0.71, -0.71, 0) '')'
   print '(2x, ''The third basis vector: (0.41, 0.41, -0.82) '')'
   print '(2x, ''Solution by tripod:'')'
   call tripod(n, t1, t2)
   print '(2x, ''The second basis vector: ('',\)'
   print '(f9.4\ )', t1
   print '(2x, '')'' )'
   print '(2x, ''The third basis vector: ('',\)'
   print '(f9.4\ )', t2
   print '(2x, '')'' )'
   print '(2x, ''Done.'')'
   print '()'
  
   
   read (*,*)

!*******************************************************************************
! Subprogram part
!*******************************************************************************

!*******************************************************************************
! 'end program' statement
!*******************************************************************************
end program test_tripod
