program test_intersection_line_polygon

!*******************************************************************************
! Copyright (C) 2009 Zheng Liang
! Copyright (C) 2009 Wojciech Lipinski
!
! This file is part of the Thermal Science Library (TSL).
!
! TSL is free software: you can redistribute it and/or modify
! it under the terms of the GNU Lesser General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! TSL is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU Lesser General Public License for more details.
!
! You should have received a copy of the GNU Lesser General Public License
! along with TSL.  If not, see <http://www.gnu.org/licenses/>.
!*******************************************************************************

!*******************************************************************************
! Description:
!    This is a driver for intersection_line_polygon.
!*******************************************************************************

!*******************************************************************************
! 'use' and 'implicit none' statements
!*******************************************************************************

   use tsl_basics_kinds,     only: k_r64, k_lg, k_i32
   use tsl_basics_utils,     only: tsl_preamble
   use tsl_math_analyticalc_geommetry, only: intersection_line_polygon
   !use tsl, only: k_i32, k_r64, tsl_preamble, intersection_line_polygon
   implicit none

!*******************************************************************************
! Declaration constructs
!*******************************************************************************

   real(k_r64),    dimension(3)    :: point, dir
   integer(k_i32)                  ::nv
   real(k_r64), allocatable, dimension(:,:)  :: vertices
   real(k_r64),    dimension(3)    :: points
   logical(k_lg)                    :: flag
   integer(k_i32)                  :: status


!*******************************************************************************
! Execution part
!*******************************************************************************

   call tsl_preamble('test_intersection_line_polygon', 'Zheng Liang', '2009')
   !No case that the line is on the plane will be considered in this program
   !The vertices of the test triangle are (2,0,0), (-2,0,0), (0,4,0)
   allocate (vertices(3,3), stat=status)
   nv = 3
   vertices(1,:) = (/2,0,0/)
   vertices(2,:) = (/-2,0,0/)
   vertices(3,:) = (/0,4,0/)
   print '()'
   print '(2x, ''First test is the triangle:'')'
   print '(2x, ''The vertices of the triangle:'')'
   print '(2x, ''('',\)'
   print '(f7.2\ )', vertices(1,:)
   print '(2x, '')'' )'
   print '(2x, ''('',\)'
   print '(f7.2\ )', vertices(2,:)
   print '(2x, '')'' )'
   print '(2x, ''('',\)'
   print '(f7.2\ )', vertices(3,:)
   print '(2x, '')'' )'
   !The point on the line is (1,1,-1), and direction vector is (0,0,1)
   point = (/1,1,-1/)
   dir = (/0,0,1/)
   print '(2x, ''The point on the line: ('',\)'
   print '(f7.2\ )', point
   print '(2x, '')'' )'
   print '(2x, ''The direction vector: ('',\)'
   print '(f7.2\ )', dir
   print '(2x, '')'' )'
   print '(2x, ''Analytical solution:'')'
   print '(2x, ''The intersecting points: ( 1, 1, 0)'')'
   print '(2x, ''Solution by intersection_line_polygon:'')'
   call intersection_line_polygon(point, dir, nv, vertices, points, flag)    
   if(flag==.false.) then
      print '(2x, ''No intersection'')'
   else
      print '(2x, ''The intersecting points:  ('', \)'
      print '(f9.4\ )', points
      print '(2x, '')'' )'
   end if
   print '()'
   !The point on the line is (1,1,1), and direction vector is (1,1,1)
   point = (/1,1,1/)
   dir = (/1,1,1/)
   print '(2x, ''The point on the line: ('',\)'
   print '(f7.2\ )', point
   print '(2x, '')'' )'
   print '(2x, ''The direction vector: ('',\)'
   print '(f7.2\ )', dir
   print '(2x, '')'' )'
   print '(2x, ''Analytical solution:'')'
   print '(2x, ''The intersecting points: ( 0, 0, 0)'')'
   print '(2x, ''Solution by intersection_line_polygon:'')'
   call intersection_line_polygon(point, dir, nv, vertices, points, flag)    
   if(flag==.false.) then
      print '(2x, ''No intersection'')'
   else
      print '(2x, ''The intersecting points:  ('', \)'
      print '(f9.4\ )', points
      print '(2x, '')'' )'
   end if
   print '()'   
   !The point on the line is (5,5,-1), and direction vector is (0,0,1)
   point = (/5,5,-1/)
   dir = (/0,0,1/)
   print '(2x, ''The point on the line: ('',\)'
   print '(f7.2\ )', point
   print '(2x, '')'' )'
   print '(2x, ''The direction vector: ('',\)'
   print '(f7.2\ )', dir
   print '(2x, '')'' )'
   print '(2x, ''Analytical solution:'')'
   print '(2x, ''No intersection'')'
   print '(2x, ''Solution by intersection_line_polygon:'')'
   call intersection_line_polygon(point, dir, nv, vertices, points, flag)    
   if(flag==.false.) then
      print '(2x, ''No intersection'')'
   else
      print '(2x, ''The intersecting points:  ('', \)'
      print '(f9.4\ )', points
      print '(2x, '')'' )'
   end if
   print '()'

   !The vertices of the test triangle are (0,2,0), (0,-2,0), (0,0,4)
   vertices(1,:) = (/0,2,0/)
   vertices(2,:) = (/0,-2,0/)
   vertices(3,:) = (/0,0,4/)
   print '()'
   print '(2x, ''The vertices of the triangle:'')'
   print '(2x, ''('',\)'
   print '(f7.2\ )', vertices(1,:)
   print '(2x, '')'' )'
   print '(2x, ''('',\)'
   print '(f7.2\ )', vertices(2,:)
   print '(2x, '')'' )'
   print '(2x, ''('',\)'
   print '(f7.2\ )', vertices(3,:)
   print '(2x, '')'' )'
   !The point on the line is (1,1,-1), and direction vector is (0,0,1)
   point = (/1,1,-1/)
   dir = (/0,0,1/)
   print '(2x, ''The point on the line: ('',\)'
   print '(f7.2\ )', point
   print '(2x, '')'' )'
   print '(2x, ''The direction vector: ('',\)'
   print '(f7.2\ )', dir
   print '(2x, '')'' )'
   print '(2x, ''Analytical solution:'')'
   print '(2x, ''No intersection'')'
   print '(2x, ''Solution by intersection_line_polygon:'')'
   call intersection_line_polygon(point, dir, nv, vertices, points, flag)    
   if(flag==.false.) then
      print '(2x, ''No intersection'')'
   else
      print '(2x, ''The intersecting points:  ('', \)'
      print '(f9.4\ )', points
      print '(2x, '')'' )'
   end if
   print '()'
   !The point on the line is (1,1,-1), and direction vector is (1,1,1)
   point = (/1,1,-1/)
   dir = (/1,1,1/)
   print '(2x, ''The point on the line: ('',\)'
   print '(f7.2\ )', point
   print '(2x, '')'' )'
   print '(2x, ''The direction vector: ('',\)'
   print '(f7.2\ )', dir
   print '(2x, '')'' )'
   print '(2x, ''Analytical solution:'')'
   print '(2x, ''No intersection'')'
   print '(2x, ''Solution by intersection_line_polygon:'')'
   call intersection_line_polygon(point, dir, nv, vertices, points, flag)    
   if(flag==.false.) then
      print '(2x, ''No intersection'')'
   else
      print '(2x, ''The intersecting points:  ('', \)'
      print '(f9.4\ )', points
      print '(2x, '')'' )'
   end if
   print '()'
   !The point on the line is (1,1,1), and direction vector is (1,1,1)
   point = (/1,1,1/)
   dir = (/1,1,1/)
   print '(2x, ''The point on the line: ('',\)'
   print '(f7.2\ )', point
   print '(2x, '')'' )'
   print '(2x, ''The direction vector: ('',\)'
   print '(f7.2\ )', dir
   print '(2x, '')'' )'
   print '(2x, ''Analytical solution:'')'
   print '(2x, ''The intersecting points: ( 0, 0, 0)'')'
   print '(2x, ''Solution by intersection_line_polygon:'')'
   call intersection_line_polygon(point, dir, nv, vertices, points, flag)    
   if(flag==.false.) then
      print '(2x, ''No intersection'')'
   else
      print '(2x, ''The intersecting points:  ('', \)'
      print '(f9.4\ )', points
      print '(2x, '')'' )'
   end if
   print '()'
   deallocate (vertices,stat=status)

   !No case that the line is on the plane will be considered in this program
   !The vertices of the test rectangle are (2,0,0), (-2,0,0), (2,2,0), (-2,2,0)
   allocate (vertices(4,3), stat=status)
   nv = 4
   vertices(1,:) = (/2,0,0/)
   vertices(2,:) = (/-2,0,0/)
   vertices(3,:) = (/2,2,0/)
   vertices(4,:) = (/-2,2,0/)
   print '()'
   print '(2x, ''Second test is the rectangle:'')'
   print '(2x, ''The vertices of the rectangle:'')'
   print '(2x, ''('',\)'
   print '(f7.2\ )', vertices(1,:)
   print '(2x, '')'' )'
   print '(2x, ''('',\)'
   print '(f7.2\ )', vertices(2,:)
   print '(2x, '')'' )'
   print '(2x, ''('',\)'
   print '(f7.2\ )', vertices(3,:)
   print '(2x, '')'' )'
   print '(2x, ''('',\)'
   print '(f7.2\ )', vertices(4,:)
   print '(2x, '')'' )'
   !The point on the line is (1,1,-1), and direction vector is (0,0,1)
   point = (/1,1,-1/)
   dir = (/0,0,1/)
   print '(2x, ''The point on the line: ('',\)'
   print '(f7.2\ )', point
   print '(2x, '')'' )'
   print '(2x, ''The direction vector: ('',\)'
   print '(f7.2\ )', dir
   print '(2x, '')'' )'
   print '(2x, ''Analytical solution:'')'
   print '(2x, ''The intersecting points: ( 1, 1, 0)'')'
   print '(2x, ''Solution by intersection_line_polygon:'')'
   call intersection_line_polygon(point, dir, nv, vertices, points, flag)    
   if(flag==.false.) then
      print '(2x, ''No intersection'')'
   else
      print '(2x, ''The intersecting points:  ('', \)'
      print '(f9.4\ )', points
      print '(2x, '')'' )'
   end if
   print '()'
   !The point on the line is (1,1,1), and direction vector is (1,1,1)
   point = (/1,1,1/)
   dir = (/1,1,1/)
   print '(2x, ''The point on the line: ('',\)'
   print '(f7.2\ )', point
   print '(2x, '')'' )'
   print '(2x, ''The direction vector: ('',\)'
   print '(f7.2\ )', dir
   print '(2x, '')'' )'
   print '(2x, ''Analytical solution:'')'
   print '(2x, ''The intersecting points: ( 0, 0, 0)'')'
   print '(2x, ''Solution by intersection_line_polygon:'')'
   call intersection_line_polygon(point, dir, nv, vertices, points, flag)    
   if(flag==.false.) then
      print '(2x, ''No intersection'')'
   else
      print '(2x, ''The intersecting points:  ('', \)'
      print '(f9.4\ )', points
      print '(2x, '')'' )'
   end if
   print '()'   
   !The point on the line is (5,5,-1), and direction vector is (0,0,1)
   point = (/5,5,-1/)
   dir = (/0,0,1/)
   print '(2x, ''The point on the line: ('',\)'
   print '(f7.2\ )', point
   print '(2x, '')'' )'
   print '(2x, ''The direction vector: ('',\)'
   print '(f7.2\ )', dir
   print '(2x, '')'' )'
   print '(2x, ''Analytical solution:'')'
   print '(2x, ''No intersection'')'
   print '(2x, ''Solution by intersection_line_polygon:'')'
   call intersection_line_polygon(point, dir, nv, vertices, points, flag)    
   if(flag==.false.) then
      print '(2x, ''No intersection'')'
   else
      print '(2x, ''The intersecting points:  ('', \)'
      print '(f9.4\ )', points
      print '(2x, '')'' )'
   end if
   print '()'   

   !The vertices of the test triangle are (0,2,0), (0,-2,0), (0,2,2), (0,-2,2)
   vertices(1,:) = (/0,2,0/)
   vertices(2,:) = (/0,-2,0/)
   vertices(3,:) = (/0,2,2/)
   vertices(4,:) = (/0,-2,2/)
   print '()'
   print '(2x, ''The vertices of the triangle:'')'
   print '(2x, ''('',\)'
   print '(f7.2\ )', vertices(1,:)
   print '(2x, '')'' )'
   print '(2x, ''('',\)'
   print '(f7.2\ )', vertices(2,:)
   print '(2x, '')'' )'
   print '(2x, ''('',\)'
   print '(f7.2\ )', vertices(3,:)
   print '(2x, '')'' )'
   print '(2x, ''('',\)'
   print '(f7.2\ )', vertices(4,:)
   print '(2x, '')'' )'
   !The point on the line is (1,1,-1), and direction vector is (0,0,1)
   point = (/1,1,-1/)
   dir = (/0,0,1/)
   print '(2x, ''The point on the line: ('',\)'
   print '(f7.2\ )', point
   print '(2x, '')'' )'
   print '(2x, ''The direction vector: ('',\)'
   print '(f7.2\ )', dir
   print '(2x, '')'' )'
   print '(2x, ''Analytical solution:'')'
   print '(2x, ''No intersection'')'
   print '(2x, ''Solution by intersection_line_polygon:'')'
   call intersection_line_polygon(point, dir, nv, vertices, points, flag)    
   if(flag==.false.) then
      print '(2x, ''No intersection'')'
   else
      print '(2x, ''The intersecting points:  ('', \)'
      print '(f9.4\ )', points
      print '(2x, '')'' )'
   end if
   print '()'
   !The point on the line is (1,1,-1), and direction vector is (1,1,1)
   point = (/1,1,-1/)
   dir = (/1,1,1/)
   print '(2x, ''The point on the line: ('',\)'
   print '(f7.2\ )', point
   print '(2x, '')'' )'
   print '(2x, ''The direction vector: ('',\)'
   print '(f7.2\ )', dir
   print '(2x, '')'' )'
   print '(2x, ''Analytical solution:'')'
   print '(2x, ''No intersection'')'
   print '(2x, ''Solution by intersection_line_polygon:'')'
   call intersection_line_polygon(point, dir, nv, vertices, points, flag)    
   if(flag==.false.) then
      print '(2x, ''No intersection'')'
   else
      print '(2x, ''The intersecting points:  ('', \)'
      print '(f9.4\ )', points
      print '(2x, '')'' )'
   end if
   print '()'
   !The point on the line is (1,1,1), and direction vector is (1,1,1)
   point = (/1,1,1/)
   dir = (/1,1,1/)
   print '(2x, ''The point on the line: ('',\)'
   print '(f7.2\ )', point
   print '(2x, '')'' )'
   print '(2x, ''The direction vector: ('',\)'
   print '(f7.2\ )', dir
   print '(2x, '')'' )'
   print '(2x, ''Analytical solution:'')'
   print '(2x, ''The intersecting points: ( 0, 0, 0)'')'
   print '(2x, ''Solution by intersection_line_polygon:'')'
   call intersection_line_polygon(point, dir, nv, vertices, points, flag)    
   if(flag==.false.) then
      print '(2x, ''No intersection'')'
   else
      print '(2x, ''The intersecting points:  ('', \)'
      print '(f9.4\ )', points
      print '(2x, '')'' )'
   end if
   print '()'
   deallocate (vertices,stat=status)
   
                  
   print '(2x, ''Done.'')'
   print '()'

  
   
   read (*,*)

!*******************************************************************************
! 'end program' statement
!*******************************************************************************
end program test_intersection_line_polygon