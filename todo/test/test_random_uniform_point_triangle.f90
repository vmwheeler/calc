program test_random_uniform_point_triangle

!*******************************************************************************
! Copyright (C) 2009 Zheng Liang
! Copyright (C) 2009 Wojciech Lipinski
!
! This file is part of the Thermal Science Library (TSL).
!
! TSL is free software: you can redistribute it and/or modify
! it under the terms of the GNU Lesser General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! TSL is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU Lesser General Public License for more details.
!
! You should have received a copy of the GNU Lesser General Public License
! along with TSL.  If not, see <http://www.gnu.org/licenses/>.
!*******************************************************************************

!*******************************************************************************
! Description:
!    This is a driver for random_uniform_point_triangle.
!*******************************************************************************

!*******************************************************************************
! 'use' and 'implicit none' statements
!*******************************************************************************

   use tsl_basics_kinds, only: k_i32, k_r64
   use tsl_basics_utils, only: tsl_preamble
   use tsl_math_analyticalc_geommetry, only: random_uniform_point_triangle
   use tsl_math_random_uniform_maclaren, only: sdprnd, dprand
   !use tsl, only: k_i32, k_r64, tsl_preamble, random_uniform_point_triangle
   implicit none

!*******************************************************************************
! Declaration constructs
!*******************************************************************************

   real(k_r64)                             :: rn1, rn2
   real(k_r64), dimension(3,3)           :: vertices
   real(k_r64), dimension(3)             :: point

   integer                               :: n 
   character (len=12), dimension(3)    :: real_clock 
   integer(k_i32), dimension(8)         ::  date_time
   integer(k_i32)                        :: timeseed

   integer(k_i32), dimension(3)        ::  num_ran
   integer                              :: i
   real(k_r64)                            :: area12, area23, area31

!*******************************************************************************
! Execution part
!*******************************************************************************

   call tsl_preamble('test_random_uniform_point_triangle',&
                      'Zheng Liang', '2009')
   !This program will test the uniformity of the random points generated in the 
   !triangle. The uniformity is judged by the area among the random point and 
   !any other two vertices. If this area is less than other two areas, we  say 
   !that this random point is near that side constituted by the two vertices.
   !In the program, the number of random points near each side is counted
   !The vertices of the test triangle are (2,0,0), (-2,0,0), (0,4,0)
   vertices(1,:) = (/2,0,0/)
   vertices(2,:) = (/-2,0,0/)
   vertices(3,:) = (/0,4,0/)
   print '()'
   print '(2x, ''The vertices of the triangle:'')'
   print '(2x, ''('',\)'
   print '(f7.2\ )', vertices(1,:)
   print '(2x, '')'' )'
   print '(2x, ''('',\)'
   print '(f7.2\ )', vertices(2,:)
   print '(2x, '')'' )'
   print '(2x, ''('',\)'
   print '(f7.2\ )', vertices(3,:)
   print '(2x, '')'' )'
   n = 100000
   !Initialize the array num_ran (counters)
   do i = 1, 3
      num_ran(i) = 0
   end do
   !Generate n random pointss and put them into different groups
   do i=1,n
      !Calculate the time seed
      call date_and_time(real_clock(1), real_clock(2), real_clock(3), date_time)
      timeseed = date_time(8) + date_time(7) * 1000+ date_time(6) * 1000 * 60 &
                 +date_time(6) * 1000 * 60 * 60
      !Generate the random number
      call sdprnd(timeseed)
      rn1 = dprand()
       !Calculate the time seed
      call date_and_time(real_clock(1), real_clock(2), real_clock(3), date_time)
      timeseed = date_time(8) + date_time(7) * 1000+ date_time(6) * 1000 * 60 &
                 +date_time(6) * 1000 * 60 * 60
      !Generate the random number
      call sdprnd(timeseed)
      rn2 = dprand()
      !Generate the random point
      point=random_uniform_point_triangle(rn1, rn2, vertices)
      !Calculate the area among the random point and vertices 1 and 2 and 3
      area12 = abs((vertices(1,1) * vertices(2,2) + vertices(2,1) * point(2)+ &
               point(1) * vertices(1,2) - vertices(1,1) * point(2) - &
               vertices(2,1) * vertices(1,2) - point(1) * vertices(2,2)))/2   
      area23 = abs((point(1) * vertices(2,2) + vertices(2,1) * vertices(3,2)+&
               vertices(3,1) * point(2) - point(1) * vertices(3,2) - &
               vertices(2,1) * point(2) - vertices(3,1) * vertices(2,2)))/2   
      area31 = abs((vertices(1,1) * point(2) + point(1) * vertices(3,2)+&
               vertices(3,1) * vertices(1,2) - vertices(1,1) * vertices(3,2)- &
               point(1) * vertices(1,2) - vertices(3,1) * point(2)))/2   
      !Put it into suitable group
      if (area12<area23 .and. area12<area31) then
         num_ran(1)=num_ran(1)+1
      else if (area23<area12 .and. area23<area31) then
         num_ran(2)=num_ran(2)+1
      else if (area31<area23 .and. area31<area12) then
         num_ran(3)=num_ran(3)+1
      endif
   end do
   print '(2x, ''Solution by random_uniform_point_triangle:'')'
   print '(2x, ''Number of random points near the side:'')'
   print '(2x, ''Side by '')'
   print '(2x, ''('',\)'
   print '(f7.2\ )', vertices(1,:)
   print '(2x, '')'' )'
   print '(2x, ''('',\)'
   print '(f7.2\ )', vertices(2,:)
   print '(2x, '')'' )'
   print '(2x, ''Number: '',2i5 )',num_ran(1)
   print '(2x, ''Side by '')'
   print '(2x, ''('',\)'
   print '(f7.2\ )', vertices(2,:)
   print '(2x, '')'' )'
   print '(2x, ''('',\)'
   print '(f7.2\ )', vertices(3,:)
   print '(2x, '')'' )'
   print '(2x, ''Number: '',2i5 )',num_ran(2)
   print '(2x, ''Side by '')'
   print '(2x, ''('',\)'
   print '(f7.2\ )', vertices(3,:)
   print '(2x, '')'' )'
   print '(2x, ''('',\)'
   print '(f7.2\ )', vertices(1,:)
   print '(2x, '')'' )'
   print '(2x, ''Number: '',2i5 )',num_ran(3)
   print '(2x, ''Done.'')'
   print '()'

  
   
   read (*,*)

!*******************************************************************************
! Subprogram part
!*******************************************************************************

!*******************************************************************************
! 'end program' statement
!*******************************************************************************
end program test_random_uniform_point_triangle