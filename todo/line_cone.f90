subroutine line_cone( PoL,   DirL,   C1,   C2,   r1,r2, SurfI,xyzI)

!*******************************************************************************
! Copyright (C) 2008 Hansmartin Friess
!
! This file is part of Thermal Science Library (TSL).
!
! TSL is free software: you can redistribute it and/or modify
! it under the terms of the GNU Lesser General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! TSL is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU Lesser General Public License for more details.
!
! You should have received a copy of the GNU Lesser General Public License
! along with TSL.  If not, see <http://www.gnu.org/licenses/>.
!*******************************************************************************

!*******************************************************************************
! Description:
!   Intersect an infinite, directed line with the surface of a finite cone.
!   The surface is considered to consist of two disks (Kreisscheiben)
!   and a "lateral wall".

!Input:
!   PoL     "Point on Line", i.e. an arbitrary point on the infinite line to be considered

!   DirL    direction of the infinite line (does not need to be normalized)

!   C1, C2  the centers of the two confining disks

!   r1      radius of the disk containing C1

!   r2      radius of the disk containing C2

!Output:
!   SurfI   Indicators for the surfaces containing the intersection points.
!           Possible values are:
!              0 = no intersection
!              1 = disk containing C1
!              2 = disk containing C2
!              3 = lateral wall
!           SurfI(1), SurfI(2) are either both 0, or both different from 0.
!           Surf(1).eq.0 implies that no intersection exists.

!           The (pathological) cases of only one intersection point or an
!           infinit number of intersection points are reported as
!           "no intersection", i.e. SurfI(1:2)=0.

!   xyzI    Coordinates of the intersection points. They are ordered according
!           to the direction DirL. This means that when moving along the
!           infinte line in direction DirL, the point xyzI(1,:) is encountered
!           before xyzI(2,:).
!*******************************************************************************

!*******************************************************************************
! Use and implicit none statements
!*******************************************************************************

   use tsl_type, only: DP, I2B
   use tsl_math, only: qroots
   implicit none

!*******************************************************************************
! Dummy argument declaration constructs
!*******************************************************************************

   real(DP),intent(in):: PoL(3),DirL(3),C1(3),C2(3),r1,r2
   integer(I2B),intent(out):: SurfI(2)
   real(DP),intent(out):: xyzI(2,3)

!*******************************************************************************
! Local declaration constructs
!*******************************************************************************

   real(DP) E(3),F(3),G(3),Y(3),fe,ge,h,x,lam(2),a,b,c,r(2),zz(2),ff,gg,fg,u,v
   integer(I2B) i, s(2)
   logical flag

!*******************************************************************************
! Execution part
!*******************************************************************************

!Compute some useful parameters.
   E=C2-C1
   h=SQRT(SUM(E**2))
   E=E/h
   F=PoL-C1
   G=DirL
   fe=SUM(F*E)
   ge=SUM(G*E)
   fg=SUM(F*G)
   ff=SUM(F**2)
   gg=SUM(G**2)

   s(:)=0
!Consider intersections with confining disks.
   if(ge .ne. 0) then
      r = [r1, r2]
      do i=1,2
         lam(i)=((i-1)*h - fe)/ge
         Y=F+lam(i)*G
         if(SUM((Y-SUM(Y*E)*E)**2) .lt. r(i)**2) s(i)=i
      enddo
      if(lam(1) .gt. lam(2)) then
         i=  s(1);   s(1)=  s(2);   s(2)=i
         x=lam(1); lam(1)=lam(2); lam(2)=x
      endif
   endif

!If we have already found 2 intersections, then we are finished;
!otherwise, consider intersections with lateral wall.
   if(s(1).eq.0 .or. s(2).eq.0) then
      u=r1 + fe*(r2-r1)/h
      v=ge*(r2-r1)/h

      a=gg - ge**2 - v**2
      b=2*(fg - fe*ge - u*v)
      c=ff - fe**2 - u**2
      call qRoots(a,b,c,zz,flag)
      if(flag) then
         do i=1,2
            if(s(i).eq.0) then
               lam(i)=zz(i)
               x=SUM((F+lam(i)*G)*E)
               if(x.gt.0 .and. x.lt.h) s(i)=3
            endif
         enddo
      endif
   endif

!Assign values to ouput arguments.
   if(s(1).eq.0 .or. s(2).eq.0) then
      SurfI=0
      xyzI=0
   else
      SurfI=s
      !if(lam(1).ge.lam(2)) stop 'crashing in vector_cone'
      do i=1,2
         xyzI(i,:)=PoL(:)+lam(i)*DirL(:)
      enddo
   endif

!*******************************************************************************
! End subroutine statement
!*******************************************************************************

end subroutine line_cone
