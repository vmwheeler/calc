subroutine intersection_line_cone(point, dir, c1, c2, r1, r2, &
                                  surface, points)

!*******************************************************************************
! Copyright (C) 2007 Hansmartin friess
! Copyright (C) 2009 Wojciech Lipinski
!
! This file is part of the Thermal Science Library (TSL).
!
! TSL is free software: you can redistribute it and/or modify
! it under the terms of the gNU Lesser general Public License as published by
! the free Software foundation, either version 3 of the License, or
! (at your option) any later version.
!
! TSL is distributed in the hope that it will be useful,
! but WITHOUT ANy WARRANTy; without even the implied warranty of
! MERCHANTABILITy or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU Lesser general Public License for more details.
!
! You should have received a copy of the gNU Lesser general Public License
! along with TSL.  If not, see <http://www.gnu.org/licenses/>.
!*******************************************************************************

!*******************************************************************************
! Description:
!    Returns coordinates of an intersection point between an infinite line
!    and a truncated cone consisting of two parallel disks and a lateral wall.
!
! Input:
!    point_line   Point on the line
!    dir          Direction of the line (does not need to be normalized)
!    c1, c2       Centers of the two disks
!    r1           Radius of the disk containing c1
!    r2           Radius of the disk containing c2
!
! Output:
!    surface      Vector containing indices of the surfaces with the intersection
!                 points. The indices can take the following values:
!                 0 = no intersection
!                 1 = disk containing c1
!                 2 = disk containing c2
!                 3 = lateral wall
!                 Either both surface(1) and surface(2) are equal to 0 or
!                 both surface(1) and surface(2) are different from 0.
!                 The pathological cases of only one intersection point or an
!                 infinit number of intersection points are reported as
!                 "no intersection", i.e. surface(1) and surface(2)
!                 are equal to 0.
!    int_points   Intersection points ordered along direction dir
!*******************************************************************************

!*******************************************************************************
! 'use' and 'implicit none' statements
!*******************************************************************************

   use tsl_basics_kinds,     only: k_i16, k_r64, k_lg
   use tsl_math_polynomials, only: solve_quadratic
   implicit none

!*******************************************************************************
! Dummy argument declaration constructs
!*******************************************************************************

   real(k_r64),    dimension(3),   intent(in)  :: point, dir, c1, c2
   real(k_r64),                    intent(in)  :: r1, r2
   integer(k_i16), dimension(2),   intent(out) :: surface
   real(k_r64),    dimension(2,3), intent(out) :: points

!*******************************************************************************
! Local declaration constructs
!*******************************************************************************

   real(k_r64),    dimension(3) :: e, f, g, y
   real(k_r64),    dimension(2) :: lam, r, s, zz
   real(k_r64)                  :: a, b, c, fe, ff, fg, ge, gg, h, u, v, x
   integer(k_i16)               :: i
   logical(k_lg)                :: flag

!*******************************************************************************
! Execution part
!*******************************************************************************

   ! Compute some useful parameters

   e  = c2-c1
   h  = sqrt(sum(e**2))
   e  = e/h
   f  = point-c1
   g  = dir
   fe = sum(f*e)
   ge = sum(g*e)
   fg = sum(f*g)
   ff = sum(f**2)
   gg = sum(g**2)

   s(:) = 0

   ! Consider intersections with confining disks

   if (abs(ge) > 1e-8) then  !ge /= 0
      r = (/ r1, r2 /)
      do i = 1, 2
         lam(i) = ((i-1)*h - fe)/ge
         y      = f+lam(i)*g
         if (sum((y-sum(y*e)*e)**2) < r(i)**2) s(i) = i
      end do
      if (lam(1) > lam(2)) then
         i      = s(1)
         s(1)   = s(2)
         s(2)   = i
         x      = lam(1)
         lam(1) = lam(2)
         lam(2) = x
      end if
   end if

   ! If 2 intersections are found, then computations are finished.
   ! Otherwise, consider intersections with lateral wall.

   if (abs(s(1)) < 1e-8 .or. abs(s(2)) < 1e-8) then  !s(1) == 0 .or. s(2) == 0
      u = r1 + fe*(r2-r1)/h
      v = ge*(r2-r1)/h
      a = gg - ge**2 - v**2
      b = 2*(fg - fe*ge - u*v)
      c = ff - fe**2 - u**2
      call solve_quadratic(a, b, c, zz, flag)
      if (flag .eqv. .true.) then
         do i = 1, 2
            if (abs(s(i)) < 1e-8) then     !s(i) == 0
               lam(i) = zz(i)
               x      = sum((f+lam(i)*g)*e)
               if (x > 0 .and. x < h) s(i) = 3
            end if
         end do
      end if
   end if

   ! Assign values to ouput arguments

   if (abs(s(1)) < 1e-8 .or. abs(s(2)) < 1e-8) then !s(1) == 0 .or. s(2) == 0
      surface = 0
      points  = 0
   else
      surface = s
      do i = 1, 2
         points(i,:) = point(:) + lam(i)*dir(:)
      end do
   end if

!*******************************************************************************
! 'end subroutine' statement
!*******************************************************************************

end subroutine intersection_line_cone
