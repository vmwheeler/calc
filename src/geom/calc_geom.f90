module calc_geom

!-------------------------------------------------------------------------------
! This file is part of the Computational Algorithm Library Collection (CALC).
!
! Copyright 2012 The CALC Team
!
! CALC is free software: you can redistribute it and/or modify
! it under the terms of the GNU Lesser General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! CALC is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU Lesser General Public License for more details.
!
! You should have received a copy of the GNU Lesser General Public License
! along with CALC.  If not, see <http://www.gnu.org/licenses/>.
!-------------------------------------------------------------------------------

!-------------------------------------------------------------------------------
! Begin specification part
!-------------------------------------------------------------------------------

  ! -- Use association --

  ! -- Null mapping --

  implicit none

  ! -- Accessibility --

  private
  public :: create_cartesian
  public :: cross_product
  public :: distance_points
  public :: end_point
  public :: int_line_cuboid
  public :: int_line_cylinder_finite
  public :: int_line_cylinder_infinite
  public :: int_line_ellipsoid
  public :: int_line_parallelepiped
  public :: int_line_parallelogram
  public :: int_line_plane
  public :: int_line_rectangle
  public :: int_line_sphere
  public :: normal_ellipsoid
  public :: rotation_matrix
  public :: rot_cartesian_coord_1
  public :: rot_cartesian_coord_2
  public :: tripod
  public :: unit_vector
  public :: vector_length
  public :: vector_norm_euclidean
  public :: vector_norm_p
  public :: vector_norm_supremum
  public :: vector_norm_sum

  ! -- Derived type definitions --

  ! -- Type declarations --

  ! -- Interface blocks --

  interface
    pure subroutine create_cartesian(a, iaxis, ei, ej, ek, ierr)
      use calc_type, only: k_i1, k_r8
      implicit none
      real    (kind = k_r8), dimension (3), intent (in)  :: a
      integer (kind = k_i1),                intent (in)  :: iaxis
      real    (kind = k_r8), dimension (3), intent (out) :: ei, ej, ek
      integer (kind = k_i1),                intent (out) :: ierr
    end subroutine create_cartesian
  end interface

  interface
    pure function cross_product(a, b) result(c)
      use calc_type, only: k_r8
      implicit none
      real (kind = k_r8), dimension (3), intent (in) :: a, b
      real (kind = k_r8), dimension (3)              :: c
    end function cross_product
  end interface

  interface
    pure function distance_points(r1, r2) result (d)
      use calc_type, only: k_r8
      implicit none
      real (kind = k_r8), dimension (3), intent (in) :: r1, r2
      real (kind = k_r8)                             :: d
    end function distance_points
  end interface

  interface
    pure function end_point (r1, s0, d) result(r2)
      use calc_type, only: k_r8
      implicit none
      real (kind = k_r8), dimension (3), intent (in) :: r1, s0
      real (kind = k_r8),                intent (in) :: d
      real (kind = k_r8), dimension (3)              :: r2
    end function end_point
  end interface

  interface
    pure subroutine int_line_cuboid(r1, s, r2, a, b, c, ri, isurf, ierr)
      use calc_type, only: k_i1, k_r8
      implicit none
      real    (kind = k_r8), dimension (3),    intent (in)  :: r1, s
      real    (kind = k_r8), dimension (3),    intent (in)  :: r2, a, b, c
      real    (kind = k_r8), dimension (2, 3), intent (out) :: ri
      integer (kind = k_i1), dimension (2),    intent (out) :: isurf
      integer (kind = k_i1),                   intent (out) :: ierr
    end subroutine int_line_cuboid
  end interface

  interface
    pure subroutine int_line_cylinder_infinite(r1, s1, r2, s2, radius, ri, ierr)
      use calc_type, only: k_i1, k_i2, k_r8
      implicit none
      real    (kind = k_r8), dimension (3),   intent (in)  :: r1, s1
      real    (kind = k_r8), dimension (3),   intent (in)  :: r2, s2
      real    (kind = k_r8),                  intent (in)  :: radius
      real    (kind = k_r8), dimension (2,3), intent (out) :: ri
      integer (kind = k_i1),                  intent (out) :: ierr
    end subroutine int_line_cylinder_infinite
  end interface

  interface
    pure subroutine int_line_cylinder_finite(r1, s, r2, r3, radius, ri, &
                                                                    isurf, ierr)
      use calc_type, only: k_i1, k_i2, k_r8
      implicit none
      real    (kind = k_r8), dimension (3),   intent (in)  :: r1, s
      real    (kind = k_r8), dimension (3),   intent (in)  :: r2, r3
      real    (kind = k_r8),                  intent (in)  :: radius
      integer (kind = k_i1), dimension (2),   intent (out) :: isurf
      real    (kind = k_r8), dimension (2,3), intent (out) :: ri
      integer (kind = k_i1),                  intent (out) :: ierr
    end subroutine int_line_cylinder_finite
  end interface

  interface
    pure subroutine int_line_ellipsoid(r1, s1, r2, spaxes, ri, ierr)
      use calc_type, only: k_i1, k_r8
      implicit none
      real    (kind = k_r8), dimension (3),   intent (in)  :: r1, s1, r2, spaxes
      real    (kind = k_r8), dimension (2,3), intent (out) :: ri
      integer (kind = k_i1),                  intent (out) :: ierr
    end subroutine int_line_ellipsoid
  end interface

  interface
    pure subroutine int_line_parallelepiped(r1, s, r2, a, b, c, ri, isurf, ierr)
      use calc_type, only: k_i1, k_r8
      implicit none
      real    (kind = k_r8), dimension (3),    intent (in)  :: r1, s
      real    (kind = k_r8), dimension (3),    intent (in)  :: r2, a, b, c
      real    (kind = k_r8), dimension (2, 3), intent (out) :: ri
      integer (kind = k_i1), dimension (2),    intent (out) :: isurf
      integer (kind = k_i1),                   intent (out) :: ierr
    end subroutine int_line_parallelepiped
  end interface

  interface
    pure subroutine int_line_parallelogram(r1, s, r2, a, b, ri, ierr)
      use calc_type, only: k_i1, k_r8
      implicit none
      real    (kind = k_r8), dimension (3), intent (in)  :: r1, s
      real    (kind = k_r8), dimension (3), intent (in)  :: r2, a, b
      real    (kind = k_r8), dimension (3), intent (out) :: ri
      integer (kind = k_i1),                intent (out) :: ierr
    end subroutine int_line_parallelogram
  end interface

  interface
    pure subroutine int_line_plane(r1, s0, r2, n0, ri, ierr)
      use calc_type, only: k_i1, k_r8
      implicit none
      real    (kind = k_r8), dimension (3), intent (in)  :: r1, r2
      real    (kind = k_r8), dimension (3), intent (in)  :: n0, s0
      real    (kind = k_r8), dimension (3), intent (out) :: ri
      integer (kind = k_i1),                intent (out) :: ierr
    end subroutine int_line_plane
  end interface

  interface
    pure subroutine int_line_rectangle(r1, s, r2, a, b, ri, ierr)
      use calc_type, only: k_i1, k_r8
      implicit none
      real    (kind = k_r8), dimension (3), intent (in)  :: r1, s
      real    (kind = k_r8), dimension (3), intent (in)  :: r2, a, b
      real    (kind = k_r8), dimension (3), intent (out) :: ri
      integer (kind = k_i1),                intent (out) :: ierr
    end subroutine int_line_rectangle
  end interface

  interface
    pure subroutine int_line_sphere(r1, s0, r2, radius, ri, ierr)
      use calc_type, only: k_i1, k_r8
      implicit none
      real    (kind = k_r8), dimension (3),   intent (in)  :: r1, r2
      real    (kind = k_r8), dimension (3),   intent (in)  :: s0
      real    (kind = k_r8),                  intent (in)  :: radius
      real    (kind = k_r8), dimension (2,3), intent (out) :: ri 
      integer (kind = k_i1),                  intent (out) :: ierr
    end subroutine int_line_sphere
  end interface

  interface
    pure subroutine normal_ellipsoid(r1, r2, spaxes, normal, ierr)
      use calc_type, only: k_i1, k_r8
      implicit none
      real    (kind = k_r8), dimension (3), intent (in)  :: r1, r2, spaxes
      real    (kind = k_r8), dimension (3), intent (out) :: normal 
      integer (kind = k_i1),                intent (out) :: ierr
    end subroutine normal_ellipsoid
  end interface

  interface
    pure subroutine rotation_matrix(x1, y1, z1, x2, y2, z2, r)
      use calc_type, only: k_r8
      implicit none
       real (kind = k_r8), dimension (3),    intent (in)  :: x1, y1, z1
       real (kind = k_r8), dimension (3),    intent (in)  :: x2, y2, z2
       real (kind = k_r8), dimension (3, 3), intent (out) :: r
    end subroutine rotation_matrix
  end interface

  interface
    pure subroutine rot_cartesian_coord_1(x1, y1, z1, x2, y2, z2, r2, r1, ierr)
      use calc_type, only: k_i1, k_r8
      implicit none
      real    (kind = k_r8), dimension (3), intent (in)  :: x1, y1, z1
      real    (kind = k_r8), dimension (3), intent (in)  :: x2, y2, z2
      real    (kind = k_r8), dimension (3), intent (in)  :: r2
      real    (kind = k_r8), dimension (3), intent (out) :: r1
      integer (kind = k_i1),                intent (out) :: ierr
    end subroutine rot_cartesian_coord_1
  end interface

  interface
    pure subroutine rot_cartesian_coord_2(x1, y1, z1, x2, y2, z2, r1, r2, ierr)
      use calc_type, only: k_i1, k_r8
      implicit none
      real    (kind = k_r8), dimension (3), intent (in)  :: x1, y1, z1
      real    (kind = k_r8), dimension (3), intent (in)  :: x2, y2, z2
      real    (kind = k_r8), dimension (3), intent (in)  :: r1
      real    (kind = k_r8), dimension (3), intent (out) :: r2
      integer (kind = k_i1),                intent (out) :: ierr
    end subroutine rot_cartesian_coord_2
  end interface

  interface
    pure subroutine tripod(a, ei, ej, ek, ierr)
      use calc_type, only: k_i1, k_r8
      implicit none
      real    (kind = k_r8), dimension (3), intent (in)  :: a
      real    (kind = k_r8), dimension (3), intent (out) :: ei, ej, ek
      integer (kind = k_i1),                intent (out) :: ierr
    end subroutine tripod
  end interface

  interface
    pure subroutine unit_vector(a, a0, ierr)
      use calc_type, only: k_i1, k_r8
      implicit none
      real    (kind = k_r8), dimension(:), intent (in)  :: a
      real    (kind = k_r8), dimension(:), intent (out) :: a0
      integer (kind = k_i1),               intent (out) :: ierr
    end subroutine unit_vector
  end interface

  interface
    pure function vector_length(a) result (length)
      use calc_type, only: k_r8
      implicit none
      real (kind = k_r8), dimension (:), intent (in) :: a
      real (kind = k_r8)                             :: length
    end function vector_length
  end interface

  interface
    pure subroutine vector_norm_euclidean(x, norm, ierr)
      use calc_type, only: k_i1, k_r8
      implicit none
      real    (kind = k_r8), dimension (:), intent (in)  :: x
      real    (kind = k_r8), dimension (:), intent (out) :: norm
      integer (kind = k_i1),                intent (out) :: ierr
    end subroutine vector_norm_euclidean
  end interface

  interface
    pure subroutine vector_norm_supremum(x, norm, ierr)
      use calc_type, only: k_i1, k_r8
      implicit none
      real    (kind = k_r8), dimension (:), intent (in)  :: x
      real    (kind = k_r8), dimension (:), intent (out) :: norm
      integer (kind = k_i1),                intent (out) :: ierr
    end subroutine vector_norm_supremum
  end interface

  interface
    pure subroutine vector_norm_sum(x, norm, ierr)
      use calc_type, only: k_i1, k_r8
      implicit none
      real    (kind = k_r8), dimension (:), intent (in)  :: x
      real    (kind = k_r8), dimension (:), intent (out) :: norm
      integer (kind = k_i1),                intent (out) :: ierr
    end subroutine vector_norm_sum
  end interface

  interface
    pure subroutine vector_norm_p(x, p, norm, ierr)
      use calc_type, only: k_i1, k_r8
      implicit none
      real    (kind = k_r8), dimension (:), intent (in)  :: x
      real    (kind = k_r8),                intent (in)  :: p
      real    (kind = k_r8), dimension (:), intent (out) :: norm
      integer (kind = k_i1),                intent (out) :: ierr
    end subroutine vector_norm_p
  end interface

!-------------------------------------------------------------------------------
! End specification part
!-------------------------------------------------------------------------------

!-------------------------------------------------------------------------------
! Begin subprogram part
!-------------------------------------------------------------------------------

!-------------------------------------------------------------------------------
! End subprogram part
!-------------------------------------------------------------------------------

end module calc_geom
