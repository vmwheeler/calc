pure subroutine int_line_plane(r1, s0, r2, n0, ri, ierr)

!-------------------------------------------------------------------------------
! This file is part of the Computational Algorithm Library Collection (CALC).
!
! Copyright 2012 The CALC Team
!
! CALC is free software: you can redistribute it and/or modify
! it under the terms of the GNU Lesser General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! CALC is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU Lesser General Public License for more details.
!
! You should have received a copy of the GNU Lesser General Public License
! along with CALC.  If not, see <http://www.gnu.org/licenses/>.
!-------------------------------------------------------------------------------

!-------------------------------------------------------------------------------
! Begin specification part
!-------------------------------------------------------------------------------

  ! -- Use association --

  use calc_type,  only: k_i1, k_r8
  use calc_cons, only: c_small

  ! -- Null mapping --

  implicit none

  ! -- Dummy argument declarations --

  real    (kind = k_r8), dimension (3), intent (in)  :: r1, r2
  real    (kind = k_r8), dimension (3), intent (in)  :: n0, s0
  real    (kind = k_r8), dimension (3), intent (out) :: ri
  integer (kind = k_i1),                intent (out) :: ierr

  ! -- Local declarations --

  real (kind = k_r8) :: rho

!-------------------------------------------------------------------------------
! End specification part
!-------------------------------------------------------------------------------

!-------------------------------------------------------------------------------
! Begin execution part
!-------------------------------------------------------------------------------

  if (abs(dot_product(n0, s0)) <= c_small) then
    ierr = -1
  else
    rho = dot_product(n0, r1 - r2) / dot_product(n0, s0)
    ri  = r1 - s0 * rho
    ierr = 0
  end if

  ! -- Last executable statement --

  return

!-------------------------------------------------------------------------------
! End execution part
!-------------------------------------------------------------------------------

!-------------------------------------------------------------------------------
! Begin subprogram part
!-------------------------------------------------------------------------------

!-------------------------------------------------------------------------------
! End subprogram part
!-------------------------------------------------------------------------------

end subroutine int_line_plane
