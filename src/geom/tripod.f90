pure subroutine tripod(a, ei, ej, ek, ierr)

!-------------------------------------------------------------------------------
! This file is part of the Computational Algorithm Library Collection (CALC).
!
! Copyright 2012 The CALC Team
! Copyright 2001 Joerg Petrasch
!
! CALC is free software: you can redistribute it and/or modify
! it under the terms of the GNU Lesser General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! CALC is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU Lesser General Public License for more details.
!
! You should have received a copy of the GNU Lesser General Public License
! along with CALC.  If not, see <http://www.gnu.org/licenses/>.
!-------------------------------------------------------------------------------

!-------------------------------------------------------------------------------
! Begin specification part
!-------------------------------------------------------------------------------

  ! -- Use association --

  use calc_type,  only: k_i1, k_r8
  use calc_cons, only: c_small, c_ei, c_ek
  use calc_geom,    only: cross_product, unit_vector, vector_length

  ! -- Null mapping --

  implicit none

  ! -- Dummy argument declarations --

  real    (kind = k_r8), dimension (3), intent (in)  :: a
  real    (kind = k_r8), dimension (3), intent (out) :: ei, ej, ek
  integer (kind = k_i1),                intent (out) :: ierr

  ! -- Local declarations --

  real (kind = k_r8), dimension (3) :: b, c

!-------------------------------------------------------------------------------
! End specification part
!-------------------------------------------------------------------------------

!-------------------------------------------------------------------------------
! Begin execution part
!-------------------------------------------------------------------------------

  ierr = 0
  if (vector_length(a) <= c_small) then
    ei = huge(ei)
    ej = huge(ej)
    ierr = -1
  else
    call unit_vector(a, ek, ierr)
    b = cross_product(c_ek, ek)
    c = cross_product(ek, c_ei)
    if (vector_length(b) >= vector_length(c)) then
      call unit_vector(b, ej, ierr)
    else
      call unit_vector(c, ej, ierr)
    end if
    call unit_vector(cross_product(ej, ek), ei, ierr)
  end if

  ! -- Last executable statement --

  return

!-------------------------------------------------------------------------------
! End execution part
!-------------------------------------------------------------------------------

!-------------------------------------------------------------------------------
! Begin subprogram part
!-------------------------------------------------------------------------------

!-------------------------------------------------------------------------------
! End subprogram part
!-------------------------------------------------------------------------------

end subroutine tripod
