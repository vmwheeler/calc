pure subroutine int_line_parallelepiped(r1, s, r2, a, b, c, ri, isurf, ierr)

!-------------------------------------------------------------------------------
! This file is part of the Computational Algorithm Library Collection (CALC).
!
! Copyright 2012 The CALC Team
!
! CALC is free software: you can redistribute it and/or modify
! it under the terms of the GNU Lesser General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! CALC is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU Lesser General Public License for more details.
!
! You should have received a copy of the GNU Lesser General Public License
! along with CALC.  If not, see <http://www.gnu.org/licenses/>.
!-------------------------------------------------------------------------------

!-------------------------------------------------------------------------------
! Begin specification part
!-------------------------------------------------------------------------------

  ! -- Use association --

  use calc_type, only: k_i1, k_r8
  use calc_geom,   only: cross_product, unit_vector, int_line_parallelogram

  ! -- Null mapping --

  implicit none

  ! -- Dummy argument declarations --
 
  real    (kind = k_r8), dimension (3),    intent (in)  :: r1, s
  real    (kind = k_r8), dimension (3),    intent (in)  :: r2, a, b, c
  real    (kind = k_r8), dimension (2, 3), intent (out) :: ri
  integer (kind = k_i1), dimension (2),    intent (out) :: isurf
  integer (kind = k_i1),                   intent (out) :: ierr

  ! -- Local declarations --

  real    (kind = k_r8), dimension (6, 2, 3) :: v
  real    (kind = k_r8), dimension (6, 3)    :: r
  real    (kind = k_r8), dimension (2, 3)    :: xi
  real    (kind = k_r8), dimension (3)       :: r3
  real    (kind = k_r8), dimension (3)       :: ua, ub, uc
  real    (kind = k_r8)                      :: check
  integer (kind = k_i1), dimension (2)       :: jsurf
  integer (kind = k_i1)                      :: isol, iface

!-------------------------------------------------------------------------------
! End specification part
!-------------------------------------------------------------------------------

!-------------------------------------------------------------------------------
! Begin execution part
!-------------------------------------------------------------------------------

  ! Check if the vectors a, b and c form the right Cartesian system
  ! of coordinates.

  call unit_vector(a, ua, ierr)
  call unit_vector(b, ub, ierr)
  call unit_vector(c, uc, ierr)

  check = dot_product(cross_product(ua, ub), uc)

  if (check <= 0.0_k_r8) then
    ierr = -2
    return
  end if

  ! Calculate r3

  r3 = r2 + a + b + c

  ! Store reference vertex for each face

  r(1, :) = r2
  r(2, :) = r3
  r(3, :) = r2 + b
  r(4, :) = r3 - b
  r(5, :) = r2
  r(6, :) = r3

  ! Store reference edge vector pairs for each face

  v(1, 1, :) =   b
  v(1, 2, :) =   a
  v(2, 1, :) =  -a
  v(2, 2, :) =  -b
  v(3, 1, :) =   c
  v(3, 2, :) =   a
  v(4, 1, :) =  -a
  v(4, 2, :) =  -c
  v(5, 1, :) =   c
  v(5, 2, :) =   b
  v(6, 1, :) =  -b
  v(6, 2, :) =  -c

  ! Find intersection points between the line and the faces

  isol  = 1_k_i1
  isurf = 0_k_i1
  jsurf = 0_k_i1
  ri    = 0.0_k_r8
  xi    = 0.0_k_r8

  do iface = 1, 6
    call int_line_parallelogram(r1, s, r(iface, :), v(iface, 1, :), &
                                                    v(iface, 2, :), &
                                                    xi(isol, :), ierr)
    if (ierr < 0) then
      cycle
    end if
    jsurf(isol) = iface
    if (isol < 2_k_i1) then
      isol = isol + 1
      cycle
    else if (isol == 2_k_i1) then
      exit
    else if (isol > 2_k_i1) then
      ierr = -3  ! More than 2 intersection points attempted
      return
    end if
  end do

  ! Return the intersection points in the desired order

  if (jsurf(1) /= 0_k_i1 .and. jsurf(2) /= 0_k_i1) then
    if (dot_product(s, xi(2, :) - xi(1, :)) > 0.0_k_r8) then
      ri    = xi
      isurf = jsurf
    else
      ri(2, :) = xi(1, :)
      ri(1, :) = xi(2, :)
      isurf(2) = jsurf(1)
      isurf(1) = jsurf(2)
    end if
    ierr = 0
  else if (jsurf(1) == 0_k_i1 .and. jsurf(2) == 0_k_i1) then
    ri   = huge(ri)
    ierr = -1
  else
    ri   = huge(ri)
    ierr = -4
  end if

  ! -- Last executable statement --

  return

!-------------------------------------------------------------------------------
! End execution part
!-------------------------------------------------------------------------------

!-------------------------------------------------------------------------------
! Begin subprogram part
!-------------------------------------------------------------------------------

!-------------------------------------------------------------------------------
! End subprogram part
!-------------------------------------------------------------------------------

end subroutine int_line_parallelepiped
 
