pure subroutine rot_cartesian_coord_2(x1, y1, z1, x2, y2, z2, r1, r2, ierr)

!-------------------------------------------------------------------------------
! This file is part of the Computational Algorithm Library Collection (CALC).
!
! Copyright 2012 The CALC Team
!
! CALC is free software: you can redistribute it and/or modify
! it under the terms of the GNU Lesser General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! CALC is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU Lesser General Public License for more details.
!
! You should have received a copy of the GNU Lesser General Public License
! along with CALC.  If not, see <http://www.gnu.org/licenses/>.
!-------------------------------------------------------------------------------

!-------------------------------------------------------------------------------
! Begin specification part
!-------------------------------------------------------------------------------

  ! -- Use association --

  use calc_type, only: k_i1, k_r8
  use calc_geom,   only: rotation_matrix, unit_vector

  ! -- Null mapping --

  implicit none

  ! -- Dummy argument declarations --

  real    (kind = k_r8), dimension (3), intent (in)  :: x1, y1, z1
  real    (kind = k_r8), dimension (3), intent (in)  :: x2, y2, z2
  real    (kind = k_r8), dimension (3), intent (in)  :: r1
  real    (kind = k_r8), dimension (3), intent (out) :: r2
  integer (kind = k_i1),                intent (out) :: ierr

  ! -- Local declarations --

  real    (kind = k_r8), dimension(3, 3) :: r
  real    (kind = k_r8), dimension(3)    :: ux1, uy1, uz1, ux2, uy2, uz2
  integer (kind = k_i1)                  :: i

!-------------------------------------------------------------------------------
! End specification part
!-------------------------------------------------------------------------------

!-------------------------------------------------------------------------------
! Begin execution part
!-------------------------------------------------------------------------------

  call unit_vector(x1, ux1, ierr)
  call unit_vector(y1, uy1, ierr)
  call unit_vector(z1, uz1, ierr)

  call unit_vector(x2, ux2, ierr)
  call unit_vector(y2, uy2, ierr)
  call unit_vector(z2, uz2, ierr)

  call rotation_matrix(ux1, uy1, uz1, ux2, uy2, uz2, r)

  forall (i = 1:3)
    r2(i) = sum(r(:, i) * r1(:))
  end forall

  ! -- Last executable statement --

  return

!-------------------------------------------------------------------------------
! End execution part
!-------------------------------------------------------------------------------

!-------------------------------------------------------------------------------
! Begin subprogram part
!-------------------------------------------------------------------------------

!-------------------------------------------------------------------------------
! End subprogram part
!-------------------------------------------------------------------------------

end subroutine rot_cartesian_coord_2
