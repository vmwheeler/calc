pure subroutine int_line_rectangle(r1, s, r2, a, b, ri, ierr)

!-------------------------------------------------------------------------------
! This file is part of the Computational Algorithm Library Collection (CALC).
!
! Copyright 2012 The CALC Team
!
! CALC is free software: you can redistribute it and/or modify
! it under the terms of the GNU Lesser General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! CALC is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU Lesser General Public License for more details.
!
! You should have received a copy of the GNU Lesser General Public License
! along with CALC.  If not, see <http://www.gnu.org/licenses/>.
!-------------------------------------------------------------------------------

!-------------------------------------------------------------------------------
! Begin specification part
!-------------------------------------------------------------------------------

  ! -- Use association --

  use calc_type, only: k_i1, k_r8
  use calc_geom,   only: cross_product, int_line_plane, unit_vector

  ! -- Null mapping --

  implicit none

 ! -- Dummy argument declarations --

  real    (kind = k_r8), dimension (3), intent (in)  :: r1, s
  real    (kind = k_r8), dimension (3), intent (in)  :: r2, a, b
  real    (kind = k_r8), dimension (3), intent (out) :: ri
  integer (kind = k_i1),                intent (out) :: ierr

! -- Local declarations --

  real    (kind = k_r8), dimension (4, 3) :: v
  real    (kind = k_r8), dimension (3)    :: n, vi
  real    (kind = k_r8)                   :: m
  integer (kind = k_i1)                   :: iedge

!-------------------------------------------------------------------------------
! End specification part
!-------------------------------------------------------------------------------

!-------------------------------------------------------------------------------
! Begin execution part
!-------------------------------------------------------------------------------

  ! Check if the angle between egde vectors a and b is 90 degrees

  if (dot_product(a,b) /= 0.0_k_r8) then
   ierr = -2
  end if

  ! Compute edge vectors

  v(1, :) = a
  v(2, :) = b
  v(3, :) = -a
  v(4, :) = -b

  ! Compute face outer normal unit vector
  
  call unit_vector(cross_product(a, b), n, ierr)

  ! Find intersection point with the entire plane

  call int_line_plane(r1, s, r2, n, ri, ierr)
  if (ierr == -1) then
    return
  end if

  ! Check if the intersection point is inside the rectangle

  vi =  ri  - r2  

  do iedge = 1, 4

    m = dot_product(v(iedge, :), vi)
    m = m / sum(v(iedge, :)**2)

    if ( m > 1.0_k_r8) then
      ierr = -3
      return
    else if (m < 0.0_k_r8) then
      ierr = -4
      return
    end if
    vi(:) = vi(:) - v(iedge, :)
  end do
  ierr = 0

  ! -- Last executable statement --

  return

!-------------------------------------------------------------------------------
! End execution part
!-------------------------------------------------------------------------------

!-------------------------------------------------------------------------------
! Begin subprogram part
!-------------------------------------------------------------------------------

!-------------------------------------------------------------------------------
! End subprogram part
!-------------------------------------------------------------------------------

 end subroutine int_line_rectangle
