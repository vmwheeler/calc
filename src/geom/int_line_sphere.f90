pure subroutine int_line_sphere(r1, s0, r2, radius, ri, ierr)

!-------------------------------------------------------------------------------
! This file is part of the Computational Algorithm Library Collection (CALC).
!
! Copyright 2012 The CALC Team
!
! CALC is free software: you can redistribute it and/or modify
! it under the terms of the GNU Lesser General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! CALC is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU Lesser General Public License for more details.
!
! You should have received a copy of the GNU Lesser General Public License
! along with CALC.  If not, see <http://www.gnu.org/licenses/>.
!-------------------------------------------------------------------------------

!-------------------------------------------------------------------------------
! Begin specification part
!-------------------------------------------------------------------------------

  ! -- Use association --

  use calc_type, only: k_i1, k_r8
  use calc_poly,  only: solve_quadratic

  ! -- Null mapping --

  implicit none

  ! -- Dummy argument declarations --

  real    (kind = k_r8), dimension (3),   intent (in)  :: r1, r2, s0
  real    (kind = k_r8),                  intent (in)  :: radius
  real    (kind = k_r8), dimension (2,3), intent (out) :: ri
  integer (kind = k_i1),                  intent (out) :: ierr

  ! -- Local declarations --

  real (kind = k_r8)                :: a, b, c
  real (kind = k_r8), dimension (2) :: x

!-------------------------------------------------------------------------------
! End specification part
!-------------------------------------------------------------------------------

!-------------------------------------------------------------------------------
! Begin execution part
!-------------------------------------------------------------------------------

  a = dot_product(s0, s0)
  b = 2.0_k_r8 * dot_product(r1 - r2, s0)
  c = dot_product(r1, r1) + dot_product(r2 - 2 * r1, r2) - radius * radius

  call solve_quadratic(a, b, c, x, ierr)

  if (ierr == 0) then
    ri(1, :) = r1 + s0 * x(1)
    ri(2, :) = r1 + s0 * x(2)
  else
    ri   = huge(ri)
    ierr = -1
  end if

  ! -- Last executable statement --

  return

!-------------------------------------------------------------------------------
! End execution part
!-------------------------------------------------------------------------------

!-------------------------------------------------------------------------------
! Begin subprogram part
!-------------------------------------------------------------------------------

!-------------------------------------------------------------------------------
! End subprogram part
!-------------------------------------------------------------------------------

end subroutine int_line_sphere
