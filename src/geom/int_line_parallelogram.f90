pure subroutine int_line_parallelogram(r1, s, r2, a, b, ri, ierr)

!-------------------------------------------------------------------------------
! This file is part of the Computational Algorithm Library Collection (CALC).
!
! Copyright 2012 The CALC Team
!
! CALC is free software: you can redistribute it and/or modify
! it under the terms of the GNU Lesser General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! CALC is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU Lesser General Public License for more details.
!
! You should have received a copy of the GNU Lesser General Public License
! along with CALC.  If not, see <http://www.gnu.org/licenses/>.
!-------------------------------------------------------------------------------

!-------------------------------------------------------------------------------
! Begin specification part
!-------------------------------------------------------------------------------

  ! -- Use association --

  use calc_type, only: k_i1, k_r8
  use calc_geom,   only: cross_product, int_line_plane, unit_vector
  use calc_geom,   only: vector_length

  ! -- Null mapping --

  implicit none

  ! -- Dummy argument declarations --
 
  real    (kind = k_r8), dimension (3), intent (in)  :: r1, s
  real    (kind = k_r8), dimension (3), intent (in)  :: r2, a, b
  real    (kind = k_r8), dimension (3), intent (out) :: ri
  integer (kind = k_i1),                intent (out) :: ierr

  ! -- Local declarations --

  real    (kind = k_r8), dimension (4, 3) :: v
  real    (kind = k_r8), dimension (3)    :: n, vi
  real    (kind = k_r8), dimension (3)    :: ua, ub
  real    (kind = k_r8)                   :: check
  integer (kind = k_i1)                   :: iedge

!-------------------------------------------------------------------------------
! End specification part
!-------------------------------------------------------------------------------

!-------------------------------------------------------------------------------
! Begin execution part
!-------------------------------------------------------------------------------

  ! Check if the angle between the vectors a and b is less than PI
  ! To be revised

! call unit_vector(a, ua, ierr)
! call unit_vector(b, ub, ierr)

!  if (vector_length(cross_product(a, b)) <= 0.0_k_r8) then
!    ri   = huge(ri)
!    ierr = -2
!    return
!  end if

  ! Compute edge vectors

  v(1, :) =  a
  v(2, :) =  b
  v(3, :) = -a
  v(4, :) = -b

  ! Compute face outer normal unit vector

  call unit_vector(cross_product(v(1, :), v(2, :)), n, ierr)

  ! Find intersection point with the entire plane

  call int_line_plane(r1, s, r2, n, ri, ierr)
  if (ierr == -1) then
    return
  end if

  ! Compute auxiliary intersection vectors

  do iedge = 2, 4
  end do

  ! Check if the intersection point is inside the parallelogram

  vi = ri - r2
  do iedge = 1, 4
    if (dot_product(cross_product(v(iedge, :), vi), n) <= 0.0_k_r8) then
      ri   = huge(ri)
      ierr = -1
      return
    end if
    vi(:) = vi(:) - v(iedge, :)
  end do
  ierr = 0

  ! -- Last executable statement --

  return

!-------------------------------------------------------------------------------
! End execution part
!-------------------------------------------------------------------------------

!-------------------------------------------------------------------------------
! Begin subprogram part
!-------------------------------------------------------------------------------

!-------------------------------------------------------------------------------
! End subprogram part
!-------------------------------------------------------------------------------

end subroutine int_line_parallelogram
