pure subroutine int_line_cylinder_finite(r1, s, r2, r3, radius, isurf, ri, ierr)

!-------------------------------------------------------------------------------
! This file is part of the Computational Algorithm Library Collection (CALC).
!
! Copyright 2012 The CALC Team
!
! CALC is free software: you can redistribute it and/or modify
! it under the terms of the GNU Lesser General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! CALC is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU Lesser General Public License for more details.
!
! You should have received a copy of the GNU Lesser General Public License
! along with CALC.  If not, see <http://www.gnu.org/licenses/>.
!-------------------------------------------------------------------------------

!-------------------------------------------------------------------------------
! Begin specification part
!-------------------------------------------------------------------------------

  ! -- Use association --

  use calc_type, only: k_i1, k_i2, k_r8
  use calc_poly,  only: solve_quadratic

  ! -- Null mapping --

  implicit none

  ! -- Dummy argument declarations --

  real    (kind = k_r8), dimension (3),   intent (in)  :: r1, s
  real    (kind = k_r8), dimension (3),   intent (in)  :: r2, r3
  real    (kind = k_r8),                  intent (in)  :: radius
  integer (kind = k_i2), dimension (2),   intent (out) :: isurf
  real    (kind = k_r8), dimension (2,3), intent (out) :: ri
  integer (kind = k_i1),                  intent (out) :: ierr

  ! -- Local declarations --

  real    (kind = k_r8), dimension (3) :: e, f, g
  real    (kind = k_r8), dimension (2) :: lam, zz
  real    (kind = k_r8)                :: a, b, c
  real    (kind = k_r8)                :: fe, ff, fg, ge, gg
  real    (kind = k_r8)                :: h, x
  integer (kind = k_i2)                :: i

!-------------------------------------------------------------------------------
! End specification part
!-------------------------------------------------------------------------------

!-------------------------------------------------------------------------------
! Begin execution part
!-------------------------------------------------------------------------------

  e = r3 - r2
  h = sqrt(sum(e * e))
  e = e / h
  f = r1 - r2
  g = s

  fe = sum(f * e)
  ge = sum(g * e)
  fg = sum(f * g)
  ff = sum(f * f)
  gg = sum(g * g)

  a = gg - ge * ge
  b = 2 * (fg - fe * ge)
  c = ff - fe * fe - radius * radius

  isurf(:) = 0
  lam(1)   = huge(lam)
  lam(2)   = -lam(1)

  if (ge .ne. 0) then
    do i = 1, 2
      x = ((i - 1) * h - fe) / ge
      if ((a * x + b) * x + c .le. 0) call lam12(i, x, isurf, lam)
    end do
  end if

  ! Consider intersections with cylindrical surface

  call solve_quadratic(a, b, c, zz, ierr)
  if (ierr == 0) then
    do i = 1, 2
      x = fe + zz(i) * ge
      if (x .ge. 0 .and. x .le. h) then
        call lam12(3_k_i2, zz(i), isurf, lam)
      end if
    end do
  end if

  ! Assign values to ouput arguments

  if (isurf(1) .ne. 0 .and. isurf(2) .ne. 0) then
    do i=1,2
      ri(i, :) = r1(:) + lam(i) * s(:)
    end do
    ierr = 0
  else if (isurf(1) .eq. 0 .and. isurf(2) .eq. 0) then
    ri   = huge(ri)
    ierr = -1
  else
    ri   = huge(ri)
    ierr = -2
  end if

  ! -- Last executable statement --

  return

!-------------------------------------------------------------------------------
! End execution part
!-------------------------------------------------------------------------------

!-------------------------------------------------------------------------------
! Begin subprogram part
!-------------------------------------------------------------------------------

contains

  pure subroutine lam12(ss, z, isurf, lam)

  !-----------------------------------------------------------------------------
  ! Begin specification part
  !-----------------------------------------------------------------------------

    ! -- Use association --

    use calc_type, only: k_i2, k_r8

    ! -- Null mapping --

    implicit none

    ! -- Dummy argument declaration constructs --

    integer (kind = k_i2),                intent (in)    :: ss
    real    (kind = k_r8),                intent (in)    :: z
    integer (kind = k_i2), dimension (2), intent (inout) :: isurf
    real    (kind = k_r8), dimension (2), intent (inout) :: lam

    ! -- Local variable declaration constructs --

  !-----------------------------------------------------------------------------
  ! End specification part
  !-----------------------------------------------------------------------------

  !-----------------------------------------------------------------------------
  ! Begin execution part
  !-----------------------------------------------------------------------------

    if (z .lt. lam(1)) then
      isurf(1) = ss
      lam(1)   = z
    end if
    if (z .gt. lam(2)) then
      isurf(2) = ss
      lam(2)   = z
    end if

    ! -- Last executable statement --

    return

  !-----------------------------------------------------------------------------
  ! End execution part
  !-----------------------------------------------------------------------------

  !-----------------------------------------------------------------------------
  ! Begin subprogram part
  !-----------------------------------------------------------------------------

  !-----------------------------------------------------------------------------
  ! End subprogram part
  !-----------------------------------------------------------------------------

  end subroutine lam12

!-------------------------------------------------------------------------------
! End subprogram part
!-------------------------------------------------------------------------------

end subroutine int_line_cylinder_finite
