module calc_unit_pref
 
!-------------------------------------------------------------------------------
! This file is part of the Computational Algorithm Library Collection (CALC).
!
! Copyright 2012 The CALC Team
!
! CALC is free software: you can redistribute it and/or modify
! it under the terms of the GNU Lesser General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! CALC is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU Lesser General Public License for more details.
!
! You should have received a copy of the GNU Lesser General Public License
! along with CALC.  If not, see <http://www.gnu.org/licenses/>.
!-------------------------------------------------------------------------------
 
!-------------------------------------------------------------------------------
! Begin specification part
!-------------------------------------------------------------------------------

  ! -- Use association --

  use calc_type, only: k_r8

  ! -- Null mapping --

  implicit none

  ! -- Accessibility --

  private

  ! -- Derived type definitions --

  ! -- Type declarations --

  ! Decimal multiples and submultiples of SI units: SI prefixes

  ! yocto- (y)

  real (kind = k_r8), parameter, public ::                             &
    c_yocto =                                                          &
      1.0e-24_k_r8

  ! zepto- (z)

  real (kind = k_r8), parameter, public ::                             &
    c_zepto =                                                          &
      1.0e-21_k_r8

  ! atto- (a)

  real (kind = k_r8), parameter, public ::                             &
    c_atto =                                                           &
      1.0e-18_k_r8

  ! femto- (f)

  real (kind = k_r8), parameter, public ::                             &
    c_femto =                                                          &
      1.0e-15_k_r8

  ! pico- (p)

  real (kind = k_r8), parameter, public ::                             &
    c_pico =                                                           &
      1.0e-12_k_r8

  ! nano- (n)

  real (kind = k_r8), parameter, public ::                             &
    c_nano =                                                           &
      1.0e-09_k_r8

  ! micro- (mu)

  real (kind = k_r8), parameter, public ::                             &
    c_micro =                                                          &
      1.0e-06_k_r8

  ! milli- (m)

  real (kind = k_r8), parameter, public ::                             &
    c_milli =                                                          &
      1.0e-03_k_r8

  ! centi- (c)

  real (kind = k_r8), parameter, public ::                             &
    c_centi =                                                          &
      1.0e-02_k_r8

  ! deci- (d)

  real (kind = k_r8), parameter, public ::                             &
    c_deci =                                                           &
      1.0e-01_k_r8

  ! deca- (da)

  real (kind = k_r8), parameter, public ::                             &
    c_deca =                                                           &
      1.0e-01_k_r8

  ! hecto- (h)

  real (kind = k_r8), parameter, public ::                             &
    c_hecto =                                                          &
      1.0e+02_k_r8

  ! kilo- (h)

  real (kind = k_r8), parameter, public ::                             &
    c_kilo =                                                           &
      1.0e+03_k_r8

  ! mega- (M)

  real (kind = k_r8), parameter, public ::                             &
    c_mega =                                                           &
      1.0e+06_k_r8

  ! giga- (G)

  real (kind = k_r8), parameter, public ::                             &
    c_giga =                                                           &
      1.0e+09_k_r8

  ! tera- (T)

  real (kind = k_r8), parameter, public ::                             &
    c_tera =                                                           &
      1.0e+12_k_r8

  ! peta- (P)

  real (kind = k_r8), parameter, public ::                             &
    c_peta =                                                           &
      1.0e+15_k_r8

  ! exa- (E)

  real (kind = k_r8), parameter, public ::                             &
    c_exa =                                                            &
      1.0e+18_k_r8

  ! zetta- (Z)

  real (kind = k_r8), parameter, public ::                             &
    c_zetta =                                                          &
      1.0e+21_k_r8

  ! yotta- (Y)

  real (kind = k_r8), parameter, public ::                             &
    c_yotta =                                                          &
      1.0e+24_k_r8

  ! -- Interface blocks --

!-------------------------------------------------------------------------------
! End specification part
!-------------------------------------------------------------------------------

!-------------------------------------------------------------------------------
! Begin subprogram part
!-------------------------------------------------------------------------------

!-------------------------------------------------------------------------------
! End subprogram part
!-------------------------------------------------------------------------------

end module calc_unit_pref
