module calc_unit_conv
 
!-------------------------------------------------------------------------------
! This file is part of the Computational Algorithm Library Collection (CALC).
!
! Copyright 2012 The CALC Team
!
! CALC is free software: you can redistribute it and/or modify
! it under the terms of the GNU Lesser General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! CALC is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU Lesser General Public License for more details.
!
! You should have received a copy of the GNU Lesser General Public License
! along with CALC.  If not, see <http://www.gnu.org/licenses/>.
!-------------------------------------------------------------------------------
 
!-------------------------------------------------------------------------------
! Begin specification part
!-------------------------------------------------------------------------------

  ! -- Use association --

  use calc_type, only: k_r8

  ! -- Null mapping --

  implicit none

  ! -- Accessibility --

  private

  ! -- Derived type definitions --

  ! -- Type declarations --

  ! Multiply-by factors for unit conversion

  ! abampere
  ! to ampere (A)

  real (kind = k_r8), parameter, public ::                             &
    c_abampere =                                                       &
      1.0e+01_k_r8

  ! abcoulomb
  ! to coulomb (C)

  real (kind = k_r8), parameter, public ::                             &
    c_abcoulomb =                                                      &
      1.0e+01_k_r8

  ! abfarad
  ! to farad (F)

  real (kind = k_r8), parameter, public ::                             &
    c_abfarad =                                                        &
      10e+09_k_r8

  ! abhenry
  ! to henry (H)

  real (kind = k_r8), parameter, public ::                             &
    c_abhenry =                                                        &
      10e-09_k_r8

  ! abmho
  ! to siemens (S)

  real (kind = k_r8), parameter, public ::                             &
    c_abmho =                                                          &
      10e+09_k_r8

  ! abohm
  ! to ohm (Omega)

  real (kind = k_r8), parameter, public ::                             &
    c_abohm =                                                          &
      10e-09_k_r8

  ! abvolt
  ! to volt (V)

  real (kind = k_r8), parameter, public ::                             &
    c_abvolt =                                                         &
      10e-08_k_r8

  ! acceleration of free fall, standard (gn)
  ! to meter per second squared (m/s**2)
 
  real (kind = k_r8), parameter, public ::                             &
    c_accelaration_of_free_fall_standard =                             &
      9.80665e+00_k_r8
 
  ! acre (based on U.S. survey foot)
  ! to square meter (m**2)
 
  real (kind = k_r8), parameter, public ::                             &
    c_acre_us =                                                        &
      4.046873e+03_k_r8
 
  ! acre foot (based on U.S. survey foot)
  ! to cubic meter (m**3)
 
  real (kind = k_r8), parameter, public ::                             &
    c_acre_foot_us =                                                   &
      1.233489e+03_k_r8

  ! ampere hour (A*h)
  ! to coulomb (C)

  real (kind = k_r8), parameter, public ::                             &
    c_ampere_hour =                                                    &
      3.6e+03_k_r8

  ! angstrom (AA)
  ! to meter (m)
    
  real (kind = k_r8), parameter, public ::                             &
    c_angstrom =                                                       &
      1.0e-10_k_r8

  ! angstrom (AA)
  ! to nanometer (nm)
    
  real (kind = k_r8), parameter, public ::                             &
    c_angstrom_to_nanometer =                                          &
      1.0e-01_k_r8

  ! are (a)
  ! to square meter (m**2)
    
  real (kind = k_r8), parameter, public ::                             &
    c_are =                                                            &
      1.0e+02_k_r8

  ! astronomical unit (ua)
  ! to meter (m)
    
  real (kind = k_r8), parameter, public ::                             &
    c_astronomicalc_unit =                                              &
      1.495979e+11_k_r8

  ! atmosphere, standard (atm)
  ! to pascal (Pa)
    
  real (kind = k_r8), parameter, public ::                             &
    c_atmosphere_standard =                                            &
      1.01325e+05_k_r8

  ! atmosphere, standard (atm)
  ! to kilopascal (kPa)
    
  real (kind = k_r8), parameter, public ::                             &
    c_atmosphere_standard_to_kilopascal =                              &
      1.01325e+02_k_r8
  
  ! atmosphere, technical (at)
  ! to pascal (Pa)

  real (kind = k_r8), parameter, public ::                             &
    c_atmosphere_technical =                                           &
      9.80665e+04_k_r8

  ! atmosphere, technical (at)
  ! to kilopascal (kPa)

  real (kind = k_r8), parameter, public ::                             &
    c_atmosphere_technicalc_to_kilopascal =                             &
      9.80665e+01_k_r8

  ! bar (bar)
  ! to pascal (Pa)
 
  real (kind = k_r8), parameter, public ::                             &
    c_bar =                                                            & 
      1.0e+05_k_r8

  ! bar (bar)
  ! to kilopascal (kPa)

  real (kind = k_r8), parameter, public ::                             &
    c_bar_to_kilopascal =                                              &
      1.0e+02_k_r8

  ! barn (b)
  ! to square meter (m**2)

  real (kind = k_r8), parameter, public ::                             &
    c_barn =                                                           &
      1.0e-28_k_r8

  ! barrel (for petroleum, 42 gallons (U.S.)) (bbl)
  ! to cubic meter (m**3)

  real (kind = k_r8), parameter, public ::                             &
    c_barrel =                                                         &
      1.589873e-01_k_r8

  ! barrel (for petroleum, 42 gallons (U.S.)) (bbl)
  ! to liter (L)

  real (kind = k_r8), parameter, public ::                             &
    c_barrel_to_liter =                                                &
      1.589873e+02_k_r8

  ! biot (Bi)
  ! to ampere (A)

  real (kind = k_r8), parameter, public ::                             &
    c_biot =                                                           &
      1.0e+01_k_r8

  ! British thermal unit IT (Btu IT)
  ! to joule (J)

  real (kind = k_r8), parameter, public ::                             &
    c_british_thermal_unit_it =                                        &
      1.055056e+03_k_r8

  ! British thermal unit th (Btu th)
  ! to joule (J)

  real (kind = k_r8), parameter, public ::                             &
    c_british_thermal_unit_th =                                        &
      1.054350e+03_k_r8

  ! British thermal unit (mean) (Btu)
  ! to joule (J)

  real (kind = k_r8), parameter, public ::                             &
    c_british_thermal_unit_mean =                                      &
      1.05587e+03_k_r8

  ! British thermal unit (39 degree F) (Btu)
  ! to joule (J)

  real (kind = k_r8), parameter, public ::                             &
    c_british_thermal_unit_39f =                                       &
      1.05967e+03_k_r8

  ! British thermal unit (59 degree F) (Btu)
  ! to joule (J)

  real (kind = k_r8), parameter, public ::                             &
    c_british_thermal_unit_59f =                                       &
      1.05480e+03_k_r8

  ! British thermal unit (60 degree F) (Btu)
  ! to joule (J)

  real (kind = k_r8), parameter, public ::                             &
    c_british_thermal_unit_60f =                                       &
      1.05468e+03_k_r8

  ! bushel (U.S.) (bu)
  ! to cubic meter (m**3)

  real (kind = k_r8), parameter, public ::                             &
    c_bushel_us =                                                      &
      3.523907e-02_k_r8

  ! bushel (U.S.) (bu)
  ! to liter (L)

  real (kind = k_r8), parameter, public ::                             &
    c_bushel_us_to_liter =                                             &
      3.523907e+01_k_r8

  ! calorie IT (cal IT)
  ! to joule (J) 

  real (kind = k_r8), parameter, public ::                             &
    c_calorie_it =                                                     &
      4.1868e+00_k_r8

  ! calorie th (cal th)
  ! to joule (J)

  real (kind = k_r8), parameter, public ::                             &
    c_calorie_th =                                                     &
      4.184e+00_k_r8

  ! calorie (mean) (cal)
  ! to joule (J) 

  real (kind = k_r8), parameter, public ::                             &
    c_calorie_mean =                                                   &
      4.19002e+00_k_r8

  ! calorie (15 degree C) (cal 15)
  ! to joule (J)

  real (kind = k_r8), parameter, public ::                             &
    c_calorie_15c                                                      &
      = 4.18580e+00_k_r8

  ! calorie (20 degree C) (cal 20)
  ! to joule (J)

  real (kind = k_r8), parameter, public ::                             &
    c_calorie_20c =                                                    &
      4.18190e+00_k_r8

  ! candela per square inch (cd/in**2)
  ! to candela per square meter (cd/m**2)

  real (kind = k_r8), parameter, public ::                             &
    c_candela_per_square_inch =                                        &
      1.550003e+03_k_r8

  ! carat, metric
  ! to kilogram (kg)

  real (kind = k_r8), parameter, public ::                             &
    c_carat_metric =                                                   &
      2.0e-04_k_r8

  ! carat, metric
  ! to gram (g)

  real (kind = k_r8), parameter, public ::                             &
    c_carat_metric_to_gram =                                           &
      2.0e-01_k_r8

  ! centimeter of mercury (0 degree C) (cmHg (0 degree C))
  ! to pascal (Pa)

  real (kind = k_r8), parameter, public ::                             &
    c_centimeter_of_mercury_0c =                                       &
      1.33322e+03_k_r8

  ! centimeter of mercury (0 degree C) (cmHg (0 degree C))
  ! to kilopascal (kPa)

  real (kind = k_r8), parameter, public ::                             &
    c_centimeter_of_mercury_0c_to_kilopascal =                         &
      1.33322e+00_k_r8

  ! centimeter of mercury, conventional (cmHg)
  ! to pascal (Pa)

  real (kind = k_r8), parameter, public ::                             &
    c_centimeter_of_mercury_conventional =                             &
      1.333224e+03_k_r8

  ! centimeter of mercury, conventional (cmHg)
  ! to kilopascal (kPa)

  real (kind = k_r8), parameter, public ::                             &
    c_centimeter_of_mercury_conventional_to_kilopascal =               &
      1.333224e+00_k_r8

  ! centimeter of water (4 degree C) (cmH2O (4 degree C))
  ! to pascal (Pa)

  real (kind = k_r8), parameter, public ::                             &
    c_centimeter_of_water_4c =                                         &
      9.80638e+01_k_r8

  ! centimeter of water, conventional (cmH2O)
  ! to pascal (Pa)

  real (kind = k_r8), parameter, public ::                             &
    c_centimeter_of_water_conventional =                               &
      9.80665e+01_k_r8

  ! centipoise (cP)
  ! to pascal second (Pa*s)

  real (kind = k_r8), parameter, public ::                             &
    c_centipoise =                                                     &
      1.0e-03_k_r8

  ! centistokes (cSt)
  ! to meter squared per second (m**2/s)

  real (kind = k_r8), parameter, public ::                             &
    c_centistokes =                                                    &
      1.0e-06_k_r8

  ! chain (based on U.S. survey foot) (ch)
  ! to meter (m)

  real (kind = k_r8), parameter, public ::                             &
    c_chain_us =                                                       &
      2.011684e+01_k_r8

  ! circular mil
  ! to square meter (m**2)

  real (kind = k_r8), parameter, public ::                             &
    c_circular_mil =                                                   &
      5.067075e-10_k_r8

  ! circular mil
  ! to square millimeter (mm**2)

  real (kind = k_r8), parameter, public ::                             &
    c_circular_mil_to_square_millimeter =                              &
      5.067075e-04_k_r8

  ! clo
  ! to square meter kelvin per watt (m**2*K/W)

  real (kind = k_r8), parameter, public ::                             &
    c_clo =                                                            &
      1.55e-01_k_r8

  ! cord (128 ft**3)
  ! to cubic meter (m**3)

  real (kind = k_r8), parameter, public ::                             &
    c_cord =                                                           &
      3.624556e+00_k_r8

  ! cubic foot (ft**3)
  ! to cubic meter (m**3)

  real (kind = k_r8), parameter, public ::                             &
    c_cubic_foot =                                                     &
      2.831685e-02_k_r8

  ! cubic inch (in**3)
  ! to cubic meter (m**3)

  real (kind = k_r8), parameter, public ::                             &
    c_cubic_inch =                                                     &
      1.638706e-05_k_r8

  ! cubic mile (mi**3)
  ! to cubic meter (m**3)

  real (kind = k_r8), parameter, public ::                             &
    c_cubic_mile =                                                     &
      4.168182e+09_k_r8

  ! cubic yard (yd**3)
  ! to cubic meter (m**3)

  real (kind = k_r8), parameter, public ::                             &
    c_cubic_yard =                                                     &
      7.645549e-01_k_r8

  ! cup (U.S.)
  ! to cubic meter (m**3)

  real (kind = k_r8), parameter, public ::                             &
    c_cup_us =                                                         &
      2.365882e-04_k_r8

  ! cup (U.S.)
  ! to liter (L)

  real (kind = k_r8), parameter, public ::                             &
    c_cup_us_to_liter =                                                &
      2.365882e-01_k_r8

  ! cup (U.S.)
  ! to milliliter (mL)

  real (kind = k_r8), parameter, public ::                             &
    c_cup_us_to_milliliter =                                           &
      2.365882e+02_k_r8

  ! curie (Ci)
  ! to becquerel (Bq)

  real (kind = k_r8), parameter, public ::                             &
    c_curie =                                                          &
      3.7e+10_k_r8
  
  ! darcy
  ! to meter squared (m**2)

  real (kind = k_r8), parameter, public ::                             &
    c_darcy =                                                          &
      9.869233e-13_k_r8

  ! day (d)
  ! to seconds (s)

  real (kind = k_r8), parameter, public ::                             &
    c_day =                                                            &
      8.64e+04_k_r8

  ! day (sidereal)
  ! to seconds (s)

  real (kind = k_r8), parameter, public ::                             &
    c_day_sidereal =                                                   &
      8.616409e+04_k_r8

  ! debye (D)
  ! to coulomb meter (C*m)

  real (kind = k_r8), parameter, public ::                             &
    c_debye =                                                          &
      3.335641e-30_k_r8

  ! degree (angle) (degree)
  ! to radian (rad)

  real (kind = k_r8), parameter, public ::                             &
    c_degree =                                                         &
      1.745329e-02_k_r8

  ! denier
  ! to kilogram per meter (kg/m)

  real (kind = k_r8), parameter, public ::                             &
    c_denier =                                                         &
      1.111111e-07_k_r8

  ! dyne (dyn)
  ! to newton (N)

  real (kind = k_r8), parameter, public ::                             &
    c_dyne =                                                           &
      1.0e-05_k_r8

  ! dyne centimeter (dyn*cm)
  ! to newton meter (N*m)

  real (kind = k_r8), parameter, public ::                             &
    c_dyne_centimenter =                                               &
      1.0e-07_k_r8

  ! dyne per square centimeter (dyn/cm**2)
  ! to pascal (Pa)

  real (kind = k_r8), parameter, public ::                             &
    c_dyne_per_square_centimenter =                                    &
      1.0e-01_k_r8

  ! electronvolt (eV)
  ! to joule (J)

  real (kind = k_r8), parameter, public ::                             &
    c_electron_volt =                                                  &
      1.602176e-19_k_r8

  ! EMU of capacitance (abfarad)
  ! to farad (F)

  real (kind = k_r8), parameter, public ::                             &
    c_emu_of_capacitance =                                             &
      1.0e+09_k_r8

  ! EMU of current (abampere)
  ! to ampere (A)

  real (kind = k_r8), parameter, public ::                             &
    c_emu_of_current =                                                 &
      1.0e+01_k_r8

  ! EMU of electric potential (abvolt)
  ! to volt (V)

  real (kind = k_r8), parameter, public ::                             &
    c_emu_of_electric_potential =                                      &
      1.0e-08_k_r8

  ! EMU of inductance (abhenry)
  ! to henry (H)

  real (kind = k_r8), parameter, public ::                             &
    c_emu_of_inductance =                                              &
      1.0e-09_k_r8

  ! EMU of resistance (abohm)
  ! to ohm (Omega)

  real (kind = k_r8), parameter, public ::                             &
    c_emu_of_resistance =                                              &
      1.0e-09_k_r8

  ! erg (erg)
  ! to joule (J)

  real (kind = k_r8), parameter, public ::                             &
    c_erg =                                                            &
      1.0e-07_k_r8

  ! erg per second (erg/s)
  ! to watt (J)

  real (kind = k_r8), parameter, public ::                             &
    c_erg_per_second =                                                 &
      1.0e-07_k_r8

  ! erg per square centimeter second (erg/(cm**2*s))
  ! to watt per square meter(W/m**2)
 
  real (kind = k_r8), parameter, public ::                             &
    c_erg_per_square_centimeter_second =                               &
      1.0e-03_k_r8
 
  ! ESU of capacitance (statfarad)
  ! to farad (F)
 
  real (kind = k_r8), parameter, public ::                             &
    c_esu_of_capacitance =                                             &
      1.112650e-12_k_r8
 
  ! ESU of current (statampere)
  ! to ampere (A)
 
  real (kind = k_r8), parameter, public ::                             &
    c_esu_of_current =                                                 &
      3.335641e-10_k_r8
 
  ! ESU of electric potential (statvolt)
  ! to volt (V)
 
  real (kind = k_r8), parameter, public ::                             &
    c_esu_of_electric_potential =                                      &
      2.997925e+02_k_r8
 
  ! ESU of inductance (stathenry)
  ! to henry (H)
 
  real (kind = k_r8), parameter, public ::                             &
    c_esu_of_inductance =                                              &
      8.987552e+11_k_r8

  ! ESU of resistance (statohm)
  ! to ohm (Omega)

  real (kind = k_r8), parameter, public ::                             &
    c_esu_of_resistance =                                              &
      8.987552e+11_k_r8

  ! faraday (based on carbon 12)
  ! to coulomb (C)

  real (kind = k_r8), parameter, public ::                             &
    c_faraday =                                                        &
      9.648531e+04_k_r8

  ! fathom (based on U.S. survey foot)
  ! to meter (m)

  real (kind = k_r8), parameter, public ::                             &
    c_fathom_us =                                                      &
      1.828804e+00_k_r8

  ! fermi
  ! to meter (m)

  real (kind = k_r8), parameter, public ::                             &
    c_fermi =                                                          &
      1.0e-15_k_r8

  ! fermi
  ! to femtometer (fm)

  real (kind = k_r8), parameter, public ::                             &
    c_fermi_to_femtometer =                                            &
      1.0e+00_k_r8

  ! fluid ounce (U.S.) (fl oz)
  ! to cubic meter (m**3)

  real (kind = k_r8), parameter, public ::                             &
    c_fluid_ounce_us =                                                 &
      2.957353e-05_k_r8

  ! fluid ounce (U.S.) (fl oz)
  ! to milliliter (mL)

  real (kind = k_r8), parameter, public ::                             &
    c_fluid_ounce__us_to_milliliter =                                  &
      2.957353e+01_k_r8

  ! foot (ft)
  ! to meter (m)

  real (kind = k_r8), parameter, public ::                             &
    c_foot =                                                           &
      3.048e-01_k_r8

  ! foot (U.S. survey) (ft)
  ! to meter (m)

  real (kind = k_r8), parameter, public ::                             &
    c_foot_us =                                                        &
      3.048006e-01_k_r8

  ! footcandle
  ! to lux (lx)

  real (kind = k_r8), parameter, public ::                             &
    c_footcandle =                                                     &
      1.076391e+01_k_r8

  ! footlambert
  ! to candela per square meter (cd/m**2)

  real (kind = k_r8), parameter, public ::                             &
    c_footlambert =                                                    &
      3.426259e+00_k_r8

  ! foot of mercury, conventional (ftHg)
  ! to pascal (Pa)

  real (kind = k_r8), parameter, public ::                             &
    c_foot_of_mercury_conventional =                                   &
      4.063666e+04_k_r8

  ! foot of mercury, conventional (ftHg)
  ! to kilopascal (kPa)

  real (kind = k_r8), parameter, public ::                             &
    c_foot_of_mercury_conventional_to_kilopascal =                     &
      4.063666e+01_k_r8

  ! foot of water (39.2 Fahrenheit) (ftH2O (39.2 Fahrenheit))
  ! to pascal (Pa)

  real (kind = k_r8), parameter, public ::                             &
    c_foot_of_water_39_2f =                                            &
      2.98898e+03_k_r8

  ! foot of water (39.2 Fahrenheit) (ftH2O (39.2 Fahrenheit))
  ! to kilopascal (kPa)

  real (kind = k_r8), parameter, public ::                             &
    c_foot_of_water_39_2f_to_kilopascal =                              &
      2.98898e+00_k_r8

  ! foot of water, conventional (ftH2O)
  ! to pascal (Pa)

  real (kind = k_r8), parameter, public ::                             &
    c_foot_of_water_conventional =                                     &
      2.989067e+03_k_r8

  ! foot of water, conventional (ftH2O)
  ! to kilopascal (kPa)

  real (kind = k_r8), parameter, public ::                             &
    c_foot_of_water_conventional_to_kilopascal =                       &
      2.989067e+00_k_r8

  ! foot per hour (ft/h)
  ! to meter per second (m/s)

  real (kind = k_r8), parameter, public ::                             &
    c_foot_per_hour =                                                  &
      8.466667e-05_k_r8

  ! foot per minute (ft/min)
  ! to meter per second (m/s)

  real (kind = k_r8), parameter, public ::                             &
    c_foot_per_minute =                                                &
      5.08e-03_k_r8

  ! foot per second (ft/s)
  ! to meter per second (m/s)

  real (kind = k_r8), parameter, public ::                             &
    c_foot_per_second =                                                &
      3.048e-01_k_r8

  ! foot per second squared (ft/s**2)
  ! to meter per second squared (m/s**2)

  real (kind = k_r8), parameter, public ::                             &
    c_foot_per_second_squared =                                        &
      3.048e-01_k_r8

  ! foot poundal
  ! to joule (J)

  real (kind = k_r8), parameter, public ::                             &
    c_foot_poundal =                                                   &
      4.214011e-02_k_r8

  ! foot pound-force (ft*lbf)
  ! to joule (J)

  real (kind = k_r8), parameter, public ::                             &
    c_foot_pound_force =                                               &
      1.355818e+00_k_r8

  ! foot pound-force per hour (ft*lbf/h)
  ! to watt (W)

  real (kind = k_r8), parameter, public ::                             &
    c_foot_pound_force_per_hour =                                      &
      3.766161e-04_k_r8

  ! foot pound-force per minute (ft*lbf/min)
  ! to watt (W)

  real (kind = k_r8), parameter, public ::                             &
    c_foot_pound_force_per_minute =                                    &
      2.259697e-02_k_r8

  ! foot pound-force per second (ft*lbf/s)
  ! to watt (W)

  real (kind = k_r8), parameter, public ::                             &
    c_foot_pound_force_per_second =                                    &
      1.355818e+00_k_r8

  ! foot
  ! to the fourth power (ft**4) to meter to the fourth power (m**4)

  real (kind = k_r8), parameter, public ::                             &
    c_foot_to_fourth_power =                                           &
      8.630975e-03_k_r8

  ! franklin (Fr)
  ! to coulomb (C)

  real (kind = k_r8), parameter, public ::                             &
    c_franklin =                                                       &
      3.335641e-10_k_r8

  ! gal (Gal)
  ! to meter per second squared (m/s**2)

  real (kind = k_r8), parameter, public ::                             &
    c_gal =                                                            &
      1.0e-02_k_r8

  ! gallon (Canadian and U.K. (Imperial)) (gal (Imperial))
  ! to cubic meter (m**3)

  real (kind = k_r8), parameter, public ::                             &
    c_gallon_imperial =                                                &
      4.54609e-03_k_r8

  ! gallon (Canadian and U.K. (Imperial)) (gal (Imperial))
  ! to liter (L)

  real (kind = k_r8), parameter, public ::                             &
    c_gallon_imperial_to_liter =                                       &
      4.54609e+00_k_r8

  ! gallon (U.S.) (gal)
  ! to cubic meter (m**3)

  real (kind = k_r8), parameter, public ::                             &
    c_gallon_us =                                                      &
      3.785412e-03_k_r8

  ! gallon (U.S.) (gal)
  ! to liter (L)

  real (kind = k_r8), parameter, public ::                             &
    c_gallon_us_to_liter =                                             &
      3.785412e+00_k_r8

  ! gallon (U.S.) per day (gal/d)
  ! to cubic meter per second (m**3/s)

  real (kind = k_r8), parameter, public ::                             &
    c_gallon_us_per_day =                                              &
      4.381264e-08_k_r8

  ! gallon (U.S.) per day (gal/d)
  ! to liter per second (L/s)

  real (kind = k_r8), parameter, public ::                             &
    c_gallon_us_per_day_to_liter_per_second =                          &
      4.381264e-05_k_r8

  ! gallon (U.S.) per horsepower hour (gal/(hp*h))
  ! to cubic meter per joule (m**3/J)

  real (kind = k_r8), parameter, public ::                             &
    c_gallon_us_per_horsepower =                                       &
      1.410089e-09_k_r8

  ! gallon (U.S.) per horsepower hour (gal/(hp*h))
  ! to liter per joule (L/J)

  real (kind = k_r8), parameter, public ::                             &
    c_gallon_per_horsepower_to_liter_per_joule =                       &
      1.410089e-06_k_r8

  ! gallon (U.S.) per minute (gpm) (gal/min) 
  ! to cubic meter per second (m**3/s)

  real (kind = k_r8), parameter, public ::                             &
    c_gallon_us_per_minute =                                           &
      6.309020e-05_k_r8

  ! gallon (U.S.) per minute (gpm) (gal/min)
  ! to liter per second (L/s)

  real (kind = k_r8), parameter, public ::                             &
    c_gallon_us_per_minute_to_liter_per_second =                       &
      6.309020e-02_k_r8

  ! gamma (gamma)
  ! to tesla (T)

  real (kind = k_r8), parameter, public ::                             &
    c_gamma =                                                          &
      1.0e-09_k_r8

  ! gauss (Gs, G)
  ! to tesla (T)

  real (kind = k_r8), parameter, public ::                             &
    c_gauss =                                                          &
      1.0e-04_k_r8

  ! gilbert (Gi)
  ! to ampere (A)

  real (kind = k_r8), parameter, public ::                             &
    c_gilbert =                                                        &
      7.957747e-01_k_r8

  ! gill (Canadian and U.K. (Imperial)) (gi (Imperial))
  ! to cubic meter (m**3)

  real (kind = k_r8), parameter, public ::                             &
    c_gill_imperial =                                                  &
      1.420653e-04_k_r8

  ! gill (Canadian and U.K. (Imperial)) (gi (Imperial))
  ! to liter (L)

  real (kind = k_r8), parameter, public ::                             &
    c_gill_imperial_to_liter =                                         &
      1.420653e-01_k_r8

  ! gill (U.S.) (gi)
  ! to cubic meter (m**3)

  real (kind = k_r8), parameter, public ::                             &
    c_gill =                                                           &
      1.182941e-04_k_r8

  ! gill (U.S.) (gi)
  ! to liter (L)

  real (kind = k_r8), parameter, public ::                             &
    c_gill_to_liter =                                                  &
      1.182941e-01_k_r8

  ! gon (also called grade) (gon)
  ! to radian (rad)

  real (kind = k_r8), parameter, public ::                             &
    c_gon =                                                            &
      1.570796e-02_k_r8

  ! gon (also called grade) (gon)
  ! to degree (angle) 

  real (kind = k_r8), parameter, public ::                             &
    c_gon_to_degree =                                                  &
      9.0e-01_k_r8

  ! grain (gr)
  ! to kilogram (kg)

  real (kind = k_r8), parameter, public ::                             &
    c_grain =                                                          &
      6.479891e-05_k_r8

  ! grain (gr)
  ! to milligram (mg)

  real (kind = k_r8), parameter, public ::                             &
    c_grain_to_milligram =                                             &
      6.479891e+01_k_r8

  ! grain per gallon (U.S.) (gr/gal)
  ! to kilogram per cubic meter (kg/m**3)

  real (kind = k_r8), parameter, public ::                             &
    c_grain_per_gallon_us =                                            &
      1.711806e-02_k_r8

  ! grain per gallon (U.S.) (gr/gal)
  ! to milligram per liter (mg/L)

  real (kind = k_r8), parameter, public ::                             &
    c_grain_per_gallon_us_to_milligram_per_liter =                     &
      1.711806e+01_k_r8

  ! gram-force per square centimeter (gf/cm**2)
  ! to pascal (Pa)

  real (kind = k_r8), parameter, public ::                             &
    c_gram_force_per_square_centimeter =                               &
      9.80665e+01_k_r8

  ! gram per cubic centimeter (g/cm**3)
  ! to kilogram per cubic meter (kg/m**3)

  real (kind = k_r8), parameter, public ::                             &
    c_gram_per_cubic_centimeter =                                      &
      1.0e+03_k_r8

  ! hectare (ha)
  ! to square meter (m**2)

  real (kind = k_r8), parameter, public ::                             &
    c_hectare =                                                        &
      1.0e+04_k_r8

  ! horsepower (550 ft*lbf/s) (hp)
  ! to watt (W)

  real (kind = k_r8), parameter, public ::                             &
    c_horsepower =                                                     &
      7.456999e+02_k_r8

  ! horsepower (boiler)
  ! to watt (W)

  real (kind = k_r8), parameter, public ::                             &
    c_horsepower_boiler =                                              &
      9.80950e+03_k_r8

  ! horsepower (electric)
  ! to watt (W)

  real (kind = k_r8), parameter, public ::                             &
    c_horsepower_electric =                                            &
      7.46e+02_k_r8

  ! horsepower (metric)
  ! to watt (W)

  real (kind = k_r8), parameter, public ::                             &
    c_horsepower_metric =                                              &
      7.354988e+02_k_r8

  ! horsepower (U.K.)
  ! to watt (W)

  real (kind = k_r8), parameter, public ::                             &
    c_horsepower_uk =                                                  &
      7.4570e+02_k_r8

  ! horsepower (water)
  ! to watt (W)

  real (kind = k_r8), parameter, public ::                             &
    c_horsepower_water =                                               &
      7.46043e+02_k_r8

  ! hour (h)
  ! to second (s)

  real (kind = k_r8), parameter, public ::                             &
    c_hour =                                                           &
      3.6e+03_k_r8

  ! hour (sidereal)
  ! to second (s)

  real (kind = k_r8), parameter, public ::                             &
    c_hour_sidereal =                                                  &
      3.590170e+03_k_r8

  ! hundredweight (long, 112 lb)
  ! to kilogram (kg)

  real (kind = k_r8), parameter, public ::                             &
    c_hundredweight_long =                                             &
      5.080235e+01_k_r8

  ! hundredweight (short, 100 lb)
  ! to kilogram (kg)

  real (kind = k_r8), parameter, public ::                             &
    c_hundredweight_short =                                            &
      4.535924e+01_k_r8

  ! inch (in)
  ! to meter (m)

  real (kind = k_r8), parameter, public ::                             &
    c_inch =                                                           &
      2.54e-02_k_r8

  ! inch (in)
  ! to centimeter (cm)

  real (kind = k_r8), parameter, public ::                             &
    c_inch_to_centimeter =                                             &
      2.54e+00_k_r8

  ! inch of mercury (32 Fahrenheit)
  ! to pascal (Pa)

  real (kind = k_r8), parameter, public ::                             &
    c_inch_of_mercury_32f =                                            &
      3.38638e+03_k_r8

  ! inch of mercury (32 Fahrenheit)
  ! to kilopascal (kPa)

  real (kind = k_r8), parameter, public ::                             &
    c_inch_of_mercury_32f_to_kilopascal =                              &
      3.38638e+00_k_r8

  ! inch of mercury (60 Fahrenheit)
  ! to pascal (Pa)

  real (kind = k_r8), parameter, public ::                             &
    c_inch_of_mercury_60f =                                            &
      3.37685e+03_k_r8

  ! inch of mercury (60 Fahrenheit)
  ! to kilopascal (kPa)

  real (kind = k_r8), parameter, public ::                             &
    c_inch_of_mercury_60f_to_kilopascal =                              &
      3.37685e+00_k_r8

  ! inch of mercury, conventional (inHg)
  ! to pascal (Pa)

  real (kind = k_r8), parameter, public ::                             &
    c_inch_of_mercury_conventional =                                   &
      3.386389e+03_k_r8

  ! inch of mercury, conventional (inHg)
  ! to kilopascal (kPa)

  real (kind = k_r8), parameter, public ::                             &
    c_inch_of_mercury_conventional_to_kilopascal =                     &
      3.386389e+00_k_r8

  ! inch of water (39.2 Fahrenheit)
  ! to pascal (Pa)

  real (kind = k_r8), parameter, public ::                             &
    c_inch_of_water_39_2f =                                            &
      2.49082e+02_k_r8

  ! inch of water (60 Fahrenheit)
  ! to pascal (Pa)

  real (kind = k_r8), parameter, public ::                             &
    c_inch_of_water_60f =                                              &
      2.4884e+02_k_r8

  ! inch of water, conventional (inH2O)
  ! to pascal (Pa)

  real (kind = k_r8), parameter, public ::                             &
    c_inch_of_water =                                                  &
      2.490889e+02_k_r8

  ! inch per second (in/s)
  ! to meter per second (m/s)

  real (kind = k_r8), parameter, public ::                             &
    c_inch_per_second =                                                &
      2.54e-02_k_r8

  ! inch per second squared (in/s**2)
  ! to meter per second squared (m/s**2)

  real (kind = k_r8), parameter, public ::                             &
    c_inch_per_second_squared =                                        &
      2.54e-02_k_r8

  ! kayser (K)
  ! to reciprocal meter (m**-1)

  real (kind = k_r8), parameter, public ::                             &
    c_kayser =                                                         &
      1.0e+02_k_r8

  ! kilocalorie IT (kcal IT)
  ! to joule (J)

  real (kind = k_r8), parameter, public ::                             &
    c_kilocalorie_it =                                                 &
      4.1868e+03_k_r8

  ! kilocalorie th (kcal th)
  ! to joule (J)

  real (kind = k_r8), parameter, public ::                             &
    c_kilocalorie_th =                                                 &
      4.184e+03_k_r8

  ! kilocalorie (mean) (kcal)
  ! to joule (J)

  real (kind = k_r8), parameter, public ::                             &
    c_kilocalorie =                                                    &
      4.19002e+03_k_r8

  ! kilocalorie th per minute (kcal th/min)
  ! to watt (W)

  real (kind = k_r8), parameter, public ::                             &
    c_kilocalorie_th_per_minute =                                      &
      6.973333e+01_k_r8

  ! kilocalorie_th per second (kcalth/s)
  ! to watt (W)

  real (kind = k_r8), parameter, public ::                             &
    c_kilocalorie_th_per_second =                                      &
      4.184e+03_k_r8

  ! kilogram-force (kgf)
  ! to newton (N)

  real (kind = k_r8), parameter, public ::                             &
    c_kilogram_force =                                                 &
      9.80665e+00_k_r8

  ! kilogram-force meter (kgf*m)
  ! to newton meter (N*m)

  real (kind = k_r8), parameter, public ::                             &
    c_kilogram_force_meter =                                           &
      9.80665e+00_k_r8

  ! kilogram-force per square centimeter (kgf/cm**2)
  ! to pascal (Pa)

  real (kind = k_r8), parameter, public ::                             &
    c_kilogram_force_per_square_centimeter =                           &
      9.80665e+04_k_r8

  ! kilogram-force per square centimeter (kgf/cm**2)
  ! to kilopascal (kPa)

  real (kind = k_r8), parameter, public ::                             &
    c_kilogram_force_per_square_centimeter_to_kilopascal =             &
      9.80665e+01_k_r8

  ! kilogram-force per square meter (kgf/m**2)
  ! to pascal (Pa)

  real (kind = k_r8), parameter, public ::                             &
    c_kilogram_force_per_square_meter =                                &
      9.80665e+00_k_r8

  ! kilogram-force per square millimeter (kgf/mm**2)
  ! to pascal (Pa)

  real (kind = k_r8), parameter, public ::                             &
    c_kilogram_force_per_square_millimeter =                           &
      9.80665e+06_k_r8

  ! kilogram-force per square millimeter (kgf/mm**2)
  ! to megapascal (MPa)

  real (kind = k_r8), parameter, public ::                             &
    c_kilogram_force_per_square_millimeter_to_megapascal =             &
      9.80665e+00_k_r8

  ! kilogram-force second squared per meter (kgf*s**2/m)
  ! to kilogram (kg)

  real (kind = k_r8), parameter, public ::                             &
    c_kilogram_force_second_squared_per_meter =                        &
      9.80665e+00_k_r8

  ! kilometer per hour (km/h)
  ! to meter per second (m/s)

  real (kind = k_r8), parameter, public ::                             &
    c_kilometer_per_hour =                                             &
      2.777778e-01_k_r8

  ! kilopond (kilogram-force) (kp)
  ! to newton (N)

  real (kind = k_r8), parameter, public ::                             &
    c_kilopond =                                                       &
      9.80665e+00_k_r8

  ! kilowatt hour (kW*h)
  ! to joule (J)

  real (kind = k_r8), parameter, public ::                             &
    c_kilowatt_hour =                                                  &
      3.6e+06_k_r8

  ! kilowatt hour (kW*h)
  ! to megajoule (MJ)

  real (kind = k_r8), parameter, public ::                             &
    c_kilowatt_hour_to_megajoule =                                     &
      3.6e+00_k_r8

  ! kip (1 kip= 1000 lbf)
  ! to newton (N)

  real (kind = k_r8), parameter, public ::                             &
    c_kip =                                                            &
      4.448222e+03_k_r8

  ! kip (1 kip= 1000 lbf)
  ! to kilonewton (kN)

  real (kind = k_r8), parameter, public ::                             &
    c_kip_to_kilonewton =                                              &
      4.448222e+00_k_r8

  ! kip per square inch (ksi) (kip/in**2)
  ! to pascal (Pa)

  real (kind = k_r8), parameter, public ::                             &
    c_kip_per_square_inch =                                            &
      6.894757e+06_k_r8

  ! kip per square inch (ksi) (kip/in**2)
  ! to kilopascal (kPa)

  real (kind = k_r8), parameter, public ::                             &
    c_kip_per_square_inch_to_kilopascal =                              &
      6.894757e+03_k_r8

  ! knot (nautical mile per hour)
  ! to meter per second (m/s)

  real (kind = k_r8), parameter, public ::                             &
    c_knot =                                                           &
      5.144444e-01_k_r8

  ! lambert
  ! to candela per square meter (cd/m**2)

  real (kind = k_r8), parameter, public ::                             &
    c_lambert =                                                        &
      3.183099e+03_k_r8

  ! langley (calth/cm**2)
  ! to joule per square meter (J/m**2)

  real (kind = k_r8), parameter, public ::                             &
    c_langley =                                                        &
      4.184e+04_k_r8

  ! light year (l.y.)
  ! to meter (m)

  real (kind = k_r8), parameter, public ::                             &
    c_light_year =                                                     &
      9.46073e+15_k_r8

  ! liter (L)
  ! to cubic meter (m**3)

  real (kind = k_r8), parameter, public ::                             &
    c_liter =                                                          &
      1.0e-03_k_r8

  ! lumen per square foot (lm/ft**2)
  ! to lux (lx)

  real (kind = k_r8), parameter, public ::                             &
    c_lumen_per_square_foot =                                          &
      1.076391e+01_k_r8

  ! maxwell (Mx)
  ! to weber (Wb)

  real (kind = k_r8), parameter, public ::                             &
    c_maxwell =                                                        &
      1.0e-08_k_r8

  ! mho
  ! to siemens (S)

  real (kind = k_r8), parameter, public ::                             &
    c_mho =                                                            &
      1.0e+00_k_r8

  ! microinch
  ! to meter (m)

  real (kind = k_r8), parameter, public ::                             &
    c_microinch =                                                      &
      2.54e-08_k_r8

  ! microinch
  ! to micrometer (mum)

  real (kind = k_r8), parameter, public ::                             &
    c_microinch_to_micrometer =                                        &
      2.54e-02_k_r8

  ! micron (mu)
  ! to meter (m)

  real (kind = k_r8), parameter, public ::                             &
    c_micron =                                                         &
      1.0e-06_k_r8

  ! micron (mu)
  ! to micrometer (mum)

  real (kind = k_r8), parameter, public ::                             &
    c_micron_to_micrometer =                                           &
      1.0e+00_k_r8

  ! mil (0.001 in)
  ! to meter (m)

  real (kind = k_r8), parameter, public ::                             &
    c_mil =                                                            &
      2.54e-05_k_r8

  ! mil (0.001 in)
  ! to millimeter (mm)

  real (kind = k_r8), parameter, public ::                             &
    c_mil_to_millimeter =                                              &
      2.54e-02_k_r8

  ! mil (angle)
  ! to radian (rad)

  real (kind = k_r8), parameter, public ::                             &
    c_mil_to_radian =                                                  &
      9.817477e-04_k_r8

  ! mil (angle)
  ! to degree (angle)

  real (kind = k_r8), parameter, public ::                             &
    c_mil_to_degree =                                                  &
      5.625e-02_k_r8

  ! mile (mi)
  ! to meter (m)

  real (kind = k_r8), parameter, public ::                             &
    c_mile =                                                           &
      1.609344e+03_k_r8

  ! mile (mi)
  ! to kilometer (km)

  real (kind = k_r8), parameter, public ::                             &
    c_mile_to_kilometer =                                              &
      1.609344e+00_k_r8

  ! mile (based on U.S. survey foot) (mi)
  ! to meter (m)

  real (kind = k_r8), parameter, public ::                             &
    c_mile_us =                                                        &
      1.609347e+03_k_r8

  ! mile (based on U.S. survey foot) (mi)
  ! to kilometer (km)

  real (kind = k_r8), parameter, public ::                             &
    c_mile_us_to_kilometer =                                           &
      1.609347e+00_k_r8

  ! mile, nautical
  ! to meter (m)

  real (kind = k_r8), parameter, public ::                             &
    c_mile_nautical =                                                  &
      1.852e+03_k_r8

  ! mile per gallon (U.S.) (mpg) (mi/gal)
  ! to meter per cubic meter (m/m**3)

  real (kind = k_r8), parameter, public ::                             &
    c_mile_per_gallon_us =                                             &
      4.251437e+05_k_r8

  ! mile per gallon (U.S.) (mpg) (mi/gal)
  ! to kilometer per liter (km/L)

  real (kind = k_r8), parameter, public ::                             &
    c_mile_per_gallon_us_to_kilometer_per_liter =                      &
      4.251437e-01_k_r8

  ! mile perhour (mi/h)
  ! to meter per second (m/s)

  real (kind = k_r8), parameter, public ::                             &
    c_mile_per_hour =                                                  &
      4.4704e-01_k_r8

  ! mile per hour (mi/h)
  ! to kilometer per hour (km/h)

  real (kind = k_r8), parameter, public ::                             &
    c_mile_per_hour_to_kilometer_per_hour =                            &
      1.609344e+00_k_r8

  ! mile per minute (mi/min)
  ! to meter per second (m/s)

  real (kind = k_r8), parameter, public ::                             &
    c_mile_per_minute =                                                &
      2.68224e+01_k_r8

  ! mile per second (mi/s)
  ! to meter per second (m/s)

  real (kind = k_r8), parameter, public ::                             &
    c_mile_per_second =                                                &
      1.609344e+03_k_r8

  ! millibar (mbar)
  ! to pascal (Pa)

  real (kind = k_r8), parameter, public ::                             &
    c_millibar =                                                       &
      1.0e+02_k_r8

  ! millibar (mbar)
  ! to kilopascal (kPa)

  real (kind = k_r8), parameter, public ::                             &
    c_millibar_to_kilopascal =                                         &
      1.0e-01_k_r8

  ! millimeter of mercury, conventional (mmHg)
  ! to pascal (Pa)

  real (kind = k_r8), parameter, public ::                             &
    c_millimeter_of_mercury_conventional =                             &
      1.333224e+02_k_r8

  ! millimeter of water, conventional (mm H2O)
  ! to pascal (Pa)

  real (kind = k_r8), parameter, public ::                             &
    c_millimeter_of_water_conventional =                               &
      9.80665e+00_k_r8

  ! minute (angle)
  ! to radian (rad)

  real (kind = k_r8), parameter, public ::                             &
    c_minute =                                                         &
      2.908882e-04_k_r8

  ! minute (min)
  ! to second (s)

  real (kind = k_r8), parameter, public ::                             &
    c_minute_to_second =                                               &
      6.0e+01_k_r8

  ! minute (sidereal)
  ! to second (s)

  real (kind = k_r8), parameter, public ::                             &
    c_minute_sidereal_to_second =                                      &
      5.983617e+01_k_r8

  ! oersted (Oe)
  ! to ampere per meter (A/m)

  real (kind = k_r8), parameter, public ::                             &
    c_oersted =                                                        &
      7.957747e+01_k_r8

  ! ohm centimeter (omega*cm)
  ! to ohm meter (omega*m)

  real (kind = k_r8), parameter, public ::                             &
    c_ohm_centimeter =                                                 &
      1.0e-02_k_r8

  ! ohm circular-mil per foot
  ! to ohm meter (omega*m)

  real (kind = k_r8), parameter, public ::                             &
    c_ohm_circular_mil_per_foot =                                      &
      1.662426e-09_k_r8

  ! ohm circular-mil per foot 
  ! to ohm square millimeter per meter (omega*mm**2/m)

  real (kind = k_r8), parameter, public ::                             &
    c_ohm_circular_mil_per_foot_to_ohm_square_millimeter_per_meter =   &
      1.662426e-03_k_r8

  ! ounce (avoirdupois) (oz)
  ! to kilogram (kg)

  real (kind = k_r8), parameter, public ::                             &
    c_ounce_avordupois =                                               &
      2.834952e-02_k_r8

  ! ounce (avoirdupois) (oz)
  ! to gram (g)

  real (kind = k_r8), parameter, public ::                             &
    c_ounce_avoiddupois_to_gram =                                      &
      2.834952e+01_k_r8

  ! ounce (troy or apothecary) (oz)
  ! to kilogram (kg)

  real (kind = k_r8), parameter, public ::                             &
    c_ounce_troy =                                                     &
      3.110348e-02_k_r8

  ! ounce (troy or apothecary) (oz)
  ! to gram (g)

  real (kind = k_r8), parameter, public ::                             &
    c_ounce_troy_to_gram =                                             &
      3.110348e+01_k_r8

  ! ounce (Canadian and U.K. fluid (Imperial)) (fl oz)
  ! to cubic meter (m**3)

  real (kind = k_r8), parameter, public ::                             &
    c_ounce_imperial_fluid =                                           &
      2.841306e-05_k_r8

  ! ounce (Canadian and U.K. fluid (Imperial)) (fl oz)
  ! to milliliter (mL)

  real (kind = k_r8), parameter, public ::                             &
    c_ounce_imperial_fluid_to_milliliter =                             &
      2.841306e+01_k_r8

  ! ounce (U.S. fluid) (fl oz)
  ! to cubic meter (m**3)

  real (kind = k_r8), parameter, public ::                             &
    c_ounce_us_fluid =                                                 &
      2.957353e-05_k_r8

  ! ounce (U.S. fluid) (fl oz)
  ! to milliliter (mL)

  real (kind = k_r8), parameter, public ::                             &
    c_ounce_us_fluid_to_milliliter =                                   &
      2.957353e+01_k_r8

  ! ounce (avoirdupois)-force (ozf)
  ! to newton (N)

  real (kind = k_r8), parameter, public ::                             &
    c_ounce_avoirdupois_force =                                        &
      2.780139e-01_k_r8

  ! ounce (avoirdupois)-force inch (ozf in)
  ! to newton meter (N*m)

  real (kind = k_r8), parameter, public ::                             &
    c_ounce_avoirdupois_force_inch =                                   &
      7.061552e-03_k_r8

  ! ounce (avoirdupois)-force inch (ozf in)
  ! to millinewton meter (mN*m)

  real (kind = k_r8), parameter, public ::                             &
    c_ounce_avoirdupois_force_inch_to_millinewton =                    &
      7.061552e+00_k_r8

  ! ounce (avoirdupois) per cubic inch (oz/in3) 
  ! to kilogram per cubic meter (kg/m**3)

  real (kind = k_r8), parameter, public ::                             &
    c_ounce_avoirdupois_per_cubic_inch =                               &
      1.729994e+03_k_r8

  ! ounce (avoirdupois) per gallon (Canadian and U.K. (Imperial)) (oz/gal) 
  ! to kilogram per cubic meter (kg/m**3)

  real (kind = k_r8), parameter, public ::                             &
    c_ounce_avoirdupois_per_gallon_imperial =                          &
      6.236023e+00_k_r8

  ! ounce (avoirdupois) per gallon (Canadian and U.K. (Imperial)) (oz/gal) 
  ! to gram per liter (g/L)

  real (kind = k_r8), parameter, public ::                             &
    c_ounce_avoirdupois_per_gallon_imperial_to_gram_per_liter =        &
      6.236023e+00_k_r8

  ! ounce (avoirdupois) per gallon (U.S.) (oz/gal) 
  ! to kilogram per cubic meter (kg/m**3)

  real (kind = k_r8), parameter, public ::                             &
    c_ounce_avoirdupois_per_gallon_us =                                &
      7.489152e+00_k_r8

  ! ounce (avoirdupois) per gallon (U.S.) (oz/gal)
  ! to gram per liter (g/L)

  real (kind = k_r8), parameter, public ::                             &
    c_ounce_ovoirdupois_per_gallon_us_to_gram_per_liter =              &
      7.489152e+00_k_r8

  ! ounce (avoirdupois) per square foot (oz/ft**2) 
  ! to kilogram per square meter (kg/m**2)

  real (kind = k_r8), parameter, public ::                             &
    c_ounce_avoirdupois_per_square_foot =                              &
      3.051517e-01_k_r8

  ! ounce (avoirdupois) per square inch (oz/in**2)
  ! to kilogram per square meter (kg/m**2)

  real (kind = k_r8), parameter, public ::                             &
    c_ounce_avoirdupois_per_square_inch =                              &
      4.394185e+01_k_r8

  ! ounce (avoirdupois) per square yard (oz/yd**2)
  ! to kilogram per square meter (kg/m**2)

  real (kind = k_r8), parameter, public ::                             &
    c_ounce_avoirdupois_per_square_yard =                              &
      3.390575e-02_k_r8

  ! parsec (pc)
  ! to meter (m)

  real (kind = k_r8), parameter, public ::                             &
    c_parsec =                                                         &
      3.085678e+16_k_r8

  ! peck (U.S.) (pk)
  ! to cubic meter (m**3)

  real (kind = k_r8), parameter, public ::                             &
    c_peck_us =                                                        &
      8.809768e-03_k_r8

  ! peck (U.S.) (pk)
  ! to liter (L)

  real (kind = k_r8), parameter, public ::                             &
    c_peck_us_to_liter =                                               &
      8.809768e+00_k_r8

  ! pennyweight (dwt)
  ! to kilogram (kg)

  real (kind = k_r8), parameter, public ::                             &
    c_pennyweight =                                                    &
      1.555174e-03_k_r8

  ! pennyweight (dwt)
  ! to gram (g)

  real (kind = k_r8), parameter, public ::                             &
    c_pennyweight_to_gram =                                            &
      1.555174e+00_k_r8

  ! perm (0 Centigrade)
  ! to kilogram per pascal second square meter (kg/(Pa*s*m**2))

  real (kind = k_r8), parameter, public ::                             &
    c_perm_0c =                                                        &
      5.72135e-11_k_r8

  ! perm (23 Centigrade)
  ! to kilogram per pascal second square meter (kg/(Pa*s*m**2))

  real (kind = k_r8), parameter, public ::                             &
    c_perm_23c =                                                       &
      5.74525e-11_k_r8

  ! perm inch (0 Centigrade)
  ! to kilogram per pascal second meter (kg/(Pa*s*m))

  real (kind = k_r8), parameter, public ::                             &
    c_perm_inch_0c =                                                   &
      1.45322e-12_k_r8

  ! perm inch (23 Centigrade)
  ! to kilogram per pascal second meter (kg/(Pa*s*m))

  real (kind = k_r8), parameter, public ::                             &
    c_perm_inch_23c =                                                  &
      1.45929e-12_k_r8

  ! phot (ph)
  ! to lux (lx)

  real (kind = k_r8), parameter, public ::                             &
    c_phot =                                                           &
      1.0e+04_k_r8

  ! pica (computer) (1/6 in)
  ! to meter (m)

  real (kind = k_r8), parameter, public ::                             &
    c_pica_pc =                                                        &
      4.233333e-03_k_r8

  ! pica (computer) (1/6 in)
  ! to millimeter (mm)

  real (kind = k_r8), parameter, public ::                             &
    c_pica_computer_to_millimeter =                                    &
      4.233333e+00_k_r8

  ! pica (printer's)
  ! to meter (m)

  real (kind = k_r8), parameter, public ::                             &
    c_pica_printer =                                                   &
      4.217518e-03_k_r8

  ! pica (printer's)
  ! to millimeter (mm)

  real (kind = k_r8), parameter, public ::                             &
    c_pica_printer_to_millimeter =                                     &
      4.217518e+00_k_r8

  ! pint (U.S. dry) (dry pt)
  ! to cubic meter (m**3)

  real (kind = k_r8), parameter, public ::                             &
    c_pint_us_dry =                                                    &
      5.506105e-04_k_r8

  ! pint (U.S. dry) (dry pt)
  ! to liter (L)

  real (kind = k_r8), parameter, public ::                             &
    c_pint_us_dry_to_liter =                                           &
      5.506105e-01_k_r8

  ! pint (U.S. liquid) (liq pt)
  ! to cubic meter (m**3)

  real (kind = k_r8), parameter, public ::                             &
    c_pint__us_liquid =                                                &
      4.731765e-04_k_r8

  ! pint (U.S. liquid) (liq pt)
  ! to liter (L)

  real (kind = k_r8), parameter, public ::                             &
    c_pint_us_liquid_to_liter =                                        &
      4.731765e-01_k_r8

  ! point (computer) (1/72 in)
  ! to meter (m)

  real (kind = k_r8), parameter, public ::                             &
    c_point_pc =                                                       &
      3.527778e-04_k_r8

  ! point (computer) (1/72 in)
  ! to millimeter (mm)

  real (kind = k_r8), parameter, public ::                             &
    c_point_computer_to_millimeter =                                   &
      3.527778e-01_k_r8

  ! point (printer's)
  ! to meter (m)

  real (kind = k_r8), parameter, public ::                             &
    c_point_printer =                                                  &
      3.514598e-04_k_r8

  ! point (printer's)
  ! to millimeter (mm)

  real (kind = k_r8), parameter, public ::                             &
    c_point_printer_to_millimeter =                                    &
      3.514598e-01_k_r8

  ! poise (P)
  ! to pascal second (Pa*s)

  real (kind = k_r8), parameter, public ::                             &
    c_poise =                                                          &
      1.0e-01_k_r8

  ! pound (avoirdupois) (lb)
  ! to kilogram (kg)

  real (kind = k_r8), parameter, public ::                             &
    c_pound =                                                          &
      4.535924e-01_k_r8

  ! pound (troy or apothecary) (lb)
  ! to kilogram (kg)

  real (kind = k_r8), parameter, public ::                             &
    c_pound_troy =                                                     &
      3.732417e-01_k_r8

  ! poundal
  ! to newton (N)

  real (kind = k_r8), parameter, public ::                             &
    c_poundal =                                                        &
      1.382550e-01_k_r8

  ! poundal per square foot
  ! to pascal (Pa)

  real (kind = k_r8), parameter, public ::                             &
    c_poundal_per_square_foot =                                        &
      1.488164e+00_k_r8

  ! poundal second per square foot
  ! to pascal second (Pa*s)

  real (kind = k_r8), parameter, public ::                             &
    c_poundal_second_per_square_foot =                                 &
      1.488164e+00_k_r8

  ! pound foot squared (lb*ft**2)
  ! to kilogram meter squared (kg*m**2)

  real (kind = k_r8), parameter, public ::                             &
    c_pound_foot_squared =                                             &
      4.214011e-02_k_r8

  ! pound-force (lbf)
  ! to newton (N)

  real (kind = k_r8), parameter, public ::                             &
    c_pound_force =                                                    &
      4.448222e+00_k_r8

  ! pound-force foot (lbf*ft)
  ! to newton meter (N*m)

  real (kind = k_r8), parameter, public ::                             &
    c_pound_force_foot =                                               &
      1.355818e+00_k_r8

  ! pound-force foot per inch (lbf*ft/in)
  ! to newton meter per meter (N*m/m)

  real (kind = k_r8), parameter, public ::                             &
    c_pound_force_foot_per_inch =                                      &
      5.337866e+01_k_r8

  ! pound-force inch (lbf*in)
  ! to newton meter (N*m)

  real (kind = k_r8), parameter, public ::                             &
    c_pound_force_inch =                                               &
      1.129848e-01_k_r8

  ! pound-force inch per inch (lbf*in/in)
  ! to newton meter per meter (N*m/m)

  real (kind = k_r8), parameter, public ::                             &
    c_pound_force_inch_per_inch =                                      &
      4.448222e+00_k_r8

  ! pound-force per foot (lbf/ft)
  ! to newton per meter (N/m)

  real (kind = k_r8), parameter, public ::                             &
    c_pound_force_per_foot =                                           &
      1.459390e+01_k_r8

  ! pound-force per inch (lbf/in)
  ! to newton per meter (N/m)

  real (kind = k_r8), parameter, public ::                             &
    c_pound_force_per_inch =                                           &
      1.751268e+02_k_r8

  ! pound-force per pound (lbf/lb) (thrust to mass ratio)
  ! to newton per kilogram (N/kg)

  real (kind = k_r8), parameter, public ::                             &
    c_pound_force_per_pound =                                          &
      9.80665e+00_k_r8

  ! pound-force per square foot (lbf/ft**2)
  ! to pascal (Pa)

  real (kind = k_r8), parameter, public ::                             &
    c_pound_force_per_square_foot =                                    &
      4.788026e+01_k_r8

  ! pound-force per square inch (psi) (lbf/in**2)
  ! to pascal (Pa)

  real (kind = k_r8), parameter, public ::                             &
    c_pound_force_per_square_inch =                                    &
      6.894757e+03_k_r8

  ! pound-force per square inch (psi) (lbf/in**2)
  ! to kilopascal (kPa)

  real (kind = k_r8), parameter, public ::                             &
    c_pound_force_per_square_inch_to_kilopascal =                      &
      6.894757e+00_k_r8

  ! pound-force second per square foot (lbf*s/ft**2)
  ! to pascal second (Pa*s)

  real (kind = k_r8), parameter, public ::                             &
    c_pound_force_second_per_square_foot =                             &
      4.788026e+01_k_r8

  ! pound-force second per square inch (lbf*s/in**2)
  ! to pascal second (Pa*s)

  real (kind = k_r8), parameter, public ::                             &
    c_pound_force_second_per_square_inch =                             &
      6.894757e+03_k_r8

  ! pound inch squared (lb in**2)
  ! to kilogram meter squared (kg*m**2)

  real (kind = k_r8), parameter, public ::                             &
    c_pound_inch_squared =                                             &
      2.926397e-04_k_r8

  ! pound per cubic foot (lb/ft3)
  ! to kilogram per cubic meter (kg/m**3)

  real (kind = k_r8), parameter, public ::                             &
    c_pound_per_cubic_foot =                                           &
      1.601846e+01_k_r8

  ! pound per cubic inch (lb/in3)
  ! to kilogram per cubic meter (kg/m**3)

  real (kind = k_r8), parameter, public ::                             &
    c_pound_per_cubic_inch =                                           &
      2.767990e+04_k_r8

  ! pound per cubic yard (lb/yd3)
  ! to kilogram per cubic meter (kg/m**3)

  real (kind = k_r8), parameter, public ::                             &
    c_pound_per_cubic_yard =                                           &
      5.932764e-01_k_r8

  ! pound per foot (lb/ft)
  ! to kilogram per meter (kg/m)

  real (kind = k_r8), parameter, public ::                             &
    c_pound_per_foot =                                                 &
      1.488164e+00_k_r8

  ! pound per foot hour (lb/(ft*h))
  ! to pascal second (Pa*s)

  real (kind = k_r8), parameter, public ::                             &
    c_pound_per_foot_hour =                                            &
      4.133789e-04_k_r8

  ! pound per foot second (lb/(ft*s))
  ! to pascal second (Pa*s)

  real (kind = k_r8), parameter, public ::                             &
    c_pound_per_foot_second =                                          &
      1.488164e+00_k_r8

  ! pound per gallon (Canadian and U.K. (Imperial)) (lb/gal)
  ! to kilogram per cubic meter (kg/m**3)

  real (kind = k_r8), parameter, public ::                             &
    c_pound_per_gallon_uk =                                            &
      9.977637e+01_k_r8

  ! pound per gallon (Canadian and U.K. (Imperial)) (lb/gal)
  ! to kilogram per liter (kg/L)

  real (kind = k_r8), parameter, public ::                             &
    c_pound_per_gallon_uk_to_liter =                                   &
      9.977637e-02_k_r8

  ! pound per gallon (U.S.) (lb/gal)
  ! to kilogram per cubic meter (kg/m**3)

  real (kind = k_r8), parameter, public ::                             &
    c_pound_per_gallon_us =                                            &
      1.198264e+02_k_r8

  ! pound per gallon (U.S.) (lb/gal)
  ! to kilogram per liter (kg/L)

  real (kind = k_r8), parameter, public ::                             &
    c_pound_per_gallon_us_to_liter =                                   &
      1.198264e-01_k_r8

  ! pound per horsepower hour (lb/(hp*h))
  ! to kilogram per joule (kg/J)

  real (kind = k_r8), parameter, public ::                             &
    c_pound_per_horsepower_hour =                                      &
      1.689659e-07_k_r8

  ! pound per hour (lb/h)
  ! to kilogram per second (kg/s)

  real (kind = k_r8), parameter, public ::                             &
    c_pound_per_hour =                                                 &
      1.259979e-04_k_r8

  ! pound per inch (lb/in)
  ! to kilogram per meter (kg/m)

  real (kind = k_r8), parameter, public ::                             &
    c_pound_per_inch =                                                 &
      1.785797e+01_k_r8

  ! pound per minute (lb/min)
  ! to kilogram per second (kg/s)

  real (kind = k_r8), parameter, public ::                             &
    c_pound_per_minute =                                               &
      7.559873e-03_k_r8

  ! pound per second (lb/s)
  ! to kilogram per second (kg/s)

  real (kind = k_r8), parameter, public ::                             &
    c_pound_per_second =                                               &
      4.535924e-01_k_r8

  ! pound per square foot (lb/ft**2)
  ! to kilogram per square meter (kg/m**2)

  real (kind = k_r8), parameter, public ::                             &
    c_pound_per_square_foot =                                          &
      4.882428e+00_k_r8

  ! pound per square inch (not pound-force) (lb/in**2)
  ! to kilogram per square meter (kg/m**2)

  real (kind = k_r8), parameter, public ::                             &
    c_pound_per_square_inch =                                          &
      7.030696e+02_k_r8

  ! pound per yard (lb/yd)
  ! to kilogram per meter (kg/m)

  real (kind = k_r8), parameter, public ::                             &
    c_pound_per_yard =                                                 &
      4.960546e-01_k_r8

  ! psi (pound-force per square inch) (lbf/in**2)
  ! to pascal (Pa)

  real (kind = k_r8), parameter, public ::                             &
    c_psi =                                                            &
      6.894757e+03_k_r8

  ! psi (pound-force per square inch) (lbf/in**2)
  ! to kilopascal (kPa)

  real (kind = k_r8), parameter, public ::                             &
    c_psi_to_kilopascal =                                              &
      6.894757e+00_k_r8

  ! quad (1015 Btu IT)
  ! to joule (J)

  real (kind = k_r8), parameter, public ::                             &
    c_quad =                                                           &
      1.055056e+18_k_r8

  ! quart (U.S. dry) (dry qt)
  ! to cubic meter (m**3)

  real (kind = k_r8), parameter, public ::                             &
    c_quart_us_dry =                                                   &
      1.101221e-03_k_r8

  ! quart (U.S. dry) (dry qt)
  ! to liter (L)

  real (kind = k_r8), parameter, public ::                             &
    c_quart_us_dry_to_liter =                                          &
      1.101221e+00_k_r8

  ! quart (U.S. liquid) (liq qt)
  ! to cubic meter (m**3)

  real (kind = k_r8), parameter, public ::                             &
    c_quart_us_liquid =                                                &
      9.463529e-04_k_r8

  ! quart (U.S. liquid) (liq qt)
  ! to liter (L)

  real (kind = k_r8), parameter, public ::                             &
    c_quart_us_liquid_to_liter =                                       &
      9.463529e-01_k_r8

  ! rad (absorbed dose) (rad)
  ! to gray (Gy)

  real (kind = k_r8), parameter, public ::                             &
    c_rad =                                                            &
      1.0e-02_k_r8

  ! rem (rem)
  ! to sievert (Sv)

  real (kind = k_r8), parameter, public ::                             &
    c_rem =                                                            &
      1.0e-02_k_r8

  ! revolution (r)
  ! to radian (rad)

  real (kind = k_r8), parameter, public ::                             &
    c_revolution =                                                     &
      6.283185e+00_k_r8

  ! revolution per minute (rpm) (r/min)
  ! to radian per second (rad/s)

  real (kind = k_r8), parameter, public ::                             &
    c_revolution_per_minute =                                          &
      1.047198e-01_k_r8

  ! rhe
  ! to reciprocal pascal second (Pa*s)**-1

  real (kind = k_r8), parameter, public ::                             &
    c_rhe =                                                            &
      1.0e+01_k_r8

  ! rod (based on U.S. survey foot) (rd)
  ! to meter (m)

  real (kind = k_r8), parameter, public ::                             &
    c_rod_us =                                                         &
      5.029210e+00_k_r8

  ! roentgen (R)
  ! to coulomb per kilogram (C/kg)

  real (kind = k_r8), parameter, public ::                             &
    c_roentgen =                                                       &
      2.58e-04_k_r8

  ! rpm (revolution per minute) (r/min)
  ! to radian per second (rad/s)

  real (kind = k_r8), parameter, public ::                             &
    c_rpm =                                                            &
      1.047198e-01_k_r8

  ! second (angle)
  ! to radian (rad)

  real (kind = k_r8), parameter, public ::                             &
    c_second =                                                         &
      4.848137e-06_k_r8

  ! second (sidereal)
  ! to second (s)

  real (kind = k_r8), parameter, public ::                             &
    c_second_sidereal =                                                &
      9.972696e-01_k_r8

  ! shake
  ! to second (s)

  real (kind = k_r8), parameter, public ::                             &
    c_shake =                                                          &
      1.0e-08_k_r8

  ! shake
  ! to nanosecond (ns)

  real (kind = k_r8), parameter, public ::                             &
    c_shake_to_nanosecond =                                            &
      1.0e+01_k_r8

  ! slug (slug)
  ! to kilogram (kg)

  real (kind = k_r8), parameter, public ::                             &
    c_slug =                                                           &
      1.459390e+01_k_r8

  ! slug per cubic foot (slug/ft**3)
  ! to kilogram per cubic meter (kg/m**3)

  real (kind = k_r8), parameter, public ::                             &
    c_slug_per_cubic_foot =                                            &
      5.153788e+02_k_r8

  ! slug per foot second (slug/(ft*s))
  ! to pascal second (Pa*s)

  real (kind = k_r8), parameter, public ::                             &
    c_slug_per_foot_second =                                           &
      4.788026e+01_k_r8

  ! square foot (ft**2)
  ! to square meter (m**2)

  real (kind = k_r8), parameter, public ::                             &
    c_square_foot =                                                    &
      9.290304e-02_k_r8

  ! square foot per hour (ft**2/h)
  ! to square meter per second (m**2/s)

  real (kind = k_r8), parameter, public ::                             &
    c_square_foot_per_hour =                                           &
      2.58064e-05_k_r8

  ! square foot per second (ft**2/s)
  ! to square meter per second (m**2/s)

  real (kind = k_r8), parameter, public ::                             &
    c_square_foot_per_second =                                         &
      9.290304e-02_k_r8

  ! square inch (in**2)
  ! to square meter (m**2)

  real (kind = k_r8), parameter, public ::                             &
    c_square_inch =                                                    &
      6.4516e-04_k_r8

  ! square inch (in**2)
  ! to square centimeter (cm**2)

  real (kind = k_r8), parameter, public ::                             &
    c_square_inch_to_square_centimeter =                               &
      6.4516e+00_k_r8

  ! square mile (mi**2)
  ! to square meter (m**2)

  real (kind = k_r8), parameter, public ::                             &
    c_square_mile =                                                    &
      2.589988e+06_k_r8

  ! square mile (mi**2)
  ! to square kilometer (km**2)

  real (kind = k_r8), parameter, public ::                             &
    c_square_mile_to_square_centimeter =                               &
      2.589988e+00_k_r8

  ! square mile (based on U.S. survey foot) (mi**2)
  ! to square meter (m**2)

  real (kind = k_r8), parameter, public ::                             &
    c_square_mile_us =                                                 &
      2.589998e+06_k_r8

  ! square mile (based on U.S. survey foot) (mi**2)
  ! to square kilometer (km**2)

  real (kind = k_r8), parameter, public ::                             &
    c_square_mile_us_to_square_kilometer =                             &
      2.589998e+00_k_r8

  ! square yard (yd**2)
  ! to square meter (m**2)

  real (kind = k_r8), parameter, public ::                             &
    c_square_yard =                                                    &
      8.361274e-01_k_r8

  ! statampere
  ! to ampere (A)

  real (kind = k_r8), parameter, public ::                             &
    c_statampere =                                                     &
      3.335641e-10_k_r8

  ! statcoulomb
  ! to coulomb (C)

  real (kind = k_r8), parameter, public ::                             &
    c_statcoulomb =                                                    &
      3.335641e-10_k_r8

  ! statfarad
  ! to farad (F)

  real (kind = k_r8), parameter, public ::                             &
    c_statfarad =                                                      &
      1.112650e-12_k_r8

  ! stathenry
  ! to henry (H)

  real (kind = k_r8), parameter, public ::                             &
    c_stathenry =                                                      &
      8.987552e+11_k_r8

  ! statmho
  ! to siemens (S)

  real (kind = k_r8), parameter, public ::                             &
    c_statmho =                                                        &
      1.112650e-12_k_r8

  ! statohm
  ! to ohm (omega)

  real (kind = k_r8), parameter, public ::                             &
    c_statohm =                                                        &
      8.987552e+11_k_r8

  ! statvolt
  ! to volt (V)

  real (kind = k_r8), parameter, public ::                             &
    c_statvolt =                                                       &
      2.997925e+02_k_r8

  ! stere (st)
  ! to cubic meter (m**3)

  real (kind = k_r8), parameter, public ::                             &
    c_stere =                                                          &
      1.0e+00_k_r8

  ! stilb (sb)
  ! to candela per square meter (cd/m**2)

  real (kind = k_r8), parameter, public ::                             &
    c_stilb =                                                          &
      1.0e+04_k_r8

  ! stokes (St)
  ! to meter squared per second (m**2/s)

  real (kind = k_r8), parameter, public ::                             &
    c_stokes =                                                         &
      1.0e-04_k_r8

  ! tablespoon
  ! to cubic meter (m**3)

  real (kind = k_r8), parameter, public ::                             &
    c_tablespoon =                                                     &
      1.478676e-05_k_r8

  ! tablespoon
  ! to milliliter (mL)

  real (kind = k_r8), parameter, public ::                             &
    c_tablespoon_to_milliliter =                                       &
      1.478676e+01_k_r8

  ! teaspoon
  ! to cubic meter (m**3)

  real (kind = k_r8), parameter, public ::                             &
    c_teaspoon =                                                       &
      4.928922e-06_k_r8

  ! teaspoon
  ! to milliliter (mL)

  real (kind = k_r8), parameter, public ::                             &
    c_teaspoon_to_milliliter =                                         &
      4.928922e+00_k_r8

  ! tex
  ! to kilogram per meter (kg/m)

  real (kind = k_r8), parameter, public ::                             &
    c_tex =                                                            &
      1.0e-06_k_r8

  ! therm (EC)
  ! to joule (J)

  real (kind = k_r8), parameter, public ::                             &
    c_therm_ec =                                                       &
      1.05506e+08_k_r8

  ! therm (U.S.)
  ! to joule (J)

  real (kind = k_r8), parameter, public ::                             &
    c_therm_us =                                                       &
      1.054804e+08_k_r8

  ! ton, assay (AT)
  ! to kilogram (kg)

  real (kind = k_r8), parameter, public ::                             &
    c_ton_assay =                                                      &
      2.916667e-02_k_r8

  ! ton, assay (AT)
  ! to gram (g)

  real (kind = k_r8), parameter, public ::                             &
    c_ton_assay_to_gram =                                              &
      2.916667e+01_k_r8

  ! ton-force (2000 lbf)
  ! to newton (N)

  real (kind = k_r8), parameter, public ::                             &
    c_ton_force =                                                      &
      8.896443e+03_k_r8

  ! ton-force (2000 lbf)
  ! to kilonewton (kN)

  real (kind = k_r8), parameter, public ::                             &
    c_ton_force_to_kilonewton =                                        &
      8.896443e+00_k_r8

  ! ton, long (2240 lb)
  ! to kilogram (kg)

  real (kind = k_r8), parameter, public ::                             &
    c_ton_long =                                                       &
      1.016047e+03_k_r8

  ! ton, long, per cubic yard
  ! to kilogram per cubic meter (kg/m**3)

  real (kind = k_r8), parameter, public ::                             &
    c_ton_long_per_cubic_yard =                                        &
      1.328939e+03_k_r8

  ! ton, metric (t)
  ! to kilogram (kg)

  real (kind = k_r8), parameter, public ::                             &
    c_ton_metric =                                                     &
      1.0e+03_k_r8

  ! tonne (called "metric ton" in U.S.) (t)
  ! to kilogram (kg)

  real (kind = k_r8), parameter, public ::                             &
    c_tonne =                                                          &
      1.0e+03_k_r8

  ! ton of refrigeration (12000 BtuIT/h)
  ! to watt (W)

  real (kind = k_r8), parameter, public ::                             &
    c_ton_of_refrigeration =                                           &
      3.516853e+03_k_r8

  ! ton of TNT (energy equivalent)
  ! to joule (J)

  real (kind = k_r8), parameter, public ::                             &
    c_ton_of_tnt =                                                     &
      4.184e+09_k_r8

  ! ton, register
  ! to cubic meter (m**3)

  real (kind = k_r8), parameter, public ::                             &
    c_ton_register =                                                   &
      2.831685e+00_k_r8

  ! ton, short (2000 lb)
  ! to kilogram (kg)

  real (kind = k_r8), parameter, public ::                             &
    c_ton_short =                                                      &
      9.071847e+02_k_r8

  ! ton, short, per cubic yard
  ! to kilogram per cubic meter (kg/m**3)

  real (kind = k_r8), parameter, public ::                             &
    c_ton_short_per_cubic_yard =                                       &
      1.186553e+03_k_r8

  ! ton, short, per hour
  ! to kilogram per second (kg/s)

  real (kind = k_r8), parameter, public ::                             &
    c_ton_short_per_hour =                                             &
      2.519958e-01_k_r8

  ! torr (Torr)
  ! to pascal (Pa)

  real (kind = k_r8), parameter, public ::                             &
    c_torr =                                                           &
      1.333224e+02_k_r8

  ! unit pole
  ! to weber (Wb)

  real (kind = k_r8), parameter, public ::                             &
    c_unit_pole =                                                      &
      1.256637e-07_k_r8
    
  ! watt hour (W*h)
  ! to joule (J)

  real (kind = k_r8), parameter, public ::                             &
    c_watt_hour =                                                      &
      3.6e+03_k_r8

  ! watt per square centimeter (W/cm**2)
  ! to watt per square meter (W/m**2)

  real (kind = k_r8), parameter, public ::                             &
    c_watt_per_square_centimeter =                                     &
      1.0e+04_k_r8

  ! watt per square inch (W/in**2)
  ! to watt per square meter (W/m**2)

  real (kind = k_r8), parameter, public ::                             &
    c_watt_per_square_inch =                                           &
      1.550003e+03_k_r8

  ! watt second (W*s)
  ! to joule (J)

  real (kind = k_r8), parameter, public ::                             &
    c_watt_second =                                                    &
      1.0e+00_k_r8

  ! yard (yd)
  ! to meter (m)

  real (kind = k_r8), parameter, public ::                             &
    c_yard =                                                           &
      9.144e-01_k_r8

  ! year (365 days)
  ! to second (s)

  real (kind = k_r8), parameter, public ::                             &
    c_year =                                                           &
      3.1536e+07_k_r8

  ! year (sidereal)
  ! to second (s)

  real (kind = k_r8), parameter, public ::                             &
    c_year_sidereal =                                                  &
      3.155815e+07_k_r8

  ! year (tropical)
  ! to second (s)

  real (kind = k_r8), parameter, public ::                             &
    c_year_tropical =                                                  &
      3.155693e+07_k_r8

  ! -- Interface blocks --

!-------------------------------------------------------------------------------
! End specification part
!-------------------------------------------------------------------------------

!-------------------------------------------------------------------------------
! Begin subprogram part
!-------------------------------------------------------------------------------

!-------------------------------------------------------------------------------
! End subprogram part
!-------------------------------------------------------------------------------

end module calc_unit_conv
