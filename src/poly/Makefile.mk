#-------------------------------------------------------------------------------
# This file is part of the Computational Algorithm Library Collection (CALC).
#
# Copyright 2012 The CALC Team
#
# CALC is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# CALC is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with CALC.  If not, see <http://www.gnu.org/licenses/>.
#-------------------------------------------------------------------------------

#-------------------------------------------------------------------------------
# Begin variables
#-------------------------------------------------------------------------------

SUBNAME   := $(NAME)_poly
SUBDIRS   :=

MOD       := $(patsubst %.f90, %.mod, $(wildcard $(SUBNAME)*.f90))
OBJ       := $(patsubst %.f90, %.o,   $(wildcard *.f90))

#-------------------------------------------------------------------------------
# End variables
#-------------------------------------------------------------------------------

#-------------------------------------------------------------------------------
# Begin rules
#-------------------------------------------------------------------------------

all       : $(MOD) $(OBJ)

#-------------------------------------------------------------------------------
# Begin directory-specific rules
#-------------------------------------------------------------------------------

#-------------------------------------------------------------------------------
# End directory-specific rules
#-------------------------------------------------------------------------------

%.mod %.o : %.f90
	$(FC) $(FCFLAGS) $(INCPATHS) -c $<

clean     :
	@for dir in $(SUBDIRS); do                    \
	   $(MAKE) -C $$dir -f $(SUBMAKEFILE) clean ; \
	done;                                         \
	$(RM) $(MOD) $(OBJ)
 
.PHONY    : all $(SUBDIRS) clean

#-------------------------------------------------------------------------------
# End rules
#-------------------------------------------------------------------------------
