pure subroutine solve_quadratic(a, b, c, x, ierr)

!-------------------------------------------------------------------------------
! This file is part of the Computational Algorithm Library Collection (CALC).
!
! Copyright 2012 The CALC Team
!
! CALC is free software: you can redistribute it and/or modify
! it under the terms of the GNU Lesser General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! CALC is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU Lesser General Public License for more details.
!
! You should have received a copy of the GNU Lesser General Public License
! along with CALC.  If not, see <http://www.gnu.org/licenses/>.
!-------------------------------------------------------------------------------

!-------------------------------------------------------------------------------
! Begin specification part
!-------------------------------------------------------------------------------

  ! -- Use association --

  use calc_type,  only: k_i1, k_r8
  use calc_cons, only: c_small

  ! -- Null mapping --

  implicit none

  ! -- Dummy argument declarations --

  real    (kind = k_r8),                intent (in)  :: a, b, c
  real    (kind = k_r8), dimension (2), intent (out) :: x
  integer (kind = k_i1),                intent (out) :: ierr

  ! -- Local declarations --

  real(kind = k_r8) :: d, q

!-------------------------------------------------------------------------------
! End specification part
!-------------------------------------------------------------------------------

!-------------------------------------------------------------------------------
! Begin execution part
!-------------------------------------------------------------------------------

  if (a < c_small) then
    x = huge(x)
    ierr = -1
  else
    d = b * b - 4.0_k_r8 * a * c
    if (d >= 0.0_k_r8) then
      q = -0.5_k_r8 * (b + sign(1.0_k_r8, b) * sqrt(d))
      if (c / q >= q / a) then
        x(1) = q / a
        x(2) = c / q
      else
        x(1) = c / q
        x(2) = q / a
      end if
      ierr = 0
    else
      x    = huge(x)
      ierr = -1
    end if
  end if

  ! -- Last executable statement --

  return

!-------------------------------------------------------------------------------
! End execution part
!-------------------------------------------------------------------------------

!-------------------------------------------------------------------------------
! Begin subprogram part
!-------------------------------------------------------------------------------

!-------------------------------------------------------------------------------
! End subprogram part
!-------------------------------------------------------------------------------

end subroutine solve_quadratic
