module calc_util

!-------------------------------------------------------------------------------
! This file is part of the Computational Algorithm Library Collection (CALC).
!
! Copyright 2012 The CALC Team
!
! CALC is free software: you can redistribute it and/or modify
! it under the terms of the GNU Lesser General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! CALC is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU Lesser General Public License for more details.
!
! You should have received a copy of the GNU Lesser General Public License
! along with CALC.  If not, see <http://www.gnu.org/licenses/>.
!-------------------------------------------------------------------------------

!-------------------------------------------------------------------------------
! Begin specification part
!-------------------------------------------------------------------------------

  ! -- Use association --

  use calc_type, only: k_ch

  ! -- Null mapping --

  implicit none

  ! -- Accessibility --

  private
  public :: calc_foot
  public :: calc_head
  public :: calc_err
  public :: calc_prmb
  public :: calc_ver
  public :: calc_warn
  public :: calc_year

  ! -- Derived type definitions --

  ! -- Type declarations --

  ! -- Interface blocks --

  interface
    subroutine calc_foot()
      implicit none
    end subroutine calc_foot
  end interface

  interface
    subroutine calc_head()
      implicit none
    end subroutine calc_head
  end interface

  interface
    subroutine calc_err(string)
      use calc_type, only: k_ch
      implicit none
      character (kind = k_ch, len = *), intent (in) :: string
    end subroutine calc_err
  end interface

  interface
    subroutine calc_prmb
      implicit none
    end subroutine calc_prmb
  end interface

  interface
    function calc_ver() result(ver)
      use calc_type, only: k_ch
      implicit none
      character (kind = k_ch, len = 4) :: ver
    end function calc_ver
  end interface

  interface
    subroutine calc_warn(string)
      use calc_type, only: k_ch
      implicit none
      character (kind = k_ch, len = *), intent (in) :: string
    end subroutine calc_warn
  end interface

  interface
    function calc_year() result(year)
      use calc_type, only: k_ch
      implicit none
      character (kind = k_ch, len = 4) :: year
    end function calc_year
  end interface

!-------------------------------------------------------------------------------
! End specification part
!-------------------------------------------------------------------------------

!-------------------------------------------------------------------------------
! Begin subprogram part
!-------------------------------------------------------------------------------

!-------------------------------------------------------------------------------
! End subprogram part
!-------------------------------------------------------------------------------

end module calc_util
