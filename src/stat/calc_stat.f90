module calc_stat

!-------------------------------------------------------------------------------
! This file is part of the Computational Algorithm Library Collection (CALC).
!
! Copyright 2012 The CALC Team
!
! CALC is free software: you can redistribute it and/or modify
! it under the terms of the GNU Lesser General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! CALC is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU Lesser General Public License for more details.
!
! You should have received a copy of the GNU Lesser General Public License
! along with CALC.  If not, see <http://www.gnu.org/licenses/>.
!-------------------------------------------------------------------------------

!-------------------------------------------------------------------------------
! Begin specification part
!-------------------------------------------------------------------------------

  ! -- Use association --

  ! -- Null mapping --

  implicit none

  ! -- Accessibility --

  private
  public :: hist_weighted
  public :: hist_discr
  public :: sample_mean
  public :: sample_variance

  ! -- Derived type definitions --

  ! -- Type declarations --

  ! -- Interface blocks --

  interface
    pure subroutine hist_discr(r, x, h, ierr)
      use calc_type, only: k_i1, k_i8, k_r8
      implicit none
      real    (kind = k_r8), dimension (:), intent (in)  :: r
      real    (kind = k_r8),                intent (in)  :: x
      integer (kind = k_i8),                intent (out) :: h
      integer (kind = k_i1),                intent (out) :: ierr
    end subroutine hist_discr
  end interface 

  interface
    pure subroutine hist_weighted(r, x, g, h, ierr)
      use calc_type, only: k_i1, k_i8, k_r8
      implicit none
      real    (kind = k_r8), dimension (:), intent (in)  :: r
      real    (kind = k_r8),                intent (in)  :: x, g
      integer (kind = k_i8),                intent (out) :: h
      integer (kind = k_i1),                intent (out) :: ierr
    end subroutine hist_weighted
  end interface 

  interface
    pure function sample_mean(x) result (xmean)
      use calc_type, only: k_r8
      implicit none
      real (kind = k_r8), dimension (:), intent (in) :: x
      real (kind = k_r8)                             :: xmean
    end function sample_mean
  end interface 

  interface
    pure function sample_variance(x) result (s2)
      use calc_type, only: k_r8
      implicit none
      real (kind = k_r8), dimension (:), intent (in) :: x
      real (kind = k_r8)                             :: s2
    end function sample_variance
  end interface 

!-------------------------------------------------------------------------------
! End specification part
!-------------------------------------------------------------------------------

!-------------------------------------------------------------------------------
! Begin subprogram part
!-------------------------------------------------------------------------------

!-------------------------------------------------------------------------------
! End subprogram part
!-------------------------------------------------------------------------------

end module calc_stat
