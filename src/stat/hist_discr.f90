pure subroutine hist_discr(r, x, h, ierr)

!-------------------------------------------------------------------------------
! This file is part of the Computational Algorithm Library Collection (CALC).
!
! Copyright 2012 The CALC Team
!
! CALC is free software: you can redistribute it and/or modify
! it under the terms of the GNU Lesser General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! CALC is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU Lesser General Public License for more details.
!
! You should have received a copy of the GNU Lesser General Public License
! along with CALC.  If not, see <http://www.gnu.org/licenses/>.
!-------------------------------------------------------------------------------

!-------------------------------------------------------------------------------
! Begin specification part
!-------------------------------------------------------------------------------

  ! -- Use association --

  use calc_type, only: k_i1, k_i4, k_i8, k_r8
  use calc_srch, only: search_vector

  ! -- Null mapping --

  implicit none

  ! Dummy argument declarations

  real    (kind = k_r8), dimension (:), intent (in)  :: r
  real    (kind = k_r8),                intent (in)  :: x
  integer (kind = k_i8), dimension (:), intent (out) :: h
  integer (kind = k_i1),                intent (out) :: ierr

  ! Local declarations

  integer (kind = k_i4) :: i

!-------------------------------------------------------------------------------
! End specification part
!-------------------------------------------------------------------------------

!-------------------------------------------------------------------------------
! Begin execution part
!-------------------------------------------------------------------------------

  if (size(r) /= size(h) + 1) then
    ierr = -1
    return
  end if
  i    = search_vector(r, x)
  h(i) = h(i) + 1

  ! -- Last executable statement --

  return

!-------------------------------------------------------------------------------
! End execution part
!-------------------------------------------------------------------------------

!-------------------------------------------------------------------------------
! Begin subprogram part
!-------------------------------------------------------------------------------

!-------------------------------------------------------------------------------
! End subprogram part
!-------------------------------------------------------------------------------

end subroutine hist_discr
