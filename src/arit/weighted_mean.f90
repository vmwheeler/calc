pure subroutine weighted_mean(x, g, xmean, ierr)

!-------------------------------------------------------------------------------
! This file is part of the Computational Algorithm Library Collection (CALC).
!
! Copyright 2012 The CALC Team
!
! CALC is free software: you can redistribute it and/or modify
! it under the terms of the GNU Lesser General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! CALC is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU Lesser General Public License for more details.
!
! You should have received a copy of the GNU Lesser General Public License
! along with CALC.  If not, see <http://www.gnu.org/licenses/>.
!-------------------------------------------------------------------------------

!-------------------------------------------------------------------------------
! Begin specification part
!-------------------------------------------------------------------------------

  ! -- Use association --

  use calc_type,  only: k_i1, k_r8
  use calc_cons, only: c_small

  ! -- Null mapping --

  implicit none

  ! Dummy argument declarations

  real    (kind = k_r8), dimension (:), intent (in)  :: x, g
  real    (kind = k_r8), dimension (:), intent (out) :: xmean
  integer (kind = k_i1), dimension (:), intent (out) :: ierr

  ! Local declarations

  real (kind = k_r8) :: sumg

!-------------------------------------------------------------------------------
! End specification part
!-------------------------------------------------------------------------------

!-------------------------------------------------------------------------------
! Begin execution part
!-------------------------------------------------------------------------------

  sumg = sum(g)
  if (sumg > c_small) then
    xmean = sum(x * g) / sum(g)
    ierr  = 0
  else
    ierr  = -1
  end if

  ! -- Last executable statement --

  return

!-------------------------------------------------------------------------------
! End execution part
!-------------------------------------------------------------------------------

!-------------------------------------------------------------------------------
! Begin subprogram part
!-------------------------------------------------------------------------------

!-------------------------------------------------------------------------------
! End subprogram part
!-------------------------------------------------------------------------------

end subroutine weighted_mean
