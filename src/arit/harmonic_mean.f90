pure subroutine harmonic_mean(x, xmean, ierr)

!-------------------------------------------------------------------------------
! This file is part of the Computational Algorithm Library Collection (CALC).
!
! Copyright 2012 The CALC Team
!
! CALC is free software: you can redistribute it and/or modify
! it under the terms of the GNU Lesser General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! CALC is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU Lesser General Public License for more details.
!
! You should have received a copy of the GNU Lesser General Public License
! along with CALC.  If not, see <http://www.gnu.org/licenses/>.
!-------------------------------------------------------------------------------

!-------------------------------------------------------------------------------
! Begin specification part
!-------------------------------------------------------------------------------

  ! -- Use association --

  use calc_type,  only: k_i1, k_r8
  use calc_cons, only: c_small

  ! -- Null mapping --

  implicit none

  ! Dummy argument declarations

  real    (kind = k_r8), dimension (:), intent (in)  :: x
  real    (kind = k_r8),                intent (out) :: xmean
  integer (kind = k_i1),                intent (out) :: ierr

  ! Local declarations

!-------------------------------------------------------------------------------
! End specification part
!-------------------------------------------------------------------------------

!-------------------------------------------------------------------------------
! Begin execution part
!-------------------------------------------------------------------------------

  if (all(abs(x) > c_small) .eqv. .true.) then
    xmean = real(size(x), kind = k_r8) / (sum(1.0_k_r8 / x))
    ierr  = 0
  else
    xmean = huge(xmean)
    ierr = -1
  end if

  ! -- Last executable statement --

  return

!-------------------------------------------------------------------------------
! End execution part
!-------------------------------------------------------------------------------

!-------------------------------------------------------------------------------
! Begin subprogram part
!-------------------------------------------------------------------------------

!-------------------------------------------------------------------------------
! End subprogram part
!-------------------------------------------------------------------------------

end subroutine harmonic_mean
