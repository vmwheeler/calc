pure subroutine factorial(n, nfact, ierr)

!-------------------------------------------------------------------------------
! This file is part of the Computational Algorithm Library Collection (CALC).
!
! Copyright 2012 The CALC Team
!
! CALC is free software: you can redistribute it and/or modify
! it under the terms of the GNU Lesser General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! CALC is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU Lesser General Public License for more details.
!
! You should have received a copy of the GNU Lesser General Public License
! along with CALC.  If not, see <http://www.gnu.org/licenses/>.
!-------------------------------------------------------------------------------

!-------------------------------------------------------------------------------
! Begin specification part
!-------------------------------------------------------------------------------

  ! -- Use association --

  use calc_type,  only: k_i1, k_i4

  ! -- Null mapping --

  implicit none

  ! Dummy argument declarations

  integer (kind = k_i4), intent (in)  :: n
  integer (kind = k_i4), intent (out) :: nfact
  integer (kind = k_i1), intent (out) :: ierr

  ! Local declarations

  integer (kind = k_i4) :: i

!-------------------------------------------------------------------------------
! End specification part
!-------------------------------------------------------------------------------

!-------------------------------------------------------------------------------
! Begin execution part
!-------------------------------------------------------------------------------

  if (n < 0) then
    nfact = huge(n)
    ierr  = -1
  else if (n == 0) then
    nfact = 1
    ierr  = 0
  else
    nfact = 1
    do i = 1, n
      nfact = nfact * i
    end do
    ierr  = 0
  end if

  ! -- Last executable statement --

  return

!-------------------------------------------------------------------------------
! End execution part
!-------------------------------------------------------------------------------

!-------------------------------------------------------------------------------
! Begin subprogram part
!-------------------------------------------------------------------------------

!-------------------------------------------------------------------------------
! End subprogram part
!-------------------------------------------------------------------------------

end subroutine factorial
