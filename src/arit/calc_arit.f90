module calc_arit

!-------------------------------------------------------------------------------
! This file is part of the Computational Algorithm Library Collection (CALC).
!
! Copyright 2012 The CALC Team
!
! CALC is free software: you can redistribute it and/or modify
! it under the terms of the GNU Lesser General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! CALC is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU Lesser General Public License for more details.
!
! You should have received a copy of the GNU Lesser General Public License
! along with CALC.  If not, see <http://www.gnu.org/licenses/>.
!-------------------------------------------------------------------------------

!-------------------------------------------------------------------------------
! Begin specification part
!-------------------------------------------------------------------------------

  ! -- Use association --

  ! -- Null mapping --

  implicit none

  ! -- Accessibility --

  private
  public :: arithmetic_mean
  public :: factorial
  public :: geometric_mean
  public :: harmonic_mean
  public :: quadratic_mean
  public :: weighted_mean

  ! -- Derived type definitions --

  ! -- Type declarations --

  ! -- Interface blocks --

  interface
    pure function arithmetic_mean(x) result (xmean)
      use calc_type, only: k_r8
      implicit none
      real (kind = k_r8), dimension (:), intent (in) :: x
      real (kind = k_r8)                             :: xmean
    end function arithmetic_mean
  end interface

  interface
    pure subroutine factorial(n, nfact, ierr)
      use calc_type, only: k_i1, k_i4
      implicit none
      integer (kind = k_i4), intent (in)  :: n
      integer (kind = k_i4), intent (out) :: nfact
      integer (kind = k_i1), intent (out) :: ierr
    end subroutine factorial
  end interface

  interface
    pure subroutine geometric_mean(x, xmean, ierr)
      use calc_type, only: k_i1, k_r8
      implicit none
      real    (kind = k_r8), dimension (:), intent (in)  :: x
      real    (kind = k_r8),                intent (out) :: xmean
      integer (kind = k_i1),                intent (out) :: ierr
    end subroutine geometric_mean
  end interface

  interface
    pure subroutine harmonic_mean(x, xmean, ierr)
      use calc_type, only: k_i1, k_r8
      implicit none
      real    (kind = k_r8), dimension (:), intent (in)  :: x
      real    (kind = k_r8),                intent (out) :: xmean
      integer (kind = k_i1),                intent (out) :: ierr
    end subroutine harmonic_mean
  end interface

  interface
    pure function quadratic_mean(x) result (xmean)
      use calc_type, only: k_r8
      implicit none
      real (kind = k_r8), dimension (:), intent (in) :: x
      real (kind = k_r8)                             :: xmean
    end function quadratic_mean
  end interface

  interface
    pure subroutine weighted_mean(x, g, xmean, ierr)
      use calc_type, only: k_i1, k_r8
      implicit none
      real    (kind = k_r8), dimension (:), intent (in)  :: x, g
      real    (kind = k_r8), dimension (:), intent (out) :: xmean
      integer (kind = k_i1), dimension (:), intent (out) :: ierr
    end subroutine weighted_mean
  end interface

!-------------------------------------------------------------------------------
! End specification part
!-------------------------------------------------------------------------------

!-------------------------------------------------------------------------------
! Begin subprogram part
!-------------------------------------------------------------------------------

!-------------------------------------------------------------------------------
! End subprogram part
!-------------------------------------------------------------------------------

end module calc_arit
