module calc_cons_phys
 
!-------------------------------------------------------------------------------
! This file is part of the Computational Algorithm Library Collection (CALC).
!
! Copyright 2012 The CALC Team
!
! CALC is free software: you can redistribute it and/or modify
! it under the terms of the GNU Lesser General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! CALC is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU Lesser General Public License for more details.
!
! You should have received a copy of the GNU Lesser General Public License
! along with CALC.  If not, see <http://www.gnu.org/licenses/>.
!-------------------------------------------------------------------------------
 
!-------------------------------------------------------------------------------
! Begin specification part
!-------------------------------------------------------------------------------

  ! -- Use association --

  use calc_type, only: k_r8

  ! -- Null mapping --

  implicit none

  ! -- Accessibility --

  private

  ! -- Derived type definitions --

  ! -- Type declarations --

  ! {220} lattice spacing of silicon (m)

  real (kind = k_r8), parameter, public ::                             &
    c_lattice_spacing_si_220 =                                         &
      192.0155762e-12_k_r8

  ! Alpha particle-electron mass ratio

  real (kind = k_r8), parameter, public ::                             &
    c_alpha_electron_mass_ratio =                                      &
      7294.2995365e+00_k_r8

  ! Alpha particle mass (kg)

  real (kind = k_r8), parameter, public ::                             &
    c_alpha_mass =                                                     &
      6.64465620e-27_k_r8

  ! Alpha particle mass energy equivalent (J)

  real (kind = k_r8), parameter, public ::                             &
    c_alpha_mass_energy_equivalent =                                   &
      5.97191917e-10_k_r8

  ! Alpha particle mass energy equivalent (MeV)

  real (kind = k_r8), parameter, public ::                             &
    c_alpha_mass_energy_equivalent_mev =                               &
      3727.379109e+00_k_r8

  ! Alpha particle mass energy equivalent (u)

  real (kind = k_r8), parameter, public ::                             &
    c_alpha_mass_energy_equivalent_u =                                 &
      4.001506179127e+00_k_r8

  ! Alpha particle molar mass (kg/mol)

  real (kind = k_r8), parameter, public ::                             &
    c_alpha_molar_mass =                                               &
      4.001506179127e-03_k_r8

  ! Alpha particle-proton mass ratio

  real (kind = k_r8), parameter, public ::                             &
    c_alpha_proton_mass_ratio =                                        &
      3.97259968951e+00_k_r8

  ! Angstrom star (m)

  real (kind = k_r8), parameter, public ::                             &
    c_angstrom_star =                                                  &
      1.00001498e-10_k_r8

  ! Atomic mass constant (unified atomic mass unit) (kg)

  real (kind = k_r8), parameter, public ::                             &
    c_atomic_mass =                                                    &
      1.660538782e-27_k_r8

  ! Atomic mass constant energy equivalent (J)

  real (kind = k_r8), parameter, public ::                             &
    c_atomic_mass_energy_equivalent =                                  &
      1.492417830e-10_k_r8

  ! Atomic mass constant energy equivalent (MeV)

  real (kind = k_r8), parameter, public ::                             &
    c_atomic_mass_energy_equivalent_mev =                              &
      931.494028e+00_k_r8
 
  ! Atomic mass unit-electron volt relationship (eV)

  real (kind = k_r8), parameter, public ::                             &
    c_u_ev_relationship =                                              &
      931.494028e+06_k_r8

  ! Atomic mass unit-hartree relationship (E_h)

  real (kind = k_r8), parameter, public ::                             &
    c_u_eh_relationship =                                              &
      3.4231777149e+07_k_r8

  ! Atomic mass unit-hertz relationship (Hz)

  real (kind = k_r8), parameter, public ::                             &
    c_u_hz_relationship =                                              &
      2.2523427369e+23_k_r8

  ! Atomic mass unit-inverse meter relationship (1/m)

  real (kind = k_r8), parameter, public ::                             &
    c_u_inverse_m_relationship =                                       &
      7.513006671e+14_k_r8

  ! Atomic mass unit-joule relationship (J)

  real (kind = k_r8), parameter, public ::                             &
    c_u_j_relationship =                                               &
      1.492417830e-10_k_r8

  ! Atomic mass unit-kelvin relationship (K)

  real (kind = k_r8), parameter, public ::                             &
    c_u_k_relationship =                                               &
      1.0809527e+13_k_r8

  ! Atomic mass unit-kilogram relationship (kg)

  real (kind = k_r8), parameter, public ::                             &
    c_u_kg_relationship =                                              &
      1.660538782e-27_k_r8

  ! Atomic unit of 1st hyperpolarizablity (C3*m**3/J**2)

  real (kind = k_r8), parameter, public ::                             &
    c_atomic_unit_1st_hyperpolarizability =                            &
      3.206361533e-53_k_r8

  ! Atomic unit of 2nd hyperpolarizablity (C4*m4/J**3)

  real (kind = k_r8), parameter, public ::                             &
    c_atomic_unit_2nd_hyperpolarizability =                            &
      6.23538095e-65_k_r8

  ! Atomic unit of action (J*s)

  real (kind = k_r8), parameter, public ::                             &
    c_atomic_unit_action =                                             &
      1.054571628e-34_k_r8

  ! Atomic unit of charge (C)

  real (kind = k_r8), parameter, public ::                             &
    c_atomic_unit_charge =                                             &
      1.602176487e-19_k_r8

  ! Atomic unit of charge density (C/m**3)

  real (kind = k_r8), parameter, public ::                             &
    c_atomic_unit_charge_density =                                     &
      1.081202300e+12_k_r8

  ! Atomic unit of current (A)

  real (kind = k_r8), parameter, public ::                             &
    c_atomic_unit_current =                                            &
      6.62361763e-03_k_r8

  ! Atomic unit of electric dipole momentum (C*m)

  real (kind = k_r8), parameter, public ::                             &
    c_atomic_unit_electric_dipole_momentum =                           &
      8.47835281e-30_k_r8

  ! Atomic unit of electric field (V/m)

  real (kind = k_r8), parameter, public ::                             &
    c_atomic_unit_electric_field =                                     &
      5.14220632e+11_k_r8

  ! Atomic unit of electric field gradient (V/m**2)

  real (kind = k_r8), parameter, public ::                             &
    c_atomic_unit_electric_field_gradient =                            &
      9.71736166e+21_k_r8

  ! Atomic unit of electric polarizablity (C2*m**2/J)

  real (kind = k_r8), parameter, public ::                             &
    c_atomic_unit_electric_polarizability =                            &
      1.6487772536e-41_k_r8

  ! Atomic unit of electric potential (V)

  real (kind = k_r8), parameter, public ::                             &
    c_atomic_unit_electric_potential =                                 &
      27.21138386e+00_k_r8

  ! Atomic unit of electric quadrupole momentum (C*m**2)

  real (kind = k_r8), parameter, public ::                             &
    c_atomic_unit_electric_quadrupole_momentum =                       &
      4.48655107e-40_k_r8

  ! Atomic unit of energy (J)

  real (kind = k_r8), parameter, public ::                             &
    c_atomic_unit_energy =                                             &
      4.35974394e-18_k_r8

  ! Atomic unit of force (N)

  real (kind = k_r8), parameter, public ::                             &
    c_atomic_unit_force =                                              &
      8.23872206e-08_k_r8

  ! Atomic unit of length (m)

  real (kind = k_r8), parameter, public ::                             &
    c_atomic_unit_length =                                             &
      0.52917720859e-10_k_r8

  ! Atomic unit of magnetic dipole momentum (J/T)

  real (kind = k_r8), parameter, public ::                             &
    c_atomic_unit_dipole_momentum =                                    &
      1.854801830e-23_k_r8

  ! Atomic unit of magnetic flux density (T)

  real (kind = k_r8), parameter, public ::                             &
    c_atomic_unit_mag_flux_density =                                   &
      2.350517382e+05_k_r8

  ! Atomic unit of magnetizability (J/T2)
  
  real (kind = k_r8), parameter, public ::                             &
    c_atomic_unit_magnetizability =                                    &
      7.891036433e-29_k_r8

  ! Atomic unit of mass (kg)

  real (kind = k_r8), parameter, public ::                             &
    c_atomic_unit_mass =                                               &
      9.10938215e-31_k_r8

  ! Atomic unit of momentum (kg*m/s)

  real (kind = k_r8), parameter, public ::                             &
    c_atomic_unit_momentum =                                           &
      1.992851565e-24_k_r8

  ! Atomic unit of permittivity (F/m)

  real (kind = k_r8), parameter, public ::                             &
    c_atomic_unit_permittivity =                                       &
      1.112650056e-10_k_r8

  ! Atomic unit of time (s)

  real (kind = k_r8), parameter, public ::                             &
    c_atomic_unit_time =                                               &
      2.418884326505e-17_k_r8

  ! Atomic unit of velocity (m/s)

  real (kind = k_r8), parameter, public ::                             &
    c_atomic_unit_velocity =                                           &
      2.1876912541e+06_k_r8

  ! Avogadro constant (1/mol)

  real (kind = k_r8), parameter, public ::                             &
    c_avogadro =                                                       &
      6.02214179e+23_k_r8

  ! Bohr magneton (J/T)

  real (kind = k_r8), parameter, public ::                             &
    c_bohr_magneton =                                                  &
      927.400915e-26_k_r8

  ! Bohr radius (m)

  real (kind = k_r8), parameter, public ::                             &
    c_bohr_radius =                                                    &
      0.52917720859e-10_k_r8

  ! Boltzmann constant (J/K)

  real (kind = k_r8), parameter, public ::                             &
    c_boltzmann =                                                      &
      1.3806504e-23_k_r8

  ! Boltzmann constant (eV/K)

  real (kind = k_r8), parameter, public ::                             &
    c_boltzmann_ev_k =                                                 &
      8.617343e-05_k_r8

  ! Boltzmann constant (Hz/K)

  real (kind = k_r8), parameter, public ::                             &
    c_boltzmann_hz_k =                                                 &
      2.0836644e+10_k_r8

  ! Boltzmann constant in inverse meters per kelvin (1/(m*k))

  real (kind = k_r8), parameter, public ::                             &
    c_boltzmann_inverse_m_k =                                          &
      69.50356e+00_k_r8

  ! Characteristic impedance of vacuum (Omega)

  real (kind = k_r8), parameter, public ::                             &
    c_char_impendance_vacuum =                                         &
      376.730313461e+00_k_r8

  ! Classical electron radius (m)

  real (kind = k_r8), parameter, public ::                             &
    c_classicalc_electron_radius =                                      &
      2.8179402894e-15_k_r8

  ! Compton wavelength (m)

  real (kind = k_r8), parameter, public ::                             &
    c_compton_wavelength =                                             &
      2.4263102175e-12_k_r8

  ! Compton wavelength over 2 pi (m)

  real (kind = k_r8), parameter, public ::                             &
    c_compton_wavelength_over_2pi =                                    &
      386.15926459e-15_k_r8
 
  ! Conductance quantum (S)

  real (kind = k_r8), parameter, public ::                             &
    c_conductance_quantum =                                            &
      7.7480917004e-05_k_r8

  ! Conventional value of Josephson constant (Hz/V)

  real (kind = k_r8), parameter, public ::                             &
    c_conventional_value_josephson =                                   &
      483597.9e+09_k_r8

  ! Conventional value of von Klitzing constant (Omega)

  real (kind = k_r8), parameter, public ::                             &
    c_conventional_value_von_klitzing =                                &
      25812.807e+00_k_r8

  ! Cone angle of solar rays (rad)

  real (kind = k_r8), parameter, public ::                             &
    c_theta_sun =                                                      &
      0.00465e+00_k_r8

  ! Coulomb's constant (N*m**2/C**2)

  real (kind = k_r8), parameter, public ::                             &
    c_coulomb =                                                        &
      8.9875517874e+09_k_r8

  ! Cu x unit (m)

  real (kind = k_r8), parameter, public ::                             &
    c_cu_times_unit =                                                  &
      1.00207699e-13_k_r8

  ! Deuteron-electron magnetic momentum ratio

  real (kind = k_r8), parameter, public ::                             &
    c_deuteron_electron_mag_mom_ratio =                                &
      -4.664345537e-04_k_r8

  ! Deuteron-electron mass ratio

  real (kind = k_r8), parameter, public ::                             &
    c_deuteron_electron_mass_ratio =                                   &
      3670.4829654e+00_k_r8

  ! Deuteron g factor

  real (kind = k_r8), parameter, public ::                             &
    c_deuteron_g_factor =                                              &
      0.8574382308e+00_k_r8

  ! Deuteron magnetic momentum (J/T)

  real (kind = k_r8), parameter, public ::                             &
    c_deuteron_mag_mom =                                               &
      0.433073465e-26_k_r8

  ! Deuteron magnetic momentum to Bohr magneton ratio

  real (kind = k_r8), parameter, public ::                             &
    c_deuteron_mag_mom_bohr_magneton_ratio =                           &
      0.4669754556e-03_k_r8

  ! Deuteron magnetic momentum to nuclear magneton ratio

  real (kind = k_r8), parameter, public ::                             &
    c_deuteron_mag_mom_nuclear_magneton_ratio =                        &
      0.8574382308e+00_k_r8

  ! Deuteron mass (kg)

  real (kind = k_r8), parameter, public ::                             &
    c_deuteron_mass =                                                  &
      3.34358320e-27_k_r8

  ! Deuteron mass energy equivalent (J)

  real (kind = k_r8), parameter, public ::                             &
    c_deuteron_mass_energy_equivalent =                                &
      3.00506272e-10_k_r8

  ! Deuteron mass energy equivalent in MeV (MeV)

  real (kind = k_r8), parameter, public ::                             &
    c_deuteron_mass_energy_equivalent_mev =                            &
      1875.612793e+00_k_r8

  ! Deuteron mass in u (u)

  real (kind = k_r8), parameter, public ::                             &
    c_deuteron_mass_u =                                                &
      2.013553212724e+00_k_r8

  ! Deuteron molar mass (kg/mol)

  real (kind = k_r8), parameter, public ::                             &
    c_deuteron_molar_mass =                                            &
      2.013553212724e-03_k_r8

  ! Deuteron-neutron magnetic momentum ratio

  real (kind = k_r8), parameter, public ::                             &
    c_deuteron_neutron_mag_mom_ratio =                                 &
      -0.44820652e+00_k_r8

  ! Deuteron-proton magnetic momentum ratio

  real (kind = k_r8), parameter, public ::                             &
    c_deuteron_proton_mag_mom_ratio =                                  &
      0.3070122070e+00_k_r8

  ! Deuteron-proton mass ratio

  real (kind = k_r8), parameter, public ::                             &
    c_deuteron_proton_mass_ratio =                                     &
      1.99900750108e+00_k_r8

  ! Deuteron rms charge radius (m)

  real (kind = k_r8), parameter, public ::                             &
    c_deuteron_rms_charge_radius =                                     &
      2.1402e-15_k_r8

  ! Electric constant (electric permittivity of vacuum) (F/m)

  real (kind = k_r8), parameter, public ::                             &
    c_electric =                                                       &
      8.854187817e-12_k_r8

  ! Electron charge to mass quotient (C/kg)

  real (kind = k_r8), parameter, public ::                             &
    c_electron_charge_to_mass =                                        &
      -1.758820150e+11_k_r8

  ! Electron-deuteron magnetic momentum ratio

  real (kind = k_r8), parameter, public ::                             &
    c_electron_deuteron_mag_mom_ratio =                                &
      -2143.923498e+00_k_r8

  ! Electron-deuteron mass ratio

  real (kind = k_r8), parameter, public ::                             &
    c_electron_deuteron_mass_ratio =                                   &
      2.7244371093e-04_k_r8

  ! Electron g factor

  real (kind = k_r8), parameter, public ::                             &
    c_electron_g_factor =                                              &
      -2.0023193043622e+00_k_r8

  ! Electron gyromagnetic ratio (1/(s*T))

  real (kind = k_r8), parameter, public ::                             &
    c_electron_giromag_ratio =                                         &
      1.760859770e+11_k_r8

  ! Electron gyromagnetic ratio over 2 pi (MHz/T)

  real (kind = k_r8), parameter, public ::                             &
    c_electron_giromag_over_2pi_ratio =                                &
      28024.95364e+00_k_r8

  ! Electron magnetic momentum (J/T)

  real (kind = k_r8), parameter, public ::                             &
    c_electron_mag_mom =                                               &
      -928.476377e-26_k_r8

  ! Electron magnetic momentum anomaly

  real (kind = k_r8), parameter, public ::                             &
    c_electron_mag_mom_anomaly =                                       &
      1.15965218111e-03_k_r8

  ! Electron magnetic momentum to Bohr magneton ratio

  real (kind = k_r8), parameter, public ::                             &
    c_electron_mag_mom_bohr_magneton_ratio =                           &
      -1.00115965218111e+00_k_r8

  ! Electron magnetic momentum to nuclear magneton ratio

  real (kind = k_r8), parameter, public ::                             &
    c_electron_mag_mom_nuclear_magneton_ratio =                        &
      -1838.28197092e+00_k_r8

  ! Electron mass (kg)

  real (kind = k_r8), parameter, public ::                             &
    c_electron_mass =                                                  &
      9.10938215e-31_k_r8

  ! Electron mass equivalent (J)

  real (kind = k_r8), parameter, public ::                             &
    c_electron_mass_equivalent =                                       &
      8.18710438e-14_k_r8
 
  ! Electron mass energy equivalent in MeV (MeV)

  real (kind = k_r8), parameter, public ::                             &
    c_electron_mass_equivalent_mev =                                   &
      0.510998910e+00_k_r8

  ! Electron mass in u (u)

  real (kind = k_r8), parameter, public ::                             &
    c_electron_mass_u =                                                &
      5.4857990943e-04_k_r8

  ! Electron molar mass (kg/mol)

  real (kind = k_r8), parameter, public ::                             &
    c_electron_molar_mass =                                            &
      5.4857990943e-07_k_r8

  ! Electron-muon magnetic momentum ratio

  real (kind = k_r8), parameter, public ::                             &
    c_electron_muon_mag_mom_ratio =                                    &
      206.7669877e+00_k_r8

  ! Electron-muon mass ratio

  real (kind = k_r8), parameter, public ::                             &
    c_electron_muon_mass_ratio =                                       &
      4.83633171e-03_k_r8

  ! Electron-neutron magnetic momentum ratio

  real (kind = k_r8), parameter, public ::                             &
    c_electron_neutron_mag_mom_ratio =                                 &
      960.92050e+00_k_r8

  ! Electron-neutron mass ratio

  real (kind = k_r8), parameter, public ::                             &
    c_electron_neutron_mass_ratio =                                    &
      5.4386734459e-04_k_r8

  ! Electron-proton magnetic momentum ratio

  real (kind = k_r8), parameter, public ::                             &
    c_electron_proton_mag_mom_ratio =                                  &
      -658.2106848e+00_k_r8

  ! Electron-proton mass ratio

  real (kind = k_r8), parameter, public ::                             &
    c_electron_proton_mass_ratio =                                     &
      5.4461702177e-04_k_r8

  ! Electron-tau mass ratio

  real (kind = k_r8), parameter, public ::                             &
    c_electron_tau_mass_ratio =                                        &
      2.87564e-04_k_r8

  ! Electron to alpha particle mass ratio

  real (kind = k_r8), parameter, public ::                             &
    c_electron_alpha_mass_ratio =                                      &
      1.37093355570e-04_k_r8

  ! Electron to shielded helion magnetic momentum ratio

  real (kind = k_r8), parameter, public ::                             &
    c_electron_shielded_helion_mag_mom_ratio =                         &
      864.058257e+00_k_r8

  ! Electron to shielded proton magnetic momentum ratio

  real (kind = k_r8), parameter, public ::                             &
    c_electron_shielded_proton_mag_mom_ratio =                         &
      -658.2275971e+00_k_r8

  ! Electron volt (J)

  real (kind = k_r8), parameter, public ::                             &
    c_ev =                                                             &
      1.602176487e-19_k_r8

  ! Electron volt-atomic mass unit relationship (u)

  real (kind = k_r8), parameter, public ::                             &
    c_ev_u_relationship =                                              &
      1.073544188e-09_k_r8

  ! Electron volt-hartree relationship (E_h)

  real (kind = k_r8), parameter, public ::                             &
    c_ev_eh_relationship =                                             &
      3.674932540e-02_k_r8

  ! Electron volt-hertz relationship (Hz)

  real (kind = k_r8), parameter, public ::                             &
    c_ev_hz_relationship =                                             &
      2.417989454e+14_k_r8 

  ! Electron volt-inverse meter relationship (1/m)

  real (kind = k_r8), parameter, public ::                             &
    c_ev_inverse_m_relationship =                                      &
      8.06554465e+05_k_r8

  ! Electron volt-joule relationship (J)

  real (kind = k_r8), parameter, public ::                             &
    c_ev_j_relationship =                                              &
      1.602176487e-19_k_r8

  ! Electron volt-kelvin relationship (K)

  real (kind = k_r8), parameter, public ::                             &
    c_ev_k_relationship =                                              &
      1.1604505e+04_k_r8
  
  ! Electron volt-kilogram relationship (kg)

  real (kind = k_r8), parameter, public ::                             &
    c_ev_kg_relationship =                                             &
      1.782661758e-36_k_r8

  ! Elementary charge (-electron charge) (C)

  real (kind = k_r8), parameter, public ::                             &
    c_electron_charge =                                                &
      1.602176487e-19_k_r8

  ! Elementary charge over h (A/J)

  real (kind = k_r8), parameter, public ::                             &
    c_elementary_charge_over_h =                                       &
      2.417989454e+14_k_r8

  ! Faraday constant (C/mol) 

  real (kind = k_r8), parameter, public ::                             &
    c_faraday =                                                        &
      96485.3399e+00_k_r8

  ! Faraday constant for conventional electric current (C_90/mol) 

  real (kind = k_r8), parameter, public ::                             &
    c_faraday_conventional_electric_current =                          &
      96485.3401e+00_k_r8

  ! Fermi coupling constant (1/GeV2)

  real (kind = k_r8), parameter, public ::                             &
    c_fermi_coupling =                                                 &
      1.16637e-05_k_r8

  ! Fine-structure constant

  real (kind = k_r8), parameter, public ::                             &
    c_fine_structure =                                                 &
      7.2973525376e-03_k_r8

  ! First radiation constant (W*m**2)

  real (kind = k_r8), parameter, public ::                             &
    c_first_radiation =                                                &
      3.74177118e-16_k_r8

  ! First radiation constant for spectral radiance (W*m**2/sr)

  real (kind = k_r8), parameter, public ::                             &
    c_first_radiation_radiance =                                       &
      1.191042759e-16_k_r8

  ! Hartree-atomic mass unit relationship (u)

  real (kind = k_r8), parameter, public ::                             &
    c_eh_u_relationship =                                              &
      2.9212622986e-08_k_r8

  ! Hartree-electron volt relationship (eV)

  real (kind = k_r8), parameter, public ::                             &
    c_eh_ev_relationship =                                             &
      27.21138386e+00_k_r8

  ! Hartree energy (J)

  real (kind = k_r8), parameter, public ::                             &
    c_eh_energy =                                                      &
      4.35974394e-18_k_r8

  ! Hartree energy in eV (eV)

  real (kind = k_r8), parameter, public ::                             &
    c_eh_energy_ev =                                                   &
      27.21138386e+00_k_r8

  ! Hartree-hertz relationship (Hz)

  real (kind = k_r8), parameter, public ::                             &
    c_eh_hz_relationship =                                             &
      6.579683920722e+15_k_r8

  ! Hartree-inverse meter relationship (1/m)

  real (kind = k_r8), parameter, public ::                             &
    c_eh_inverse_m_relationship =                                      &
      2.194746313705e+07_k_r8

  ! Hartree-joule relationship (J)

  real (kind = k_r8), parameter, public ::                             &
    c_eh_j_relationship =                                              &
      4.35974394e-18_k_r8

  ! Hartree-kelvin relationship (K)

  real (kind = k_r8), parameter, public ::                             &
    c_eh_k_relationship =                                              &
      3.1577465e+05_k_r8

  ! Hartree-kilogram relationship (kg)

  real (kind = k_r8), parameter, public ::                             &
    c_eh_kg_relationship =                                             &
      4.85086934e-35_k_r8

  ! Helion-electron mass ratio

  real (kind = k_r8), parameter, public ::                             &
    c_helion_electron_mass_ratio =                                     &
      5495.8852765e+00_k_r8
  
  ! Helion mass (kg)

  real (kind = k_r8), parameter, public ::                             &
    c_helion_mass =                                                    &
      5.00641192e-27_k_r8

  ! Helion mass energy equivalent (J)

  real (kind = k_r8), parameter, public ::                             &
    c_helion_mass_energy_equivalent =                                  &
      4.49953864e-10_k_r8

  ! Helion mass energy equivalent in MeV (MeV)

  real (kind = k_r8), parameter, public ::                             &
    c_helion_mass_energy_equivalent_mev =                              &
      2808.391383e+00_k_r8

  ! Helion mass in u (u)

  real (kind = k_r8), parameter, public ::                             &
    c_helion_mass_u =                                                  &
      3.0149322473e+00_k_r8
  
  ! Helion molar mass (kg/mol)

  real (kind = k_r8), parameter, public ::                             &
    c_helion_molar_mass_u =                                            &
      3.0149322473e-03_k_r8

  ! Helion-proton mass ratio

  real (kind = k_r8), parameter, public ::                             &
    c_helion_proton_mass_ratio =                                       &
      2.9931526713e+00_k_r8

  ! Hertz-atomic mass unit relationship (u)

  real (kind = k_r8), parameter, public ::                             &
    c_hz_u_relationship =                                              &
      4.4398216294e-24_k_r8

  ! Hertz-electron volt relationship (eV)

  real (kind = k_r8), parameter, public ::                             &
    c_hz_ev_relationship =                                             &
      4.13566733e-15_k_r8

  ! Hertz-hartree relationship (E_h)

  real (kind = k_r8), parameter, public ::                             &
    c_hz_eh_relationship =                                             &
      1.519829846006e-16_k_r8

  ! Hertz-inverse meter relationship (1/m)

  real (kind = k_r8), parameter, public ::                             &
    c_hz_inverse_m_relationship =                                      &
      3.335640951e-09_k_r8

  ! Hertz-joule relationship (J)

  real (kind = k_r8), parameter, public ::                             &
    c_hz_j_relationship =                                              &
      6.62606896e-34_k_r8

  ! Hertz-kelvin relationship (K)

  real (kind = k_r8), parameter, public ::                             &
    c_hz_k_relationship =                                              &
      4.7992374e-11_k_r8

  ! Hertz-kilogram relationship (K)

  real (kind = k_r8), parameter, public ::                             &
    c_hz_kg_relationship =                                             &
      7.37249600e-51_k_r8

  ! Inverse fine-structure constant

  real (kind = k_r8), parameter, public ::                             &
    c_inverse_fine_structure =                                         &
      137.035999679e+00_k_r8
  
  ! Inverse meter-atomic mass unit relationship (u)

  real (kind = k_r8), parameter, public ::                             &
    c_inverse_m_u_relationship =                                       &
      1.3310250394e-15_k_r8

  ! Inverse meter-electron volt relationship (eV)

  real (kind = k_r8), parameter, public ::                             &
    c_inverse_m_ev_relationship =                                      &
      1.239841875e-06_k_r8

  ! Inverse meter-hartree relationship (E_h)

  real (kind = k_r8), parameter, public ::                             &
    c_inverse_m_eh_relationship =                                      &
      4.556335252760e-08_k_r8
 
  ! Inverse meter-hertz relationship (Hz)

  real (kind = k_r8), parameter, public ::                             &
    c_inverse_m_hz_relationship =                                      &
      299792458e+00_k_r8
 
  ! Inverse meter-joule relationship (J)

  real (kind = k_r8), parameter, public ::                             &
    c_inverse_m_j_relationship =                                       &
      1.986445501e-25_k_r8
  
  ! Inverse meter-kelvin relationship (K)

  real (kind = k_r8), parameter, public ::                             &
    c_inverse_m_k_relationship =                                       &
      1.4387752e-02_k_r8

  ! Inverse meter-kilogram relationship (kg)

  real (kind = k_r8), parameter, public ::                             &
    c_inverse_m_kg_relationship =                                      &
      2.21021870e-42_k_r8

  ! Inverse of conductance quantum (Omega)

  real (kind = k_r8), parameter, public ::                             &
    c_inverse_conductance_quantum =                                    &
      12906.4037787e+00_k_r8

  ! Josephson constant (Hz/V)

  real (kind = k_r8), parameter, public ::                             &
    c_josephson =                                                      &
      483597.891e+09_k_r8
  
  ! Joule-atomic mass unit relationship (u)

  real (kind = k_r8), parameter, public ::                             &
    c_j_u_relationship =                                               &
      6.70053641e+09_k_r8

  ! Joule-electron volt relationship (eV)

  real (kind = k_r8), parameter, public ::                             &
    c_j_ev_relationship =                                              &
      6.24150965e+18_k_r8

  ! Joule-hartree relationship (E_h)

  real (kind = k_r8), parameter, public ::                             &
    c_j_eh_relationship =                                              &
      2.29371269e+17_k_r8

  ! Joule-hertz relationship (Hz)

  real (kind = k_r8), parameter, public ::                             &
    c_j_hz_relationship =                                              &
      1.509190450e+33_k_r8

  ! Joule-inverse meter relationship (1/m)

  real (kind = k_r8), parameter, public ::                             &
    c_j_inverse_m_relationship =                                       &
      5.03411747e+24_k_r8

  ! Joule-kelvin relationship (K)

  real (kind = k_r8), parameter, public ::                             &
    c_j_k_relationship =                                               &
      7.242963e+22_k_r8

  ! Joule-kilogram relationship (kg)

  real (kind = k_r8), parameter, public ::                             &
    c_j_kg_relationship =                                              &
      1.112650056e-17_k_r8
  
  ! Kelvin-atomic mass unit relationship (u)

  real (kind = k_r8), parameter, public ::                             &
    c_k_u_relationship =                                               &
      9.251098e-14_k_r8

  ! Kelvin-electron volt relationship (eV)

  real (kind = k_r8), parameter, public ::                             &
    c_k_ev_relationship =                                              &
      8.617343e-05_k_r8

  ! Kelvin-hartree relationship (E_h)

  real (kind = k_r8), parameter, public ::                             &
    c_k_eh_relationship =                                              &
      3.1668153e-06_k_r8

  ! Kelvin-hertz relationship (Hz)

  real (kind = k_r8), parameter, public ::                             &
    c_k_hz_relationship =                                              &
      2.0836644e+10_k_r8
  
  ! Kelvin-inverse meter relationship (1/m)

  real (kind = k_r8), parameter, public ::                             &
    c_k_inverse_m_relationship =                                       &
      69.50356e+00_k_r8

  ! Kelvin-joule relationship (J)

  real (kind = k_r8), parameter, public ::                             &
    c_k_j_relationship =                                               &
      1.3806504e-23_k_r8

  ! Kelvin-kilogram relationship (kg)

  real (kind = k_r8), parameter, public ::                             &
    c_k_kg_relationship =                                              &
      1.5361807e-40_k_r8

  ! Kilogram-atomic mass unit relationship (u)

  real (kind = k_r8), parameter, public ::                             &
    c_kg_u_relationship =                                              &
      6.02214179e+26_k_r8

  ! Kilogram-electron volt relationship (eV)

  real (kind = k_r8), parameter, public ::                             &
    c_kg_ev_relationship =                                             &
      5.60958912e+35_k_r8

  ! Kilogram-hartree relationship (E_h)

  real (kind = k_r8), parameter, public ::                             &
    c_kg_eh_relationship =                                             &
      2.06148616e+34_k_r8

  ! Kilogram-hertz relationship (Hz)

  real (kind = k_r8), parameter, public ::                             &
    c_kg_hz_relationship =                                             &
      1.356392733e+50_k_r8

  ! Kilogram-inverse meter relationship (1/m)

  real (kind = k_r8), parameter, public ::                             &
    c_kg_inverse_m_relationship =                                      &
      4.52443915e+41_k_r8

  ! Kilogram-joule relationship (J)

  real (kind = k_r8), parameter, public ::                             &
    c_kg_j_relationship =                                              &
      8.987551787e+16_k_r8
  
  ! Kilogram-kelvin relationship (K)

  real (kind = k_r8), parameter, public ::                             &
    c_kg_k_relationship =                                              &
      6.509651e+39_k_r8
  
  ! Lattice parameter of silicon (m)

  real (kind = k_r8), parameter, public ::                             &
    c_lattice_parameter_silicon =                                      &
      543.102064e-12_k_r8

  ! Loschmidt constant at 273.15 K and 101.325 kPa (1/m**3)

  real (kind = k_r8), parameter, public ::                             &
    c_loschmidt =                                                      &
      2.6867774e+25_k_r8

  ! Magnetic constant (vacuum permeability) (N/A2)

  real (kind = k_r8), parameter, public ::                             &
    c_magnetic =                                                       &
      12.566370614e-07_k_r8

  ! Magnetic flux quantum (Wb)

  real (kind = k_r8), parameter, public ::                             &
    c_mag_flux_quantum =                                               &
      2.067833667e-15_k_r8

  ! Molar gas constant (J/(mol*K))

  real (kind = k_r8), parameter, public ::                             &
    c_molar_gas =                                                      &
      8.314472e+00_k_r8

  ! Molar mass constant (kg/mol)

  real (kind = k_r8), parameter, public ::                             &
    c_molar_mass =                                                     &
      1.0e-03_k_r8

  ! Molar mass of carbon-12 (kg/mol)

  real (kind = k_r8), parameter, public ::                             &
    c_molar_mass_c12 =                                                 &
      12e-03_k_r8

  ! Molar Planck constant (J*s/mol) 

  real (kind = k_r8), parameter, public ::                             &
    c_molar_planck =                                                   &
      3.9903126821e-10_k_r8

  ! Molar Planck constant times c (J*m/mol)

  real (kind = k_r8), parameter, public ::                             &
    c_molar_planck_times_c =                                           &
      0.11962656472e+00_k_r8

  ! Molar volume of ideal gas at 273.15 K and 100 kPa (m**3/mol)

  real (kind = k_r8), parameter, public ::                             &
    c_molar_volume_ideal_gas_100000 =                                  &
      22.710981e-03_k_r8

  ! Molar volume of ideal gas at 273.15 K and 101.325 kPa (m**3/mol)

  real (kind = k_r8), parameter, public ::                             &
    c_molar_volume_ideal_gas_101325 =                                  &
      22.413996e-03_k_r8

  ! Molar volume of silicon (m**3/mol)
 
  real (kind = k_r8), parameter, public ::                             &
    c_molar_volume_silicon =                                           &
      12.0588349e-06_k_r8

  ! Mo x unit (m)

  real (kind = k_r8), parameter, public ::                             &
    c_mo_x_unit =                                                      &
      1.00209955e-13_k_r8

  ! Muon Compton wavelength (m)

  real (kind = k_r8), parameter, public ::                             &
    c_muon_compton_wavelength =                                        &
      11.73444104e-15_k_r8

  ! Muon Compton wavelength over 2 pi (m)

  real (kind = k_r8), parameter, public ::                             &
    c_muon_compton_wavelength_over_2pi =                               &
      1.867594295e-15_k_r8

  ! Muon-electron mass ratio

  real (kind = k_r8), parameter, public ::                             &
    c_muon_electron_mass_ratio =                                       &
      206.7682823e+00_k_r8
  
  ! Muon g factor

  real (kind = k_r8), parameter, public ::                             &
    c_muon_g_factor =                                                  &
      -2.0023318414e+00_k_r8

  ! Muon magnetic momentum (J/T)
  
  real (kind = k_r8), parameter, public ::                             &
    c_muon_mag_mom =                                                   &
      -4.49044786e-26_k_r8

  ! Muon magnetic momentum anomaly

  real (kind = k_r8), parameter, public ::                             &
    c_muon_mag_mom_anomaly =                                           &
      1.16592069e-03_k_r8

  ! Muon magnetic momentum to Bohr magneton ratio

  real (kind = k_r8), parameter, public ::                             &
    c_muon_mag_mom_bohr_magneton_ratio =                               &
      -4.84197049e-03_k_r8

  ! Muon magnetic momentum to nuclear magneton ratio

  real (kind = k_r8), parameter, public ::                             &
    c_muon_mag_mom_nuclear_magneton_ratio =                            &
      -8.89059705e+00_k_r8

  ! Muon mass (kg)

  real (kind = k_r8), parameter, public ::                             &
    c_muon_mass =                                                      &
      1.88353130e-28_k_r8

  ! Muon mass energy equivalent (J)

  real (kind = k_r8), parameter, public ::                             &
    c_muon_mass_energy_equivalent =                                    &
      1.692833510e-11_k_r8

  ! Muon mass energy equivalent in MeV (J)

  real (kind = k_r8), parameter, public ::                             &
    c_muon_mass_energy_equivalent_mev =                                &
      105.6583668e+00_k_r8

  ! Muon mass in u (u)

  real (kind = k_r8), parameter, public ::                             &
    c_muon_mass_u =                                                    &
      0.1134289256e+00_k_r8

  ! Muon molar mass (kg/mol)

  real (kind = k_r8), parameter, public ::                             &
    c_muon_molar_mass =                                                &
      0.1134289256e-03_k_r8

  ! Muon-neutron mass ratio

  real (kind = k_r8), parameter, public ::                             &
    c_muon_neutron_mass_ratio =                                        &
      0.1124545167e+00_k_r8

  ! Muon-proton magnetic momentum ratio

  real (kind = k_r8), parameter, public ::                             &
    c_muon_proton_mag_mom_ratio =                                      &
      -3.183345137e+00_k_r8

  ! Muon-proton mass ratio

  real (kind = k_r8), parameter, public ::                             &
    c_muon_proton_mass_ratio =                                         &
      0.1126095261e+00_k_r8

  ! Muon-tau mass ratio

  real (kind = k_r8), parameter, public ::                             &
    c_muon_tau_mass_ratio =                                            &
      5.94592e-02_k_r8

  ! Natural unit of action (J*s)

  real (kind = k_r8), parameter, public ::                             &
    c_natural_unit_action =                                            &
      1.054571628e-34_k_r8

  ! Natural unit of action in eV*s (eV*s)

  real (kind = k_r8), parameter, public ::                             &
    c_natural_unit_action_ev_s =                                       &
      6.58211899e-16_k_r8

  ! Natural unit of energy (J)

  real (kind = k_r8), parameter, public ::                             &
    c_natural_unit_energy =                                            &
      8.18710438e-14_k_r8

  ! Natural unit of energy in MeV (MeV)

  real (kind = k_r8), parameter, public ::                             &
    c_natural_unit_energy_mev =                                        &
      0.510998910e+00_k_r8

  ! Natural unit of length (m)

  real (kind = k_r8), parameter, public ::                             &
    c_natural_unit_length =                                            &
      386.15926459e-15_k_r8

  ! Natural unit of mass (kg)

  real (kind = k_r8), parameter, public ::                             &
    c_natural_unit_mass =                                              &
      9.10938215e-31_k_r8

  ! Natural unit of momentum (kg*m/s)

  real (kind = k_r8), parameter, public ::                             &
    c_natural_unit_momentum =                                          &
      2.73092406e-22_k_r8

  ! Natural unit of momentum in MeV/c (MeV/c)

  real (kind = k_r8), parameter, public ::                             &
    c_natural_unit_momentum_mev_c =                                    &
      0.510998910_k_r8

  ! Natural unit of time (s)

  real (kind = k_r8), parameter, public ::                             &
    c_natural_unit_time =                                              &
      1.2880886570e-21_k_r8

  ! Natural unit of velocity (m/s)

  real (kind = k_r8), parameter, public ::                             &
    c_natural_unit_velocity =                                          &
      299792458e+00_k_r8

  ! Neutron Compton wavelength (m)

  real (kind = k_r8), parameter, public ::                             &
    c_neutron_compton_wavelength =                                     &
      1.3195908951e-15_k_r8

  ! Neutron Compton wavelength over 2 pi (m)

  real (kind = k_r8), parameter, public ::                             &
    c_neutron_compton_wavelength_over_2pi =                            &
      0.21001941382e-15_k_r8

  ! Neutron-electron magnetic momentum ratio

  real (kind = k_r8), parameter, public ::                             &
    c_neutron_electron_mag_mom_ratio =                                 &
      1.04066882e-03_k_r8

  ! Neutron-electron mass ratio

  real (kind = k_r8), parameter, public ::                             &
    c_neutron_electron_mass_ratio =                                    &
      1838.6836605e+00_k_r8

  ! Neutron g factor

  real (kind = k_r8), parameter, public ::                             &
    c_neutron_g_factor =                                               &
      -3.82608545e+00_k_r8

  ! Neutron gyromagnetic ratio (1/(s*T))

  real (kind = k_r8), parameter, public ::                             &
    c_neutron_giromag_ratio =                                          &
      1.83247185e+08_k_r8

  ! Neutron gyromagnetic ratio over 2 pi (MHz/T)

  real (kind = k_r8), parameter, public ::                             &
    c_neutron_giromag_ratio_over_2pi =                                 &
      29.1646954e+00_k_r8

  ! Neutron magnetic momentum (J/T)

  real (kind = k_r8), parameter, public ::                             &
    c_neutron_mag_mom =                                                &
      -0.96623641e-26_k_r8

  ! Neutron magnetic momentum to Bohr magneton ratio

  real (kind = k_r8), parameter, public ::                             &
    c_neutron_mag_mom_bohr_magneton_ratio =                            &
      -1.04187563e-03_k_r8

  ! Neutron magnetic momentum to nuclear magneton ratio

  real (kind = k_r8), parameter, public ::                             &
    c_neutron_mag_mom_nuclear_magneton_ratio =                         &
      -1.91304273e+00_k_r8

  ! Neutron mass (kg)

  real (kind = k_r8), parameter, public ::                             &
    c_neutron_mass =                                                   &
      1.674927211e-27_k_r8

  ! Neutron mass energy equivalent (J)

  real (kind = k_r8), parameter, public ::                             &
    c_neutron_mass_energy_equivalent =                                 &
      1.505349505e-10_k_r8

  ! Neutron mass energy equivalent in MeV (MeV)

  real (kind = k_r8), parameter, public ::                             &
    c_neutron_mass_energy_equivalent_mev =                             &
      939.565346e+00_k_r8

  ! Neutron mass in u (u)

  real (kind = k_r8), parameter, public ::                             &
    c_neutron_mass_u =                                                 &
      1.00866491597e+00_k_r8

  ! Neutron molar mass (kg/mol)

  real (kind = k_r8), parameter, public ::                             &
    c_neutron_molar_mass =                                             &
      1.00866491597e-03_k_r8

  ! Neutron-muon mass ratio

  real (kind = k_r8), parameter, public ::                             &
    c_neutron_muon_mass_ratio =                                        &
      8.89248409e+00_k_r8

  ! Neutron-proton magnetic momentum ratio

  real (kind = k_r8), parameter, public ::                             &
    c_neutron_proton_mag_mom_ratio =                                   &
      -0.68497934e+00_k_r8

  ! Neutron-proton mass ratio

  real (kind = k_r8), parameter, public ::                             &
    c_neutron_proton_mass_ratio =                                      &
      1.00137841918e+00_k_r8

  ! Neutron-tau mass ratio

  real (kind = k_r8), parameter, public ::                             &
    c_neutron_tau_mass_ratio =                                         &
      0.528740e+00_k_r8

  ! Neutron to shielded proton magnetic momentum ratio

  real (kind = k_r8), parameter, public ::                             &
    c_neutron_shielded_proton_mag_mom_ratio =                          &
      -0.68499694e+00

  ! Newtonian constant of gravitation (m**3/(kg*s**2))

  real (kind = k_r8), parameter, public ::                             &
    c_newton =                                                         &
      6.67428e-11_k_r8

  ! Newtonian constant of gravitation over h-bar c (m**3/(kg*s**2))

  real (kind = k_r8), parameter, public ::                             &
    c_newton_over_h_bar_c =                                            &
      6.70881e-39_k_r8

  ! Nuclear magneton (J/T)

  real (kind = k_r8), parameter, public ::                             &
    c_nuclear_magneton =                                               &
      5.05078324e-27_k_r8

  ! Nuclear magneton in eV/T (eV/T)

  real (kind = k_r8), parameter, public ::                             &
    c_nuclear_magneton_ev_per_t =                                      &
      3.1524512326e-08_k_r8 

  ! Nuclear magneton in inverse meters per tesla

  real (kind = k_r8), parameter, public ::                             &
    c_nuclear_magneton_inverse_m_per_t =                               &
      2.542623616e-02_k_r8

  ! Nuclear magneton in K/T (K/T)

  real (kind = k_r8), parameter, public ::                             &
    c_nuclear_magneton_k_per_t =                                       &
      3.6582637e-04_k_r8

  ! Nuclear magneton in MHz/T (MHz/T)

  real (kind = k_r8), parameter, public ::                             &
    c_nuclear_magneton_mhz_per_t =                                     &
      7.62259384e+00_k_r8

  ! Planck constant (J*s)

  real (kind = k_r8), parameter, public ::                             &
    c_planck =                                                         &
      6.62606896e-34_k_r8

  ! Planck constant over 2 pi (J*s)

  real (kind = k_r8), parameter, public ::                             &
    c_planck_over_2pi =                                                &
      1.054571628e-34_k_r8

  ! Planck constant over 2 pi in eV s (eV*s)

  real (kind = k_r8), parameter, public ::                             &
    c_planck_over_2pi_ev_per_s =                                       &
      6.58211899e-16_k_r8

  ! Planck constant over 2 pi times c in MeV fm (MeV*fm)

  real (kind = k_r8), parameter, public ::                             &
    c_planck_over_2pi_times_c_mev_per_fm =                             &
      197.3269631e+00_k_r8

  ! Planck length (m)

  real (kind = k_r8), parameter, public ::                             &
    c_planck_length =                                                  &
      1.616252e-35_k_r8

  ! Planck mass (kg)

  real (kind = k_r8), parameter, public ::                             &
    c_planck_mass =                                                    &
      2.17644e-08_k_r8

  ! Planck mass energy equivalent in GeV (GeV)

  real (kind = k_r8), parameter, public ::                             &
    c_planck_mass_energy_equivalent_gev =                              &
      1.220892e+19_k_r8

  ! Planck temperature (K)

  real (kind = k_r8), parameter, public ::                             &
    c_planck_temperature =                                             &
      1.416785e+32_k_r8

  ! Planck time (s)

  real (kind = k_r8), parameter, public ::                             &
    c_planck_time =                                                    &
      5.39124e-44_k_r8

  ! Proton charge to mass quotient (C/kg)

  real (kind = k_r8), parameter, public ::                             &
    c_proton_charge_to_mass =                                          &
      9.57883392e+07_k_r8

  ! Proton Compton wavelegth (m)

  real (kind = k_r8), parameter, public ::                             &
    c_proton_compton_wavelength =                                      &
      1.3214098446e-15_k_r8

  ! Proton Compton wavelegth over 2 pi (m)

  real (kind = k_r8), parameter, public ::                             &
    c_proton_compton_wavelength_over_2pi =                             &
      0.21030890861e-15_k_r8

  ! Proton-electron mass ratio

  real (kind = k_r8), parameter, public ::                             &
    c_proton_electron_mass_ratio =                                     &
      1836.15267247e+00_k_r8

  ! Proton g factor

  real (kind = k_r8), parameter, public ::                             &
    c_proton_g_factor =                                                &
      5.585694713e+00_k_r8

  ! Proton gyromagnetic ratio (1/(s*T))

  real (kind = k_r8), parameter, public ::                             &
    c_proton_giromag_ratio =                                           &
      2.675222099e+08_k_r8

  ! Proton gyromagnetic ratio over 2 pi (MHz/T)

  real (kind = k_r8), parameter, public ::                             &
    c_proton_giromag_ratio_over_2pi =                                  &
      42.5774821e+00_k_r8

  ! Proton magnetic momentum (J/T)

  real (kind = k_r8), parameter, public ::                             &
    c_proton_mag_mom =                                                 &
      1.410606662e-26_k_r8

  ! Proton magnetic momentum to Bohr magneton ratio

  real (kind = k_r8), parameter, public ::                             &
    c_proton_mag_mom_bohr_magneton_ratio =                             &
      1.521032209e-03_k_r8

  ! Proton magnetic momentum to nuclear magneton ratio

  real (kind = k_r8), parameter, public ::                             &
    c_proton_mag_mom_nuclear_magneton_ratio =                          &
      2.792847356e+00_k_r8

  ! Proton magnetic shielding correction

  real (kind = k_r8), parameter, public ::                             &
    c_proton_mag_shielding_correction =                                &
      25.694e-06_k_r8

  ! Proton mass (kg)

  real (kind = k_r8), parameter, public ::                             &
    c_proton_mass =                                                    &
      1.672621637e-27_k_r8
  
  ! Proton mass energy equivalent (J)

  real (kind = k_r8), parameter, public ::                             &
    c_proton_mass_energy_equivalent =                                  &
      1.503277359e-10_k_r8

  ! Proton mass energy equivalent in MeV (MeV)

  real (kind = k_r8), parameter, public ::                             &
    c_proton_mass_energy_equivalent_mev =                              &
      938.272013e+00_k_r8

  ! Proton mass in u (u)

  real (kind = k_r8), parameter, public ::                             &
    c_proton_mass_u =                                                  &
      1.00727646677e+00_k_r8

  ! Proton molar mass (kg/mol)

  real (kind = k_r8), parameter, public ::                             &
    c_proton_molar_mass =                                              &
      1.00727646677e-03_k_r8

  ! Proton-muon mass ratio

  real (kind = k_r8), parameter, public ::                             &
    c_proton_muon_mass_ratio =                                         &
      8.88024339e+00_k_r8

  ! Proton-neutron magnetic momentum ratio

  real (kind = k_r8), parameter, public ::                             &
    c_proton_neutron_mag_mom_ratio =                                   &
      -1.45989806e+00_k_r8

  ! Proton-neutron mass ratio

  real (kind = k_r8), parameter, public ::                             &
    c_proton_neutron_mass_ratio =                                      &
      0.99862347824e+00_k_r8

  ! Proton rms charge radius (m)

  real (kind = k_r8), parameter, public ::                             &
    c_proton_rms_charge_radius =                                       &
      0.8768e-15_k_r8

  ! Proton-tau mass ratio

  real (kind = k_r8), parameter, public ::                             &
    c_proton_tau_mass_ratio =                                          &
      0.528012e+00_k_r8

  ! Quantum of circulation (m**2/s)

  real (kind = k_r8), parameter, public ::                             &
    c_quantum_circulation =                                            &
      3.6369475199e-04_k_r8

  ! Quantum of circulation times 2 (m**2/s)

  real (kind = k_r8), parameter, public ::                             &
    c_quantum_circulation_times_2 =                                    &
      7.273895040e-04_k_r8

  ! Rydberg constant (1/m)

  real (kind = k_r8), parameter, public ::                             &
    c_rydberg =                                                        &
      10973731.568527e+00_k_r8

  ! Rydberg constant times c in Hz (Hz)

  real (kind = k_r8), parameter, public ::                             &
    c_rydberg_times_c_hz =                                             &
      3.289841960361e+15_k_r8

  ! Rydberg constant times hc in eV (eV)

  real (kind = k_r8), parameter, public ::                             &
    c_rydberg_times_hc_ev =                                            &
      13.60569193e+00_k_r8

  ! Rydberg constant times hc in J (J)

  real (kind = k_r8), parameter, public ::                             &
    c_rydberg_times_hc_j =                                             &
      2.17987197e-18_k_r8

  ! Sackur-Tetrode constant at 1 K and 100 kPa

  real (kind = k_r8), parameter, public ::                             &
    c_sackur_tetrode_100000pa =                                        &
      -1.1517047e+00_k_r8

  ! Sackur-Tetrode constant at 1 K and 101.325 kPa

  real (kind = k_r8), parameter, public ::                             &
    c_sackur_tetrode_101325pa =                                        &
      -1.1648677e+00_k_r8

  ! Second radiation constant (m*K)

  real (kind = k_r8), parameter, public ::                             &
    c_second_radiation =                                               &
      14387.75225e-06_k_r8

  ! Shielded helion gyromagnetic ratio (1/(s*T))

  real (kind = k_r8), parameter, public ::                             &
    c_shielded_helion_gyromag_ratio =                                  &
      2.037894730e+08_k_r8

  ! Shielded helion gyromagnetic ratio over 2 pi (MHz/T)

  real (kind = k_r8), parameter, public ::                             &
    c_shielded_helion_gyromag_ratio_over_2pi =                         &
      32.43410198e+00_k_r8

  ! Shielded helion magnetic momentum (J/T)

  real (kind = k_r8), parameter, public ::                             &
    c_shielded_helion_mag_mom =                                        &
      -1.074552982e-26_k_r8

  ! Shielded helion magnetic momentum to Bohr magneton ratio

  real (kind = k_r8), parameter, public ::                             &
    c_shielded_helion_mag_mom_bohr_magneton_ratio =                    &
      -1.158671471e-03_k_r8

  ! Shielded helion magnetic momentum to nuclear magneton ratio

  real (kind = k_r8), parameter, public ::                             &
    c_shielded_helion_mag_mom_nuclear_magneton_ratio =                 &
      -2.127497718e+00_k_r8

  ! Shielded helion to proton magnetic momentum ratio

  real (kind = k_r8), parameter, public ::                             &
    c_shielded_helion_proton_mag_mom_ratio =                           &
      -0.761766558e+00_k_r8

  ! Shielded helion to shielded proton mag. mom. ratio

  real (kind = k_r8), parameter, public ::                             &
    c_shielded_helion_shielded_proton_mag_mom_ratio =                  &
      -0.7617861313e+00_k_r8

  ! Shielded proton gyromag. ratio (1/(s*T))

  real (kind = k_r8), parameter, public ::                             &
    c_shielded_proton_gyromag_ratio =                                  &
      2.675153362e+08_k_r8

  ! Shielded proton gyromagnetic ratio over 2 pi (MHz/T)

  real (kind = k_r8), parameter, public ::                             &
    c_shielded_proton_gyromag_ratio_over_2pi =                         &
      42.5763881e+00_k_r8

  ! Shielded proton magnetic momentum (J/T)

  real (kind = k_r8), parameter, public ::                             &
    c_shielded_proton_mag_mom =                                        &
      1.410570419e-26_k_r8

  ! Shielded proton magnetic momentum to Bohr magneton ratio

  real (kind = k_r8), parameter, public ::                             &
    c_shielded_proton_mag_mom_bohr_magneton_ratio =                    &
      1.520993128e-03_k_r8

  ! Shielded proton magnetic momentum to nuclear magneton ratio

  real (kind = k_r8), parameter, public ::                             &
    c_shielded_proton_mag_mom_nuclear_magneton_ratio =                 &
      2.792775598e+00_k_r8
 
  ! Speed of light in vacuum (m/s)
 
  real (kind = k_r8), parameter, public ::                             &
    c_speed_light_vacuum =                                             &
      299792458.0e+00_k_r8

! Standard acceleration of gravity (m/s**2)

  real (kind = k_r8), parameter, public ::                             &
    c_standard_acceleration_gravity =                                  &
      9.80665e+00_k_r8

  ! Standard atmosphere (Pa)

  real (kind = k_r8), parameter, public ::                             &
    c_standard_atmosphere =                                            &
      101325.0e+00_k_r8

  ! Stefan-Boltzmann constant (W/(m**2*K**4))

  real (kind = k_r8), parameter, public ::                             &
    c_stefan_boltzmann =                                               &
      5.670400e-08_k_r8

  ! Tau Compton wavelength (m)

  real (kind = k_r8), parameter, public ::                             &
    c_tau_compton_wavelength =                                         &
      0.69772e-15_k_r8

  ! Tau Compton wavelength over 2 pi (m)

  real (kind = k_r8), parameter, public ::                             &
    c_tau_compton_wavelength_over_2pi =                                &
      0.111046e-15_k_r8

  ! Tau-electron mass ratio

  real (kind = k_r8), parameter, public ::                             &
    c_tau_electron_mass_ratio =                                        &
      3477.48e+00_k_r8

  ! Tau mass (kg)

  real (kind = k_r8), parameter, public ::                             &
    c_tau_mass =                                                       &
      3.16777e-27_k_r8

  ! Tau mass energy equivalent (J)

  real (kind = k_r8), parameter, public ::                             &
    c_tau_mass_energy_equivalent =                                     &
      2.84705e-10_k_r8

  ! Tau mass energy equivalent in MeV (MeV)

  real (kind = k_r8), parameter, public ::                             &
    c_tau_mass_energy_equivalent_mev =                                 &
      1776.99e+00_k_r8

  ! Tau mass in u (u)

  real (kind = k_r8), parameter, public ::                             &
    c_tau_mass_u =                                                     &
      1.90768e+00_k_r8

  ! Tau molar mass (kg)

  real (kind = k_r8), parameter, public ::                             &
    c_tau_molar_mass =                                                 &
      1.90768e-03_k_r8

  ! Tau-muon mass ratio

  real (kind = k_r8), parameter, public ::                             &
    c_tau_muon_mass_ratio =                                            &
      16.8183e+00_k_r8

  ! Tau-neutron mass ratio

  real (kind = k_r8), parameter, public ::                             &
    c_tau_neutron_mass_ratio =                                         &
      1.89129e+00_k_r8

  ! Tau-proton mass ratio

  real (kind = k_r8), parameter, public ::                             &
    c_tau_proton_mass_ratio =                                          &
      1.89390e+00_k_r8

  ! Thomson cross section (m**2)

  real (kind = k_r8), parameter, public ::                             &
    c_thomson_cross_section =                                          &
      0.6652458558e-28_k_r8

  ! Triton-electron magnetic momentum ratio

  real (kind = k_r8), parameter, public ::                             &
    c_triton_electron_mag_mom_ratio =                                  &
      -1.620514423e-03_k_r8

  ! Triton-electron mass ratio

  real (kind = k_r8), parameter, public ::                             &
    c_triton_electron_mass_ratio =                                     &
      5496.9215269e+00_k_r8

  ! Triton g factor

  real (kind = k_r8), parameter, public ::                             &
    c_triton_g_factor =                                                &
      5.957924896e+00_k_r8

  ! Triton magnetic momentum (J/T)

  real (kind = k_r8), parameter, public ::                             &
    c_triton_mag_mom =                                                 &
      1.504609361e-26_k_r8
 
  ! Triton magnetic momentum to Bohr magneton ratio

  real (kind = k_r8), parameter, public ::                             &
    c_triton_mag_mom_bohr_magneton_ratio =                             &
      1.622393657e-03_k_r8

  ! Triton magnetic momentum to nuclear magneton ratio

  real (kind = k_r8), parameter, public ::                             &
    c_triton_mag_mom_nuclear_magneton_ratio =                          &
      2.978962448e+00_k_r8

  ! Triton mass (kg)

  real (kind = k_r8), parameter, public ::                             &
    c_triton_mass =                                                    &
      5.00735588e-27_k_r8

  ! Triton mass energy equivalent (J)

  real (kind = k_r8), parameter, public ::                             &
    c_triton_mass_energy_equivalent =                                  &
      4.50038703e-10_k_r8

  ! Triton mass energy equivalent in MeV (MeV)

  real(kind = k_r8), parameter, public ::                                     &
    c_triton_mass_energy_equivalent_mev =                              &
      2808.920906e+00_k_r8

  ! Triton mass in u (u)

  real(k_r8), parameter, public ::                                     &
    c_triton_mass_u =                                                  &
      3.0155007134e+00_k_r8

  ! Triton molar mass (kg/mol)

  real(k_r8), parameter, public ::                                     &
    c_triton_molar_mass =                                              &
      3.0155007134e-03_k_r8

  ! Triton-neutron magnetic momentum ratio

  real(k_r8), parameter, public ::                                     &
    c_triton_neutron_mag_mom_ratio =                                   &
      -1.55718553e+00_k_r8

  ! Triton-proton magnetic momentum ratio

  real(k_r8), parameter, public ::                                     &
    c_triton_proton_mag_mom_ratio =                                    &
      1.066639908e+00_k_r8

  ! Triton-proton mass ratio

  real(k_r8), parameter, public ::                                     &
    c_triton_proton_mass_ratio =                                       &
      2.9937170309e+00_k_r8

  ! Unified atomic mass unit (kg)

  real(k_r8), parameter, public ::                                     &
    c_unified_atomic_mass_unit =                                       &
      1.660538782e-27_k_r8

  ! von Klitzing constant (Omega)

  real(k_r8), parameter, public ::                                     &
    c_von_klitzing =                                                   &
      25812.807557e+00_k_r8

  ! Weak mixing angle

  real(k_r8), parameter, public ::                                     &
    c_weak_mixing_angle =                                              &
      0.22255e+00_k_r8

  ! Third radiation constant for frequency (Hz/K)
 
  real(k_r8), parameter, public ::                                     &
    c_third_radiation_frequency =                                      &
      5.878933e+10_k_r8
 
  ! Third radiation constant for wavelength (m*K)
  
  real(k_r8), parameter, public ::                                     &
    c_third_radiation_wavelength =                                     &
      2.8977685e-03_k_r8

  ! -- Interface blocks --

!-------------------------------------------------------------------------------
! End specification part
!-------------------------------------------------------------------------------

!-------------------------------------------------------------------------------
! Begin subprogram part
!-------------------------------------------------------------------------------

!-------------------------------------------------------------------------------
! End subprogram part
!-------------------------------------------------------------------------------

end module calc_cons_phys
