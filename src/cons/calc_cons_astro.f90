module calc_cons_astro

!-------------------------------------------------------------------------------
! This file is part of the Computational Algorithm Library Collection (CALC).
!
! Copyright 2012 The CALC Team
!
! CALC is free software: you can redistribute it and/or modify
! it under the terms of the GNU Lesser General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! CALC is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU Lesser General Public License for more details.
!
! You should have received a copy of the GNU Lesser General Public License
! along with CALC.  If not, see <http://www.gnu.org/licenses/>.
!-------------------------------------------------------------------------------
 
!-------------------------------------------------------------------------------
! Begin specification part
!-------------------------------------------------------------------------------

  ! -- Use association --

  use calc_type, only: k_r8

  ! -- Null mapping --

  implicit none

  ! -- Accessibility --

  private

  ! -- Derived type definitions --

  ! -- Type declarations --

  ! Earth's average density (kg/m**3)

  real (kind = k_r8), parameter, public ::                             &
    c_earth_average_density =                                          &
      5.517e+03_k_r8

  ! Earth's gravitational acceleration (m/s**2)

  real (kind = k_r8), parameter, public ::                             &
    c_earth_gravitational_accelaration =                               &
      9.80665e+00_k_r8

  ! Earth's magnetic field, horizontal component of (T)

  real (kind = k_r8), parameter, public ::                             &
    c_earth_magnetic_field_horizontal_component =                      &
      1.8e-05_k_r8

  ! Earth's mass (kg)

  real (kind = k_r8), parameter, public ::                             &
    c_earth_mass =                                                     &
      5.972e+24_k_r8

  ! Earth's mean equitorial radius (m)

  real (kind = k_r8), parameter, public ::                             &
    c_earth_mean_equitorial_radius =                                   &
      6.378140e+06_k_r8

  ! Earth's mean radius (m)

  real (kind = k_r8), parameter, public ::                             &
    c_earth_mean_radius =                                              &
      6.3710e+06_k_r8

  ! Earth-Moon mean distance (m)

  real (kind = k_r8), parameter, public ::                             &
    c_earth_moon_mean_distance =                                       &
      3.844e+08_k_r8

  ! Earth's surface area (m**2)

  real (kind = k_r8), parameter, public ::                             &
    c_earth_surface_area =                                             &
      5.10e+14_k_r8

  ! Moon's gravitational acceleration (m/s**2)

  real (kind = k_r8), parameter, public ::                             &
    c_moon_gravitational_acceleration =                                &
      1.619e+00_k_r8

  ! Moon's mean distance from Earth (m)

  real (kind = k_r8), parameter, public ::                             &
    c_moon_earth_mean_distance =                                       &
      3.844e+08_k_r8

  ! Moon's mean density (kg/m**3)

  real (kind = k_r8), parameter, public ::                             &
    c_moon_mean_density =                                              &
      3.33e+03_k_r8

  ! Moon's mean mass (kg)

  real (kind = k_r8), parameter, public ::                             &
    c_moon_mean_mass =                                                 &
      7.33e+22_k_r8

  ! Moon's mean radius (m)

  real (kind = k_r8), parameter, public ::                             &
    c_moon_mean_radius =                                               &
      1.738e+06_k_r8

  ! Solar constant (W/m**2)

  real (kind = k_r8), parameter, public ::                             &
    c_solar_consant =                                                 &
      1373.0e+00_k_r8

  ! Sun's mean distance from Earth (m)

  real (kind = k_r8), parameter, public ::                             &
    c_sun_earth_mean_distance =                                        &
      1.495e+11_k_r8

  ! Sun's effective temperature (K)

  real (kind = k_r8), parameter, public ::                             &
    c_sun_effective_temperature =                                      &
      5777.0e+00_k_r8

  ! Sun's energy production (W)

  real (kind = k_r8), parameter, public ::                             &
    c_sun_energy_production =                                          &
      3.90e+26_k_r8

  ! Sun's mass (kg)

  real (kind = k_r8), parameter, public ::                             &
    c_sun_mass =                                                       &
      1.99e+30_k_r8

  ! Sun's mean radius (m)

  real (kind = k_r8), parameter, public ::                             &
    c_sun_mean_radius =                                                &
      6.9599e+08_k_r8

  ! Sun's surface area (m**2)

  real (kind = k_r8), parameter, public ::                             &
    c_sun_surface_area =                                               &
      6.087e+18_k_r8

  ! -- Interface blocks --

!-------------------------------------------------------------------------------
! End specification part
!-------------------------------------------------------------------------------

!-------------------------------------------------------------------------------
! Begin subprogram part
!-------------------------------------------------------------------------------

!-------------------------------------------------------------------------------
! End subprogram part
!-------------------------------------------------------------------------------

end module calc_cons_astro
