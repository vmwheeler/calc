module calc_cons_math
 
!-------------------------------------------------------------------------------
! This file is part of the Computational Algorithm Library Collection (CALC).
!
! Copyright 2012 The CALC Team
!
! CALC is free software: you can redistribute it and/or modify
! it under the terms of the GNU Lesser General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! CALC is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU Lesser General Public License for more details.
!
! You should have received a copy of the GNU Lesser General Public License
! along with CALC.  If not, see <http://www.gnu.org/licenses/>.
!-------------------------------------------------------------------------------
 
!-------------------------------------------------------------------------------
! Begin specification part
!-------------------------------------------------------------------------------

  ! -- Use association --

  use calc_type, only: k_r8

  ! -- Null mapping --
  
  implicit none

  ! -- Accessibility --

  private

  ! -- Derived type definitions --

  ! -- Type declarations --

  ! Alladi-Grinstead constant

  real (kind = k_r8), parameter, public ::                             &
    c_alladi_grinstead =                                               &
      0.8093940205e+00_k_r8

  ! Apery's constant

  real (kind = k_r8), parameter, public ::                             &
    c_apery =                                                          &
      1.20205690315959428539973816151144999e+00_k_r8

  ! Backhouse's constant

  real (kind = k_r8), parameter, public ::                             &
    c_backhouse =                                                      &
      1.45607494858268967139959535111654356e+00_k_r8

  ! Base of the natural logarithm (Napier's constant, Euler's number)

  real (kind = k_r8), parameter, public ::                             &
    c_ln =                                                        &
      2.71828182845904523536028747135266249e+00_k_r8

  ! Basis vectors for the right-handed 3D Cartesian system of coordinates

  real (kind = k_r8), dimension (3), parameter, public ::              &
    c_ei =                                                             &
      (/1.0e+00_k_r8, 0.0e+00_k_r8, 0.0e+00_k_r8/)

  real (kind = k_r8), dimension (3), parameter, public ::              &
    c_ej =                                                             &
      (/0.0e+00_k_r8, 1.0e+00_k_r8, 0.0e+00_k_r8/)

  real (kind = k_r8), dimension (3), parameter, public ::              &
    c_ek =                                                             &
      (/0.0e+00_k_r8, 0.0e+00_k_r8, 1.0e+00_k_r8/)

  ! Bernstein's constant

  real (kind = k_r8), parameter, public ::                             &
    c_bernstein =                                                      &
      0.28016949902386913303e+00_k_r8

  ! Brun's constant for prime quadruplets

  real (kind = k_r8), parameter, public ::                             &
    c_brun_prime_quadruplets =                                         &
      1.9021605823e+00_k_r8

  ! Brun's constant for twin primes

  real (kind = k_r8), parameter, public ::                             &
    c_brun_twin_primes =                                               &
      1.9021605823e+00_k_r8

  ! Cahen's constant

  real (kind = k_r8), parameter, public ::                             &
    c_cahen =                                                          &
      0.6434105463e+00_k_r8

  ! Catalan's constant

  real (kind = k_r8), parameter, public ::                             &
    c_catalan =                                                        &
      0.91596559417721901505460351493238411e+00_k_r8

  ! De Bruijn-Newman constant

  real (kind = k_r8), parameter, public ::                             &
    c_de_bruijn_newman =                                               &
      -2.7e-09_k_r8

  ! Embree-Trefethen constant

  real (kind = k_r8), parameter, public ::                             &
    c_embree_trefethen =                                               &
      0.70258e+00_k_r8

  ! Erdos–Borwein constant

  real (kind = k_r8), parameter, public ::                             &
    c_erdos_borwein =                                                  &
      1.60669515241529176378330152319092458e+00_k_r8

  ! Euler-Mascheroni constant

  real (kind = k_r8), parameter, public ::                             &
    c_euler_mascheroni =                                               &
      0.57721566490153286060651209008240243e+00_k_r8

  ! Feigenbaum constant

  real (kind = k_r8), parameter, public ::                             &
    c_feigenbaum =                                                     &
      4.66920160910299067185320382046620161e+00_k_r8 

  ! Second Feigenbaum constant

  real (kind = k_r8), parameter, public ::                             &
    c_second_feigenbaum =                                              &
      2.50290787509589282228390287321821578e+00_k_r8

  ! Fransen-Robinson constant

  real (kind = k_r8), parameter, public ::                             &
    c_fransen_robinson =                                               &
      2.80777024202851936522150118655777293e+00_k_r8

  ! Gauss-Kuzmin-Wirsing constant

  real (kind = k_r8), parameter, public ::                             &
    c_gauss_kuzmin_wirsing =                                           &
      0.30366300289873265859744812190155623e+00_k_r8

  ! Golden ratio 

  real (kind = k_r8), parameter, public ::                             &
    c_golden_ratio =                                                   &
      1.61803398874989484820458683436563811e+00_k_r8

  ! Golomb–Dickman constant

  real (kind = k_r8), parameter, public ::                             &
    c_golomb_dickman =                                                 &
      0.62432998854355087099293638310083724e+00_k_r8

  ! Hafner-Sarnak-McCurley constant

  real (kind = k_r8), parameter, public ::                             &
    c_hafner_sarnak_mccurley =                                         &
      0.35323637185499598454e+00_k_r8

  ! Khinchin's constant

  real (kind = k_r8), parameter, public ::                             &
    c_khinchin =                                                       &
      2.68545200106530644530971483548179569e+00_k_r8

  ! Landau's constant

  real (kind = k_r8), parameter, public ::                             &
    c_landau =                                                         &
      0.5e+00_k_r8

  ! Landau-Ramanujan constant

  real (kind = k_r8), parameter, public ::                             &
    c_landau_ramanujan =                                               &
      0.76422365358922066299069873125009232e+00_k_r8

  ! Laplace limit

  real (kind = k_r8), parameter, public ::                             &
    c_laplace =                                                        &
      0.66274341934918158097474209710925290e+00_k_r8

  ! Legendre's constant

  real (kind = k_r8), parameter, public ::                             &
    c_legendre =                                                       &
      1.0e+00_k_r8

  ! Lengyel's constant

  real (kind = k_r8), parameter, public ::                             &
    c_lengyel =                                                        &
      1.0986858055e+00_k_r8

  ! Levy's constant

  real (kind = k_r8), parameter, public ::                             &
    c_levy =                                                           &
      3.27582291872181115978768188245384386e+00_k_r8

  ! Lieb's square ice constant

  real (kind = k_r8), parameter, public ::                             &
    c_lieb_square_ice =                                                &
      1.5396007178e+00_k_r8

  ! Meiscalc-Mertens constant

  real (kind = k_r8), parameter, public ::                             &
    c_meiscalc_mertens =                                                &
      0.26149721284764278375542683860869585e+00_k_r8

  ! Mills' constant

  real (kind = k_r8), parameter, public ::                             &
    c_mill =                                                           &
      1.30637788386308069046861449260260571e+00_k_r8

  ! Niven's constant

  real (kind = k_r8), parameter, public ::                             &
    c_niven =                                                          &
      1.70521114010536776428855145343450816e+00_k_r8

  ! Null vector in Cartesian coordinates

  real (kind = k_r8), dimension (3), parameter, public ::              &
    c_null =                                                           &
      (/0.0e+00_k_r8, 0.0e+00_k_r8, 0.0e+00_k_r8/)

  ! Omega constant

  real (kind = k_r8), parameter, public ::                             &
    c_omega =                                                          &
      0.56714329040978387299996866221035555e+00_k_r8

  ! Parabolic constant

  real (kind = k_r8), parameter, public ::                             &
    c_parabolic =                                                      &
      2.29558714939263807403429804918949039e+00_k_r8

  ! Pi (Archimedes' constant, Ludolph's number)

  real (kind = k_r8), parameter, public ::                             &
    c_pi =                                                             &
      3.14159265358979323846264338327950288e+00_k_r8

  ! Pi over 2

  real (kind = k_r8), parameter, public ::                             &
    c_pio2 =                                                           &
      1.57079632679489661923132169163975144e+00_k_r8

  ! Plastic constant

  real (kind = k_r8), parameter, public ::                             &
    c_plastic =                                                        &
      1.32471795724474602596090885447809734e+00_k_r8

  ! Porter's constant

  real (kind = k_r8), parameter, public ::                             &
    c_porter =                                                         &
      1.4670780794e+00_k_r8

  ! Ramanujan-Soldner constant

  real (kind = k_r8), parameter, public ::                             &
    c_ramanujan_soldner =                                              &
      1.45136923488338105028396848589202744e+00_k_r8

  ! Sierpinski's constant

  real (kind = k_r8), parameter, public ::                             &
    c_sierpinski =                                                     &
      2.58498175957925321706589358738317116e+00_k_r8

  ! Square root of two (Pythagoras' constant)

  real (kind = k_r8), parameter, public ::                             &
    c_sqrt_of_two =                                                    &
      1.41421356237309504880168872420969807e+00_k_r8

  ! Square root of three (Theodorus' constant)

  real (kind = k_r8), parameter, public ::                             &
    c_sqrt_of_three =                                                  &
      1.73205080756887729352744634150587236e+00_k_r8

  ! Square root of five

  real (kind = k_r8), parameter, public ::                             &
    c_sqrt_of_five =                                                   &
      2.23606797749978969640917366873127623e+00_k_r8

  ! Twin prime constant

  real (kind = k_r8), parameter, public ::                             &
    c_twin_prime =                                                     &
      0.66016181584686957392781211001455577e+00_k_r8

  ! Two pi

  real (kind = k_r8), parameter, public ::                             &
    c_twopi =                                                          &
      2.0_k_r8 * c_pi

  ! Viswanath's constant

  real (kind = k_r8), parameter, public ::                             &
    c_viswanath =                                                      &
      1.13198824e+00_k_r8

  ! -- Interface blocks --

!-------------------------------------------------------------------------------
! End specification part
!-------------------------------------------------------------------------------

!-------------------------------------------------------------------------------
! Begin subprogram part
!-------------------------------------------------------------------------------

!-------------------------------------------------------------------------------
! End subprogram part
!-------------------------------------------------------------------------------

end module calc_cons_math
