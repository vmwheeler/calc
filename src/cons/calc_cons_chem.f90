module calc_cons_chem
 
!-------------------------------------------------------------------------------
! This file is part of the Computational Algorithm Library Collection (CALC).
!
! Copyright 2012 The CALC Team
!
! CALC is free software: you can redistribute it and/or modify
! it under the terms of the GNU Lesser General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! CALC is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU Lesser General Public License for more details.
!
! You should have received a copy of the GNU Lesser General Public License
! along with CALC.  If not, see <http://www.gnu.org/licenses/>.
!-------------------------------------------------------------------------------
 
!-------------------------------------------------------------------------------
! Begin specification part
!-------------------------------------------------------------------------------

  ! -- Use association --

  use calc_type, only: k_i1, k_r8, t_chem_element

  ! -- Null mapping --

  implicit none

  ! -- Accessibility --

  private

  ! -- Derived type definitions --

  ! -- Type declarations --

  ! Chemical elements sorted by atomic number

  type(t_chem_element), public, parameter :: c_hydrogen =              &
    t_chem_element(                                                    &
      'H  '                                                          , &
      1_k_i1                                                         , &
      1.00794e+00_k_r8                                               , &
      1_k_i1                                                         , &
      1_k_i1                                                           &
    )

  type(t_chem_element), public, parameter :: c_helium =                &
    t_chem_element(                                                    &
      'He '                                                          , &
      2_k_i1                                                         , &
      4.002602e+00_k_r8                                              , &
      1_k_i1                                                         , &
      18_k_i1                                                          &
    )

  type(t_chem_element), public, parameter :: c_lithium =               &
    t_chem_element(                                                    &
      'Li '                                                          , &
      3_k_i1                                                         , &
      6.941e+00_k_r8                                                 , &
      2_k_i1                                                         , &
      1_k_i1                                                           &
    )

  type(t_chem_element), public, parameter :: c_beryllium =             &
    t_chem_element(                                                    &
      'Be '                                                          , &
      4_k_i1                                                         , &
      9.012182e+00_k_r8                                              , &
      2_k_i1                                                         , &
      2_k_i1                                                           &
    )

  type(t_chem_element), public, parameter :: c_boron =                 &
    t_chem_element(                                                    &
      'B  '                                                          , &
      5_k_i1                                                         , &
      10.811e+00_k_r8                                                , &
      2_k_i1                                                         , &
      13_k_i1                                                          &
    )

  type(t_chem_element), public, parameter :: c_carbon =                &
    t_chem_element(                                                    &
      'C  '                                                          , &
      6_k_i1                                                         , &
      12.0107e+00_k_r8                                               , &
      2_k_i1                                                         , &
      14_k_i1                                                          &
    )

  type(t_chem_element), public, parameter :: c_nitrogen =              &
    t_chem_element(                                                    &
      'N  '                                                          , &
      7_k_i1                                                         , &
      14.0067e+00_k_r8                                               , &
      2_k_i1                                                         , &
      15_k_i1                                                          &
    )

  type(t_chem_element), public, parameter :: c_oxygen =                &
    t_chem_element(                                                    &
      'O  '                                                          , &
      8_k_i1                                                         , &
      15.9994e+00_k_r8                                               , &
      2_k_i1                                                         , &
      16_k_i1                                                          &
    )

  type(t_chem_element), public, parameter :: c_fluorine =              &
    t_chem_element(                                                    &
      'F  '                                                          , &
      9_k_i1                                                         , &
      18.9984032e+00_k_r8                                            , &
      2_k_i1                                                         , &
      17_k_i1                                                          &
    )

  type(t_chem_element), public, parameter :: c_neon =                  &
    t_chem_element(                                                    &
      'Ne '                                                          , &
      10_k_i1                                                        , &
      20.1797e+00_k_r8                                               , &
      2_k_i1                                                         , &
      18_k_i1                                                          &
    )

  type(t_chem_element), public, parameter :: c_sodium =                &
    t_chem_element(                                                    &
      'Na '                                                          , &
      11_k_i1                                                        , &
      22.98976928e+00_k_r8                                           , &
      3_k_i1                                                         , &
      1_k_i1                                                           &
    )

  type(t_chem_element), public, parameter :: c_magnesium =             &
    t_chem_element(                                                    &
      'Mg '                                                          , &
      12_k_i1                                                        , &
      24.3050e+00_k_r8                                               , &
      3_k_i1                                                         , &
      2_k_i1                                                           &
    )

  type(t_chem_element), public, parameter :: c_aluminium =             &
    t_chem_element(                                                    &
      'Al '                                                          , &
      13_k_i1                                                        , &
      26.9815386e+00_k_r8                                            , &
      3_k_i1                                                         , &
      13_k_i1                                                          &
    )

  type(t_chem_element), public, parameter :: c_silicon =               &
    t_chem_element(                                                    &
      'Si '                                                          , &
      14_k_i1                                                        , &
      28.0855e+00_k_r8                                               , &
      3_k_i1                                                         , &
      14_k_i1                                                          &
    )

  type(t_chem_element), public, parameter :: c_phosphorus =            &
    t_chem_element(                                                    &
      'P  '                                                          , &
      15_k_i1                                                        , &
      30.973762e+00_k_r8                                             , &
      3_k_i1                                                         , &
      15_k_i1                                                          &
    )

  type(t_chem_element), public, parameter :: c_sulfur =                &
    t_chem_element(                                                    &
      'S  '                                                          , &
      16_k_i1                                                        , &
      32.065e+00_k_r8                                                , &
      3_k_i1                                                         , &
      16_k_i1                                                          &
    )

  type(t_chem_element), public, parameter :: c_chlorine =              &
    t_chem_element(                                                    &
      'Cl '                                                          , &
      17_k_i1                                                        , &
      35.45e+00_k_r8                                                 , &
      3_k_i1                                                         , &
      17_k_i1                                                          &
    )

  type(t_chem_element), public, parameter :: c_argon =                 &
    t_chem_element(                                                    &
      'Ar '                                                          , &
      18_k_i1                                                        , &
      39.948e+00_k_r8                                                , &
      3_k_i1                                                         , &
      18_k_i1                                                          &
    )

  type(t_chem_element), public, parameter :: c_potassium =             &
    t_chem_element(                                                    &
      'K  '                                                          , &
      19_k_i1                                                        , &
      39.0983e+00_k_r8                                               , &
      4_k_i1                                                         , &
      1_k_i1                                                           &
    )

  type(t_chem_element), public, parameter :: c_calcium =               &
    t_chem_element(                                                    &
      'Ca '                                                          , &
      20_k_i1                                                        , &
      40.078e+00_k_r8                                                , &
      4_k_i1                                                         , &
      2_k_i1                                                           &
    )

  type(t_chem_element), public, parameter :: c_scandium =              &
    t_chem_element(                                                    &
      'Sc '                                                          , &
      21_k_i1                                                        , &
      44.955912e+00_k_r8                                             , &
      4_k_i1                                                         , &
      3_k_i1                                                           &
    )

  type(t_chem_element), public, parameter :: c_titanium =              &
    t_chem_element(                                                    &
      'Ti '                                                          , &
      22_k_i1                                                        , &
      47.867e+00_k_r8                                                , &
      4_k_i1                                                         , &
      4_k_i1                                                           &
    )

  type(t_chem_element), public, parameter :: c_vanadium =              &
    t_chem_element(                                                    &
      'V  '                                                          , &
      23_k_i1                                                        , &
      50.9415e+00_k_r8                                               , &
      4_k_i1                                                         , &
      5_k_i1                                                           &
    )

  type(t_chem_element), public, parameter :: c_chromium =              &
    t_chem_element(                                                    &
      'Cr '                                                          , &
      24_k_i1                                                        , &
      51.9961e+00_k_r8                                               , &
      4_k_i1                                                         , &
      6_k_i1                                                           &
    )

  type(t_chem_element), public, parameter :: c_manganese =             &
    t_chem_element(                                                    &
      'Mn '                                                          , &
      25_k_i1                                                        , &
      54.938045e+00_k_r8                                             , &
      4_k_i1                                                         , &
      7_k_i1                                                           &
    )

  type(t_chem_element), public, parameter :: c_iron =                  &
    t_chem_element(                                                    &
      'Fe '                                                          , &
      26_k_i1                                                        , &
      55.845e+00_k_r8                                                , &
      4_k_i1                                                         , &
      8_k_i1                                                           &
    )

  type(t_chem_element), public, parameter :: c_cobalt =                &
    t_chem_element(                                                    &
      'Co '                                                          , &
      27_k_i1                                                        , &
      58.933195e+00_k_r8                                             , &
      4_k_i1                                                         , &
      9_k_i1                                                           &
    )

  type(t_chem_element), public, parameter :: c_nickel =                &
    t_chem_element(                                                    &
      'Ni '                                                          , &
      28_k_i1                                                        , &
      58.6934e+00_k_r8                                               , &
      4_k_i1                                                         , &
      10_k_i1                                                          &
    )

  type(t_chem_element), public, parameter :: c_copper =                &
    t_chem_element(                                                    &
      'Cu '                                                          , &
      29_k_i1                                                        , &
      63.546e+00_k_r8                                                , &
      4_k_i1                                                         , &
      11_k_i1                                                          &
    )

  type(t_chem_element), public, parameter :: c_zinc =                  &
    t_chem_element(                                                    &
      'Zn '                                                          , &
      30_k_i1                                                        , &
      65.38e+00_k_r8                                                 , &
      4_k_i1                                                         , &
      12_k_i1                                                          &
    )

  type(t_chem_element), public, parameter :: c_gallium =               &
    t_chem_element(                                                    &
      'Ga '                                                          , &
      31_k_i1                                                        , &
      69.723e+00_k_r8                                                , &
      4_k_i1                                                         , &
      13_k_i1                                                          &
    )

  type(t_chem_element), public, parameter :: c_germanium =             &
    t_chem_element(                                                    &
      'Ge '                                                          , &
      32_k_i1                                                        , &
      72.64e+00_k_r8                                                 , &
      4_k_i1                                                         , &
      14_k_i1                                                          &
    )

  type(t_chem_element), public, parameter :: c_arsenic =               &
    t_chem_element(                                                    &
      'As '                                                          , &
      33_k_i1                                                        , &
      74.92160e+00_k_r8                                              , &
      4_k_i1                                                         , &
      15_k_i1                                                          &
    )

  type(t_chem_element), public, parameter :: c_calcenium =              &
    t_chem_element(                                                    &
      'Se '                                                          , &
      34_k_i1                                                        , &
      78.96e+00_k_r8                                                 , &
      4_k_i1                                                         , &
      16_k_i1                                                          &
    )

  type(t_chem_element), public, parameter :: c_bromine =               &
    t_chem_element(                                                    &
      'Br '                                                          , &
      35_k_i1                                                        , &
      79.904e+00_k_r8                                                , &
      4_k_i1                                                         , &
      17_k_i1                                                          &
    )

  type(t_chem_element), public, parameter :: c_krypton =               &
    t_chem_element(                                                    &
      'Kr '                                                          , &
      36_k_i1                                                        , &
      83.798e+00_k_r8                                                , &
      4_k_i1                                                         , &
      18_k_i1                                                          &
    )

  type(t_chem_element), public, parameter :: c_rubidium =              &
    t_chem_element(                                                    &
      'Rb '                                                          , &
      37_k_i1                                                        , &
      85.4678e+00_k_r8                                               , &
      5_k_i1                                                         , &
      1_k_i1                                                           &
    )

  type(t_chem_element), public, parameter :: c_strontium =             &
    t_chem_element(                                                    &
      'Sr '                                                          , &
      38_k_i1                                                        , &
      87.62e+00_k_r8                                                 , &
      5_k_i1                                                         , &
      2_k_i1                                                           &
    )

  type(t_chem_element), public, parameter :: c_yttrium =               &
    t_chem_element(                                                    &
      'Y  '                                                          , &
      39_k_i1                                                        , &
      88.90585e+00_k_r8                                              , &
      5_k_i1                                                         , &
      3_k_i1                                                           &
    )

  type(t_chem_element), public, parameter :: c_zirconium =             &
    t_chem_element(                                                    &
      'Zr '                                                          , &
      40_k_i1                                                        , &
      91.224e+00_k_r8                                                , &
      5_k_i1                                                         , &
      4_k_i1                                                           &
    )

  type(t_chem_element), public, parameter :: c_niobium =               &
    t_chem_element(                                                    &
      'Nb '                                                          , &
      41_k_i1                                                        , &
      92.90638e+00_k_r8                                              , &
      5_k_i1                                                         , &
      5_k_i1                                                           &
    )

  type(t_chem_element), public, parameter :: c_molybdenum =            &
    t_chem_element(                                                    &
      'Mo '                                                          , &
      42_k_i1                                                        , &
      95.94e+00_k_r8                                                 , &
      5_k_i1                                                         , &
      6_k_i1                                                           &
    )

  type(t_chem_element), public, parameter :: c_technetium =            &
    t_chem_element(                                                    &
      'Tc '                                                          , &
      43_k_i1                                                        , &
      98.9063e+00_k_r8                                               , &
      5_k_i1                                                         , &
      7_k_i1                                                           &
    )

  type(t_chem_element), public, parameter :: c_ruthenium =             &
    t_chem_element(                                                    &
      'Ru '                                                          , &
      44_k_i1                                                        , &
      101.07e+00_k_r8                                                , &
      5_k_i1                                                         , &
      8_k_i1                                                           &
    )

  type(t_chem_element), public, parameter :: c_rhodium =               &
    t_chem_element(                                                    &
      'Rh '                                                          , &
      45_k_i1                                                        , &
      102.90550e+00_k_r8                                             , &
      5_k_i1                                                         , &
      9_k_i1                                                           &
    )

  type(t_chem_element), public, parameter :: c_palladium =             &
    t_chem_element(                                                    &
      'Pd '                                                          , &
      46_k_i1                                                        , &
      106.42e+00_k_r8                                                , &
      5_k_i1                                                         , &
      10_k_i1                                                          &
    )

  type(t_chem_element), public, parameter :: c_silver =                &
    t_chem_element(                                                    &
      'Ag '                                                          , &
      47_k_i1                                                        , &
      107.8682e+00_k_r8                                              , &
      5_k_i1                                                         , &
      11_k_i1                                                          &
    )

  type(t_chem_element), public, parameter :: c_cadmium =               &
    t_chem_element(                                                    &
      'Cd '                                                          , &
      48_k_i1                                                        , &
      112.411e+00_k_r8                                               , &
      5_k_i1                                                         , &
      12_k_i1                                                          &
    )

  type(t_chem_element), public, parameter :: c_indium =                &
    t_chem_element(                                                    &
      'In '                                                          , &
      49_k_i1                                                        , &
      114.818e+00_k_r8                                               , &
      5_k_i1                                                         , &
      13_k_i1                                                          &
    )

  type(t_chem_element), public, parameter :: c_tin =                   &
    t_chem_element(                                                    &
      'Sn '                                                          , &
      50_k_i1                                                        , &
      118.710e+00_k_r8                                               , &
      5_k_i1                                                         , &
      14_k_i1                                                          &
    )

  type(t_chem_element), public, parameter :: c_antimony =              &
    t_chem_element(                                                    &
      'Sb '                                                          , &
      51_k_i1                                                        , &
      121.760e+00_k_r8                                               , &
      5_k_i1                                                         , &
      15_k_i1                                                          &
    )

  type(t_chem_element), public, parameter :: c_tellurium =             &
    t_chem_element(                                                    &
      'Te '                                                          , &
      52_k_i1                                                        , &
      127.60e+00_k_r8                                                , &
      5_k_i1                                                         , &
      16_k_i1                                                          &
    )

  type(t_chem_element), public, parameter :: c_iodine =                &
    t_chem_element(                                                    &
      'I  '                                                          , &
      53_k_i1                                                        , &
      126.90447e+00_k_r8                                             , &
      5_k_i1                                                         , &
      17_k_i1                                                          &
    )

  type(t_chem_element), public, parameter :: c_xenon =                 &
    t_chem_element(                                                    &
      'Xe '                                                          , &
      54_k_i1                                                        , &
      131.293e+00_k_r8                                               , &
      5_k_i1                                                         , &
      18_k_i1                                                          &
    )

  type(t_chem_element), public, parameter :: c_caesium =               &
    t_chem_element(                                                    &
      'Cs '                                                          , &
      55_k_i1                                                        , &
      132.9054519e+00_k_r8                                           , &
      6_k_i1                                                         , &
      1_k_i1                                                           &
    )

  type(t_chem_element), public, parameter :: c_barium =                &
    t_chem_element(                                                    &
      'Ba '                                                          , &
      56_k_i1                                                        , &
      137.327e+00_k_r8                                               , &
      6_k_i1                                                         , &
      2_k_i1                                                           &
    )

  type(t_chem_element), public, parameter :: c_lanthanum =             &
    t_chem_element(                                                    &
      'La '                                                          , &
      57_k_i1                                                        , &
      138.90547e+00_k_r8                                             , &
      6_k_i1                                                         , &
      -1_k_i1                                                          &
    )

  type(t_chem_element), public, parameter :: c_cerium =                &
    t_chem_element(                                                    &
      'Ce '                                                          , &
      58_k_i1                                                        , &
      140.116e+00_k_r8                                               , &
      6_k_i1                                                         , &
      -1_k_i1                                                          &
    )

  type(t_chem_element), public, parameter :: c_praseodymium =          &
    t_chem_element(                                                    &
      'Pr '                                                          , &
      59_k_i1                                                        , &
      140.90765e+00_k_r8                                             , &
      6_k_i1                                                         , &
      -1_k_i1                                                          &
    )

  type(t_chem_element), public, parameter :: c_neodymium =             &
    t_chem_element(                                                    &
      'Nd '                                                          , &
      60_k_i1                                                        , &
      144.242e+00_k_r8                                               , &
      6_k_i1                                                         , &
      -1_k_i1                                                          &
    )

  type(t_chem_element), public, parameter :: c_promethium =            &
    t_chem_element(                                                    &
      'Pm '                                                          , &
      61_k_i1                                                        , &
      146.9151e+00_k_r8                                              , &
      6_k_i1                                                         , &
      -1_k_i1                                                          &
    )

  type(t_chem_element), public, parameter :: c_samarium =              &
    t_chem_element(                                                    &
      'Sm '                                                          , &
      62_k_i1                                                        , &
      150.36e+00_k_r8                                                , &
      6_k_i1                                                         , &
      -1_k_i1                                                          &
    )

  type(t_chem_element), public, parameter :: c_europium =              &
    t_chem_element(                                                    &
      'Eu '                                                          , &
      63_k_i1                                                        , &
      151.964e+00_k_r8                                               , &
      6_k_i1                                                         , &
      -1_k_i1                                                          &
    )

  type(t_chem_element), public, parameter :: c_gadolinium =            &
    t_chem_element(                                                    &
      'Gd '                                                          , &
      64_k_i1                                                        , &
      157.25e+00_k_r8                                                , &
      6_k_i1                                                         , &
      -1_k_i1                                                          &
    )

  type(t_chem_element), public, parameter :: c_terbium =               &
    t_chem_element(                                                    &
      'Tb '                                                          , &
      65_k_i1                                                        , &
      158.92535e+00_k_r8                                             , &
      6_k_i1                                                         , &
      -1_k_i1                                                          &
    )

  type(t_chem_element), public, parameter :: c_dysprosium =            &
    t_chem_element(                                                    &
      'Dy '                                                          , &
      66_k_i1                                                        , &
      162.500e+00_k_r8                                               , &
      6_k_i1                                                         , &
      -1_k_i1                                                          &
    )

  type(t_chem_element), public, parameter :: c_holmium =               &
    t_chem_element(                                                    &
      'Ho '                                                          , &
      67_k_i1                                                        , &
      164.93032e+00_k_r8                                             , &
      6_k_i1                                                         , &
      -1_k_i1                                                          &
    )

  type(t_chem_element), public, parameter :: c_erbium =                &
    t_chem_element(                                                    &
      'Er '                                                          , &
      68_k_i1                                                        , &
      167.259e+00_k_r8                                               , &
      6_k_i1                                                         , &
      -1_k_i1                                                          &
    )

  type(t_chem_element), public, parameter :: c_thulium =               &
    t_chem_element(                                                    &
      'Tm '                                                          , &
      69_k_i1                                                        , &
      168.93421e+00_k_r8                                             , &
      6_k_i1                                                         , &
      -1_k_i1                                                          &
    )

  type(t_chem_element), public, parameter :: c_ytterbium =             &
    t_chem_element(                                                    &
      'Yb '                                                          , &
      70_k_i1                                                        , &
      173.054e+00_k_r8                                               , &
      6_k_i1                                                         , &
      -1_k_i1                                                          &
    )

  type(t_chem_element), public, parameter :: c_lutetium =              &
    t_chem_element(                                                    &
      'Lu '                                                          , &
      71_k_i1                                                        , &
      174.9668e+00_k_r8                                              , &
      6_k_i1                                                         , &
      3_k_i1                                                           &
    )

  type(t_chem_element), public, parameter :: c_hafnium =               &
    t_chem_element(                                                    &
      'Hf '                                                          , &
      72_k_i1                                                        , &
      178.49e+00_k_r8                                                , &
      6_k_i1                                                         , &
      4_k_i1                                                           &
    )

  type(t_chem_element), public, parameter :: c_tantalum =              &
    t_chem_element(                                                    &
      'Ta '                                                          , &
      73_k_i1                                                        , &
      180.9479e+00_k_r8                                              , &
      6_k_i1                                                         , &
      5_k_i1                                                           &
    )

  type(t_chem_element), public, parameter :: c_tungsten =              &
    t_chem_element(                                                    &
      'W  '                                                          , &
      74_k_i1                                                        , &
      183.84e+00_k_r8                                                , &
      6_k_i1                                                         , &
      6_k_i1                                                           &
    )

  type(t_chem_element), public, parameter :: c_rhenium =               &
    t_chem_element(                                                    &
      'Re '                                                          , &
      75_k_i1                                                        , &
      186.207e+00_k_r8                                               , &
      6_k_i1                                                         , &
      7_k_i1                                                           &
    )

  type(t_chem_element), public, parameter :: c_osmium =                &
    t_chem_element(                                                    &
      'Os '                                                          , &
      76_k_i1                                                        , &
      190.23e+00_k_r8                                                , &
      6_k_i1                                                         , &
      8_k_i1                                                           &
    )

  type(t_chem_element), public, parameter :: c_iridium =               &
    t_chem_element(                                                    &
      'Ir '                                                          , &
      77_k_i1                                                        , &
      192.217e+00_k_r8                                               , &
      6_k_i1                                                         , &
      9_k_i1                                                           &
    )

  type(t_chem_element), public, parameter :: c_platinum =              &
    t_chem_element(                                                    &
      'Pt '                                                          , &
      78_k_i1                                                        , &
      195.084e+00_k_r8                                               , &
      6_k_i1                                                         , &
      10_k_i1                                                          &
    )

  type(t_chem_element), public, parameter :: c_gold =                  &
    t_chem_element(                                                    &
      'Au '                                                          , &
      79_k_i1                                                        , &
      196.966569e+00_k_r8                                            , &
      6_k_i1                                                         , &
      11_k_i1                                                          &
    )

  type(t_chem_element), public, parameter :: c_mercury =               &
    t_chem_element(                                                    &
      'Hg '                                                          , &
      80_k_i1                                                        , &
      200.59e+00_k_r8                                                , &
      6_k_i1                                                         , &
      12_k_i1                                                          &
    )

  type(t_chem_element), public, parameter :: c_thalium =               &
    t_chem_element(                                                    &
      'Tl '                                                          , &
      81_k_i1                                                        , &
      204.3833e+00_k_r8                                              , &
      6_k_i1                                                         , &
      13_k_i1                                                          &
    )

  type(t_chem_element), public, parameter :: c_lead =                  &
    t_chem_element(                                                    &
      'Pb '                                                          , &
      82_k_i1                                                        , &
      207.2e+00_k_r8                                                 , &
      6_k_i1                                                         , &
      14_k_i1                                                          &
    )

  type(t_chem_element), public, parameter :: c_bismuth =               &
    t_chem_element(                                                    &
      'Bi '                                                          , &
      83_k_i1                                                        , &
      208.98040e+00_k_r8                                             , &
      6_k_i1                                                         , &
      15_k_i1                                                          &
    )

  type(t_chem_element), public, parameter :: c_polonium =              &
    t_chem_element(                                                    &
      'Po '                                                          , &
      84_k_i1                                                        , &
      208.9824e+00_k_r8                                              , &
      6_k_i1                                                         , &
      16_k_i1                                                          &
    )

  type(t_chem_element), public, parameter :: c_astatine =              &
    t_chem_element(                                                    &
      'At '                                                          , &
      85_k_i1                                                        , &
      209.9871e+00_k_r8                                              , &
      6_k_i1                                                         , &
      17_k_i1                                                          &
    )

  type(t_chem_element), public, parameter :: c_radon =                 &
    t_chem_element(                                                    &
      'Rn '                                                          , &
      86_k_i1                                                        , &
      222.0176e+00_k_r8                                              , &
      6_k_i1                                                         , &
      18_k_i1                                                          &
    )

  type(t_chem_element), public, parameter :: c_francium =              &
    t_chem_element(                                                    &
      'Fr '                                                          , &
      87_k_i1                                                        , &
      223.0197e+00_k_r8                                              , &
      7_k_i1                                                         , &
      1_k_i1                                                           &
    )

  type(t_chem_element), public, parameter :: c_radium =                &
    t_chem_element(                                                    &
      'Ra '                                                          , &
      88_k_i1                                                        , &
      226.0254e+00_k_r8                                              , &
      7_k_i1                                                         , &
      2_k_i1                                                           &
    )

  type(t_chem_element), public, parameter :: c_actinium =              &
    t_chem_element(                                                    &
      'Ac '                                                          , &
      89_k_i1                                                        , &
      227.0278e+00_k_r8                                              , &
      7_k_i1                                                         , &
      -2_k_i1                                                          &
    )

  type(t_chem_element), public, parameter :: c_thorium =               &
    t_chem_element(                                                    &
      'Th '                                                          , &
      90_k_i1                                                        , &
      232.03806e+00_k_r8                                             , &
      7_k_i1                                                         , &
      -2_k_i1                                                          &
    )

  type(t_chem_element), public, parameter :: c_protactinium =          &
    t_chem_element(                                                    &
      'Pa '                                                          , &
      91_k_i1                                                        , &
      231.03588e+00_k_r8                                             , &
      7_k_i1                                                         , &
      -2_k_i1                                                          &
    )

  type(t_chem_element), public, parameter :: c_uranium =               &
    t_chem_element(                                                    &
      'U  '                                                          , &
      92_k_i1                                                        , &
      238.02891e+00_k_r8                                             , &
      7_k_i1                                                         , &
      -2_k_i1                                                          &
    )

  type(t_chem_element), public, parameter :: c_neptunium =             &
    t_chem_element(                                                    &
      'Np '                                                          , &
      93_k_i1                                                        , &
      237.048e+00_k_r8                                               , &
      7_k_i1                                                         , &
      -2_k_i1                                                          &
    )

  type(t_chem_element), public, parameter :: c_plutonium =             &
    t_chem_element(                                                    &
      'Pu '                                                          , &
      94_k_i1                                                        , &
      244.0642e+00_k_r8                                              , &
      7_k_i1                                                         , &
      -2_k_i1                                                          &
    )

  type(t_chem_element), public, parameter :: c_americium =             &
    t_chem_element(                                                    &
      'Am '                                                          , &
      95_k_i1                                                        , &
      243.0614e+00_k_r8                                              , &
      7_k_i1                                                         , &
      -2_k_i1                                                          &
    )

  type(t_chem_element), public, parameter :: c_curium =                &
    t_chem_element(                                                    &
      'Cm '                                                          , &
      96_k_i1                                                        , &
      247.0704e+00_k_r8                                              , &
      7_k_i1                                                         , &
      -2_k_i1                                                          &
    )

  type(t_chem_element), public, parameter :: c_berkelium =             &
    t_chem_element(                                                    &
      'Bk '                                                          , &
      97_k_i1                                                        , &
      247.0703e+00_k_r8                                              , &
      7_k_i1                                                         , &
      -2_k_i1                                                          &
    )

  type(t_chem_element), public, parameter :: c_californium =           &
    t_chem_element(                                                    &
      'Cf '                                                          , &
      98_k_i1                                                        , &
      251.0796e+00_k_r8                                              , &
      7_k_i1                                                         , &
      -2_k_i1                                                          &
    )

  type(t_chem_element), public, parameter :: c_einsteinium =           &
    t_chem_element(                                                    &
      'Es '                                                          , &
      99_k_i1                                                        , &
      252.0829e+00_k_r8                                              , &
      7_k_i1                                                         , &
      -2_k_i1                                                          &
    )

  type(t_chem_element), public, parameter :: c_fermium =               &
    t_chem_element(                                                    &
      'Fm '                                                          , &
      100_k_i1                                                       , &
      257.0951e+00_k_r8                                              , &
      7_k_i1                                                         , &
      -2_k_i1                                                          &
    )

  type(t_chem_element), public, parameter :: c_mendelevium =           &
    t_chem_element(                                                    &
      'Md '                                                          , &
      101_k_i1                                                       , &
      258.0986e+00_k_r8                                              , &
      7_k_i1                                                         , &
      -2_k_i1                                                          &
    )

  type(t_chem_element), public, parameter :: c_nobelium =              &
    t_chem_element(                                                    &
      'No '                                                          , &
      102_k_i1                                                       , &
      259.1009e+00_k_r8                                              , &
      7_k_i1                                                         , &
      -2_k_i1                                                          &
    )

  type(t_chem_element), public, parameter :: c_lawrencium =            &
    t_chem_element(                                                    &
      'Lr '                                                          , &
      103_k_i1                                                       , &
      264.0e+00_k_r8                                                 , &
      7_k_i1                                                         , &
      3_k_i1                                                           &
    )

  type(t_chem_element), public, parameter :: c_rutherfordium =         &
    t_chem_element(                                                    &
      'Rf '                                                          , &
      104_k_i1                                                       , &
      265.0e+00_k_r8                                                 , &
      7_k_i1                                                         , &
      4_k_i1                                                           &
    )

  type(t_chem_element), public, parameter :: c_dubnium =               &
    t_chem_element(                                                    &
      'Db '                                                          , &
      105_k_i1                                                       , &
      268.0e+00_k_r8                                                 , &
      7_k_i1                                                         , &
      5_k_i1                                                           &
    )

  type(t_chem_element), public, parameter :: c_seaborgium =            &
    t_chem_element(                                                    &
      'Sg '                                                          , &
      106_k_i1                                                       , &
      272.0e+00_k_r8                                                 , &
      7_k_i1                                                         , &
      6_k_i1                                                           &
    )

  type(t_chem_element), public, parameter :: c_bohrium =               &
    t_chem_element(                                                    &
      'Bh '                                                          , &
      107_k_i1                                                       , &
      273.0e+00_k_r8                                                 , &
      7_k_i1                                                         , &
      7_k_i1                                                           &
    )

  type(t_chem_element), public, parameter :: c_hassium =               &
    t_chem_element(                                                    &
      'Hs '                                                          , &
      108_k_i1                                                       , &
      276.0e+00_k_r8                                                 , &
      7_k_i1                                                         , &
      8_k_i1                                                           &
    )

  type(t_chem_element), public, parameter :: c_meitnerium =            &
    t_chem_element(                                                    &
      'Mt '                                                          , &
      109_k_i1                                                       , &
      279.0e+00_k_r8                                                 , &
      7_k_i1                                                         , &
      9_k_i1                                                           &
    )

  type(t_chem_element), public, parameter :: c_darmstadtium =          &
    t_chem_element(                                                    &
      'Ds '                                                          , &
      110_k_i1                                                       , &
      278.0e+00_k_r8                                                 , &
      7_k_i1                                                         , &
      10_k_i1                                                          &
    )

  type(t_chem_element), public, parameter :: c_roentgenium =           &
    t_chem_element(                                                    &
      'Rg '                                                          , &
      111_k_i1                                                       , &
      283e+00_k_r8                                                   , &
      7_k_i1                                                         , &
      11_k_i1                                                          &
    )

  type(t_chem_element), public, parameter :: c_ununbium =              &
    t_chem_element(                                                    &
      'Uub'                                                          , &
      112_k_i1                                                       , &
      285.0e+00_k_r8                                                 , &
      7_k_i1                                                         , &
      12_k_i1                                                          &
    )

  type(t_chem_element), public, parameter :: c_ununtrium =             &
    t_chem_element(                                                    &
      'Uut'                                                          , &
      113_k_i1                                                       , &
      287.0e+00_k_r8                                                 , &
      7_k_i1                                                         , &
      13_k_i1                                                          &
    )

  type(t_chem_element), public, parameter :: c_ununquadium =           &
    t_chem_element(                                                    &
      'Uuq'                                                          , &
      114_k_i1                                                       , &
      289.0e+00_k_r8                                                 , &
      7_k_i1                                                         , &
      14_k_i1                                                          &
    )

  type(t_chem_element), public, parameter :: c_ununpentium =           &
    t_chem_element(                                                    &
      'Uup'                                                          , &
      115_k_i1                                                       , &
      291.0e+00_k_r8                                                 , &
      7_k_i1                                                         , &
      15_k_i1                                                          &
    )

  type(t_chem_element), public, parameter :: c_ununhexium =            &
    t_chem_element(                                                    &
      'Uuh'                                                          , &
      116_k_i1                                                       , &
      293.0e+00_k_r8                                                 , &
      7_k_i1                                                         , &
      16_k_i1                                                          &
    )

  type(t_chem_element), public, parameter :: c_ununseptium =           &
    t_chem_element(                                                    &
      'Uus'                                                          , &
      117_k_i1                                                       , &
      295.0e+00_k_r8                                                 , &
      7_k_i1                                                         , &
      17_k_i1                                                          &
    )

  type(t_chem_element), public, parameter :: c_ununoctium =            &
    t_chem_element(                                                    &
      'Uuo'                                                          , &
      118_k_i1                                                       , &
      294.0e+00_k_r8                                                 , &
      7_k_i1                                                         , &
      18_k_i1                                                          &
    )

  ! Periodic table

  type(t_chem_element), public, parameter, dimension(118) ::           &
    c_periodic_system =                                                &
      (/                                                               &
        c_hydrogen     , c_helium        , c_lithium                 , &
        c_beryllium    , c_boron         , c_carbon                  , &
        c_nitrogen     , c_oxygen        , c_fluorine                , &
        c_neon         , c_sodium        , c_magnesium               , &
        c_aluminium    , c_silicon       , c_phosphorus              , &
        c_sulfur       , c_chlorine      , c_argon                   , &
        c_potassium    , c_calcium       , c_scandium                , &
        c_titanium     , c_vanadium      , c_chromium                , &
        c_manganese    , c_iron          , c_cobalt                  , &
        c_nickel       , c_copper        , c_zinc                    , &
        c_gallium      , c_germanium     , c_arsenic                 , &
        c_calcenium     , c_bromine       , c_krypton                 , &
        c_rubidium     , c_strontium     , c_yttrium                 , &
        c_zirconium    , c_niobium       , c_molybdenum              , &
        c_technetium   , c_ruthenium     , c_rhodium                 , &
        c_palladium    , c_silver        , c_cadmium                 , &
        c_indium       , c_tin           , c_antimony                , &
        c_tellurium    , c_iodine        , c_xenon                   , &
        c_caesium      , c_barium        , c_lanthanum               , &
        c_cerium       , c_praseodymium  , c_neodymium               , &
        c_promethium   , c_samarium      , c_europium                , &
        c_gadolinium   , c_terbium       , c_dysprosium              , &
        c_holmium      , c_erbium        , c_thulium                 , &
        c_ytterbium    , c_lutetium      , c_hafnium                 , &
        c_tantalum     , c_tungsten      , c_rhenium                 , &
        c_osmium       , c_iridium       , c_platinum                , &
        c_gold         , c_mercury       , c_thalium                 , &
        c_lead         , c_bismuth       , c_polonium                , &
        c_astatine     , c_radon         , c_francium                , &
        c_radium       , c_actinium      , c_thorium                 , &
        c_protactinium , c_uranium       , c_neptunium               , &
        c_plutonium    , c_americium     , c_curium                  , &
        c_berkelium    , c_californium   , c_einsteinium             , &
        c_fermium      , c_mendelevium   , c_nobelium                , &
        c_lawrencium   , c_rutherfordium , c_dubnium                 , &
        c_seaborgium   , c_bohrium       , c_hassium                 , &
        c_meitnerium   , c_darmstadtium  , c_roentgenium             , &
        c_ununbium     , c_ununtrium     , c_ununquadium             , &
        c_ununpentium  , c_ununhexium    , c_ununseptium             , &
        c_ununoctium                                                   &
      /)

  ! -- Interface blocks --

!-------------------------------------------------------------------------------
! End specification part
!-------------------------------------------------------------------------------

!-------------------------------------------------------------------------------
! Begin subprogram part
!-------------------------------------------------------------------------------

!-------------------------------------------------------------------------------
! End subprogram part
!-------------------------------------------------------------------------------

end module calc_cons_chem
