module calc_cons_num
 
!-------------------------------------------------------------------------------
! This file is part of the Computational Algorithm Library Collection (CALC).
!
! Copyright 2012 The CALC Team
!
! CALC is free software: you can redistribute it and/or modify
! it under the terms of the GNU Lesser General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! CALC is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU Lesser General Public License for more details.
!
! You should have received a copy of the GNU Lesser General Public License
! along with CALC.  If not, see <http://www.gnu.org/licenses/>.
!-------------------------------------------------------------------------------
 
!-------------------------------------------------------------------------------
! Begin specification part
!-------------------------------------------------------------------------------

  ! -- Use association --

  use calc_type, only: k_r8

  ! -- Null mapping --

  implicit none

  ! -- Accessibility --

  private

  ! -- Derived type definitions --

  ! -- Type declarations --

  ! Small number

  real (kind = k_r8), parameter, public ::                             &
    c_small =                                                          &
      1.e+01_k_r8*tiny(1.0_k_r8)

  ! Large number

  real (kind = k_r8), parameter, public ::                             &
    c_large =                                                          &
      1.e-01_k_r8*huge(1.0_k_r8)

  ! -- Interface blocks --

!-------------------------------------------------------------------------------
! End specification part
!-------------------------------------------------------------------------------

!-------------------------------------------------------------------------------
! Begin subprogram part
!-------------------------------------------------------------------------------

!-------------------------------------------------------------------------------
! End subprogram part
!-------------------------------------------------------------------------------

end module calc_cons_num
