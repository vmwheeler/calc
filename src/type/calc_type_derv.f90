module calc_type_derv

!-------------------------------------------------------------------------------
! This file is part of the Computational Algorithm Library Collection (CALC).
!
! Copyright 2012 The CALC Team
!
! CALC is free software: you can redistribute it and/or modify
! it under the terms of the GNU Lesser General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! CALC is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU Lesser General Public License for more details.
!
! You should have received a copy of the GNU Lesser General Public License
! along with CALC.  If not, see <http://www.gnu.org/licenses/>.
!-------------------------------------------------------------------------------

!-------------------------------------------------------------------------------
! Begin specification part
!-------------------------------------------------------------------------------

  ! -- Use association --

  use calc_type_intr, only: k_ch, k_i1, k_r8

  ! -- Null mapping --

  implicit none

  ! -- Accessibility --

  private

  ! -- Derived type defitions --

  type, public :: t_chem_element
    character (kind = k_ch, len = 3) :: symbol
    integer   (kind = k_i1)          :: atomic_number
    real      (kind = k_r8)          :: std_atomic_weight
    integer   (kind = k_i1)          :: period
    integer   (kind = k_i1)          :: group
  end type t_chem_element

  type, public :: t_cartesian_point
    real (kind = k_r8) :: x
    real (kind = k_r8) :: y
    real (kind = k_r8) :: z
  end type t_cartesian_point

  type, public :: t_cartesian_free_vector
    real (kind = k_r8) :: x
    real (kind = k_r8) :: y
    real (kind = k_r8) :: z
  end type t_cartesian_free_vector

  type, public :: t_cartesian_bound_vector
    type (t_cartesian_point)       :: r
    type (t_cartesian_free_vector) :: s
  end type t_cartesian_bound_vector

  type :: t_ray
    type (t_cartesian_point)       :: r
    type (t_cartesian_free_vector) :: s
    real(kind = k_r8)              :: scalar1
    real(kind = k_r8)              :: scalar2
  end type t_ray

  ! -- Type declarations --

  ! -- Interface blocks --

!-------------------------------------------------------------------------------
! End specification part
!-------------------------------------------------------------------------------

!-------------------------------------------------------------------------------
! Begin subprogram part
!-------------------------------------------------------------------------------

!-------------------------------------------------------------------------------
! End subprogram part
!-------------------------------------------------------------------------------

end module calc_type_derv
