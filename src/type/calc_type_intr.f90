module calc_type_intr

!-------------------------------------------------------------------------------
! This file is part of the Computational Algorithm Library Collection (CALC).
!
! Copyright 2012 The CALC Team
!
! CALC is free software: you can redistribute it and/or modify
! it under the terms of the GNU Lesser General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! CALC is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU Lesser General Public License for more details.
!
! You should have received a copy of the GNU Lesser General Public License
! along with CALC.  If not, see <http://www.gnu.org/licenses/>.
!-------------------------------------------------------------------------------

!-------------------------------------------------------------------------------
! Begin specification part
!-------------------------------------------------------------------------------

  ! -- Use association --

  ! -- Null mapping --

  implicit none

  ! -- Accessibility --

  private

  ! -- Derived type definitions --

  ! -- Type declarations --

  ! Kind type parameters for 1, 2, 4, and 8 byte integer types

  integer, parameter, public :: k_i1 = selected_int_kind( 2)
  integer, parameter, public :: k_i2 = selected_int_kind( 4)
  integer, parameter, public :: k_i4 = selected_int_kind( 9)
  integer, parameter, public :: k_i8 = selected_int_kind(18)

  ! Kind type parameters for single (4-byte), double (8-byte),
  ! extended (10-byte) and quadruple (16-byte) precision real types

  integer, parameter, public :: k_r4  = selected_real_kind( 6,   37)
  integer, parameter, public :: k_r8  = selected_real_kind(15,  307)
  integer, parameter, public :: k_r10 = selected_real_kind(18, 4931)
  integer, parameter, public :: k_r16 = selected_real_kind(32, 4931)

  integer, parameter, public :: k_sp  = selected_real_kind( 6,   37)
  integer, parameter, public :: k_dp  = selected_real_kind(15,  307)
  integer, parameter, public :: k_ep  = selected_real_kind(18, 4931)
  integer, parameter, public :: k_qp  = selected_real_kind(32, 4931)

  ! Kind type parameters for single (2x4-byte), double (2x8-byte),
  ! extended (2x10-byte) and quadruple (2x16-byte) precision complex types

  integer, parameter, public :: k_c4  = selected_real_kind( 6,   37)
  integer, parameter, public :: k_c8  = selected_real_kind(15,  307)
  integer, parameter, public :: k_c10 = selected_real_kind(18, 4931)
  integer, parameter, public :: k_c16 = selected_real_kind(32, 4931)

  integer, parameter, public :: k_spc = selected_real_kind( 6,   37)
  integer, parameter, public :: k_dpc = selected_real_kind(15,  307)
  integer, parameter, public :: k_epc = selected_real_kind(18, 4931)
  integer, parameter, public :: k_qpc = selected_real_kind(32, 4931)

  ! Kind type parameters for the character types 

  integer, parameter, public :: k_ch  = kind('A')

  ! Kind type parameters for the character types 

  integer, parameter, public :: k_lg = kind(.false.)

  ! -- Interface blocks --

!-------------------------------------------------------------------------------
! End specification part
!-------------------------------------------------------------------------------

!-------------------------------------------------------------------------------
! Begin subprogram part
!-------------------------------------------------------------------------------

!-------------------------------------------------------------------------------
! End subprogram part
!-------------------------------------------------------------------------------

end module calc_type_intr
