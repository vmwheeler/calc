pure function search_vector(x, a) result (ipos)

!-------------------------------------------------------------------------------
! This file is part of the Computational Algorithm Library Collection (CALC).
!
! Copyright 2012 The CALC Team
!
! CALC is free software: you can redistribute it and/or modify
! it under the terms of the GNU Lesser General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! CALC is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU Lesser General Public License for more details.
!
! You should have received a copy of the GNU Lesser General Public License
! along with CALC.  If not, see <http://www.gnu.org/licenses/>.
!-------------------------------------------------------------------------------

!-------------------------------------------------------------------------------
! Begin specification part
!-------------------------------------------------------------------------------

  ! -- Use association --

  use calc_type, only: k_i4, k_r8

  ! -- Null mapping --

  implicit none

  ! Dummy argument declarations

  real    (kind = k_r8), dimension (:), intent (in) :: x
  real    (kind = k_r8),                intent (in) :: a
  integer (kind = k_i4)                             :: ipos

  ! Local declarations

  real    (kind = k_r8) :: sgn, asgn
  integer (kind = k_i4) :: ih, i, n

!-------------------------------------------------------------------------------
! End specification part
!-------------------------------------------------------------------------------

!-------------------------------------------------------------------------------
! Begin execution part
!-------------------------------------------------------------------------------

  n    = size(x)
  sgn  = sign(1.0_k_r8, x(n) - x(1))
  asgn = a * sgn
  if (asgn < x(1) * sgn) then
    ipos = 0
  else if (asgn > x(n) * sgn) then
    ipos = n
  else if (a == x(1)) then
    ipos = 1
  else if (a == x(n)) then
    ipos = n - 1
  else
    ipos = 1
    ih   = n
    do
      if (ipos + 1 >= ih) exit
      i = (ipos + ih) / 2
      if (asgn >= sgn * x(i)) then
        ipos = i
      else
        ih = i
      end if
    end do
  end if

  ! -- Last executable statement --

  return

!-------------------------------------------------------------------------------
! End execution part
!-------------------------------------------------------------------------------

!-------------------------------------------------------------------------------
! Begin subprogram part
!-------------------------------------------------------------------------------

!-------------------------------------------------------------------------------
! End subprogram part
!-------------------------------------------------------------------------------

end function search_vector
