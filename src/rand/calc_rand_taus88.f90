module calc_rand_taus88

!-------------------------------------------------------------------------------
! This file is part of the Computational Algorithm Library Collection (CALC).
!
! Copyright 2012 The CALC Team
! Copyright 1999 Alan Miller
! Copyright 1996 P. L'Ecuyer
!
! CALC is free software: you can redistribute it and/or modify
! it under the terms of the GNU Lesser General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! CALC is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU Lesser General Public License for more details.
!
! You should have received a copy of the GNU Lesser General Public License
! along with CALC.  If not, see <http://www.gnu.org/licenses/>.
!-------------------------------------------------------------------------------

!-------------------------------------------------------------------------------
! Begin specification part
!-------------------------------------------------------------------------------

  ! -- Use association --

  use calc_type, only: k_i4

  ! -- Null mapping --

  implicit none

  ! -- Accessibility --

  private
  public :: init_taus88
  public :: taus88

  ! -- Derived type definitions --

  ! -- Type declarations --

  ! These are unsigned integers in the C version

  integer (kind = k_i4), save :: s1 = 1234, s2 = -4567, s3 = 7890

  ! -- Interface blocks --

  interface init_taus88
    module procedure init_taus88
  end interface init_taus88

  interface taus88
    module procedure taus88
  end interface taus88

!-------------------------------------------------------------------------------
! End specification part
!-------------------------------------------------------------------------------

!-------------------------------------------------------------------------------
! Begin subprogram part
!-------------------------------------------------------------------------------

contains

  subroutine init_taus88(i1, i2, i3)

  !-----------------------------------------------------------------------------
  ! Begin specification part
  !-----------------------------------------------------------------------------

    ! -- Use association --

    use calc_type, only: k_i4

    ! -- Null mapping --

    implicit none

    ! -- Dummy argument declarations --

    integer (kind = k_i4), intent (in) :: i1, i2, i3

    ! -- Local declarations --

  !-----------------------------------------------------------------------------
  ! End specification part
  !-----------------------------------------------------------------------------

  !-----------------------------------------------------------------------------
  ! Begin execution part
  !-----------------------------------------------------------------------------

    s1 = i1
    s2 = i2
    s3 = i3
    if (iand(s1,-2)  == 0) s1 = i1 - 1023
    if (iand(s2,-8)  == 0) s2 = i2 - 1023
    if (iand(s3,-16) == 0) s3 = i3 - 1023

    ! -- Last executable statement --

    return

  !-----------------------------------------------------------------------------
  ! End execution part
  !-----------------------------------------------------------------------------

  !-----------------------------------------------------------------------------
  ! Begin subprogram part
  !-----------------------------------------------------------------------------

  !-----------------------------------------------------------------------------
  ! End subprogram part
  !-----------------------------------------------------------------------------

  end subroutine init_taus88

  function taus88() result (rn)

  !-----------------------------------------------------------------------------
  ! Begin specification part
  !-----------------------------------------------------------------------------

    ! -- Use association --

    use calc_type, only: k_i4, k_r8

    ! -- Null mapping --

    implicit none

    ! -- Dummy argument declarations --

    real (kind = k_r8) :: rn

    ! -- Local declarations --

    integer (kind = k_i4) :: b

  !-----------------------------------------------------------------------------
  ! End specification part
  !-----------------------------------------------------------------------------

  !-----------------------------------------------------------------------------
  ! Begin execution part
  !-----------------------------------------------------------------------------

    ! N.B. ishft(i,j) is a bitwise (non-circular) shift operation;
    ! to the left if j > 0, otherwise to the right.

    b  = ishft(ieor ( ishft(s1,  13), s1), -19)
    s1 = ieor (ishft( iand (s1,  -2), 12),   b)
    b  = ishft(ieor ( ishft(s2,   2), s2), -25)
    s2 = ieor (ishft( iand (s2,  -8),  4),   b)
    b  = ishft(ieor ( ishft(s3,   3), s3), -11)
    s3 = ieor (ishft( iand (s3, -16), 17),   b)

    rn = ieor(ieor(s1,s2), s3) * 2.3283064365E-10_k_r8 + 0.5_k_r8

    ! -- Last executable statement --

    return

  !-----------------------------------------------------------------------------
  ! End execution part
  !-----------------------------------------------------------------------------

  !-----------------------------------------------------------------------------
  ! Begin subprogram part
  !-----------------------------------------------------------------------------

  !-----------------------------------------------------------------------------
  ! End subprogram part
  !-----------------------------------------------------------------------------

  end function taus88

!-------------------------------------------------------------------------------
! End subprogram part
!-------------------------------------------------------------------------------

end module calc_rand_taus88
