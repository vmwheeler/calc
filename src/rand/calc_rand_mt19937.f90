module calc_rand_mt19937

!-------------------------------------------------------------------------------
! This file is part of the Computational Algorithm Library Collection (CALC).
!
! Copyright 2012 The CALC Team
! Copyright 1999, 2002 Alan Miller
! Copyright 1999 Hiroschi Takano
! Copyright 1997 Makoto Matsumoto and Takuji Nishimura
!
! CALC is free software: you can redistribute it and/or modify
! it under the terms of the GNU Lesser General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! CALC is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU Lesser General Public License for more details.
!
! You should have received a copy of the GNU Lesser General Public License
! along with CALC.  If not, see <http://www.gnu.org/licenses/>.
!-------------------------------------------------------------------------------

!-------------------------------------------------------------------------------
! Begin specification part
!-------------------------------------------------------------------------------

  ! -- Use association --

  use calc_type, only: k_i4, k_r8

  ! -- Null mapping --

  implicit none

  ! -- Accessibility --

  private
  public :: init_mt19937
  public :: mt19937

  ! -- Derived type definitions --

  ! -- Type declarations --

  ! Period parameters

  integer (kind = k_i4), parameter :: n = 624, n1 = n+1
  integer (kind = k_i4), parameter :: m = 397, mata = -1727483681

  ! Most significant w-r bits

  integer (kind = k_i4), parameter :: umask = -2147483647 - 1

  ! Least significant r bits

  integer (kind = k_i4), parameter :: lmask =  2147483647

  ! Tempering parameters

  integer (kind = k_i4), parameter :: tmaskb = -1658038656
  integer (kind = k_i4), parameter :: tmaskc = -272236544

  ! The array for the state vector

  integer (kind = k_i4), dimension (0:n-1), save :: mt

  ! mti == n + 1 means mt(n) is not initialized

  integer (kind = k_i4), save :: mti = n1

  ! -- Interface blocks --

  interface init_mt19937
    module procedure init_mt19937
  end interface init_mt19937

  interface mt19937
    module procedure mt19937
  end interface mt19937

!-------------------------------------------------------------------------------
! End specification part
!-------------------------------------------------------------------------------

!-------------------------------------------------------------------------------
! Begin subprogram part
!-------------------------------------------------------------------------------

contains

  subroutine init_mt19937(seed)

  !-----------------------------------------------------------------------------
  ! Begin specification part
  !-----------------------------------------------------------------------------

    ! -- Use association --

    use calc_type, only: k_i4

    ! -- Null mapping --

    implicit none

    ! -- Dummy argument declarations --

    integer (kind = k_i4), intent (in)   :: seed

    ! -- Local declarations --

    real    (kind = k_r8) :: two31, temp
    integer (kind = k_i4) :: itemp, itemp2

  !-----------------------------------------------------------------------------
  ! End specification part
  !-----------------------------------------------------------------------------

  !-----------------------------------------------------------------------------
  ! Begin execution part
  !-----------------------------------------------------------------------------

    ! setting initial seeds to mt[N] using the generator Line 25 of Table 1 in
    ! [KNUTH 1981, The Art of Computer Programming Vol. 2 (2nd Ed.), pp102]

    mt(0) = iand(seed, -1)
    two31 = 2.0_k_r8**31
    do mti = 1, n - 1

      ! The following code in this loop is equivalent to:
      ! the single line of code:
      ! mt(mti) = iand(69069 * mt(mti-1), -1)
      !
      ! The code here is used instead to prevent integer overflow.

      temp   = 69069.0_k_r8 * real(mt(mti-1), kind = k_r8)
      itemp  = mod(temp, two31)
      itemp2 = temp / two31
      if (mod(itemp2, 2) .ne. 0) then
        if (itemp > 0) then
          itemp = itemp - two31
        else
          itemp = itemp + two31
        end if
      end if
      mt(mti) = itemp
    end do

    ! -- Last executable statement --

    return

  !-----------------------------------------------------------------------------
  ! End execution part
  !-----------------------------------------------------------------------------

  !-----------------------------------------------------------------------------
  ! Begin subprogram part
  !-----------------------------------------------------------------------------

  !-----------------------------------------------------------------------------
  ! End subprogram part
  !-----------------------------------------------------------------------------

  end subroutine init_mt19937

  function mt19937() result (rn)

  !-----------------------------------------------------------------------------
  ! Begin specification part
  !-----------------------------------------------------------------------------

    ! -- Use association --

    use calc_type, only: k_i4, k_r8

    ! -- Null mapping --

    implicit none

    ! -- Dummy argument declarations --

    real (kind = k_r8) :: rn

    ! -- Local declarations --

    ! mag01(x) = x * MATA for x=0,1

    integer (kind = k_i4), dimension (0:1), save :: mag01 = (/ 0, mata /)
    integer (kind = k_i4)                        :: kk, y

  !-----------------------------------------------------------------------------
  ! End specification part
  !-----------------------------------------------------------------------------

  !-----------------------------------------------------------------------------
  ! Begin execution part
  !-----------------------------------------------------------------------------

    ! Generate N words at one time

    if (mti >= n) then

      ! If sgrnd() has not been called,

      if (mti == n + 1) then

        ! A default initial seed is used

        call init_mt19937(4357)
      end if

      do  kk = 0, n - m - 1
        y      = ior (iand(mt(kk),umask), iand(mt(kk + 1), lmask))
        mt(kk) = ieor(ieor(mt(kk + m), ishft(y, -1)), mag01(iand(y, 1)))
      end do
      do  kk = n - m, n - 2
        y      = ior (iand(mt(kk), umask), iand(mt(kk + 1), lmask))
        mt(kk) = ieor(ieor(mt(kk + (m - n)), ishft(y, -1)),mag01(iand(y, 1)))
      end do
      y         = ior (iand(mt(n - 1), umask), iand(mt(0), lmask))
      mt(n - 1) = ieor(ieor(mt(m - 1), ishft(y, -1)), mag01(iand(y, 1)))
      mti       = 0
    end if

    y   = mt(mti)
    mti = mti + 1
    y   = ieor(y, tshftu(y))
    y   = ieor(y, iand(tshfts(y), tmaskb))
    y   = ieor(y, iand(tshftt(y), tmaskc))
    y   = ieor(y, tshftl(y))

    if (y < 0) then
      rn = (real(y, kind = k_r8) + 2.0_k_r8**32) / &
           (2.0_k_r8**32 - 1.0_k_r8)
    else
      rn = real(y, kind = k_r8)                  / &
           (2.0_k_r8**32 - 1.0_k_r8)
    end if

    ! -- Last executable statement --

    return

  !-----------------------------------------------------------------------------
  ! End execution part
  !-----------------------------------------------------------------------------

  !-----------------------------------------------------------------------------
  ! Begin subprogram part
  !-----------------------------------------------------------------------------

  !-----------------------------------------------------------------------------
  ! End subprogram part
  !-----------------------------------------------------------------------------

  end function mt19937

  function tshftu(y) result (fn_val)

  !-----------------------------------------------------------------------------
  ! Begin specification part
  !-----------------------------------------------------------------------------

    ! -- Use association --

    use calc_type, only: k_i4

    ! -- Null mapping --

    implicit none

    ! -- Dummy argument declarations --

    integer (kind = k_i4), intent (in) :: y
    integer (kind = k_i4)              :: fn_val

    ! -- Local declarations --

  !-----------------------------------------------------------------------------
  ! End specification part
  !-----------------------------------------------------------------------------

  !-----------------------------------------------------------------------------
  ! Begin execution part
  !-----------------------------------------------------------------------------

    fn_val = ishft(y, -11)

    ! -- Last executable statement --

    return

  !-----------------------------------------------------------------------------
  ! End execution part
  !-----------------------------------------------------------------------------

  !-----------------------------------------------------------------------------
  ! Begin subprogram part
  !-----------------------------------------------------------------------------

  !-----------------------------------------------------------------------------
  ! End subprogram part
  !-----------------------------------------------------------------------------

  end function tshftu

  function tshfts(y) result (fn_val)

  !-----------------------------------------------------------------------------
  ! Begin specification part
  !-----------------------------------------------------------------------------

    ! -- Use association --

    use calc_type, only: k_i4

    ! -- Null mapping --

    implicit none

    ! -- Dummy argument declarations --

    integer (kind = k_i4), intent (in) :: y
    integer (kind = k_i4)              :: fn_val

    ! -- Local declarations --

  !-----------------------------------------------------------------------------
  ! End specification part
  !-----------------------------------------------------------------------------

  !-----------------------------------------------------------------------------
  ! Begin execution part
  !-----------------------------------------------------------------------------

    fn_val = ishft(y, 7)

    ! -- Last executable statement --

    return

  !-----------------------------------------------------------------------------
  ! End execution part
  !-----------------------------------------------------------------------------

  !-----------------------------------------------------------------------------
  ! Begin subprogram part
  !-----------------------------------------------------------------------------

  !-----------------------------------------------------------------------------
  ! End subprogram part
  !-----------------------------------------------------------------------------

  end function tshfts

  function tshftt(y) result (fn_val)

  !-----------------------------------------------------------------------------
  ! Begin specification part
  !-----------------------------------------------------------------------------

    ! -- Use association --

    use calc_type, only: k_i4

    ! -- Null mapping --

    implicit none

    ! -- Dummy argument declarations --

    integer (kind = k_i4), intent (in) :: y
    integer (kind = k_i4)              :: fn_val

    ! -- Local declarations --

  !-----------------------------------------------------------------------------
  ! End specification part
  !-----------------------------------------------------------------------------

  !-----------------------------------------------------------------------------
  ! Begin execution part
  !-----------------------------------------------------------------------------

    fn_val = ishft(y, 15)

    ! -- Last executable statement --

    return

  !-----------------------------------------------------------------------------
  ! End execution part
  !-----------------------------------------------------------------------------

  !-----------------------------------------------------------------------------
  ! Begin subprogram part
  !-----------------------------------------------------------------------------

  !-----------------------------------------------------------------------------
  ! End subprogram part
  !-----------------------------------------------------------------------------

  end function tshftt

  function tshftl(y) result (fn_val)

  !-----------------------------------------------------------------------------
  ! Begin specification part
  !-----------------------------------------------------------------------------

    ! -- Use association --

    use calc_type, only: k_i4

    ! -- Null mapping --

    implicit none

    ! -- Dummy argument declarations --

    integer (kind = k_i4), intent (in) :: y
    integer (kind = k_i4)              :: fn_val

    ! -- Local declarations --

  !-----------------------------------------------------------------------------
  ! End specification part
  !-----------------------------------------------------------------------------

  !-----------------------------------------------------------------------------
  ! Begin execution part
  !-----------------------------------------------------------------------------

    fn_val = ishft(y, -18)

    ! -- Last executable statement --

    return

  !-----------------------------------------------------------------------------
  ! End execution part
  !-----------------------------------------------------------------------------

  !-----------------------------------------------------------------------------
  ! Begin subprogram part
  !-----------------------------------------------------------------------------

  !-----------------------------------------------------------------------------
  ! End subprogram part
  !-----------------------------------------------------------------------------

  end function tshftl

!-------------------------------------------------------------------------------
! End subprogram part
!-------------------------------------------------------------------------------

end module calc_rand_mt19937
