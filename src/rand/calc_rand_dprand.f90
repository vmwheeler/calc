module calc_rand_dprand

!-------------------------------------------------------------------------------
! This file is part of the Computational Algorithm Library Collection (CALC).
!
! Copyright 2012 The CALC Team
! Copyright 1999 Alan Miller
! Copyright 1992 The University of Cambridge
! Copyright 1992 N.M. Maclaren
!
! CALC is free software: you can redistribute it and/or modify
! it under the terms of the GNU Lesser General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! CALC is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU Lesser General Public License for more details.
!
! You should have received a copy of the GNU Lesser General Public License
! along with CALC.  If not, see <http://www.gnu.org/licenses/>.
!-------------------------------------------------------------------------------

!-------------------------------------------------------------------------------
! Begin specification part
!-------------------------------------------------------------------------------

  ! -- Use association --

  use calc_type, only: k_i4, k_r8

  ! -- Null mapping --

  implicit none

  ! -- Accessibility --

  private
  public :: init_dprand
  public :: dprand

  ! -- Derived type definitions --

  ! -- Type declarations --

  real    (kind = k_r8), dimension(100), save, private :: poly
  real    (kind = k_r8),                 save, private :: other, offset
  integer (kind = k_i4),                 save, private :: index

  ! -- Interface blocks --

  interface init_dprand
    module procedure init_dprand
  end interface init_dprand

  interface dprand
    module procedure dprand
  end interface dprand

!-------------------------------------------------------------------------------
! End specification part
!-------------------------------------------------------------------------------

!-------------------------------------------------------------------------------
! Begin subprogram part
!-------------------------------------------------------------------------------

contains

  subroutine init_dprand(iseed)

  !-----------------------------------------------------------------------------
  ! Begin specification part
  !-----------------------------------------------------------------------------

    ! -- Use association --

    use calc_type, only: k_i4, k_lg, k_r8

    ! -- Null mapping --

    implicit none

    ! -- Dummy argument declarations --

    integer (kind = k_i4), intent (in) :: iseed
    real    (kind = k_r8)              :: x

    ! -- Local declarations --

    real    (kind = k_r8), parameter   :: c_xmod = 1000009711.0_k_r8
    real    (kind = k_r8), parameter   :: c_ymod = 33554432.0_k_r8
    integer (kind = k_i4)              :: i, ix, iy, iz
    logical (kind = k_lg), save        :: initial = .true.

  !-----------------------------------------------------------------------------
  ! End specification part
  !-----------------------------------------------------------------------------

  !-----------------------------------------------------------------------------
  ! Begin execution part
  !-----------------------------------------------------------------------------

    ! iseed should be set to an integer between 0 and 9999 inclusive;
    ! a value of 0 will initialise the generator only if it has not
    ! already been done.

    if (initial .eqv. .true. .or. iseed /= 0) then
      initial = .false.
    else
      return
    end if

    ! index must be initialised to an integer between 1 and 101
    ! inclusive, poly(1...n) to integers between 0 and 1000009710
    ! inclusive (not all 0), and other to a non-negative proper fraction
    ! with denominator 33554432. It uses the Wichmann-Hill generator to
    ! do this.

    ix = mod(abs(iseed), 10000) + 1
    iy = 2 * ix + 1
    iz = 3 * ix + 1
    do i = -10, 101
      if (i >= 1) poly(i) = aint(c_xmod * x)
      ix = mod(171 * ix, 30269)
      iy = mod(172 * iy, 30307)
      iz = mod(170 * iz, 30323)
      x  = mod(  real(ix, kind = k_r8) / 30269.0_k_r8            &
               + real(iy, kind = k_r8) / 30307.0_k_r8            &
               + real(iz, kind = k_r8) / 30323.0_k_r8, 1.0_k_r8)
    end do
    other  = aint(c_ymod * x) / c_ymod
    offset = 1.0_k_r8         / c_ymod
    index  = 1

    ! -- Last executable statement --

    return

  !-----------------------------------------------------------------------------
  ! End execution part
  !-----------------------------------------------------------------------------

  !-----------------------------------------------------------------------------
  ! Begin subprogram part
  !-----------------------------------------------------------------------------

  !-----------------------------------------------------------------------------
  ! End subprogram part
  !-----------------------------------------------------------------------------

  end subroutine init_dprand

  function dprand() result (rn)

  !-----------------------------------------------------------------------------
  ! Begin specification part
  !-----------------------------------------------------------------------------

    ! -- Use association --

    use calc_type, only: k_i4, k_lg, k_r8

    ! -- Null mapping --

    implicit none

    ! -- Dummy argument declarations --

    real(kind = k_r8) :: rn

    ! -- Local declarations --

    real   (kind=k_r8)            :: x, y
    real   (kind=k_r8), parameter :: c_xmod  = 1000009711.0_k_r8
    real   (kind=k_r8), parameter :: c_xmod2 = 2000019422.0_k_r8
    real   (kind=k_r8), parameter :: c_xmod4 = 4000038844.0_k_r8
    real   (kind=k_r8), parameter :: tiny    = 1.0e-17_k_r8
    integer(kind=k_i4)            :: n
    logical(kind=k_lg), save      :: initial = .true.

  !-----------------------------------------------------------------------------
  ! End specification part
  !-----------------------------------------------------------------------------

  !-----------------------------------------------------------------------------
  ! Begin execution part
  !-----------------------------------------------------------------------------

    ! This returns a uniform (0,1) random number, with extremely good
    ! uniformity properties.  It assumes that double precision provides
    ! at least 33 bits of accuracy, and uses a power of two base.

    if (initial .eqv. .true.) then
      call init_dprand(0)
      initial = .false.
    end if

    ! See [Knuth] for why this implements the algorithm described in
    ! the paper.  Note that this code is tuned for machines with fast
    ! double precision, but slow multiply and divide; many, many other
    ! options are possible.

    n = index - 64
    if (n <= 0) n = n + 101
    x = poly(index) + poly(index)
    x = c_xmod4 - poly(n) - poly(n) - x - x - poly(index)
    if (x < 0.0_k_r8) then
      if (x < -c_xmod)  x = x + c_xmod2
      if (x < 0.0_k_r8) x = x + c_xmod
    else
      if (x >= c_xmod2) then
        x = x - c_xmod2
        if (x >= c_xmod) x = x - c_xmod
      end if
      if (x >= c_xmod) x = x - c_xmod
    end if
    poly(index) = x
    index = index + 1
    if (index > 101) index = index - 101

    ! Add in the second generator modulo 1, and force to be non-zero.
    ! The restricted ranges largely cancel themcalcves out.

    do
      y     = 37.0_k_r8 * other + offset
      other = y - aint(y)
      if (other /= 0.0_k_r8) exit
    end do
    x = x / c_xmod + other
    if (x >= 1.0_k_r8) x = x - 1.0_k_r8
    rn = x + tiny

    ! -- Last executable statement --

    return

  !-----------------------------------------------------------------------------
  ! End execution part
  !-----------------------------------------------------------------------------

  !-----------------------------------------------------------------------------
  ! Begin subprogram part
  !-----------------------------------------------------------------------------

  !-----------------------------------------------------------------------------
  ! End subprogram part
  !-----------------------------------------------------------------------------

   end function dprand

!-------------------------------------------------------------------------------
! End subprogram part
!-------------------------------------------------------------------------------

end module calc_rand_dprand
