module calc_rand_ranarray

!-------------------------------------------------------------------------------
! This file is part of the Computational Algorithm Library Collection (CALC).
!
! Copyright 2012 The CALC Team
!
! CALC is free software: you can redistribute it and/or modify
! it under the terms of the GNU Lesser General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! CALC is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU Lesser General Public License for more details.
!
! You should have received a copy of the GNU Lesser General Public License
! along with CALC.  If not, see <http://www.gnu.org/licenses/>.
!-------------------------------------------------------------------------------

!-------------------------------------------------------------------------------
! Begin specification part
!-------------------------------------------------------------------------------

  ! -- Use association --
 
  use calc_type, only: k_i4, k_r8

  ! -- Null mapping --

  implicit none

  ! -- Accessibility --

  private
  public :: init_ranarray_int
  public :: init_ranarray_dp
  public :: ranarray_int
  public :: ranarray_dp

  ! -- Derived type definitions --

  ! -- Type declarations --

  integer(kind=k_i4), parameter,     private       :: kk  = 100
  integer(kind=k_i4), parameter,     private       :: kkk = kk + kk - 1
  integer(kind=k_i4), parameter,     private       :: ll  = 37
  integer(kind=k_i4), parameter,     private       :: mm = 2**30
  integer(kind=k_i4), parameter,     private       :: tt  = 70
  integer(kind=k_i4), dimension(kk), private, save :: ran_x_int(kk)
  real   (kind=k_r8), dimension(kk), private, save :: ran_x_dp(kk)

  ! -- Interface blocks --

  interface init_ranarray_int
    module procedure init_ranarray_int
  end interface init_ranarray_int

  interface init_ranarray_dp
    module procedure init_ranarray_dp
  end interface init_ranarray_dp

  interface ranarray_int
    module procedure ranarray_int
  end interface ranarray_int

  interface ranarray_dp
    module procedure ranarray_dp
  end interface ranarray_dp

!-------------------------------------------------------------------------------
! End specification part
!-------------------------------------------------------------------------------

!-------------------------------------------------------------------------------
! Begin subprogram part
!-------------------------------------------------------------------------------

contains

  subroutine init_ranarray_int(seed)

  !-----------------------------------------------------------------------------
  ! Begin specification part
  !-----------------------------------------------------------------------------

    ! -- Use association --
 
    use calc_type, only: k_i4

    ! -- Null mapping --

    implicit none

    ! -- Dummy argument declarations --

    integer(kind=k_i4), intent(in) :: seed

    ! -- Local declarations --

    integer(kind=k_i4)                 :: j, ss, sseed, t
    integer(kind=k_i4), dimension(kkk) :: x

  !-----------------------------------------------------------------------------
  ! End specification part
  !----------------------------------------------------------------------------- 
  !-----------------------------------------------------------------------------
  ! Begin execution part
  !----------------------------------------------------------------------------- 
    if (seed < 0) then
      sseed = mm - 1 - mod(-1 - seed, mm)
    else
      sseed = mod(seed, mm)
    end if
    ss = sseed - mod(sseed, 2) + 2
    do j = 1, kk
      x(j) = ss
      ss   = ss + ss
      if (ss >= mm) ss = ss - mm + 2
    end do
    x(2) = x(2) + 1
    ss   = sseed
    t    = tt - 1
    do
      do j = kk, 2 , -1
        x(j+j-1) = x(j)
        x(j+j-2) = 0
      end do
      do j = kkk, kk + 1, -1
        x(j-(kk-ll)) = x(j-(kk-ll)) - x(j)
        if (x(j-(kk-ll)) < 0) x(j-(kk-ll)) = x(j-(kk-ll)) + mm
        x(j-kk) = x(j-kk) - x(j)
        if (x(j-kk) < 0) x(j-kk) = x(j-kk) + mm
      end do 
      if (mod(ss, 2) == 1) then
        do j = kk, 1, -1
          x(j+1) = x(j)
        end do
        x(1)    = x(kk+1)
        x(ll+1) = x(ll+1) - x(kk+1)
        if (x(ll+1) < 0) x(ll+1) = x(ll+1) + mm
      end if
      if (ss /= 0) then
        ss = ss / 2
      else
        t = t - 1
      end if
      if (t <= 0) exit
    end do
    do j = 1, ll
     ran_x_int(j+kk-ll) = x(j)
    end do
    do j = ll + 1, kk
      ran_x_int(j-ll) = x(j)
    end do
    do j = 1, 10
      call ranarray_int(x, kkk)
    end do

    ! -- Last executable statement --

    return

  !-----------------------------------------------------------------------------
  ! End execution part
  !----------------------------------------------------------------------------- 
  !-----------------------------------------------------------------------------
  ! Begin subprogram part
  !----------------------------------------------------------------------------- 
  !-----------------------------------------------------------------------------
  ! End subprogram part
  !----------------------------------------------------------------------------- 
  end subroutine init_ranarray_int

  subroutine init_ranarray_dp(seed)

  !-----------------------------------------------------------------------------
  ! Begin specification part
  !-----------------------------------------------------------------------------

    ! -- Use association --

    use calc_type, only: k_i4, k_r8

    ! -- Null mapping --

    implicit none

    ! -- Dummy argument declarations --

    integer(kind=k_i4), intent(in)     :: seed

    ! -- Local declarations --

    real   (kind=k_r8), parameter      :: ulp = 1.0_k_r8 / (2.0_k_r8**52)
    integer(kind=k_i4)                 :: j, s, sseed, t
    real   (kind=k_r8)                 :: ss, v
    real   (kind=k_r8), dimension(kkk) :: x

  !-----------------------------------------------------------------------------
  ! End specification part
  !-----------------------------------------------------------------------------

  !-----------------------------------------------------------------------------
  ! Begin execution part
  !-----------------------------------------------------------------------------

    if (seed < 0) then
      sseed = mm - 1 - mod(-1 - seed, mm)
    else
      sseed = mod(seed, mm)
    end if
    ss = 2.0_k_r8 * ulp * (sseed + 2)
    do j = 1, kk
      x(j) = ss
      ss   = ss + ss
      if (ss >= 1.0_k_r8) ss = ss - 1.0_k_r8 + 2 * ulp
    end do
    x(2) = x(2) + ulp
    s    = sseed
    t    = tt - 1
    do
      do j = kk, 2, -1
        x(j+j-1) = x(j)
        x(j+j-2) = 0.0_k_r8
      end do
      do j = kkk, kk + 1, -1
        v            = x(j-(kk-ll)) + x(j)
        x(j-(kk-ll)) = v - int(v)
        v            = x(j-kk) + x(j)
        x(j - kk)    = v - int(v)
      end do
      if (mod(s, 2) == 1) then
        do j = kk, 1, -1
          x(j + 1) = x(j)
        end do
        x(1)    = x(kk+1)
        v       = x(ll+1) + x(kk+1)
        x(ll+1) = v - int(v)
      end if
      if (s /= 0) then
        s = s/2
      else
        t = t - 1
      end if
      if (t <= 0) exit
    end do
    ran_x_dp(1+kk-ll : kk) = x(1 : ll)
    ran_x_dp(1 : kk-ll)    = x(ll+1 : kk)
    do j = 1, 10
      call ranarray_dp(x, kkk)
    end do

    ! -- Last executable statement --

    return

  !-----------------------------------------------------------------------------
  ! End execution part
  !-----------------------------------------------------------------------------

  !-----------------------------------------------------------------------------
  ! Begin subprogram part
  !-----------------------------------------------------------------------------

  !-----------------------------------------------------------------------------
  ! End subprogram part
  !-----------------------------------------------------------------------------

  end subroutine init_ranarray_dp

  subroutine ranarray_int(aa, n)

  !-----------------------------------------------------------------------------
  ! Begin specification part
  !-----------------------------------------------------------------------------

    ! -- Use association --

    use calc_type, only: k_i4

    ! -- Null mapping --

    implicit none

    ! -- Dummy argument declarations --

    integer(kind=k_i4),               intent(in)  :: n
    integer(kind=k_i4), dimension(:), intent(out) :: aa

    ! -- Local declarations --

    integer(kind=k_i4) :: j

  !-----------------------------------------------------------------------------
  ! End specification part
  !-----------------------------------------------------------------------------

  !-----------------------------------------------------------------------------
  ! Begin execution part
  !-----------------------------------------------------------------------------

    do j = 1, kk
      aa(j) = ran_x_int(j)
    end do
    do j = kk + 1, n
      aa(j) = aa(j-kk) - aa(j-ll)
      if (aa(j) < 0) aa(j) = aa(j) + mm
    end do
    do j = 1, ll
      ran_x_int(j) = aa(n+j-kk) - aa(n+j-ll)
      if (ran_x_int(j) < 0) ran_x_int(j) = ran_x_int(j) + mm
    end do
    do j = ll + 1, kk
      ran_x_int(j) = aa(n+j-kk) - ran_x_int(j-ll)
      if (ran_x_int(j) < 0) ran_x_int(j) = ran_x_int(j) + mm
    end do

    ! -- Last executable statement --

    return

  !-----------------------------------------------------------------------------
  ! End execution part
  !-----------------------------------------------------------------------------

  !-----------------------------------------------------------------------------
  ! Begin subprogram part
  !-----------------------------------------------------------------------------

  !-----------------------------------------------------------------------------
  ! End subprogram part
  !-----------------------------------------------------------------------------

  end subroutine ranarray_int
 
  subroutine ranarray_dp(aa, n)

  !-----------------------------------------------------------------------------
  ! Begin specification part
  !-----------------------------------------------------------------------------

    ! -- Use association --

    use calc_type, only: k_i4, k_r8

    ! -- Null mapping --

    implicit none

    ! -- Dummy argument declarations --

    integer(kind=k_i4),               intent(in)  :: n
    real   (kind=k_r8), dimension(:), intent(out) :: aa

    ! -- Local declarations --

    real   (kind=k_r8)                            :: y
    integer(kind=k_i4)                            :: j

  !-----------------------------------------------------------------------------
  ! End specification part
  !-----------------------------------------------------------------------------

  !-----------------------------------------------------------------------------
  ! Begin execution part
  !-----------------------------------------------------------------------------

    do j = 1, kk
      aa(j) = ran_x_dp(j)
    end do
    do j = kk + 1, n
      y     = aa(j-kk) + aa(j-ll)
      aa(j) = y - int(y)
    end do
    do j = 1, ll
      y           = aa(n+j-kk) + aa(n+j-ll)
      ran_x_dp(j) = y - int(y)
    end do
    do j = ll + 1, kk
      y           = aa(n+j-kk) + ran_x_dp(j-ll)
      ran_x_dp(j) = y - int(y)
    end do

    ! -- Last executable statement --

    return

  !-----------------------------------------------------------------------------
  ! End execution part
  !-----------------------------------------------------------------------------

  !-----------------------------------------------------------------------------
  ! Begin subprogram part
  !-----------------------------------------------------------------------------

  !-----------------------------------------------------------------------------
  ! End subprogram part
  !-----------------------------------------------------------------------------

  end subroutine ranarray_dp

!-------------------------------------------------------------------------------
! End subprogram part
!-------------------------------------------------------------------------------

end module calc_rand_ranarray
