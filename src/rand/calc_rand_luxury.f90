module calc_rand_luxury

!-------------------------------------------------------------------------------
! This file is part of the Computational Algorithm Library Collection (CALC).
!
! Copyright 2012 The CALC Team
! Copyright 1993 F. James
! Copyright 1993 M. Luscher
! Copyright 1991 F. James
!
! CALC is free software: you can redistribute it and/or modify
! it under the terms of the GNU Lesser General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! CALC is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU Lesser General Public License for more details.
!
! You should have received a copy of the GNU Lesser General Public License
! along with CALC.  If not, see <http://www.gnu.org/licenses/>.
!-------------------------------------------------------------------------------

!     Subtract-and-borrow random number generator proposed by
!     Marsaglia and Zaman, implemented by F. James with the name
!     RCARRY in 1991, and later improved by Martin Luescher
!     in 1993 to produce "Luxury Pseudorandom Numbers".
!     Fortran 77 coded by F. James, 1993

!  References:
!  M. Luscher, Computer Physics Communications  79 (1994) 100
!  F. James, Computer Physics Communications 79 (1994) 111

!     This Fortran 90 version is by Alan Miller (alan @ mel.dms.csiro.au)
!     Latest revision - 11 September 1995

!   LUXURY LEVELS.
!   ------ ------      The available luxury levels are:

!  level 0  (p=24): equivalent to the original RCARRY of Marsaglia
!           and Zaman, very long period, but fails many tests.
!  level 1  (p=48): considerable improvement in quality over level 0,
!           now passes the gap test, but still fails spectral test.
!  level 2  (p=97): passes all known tests, but theoretically still
!           defective.
!  level 3  (p=223): DEFAULT VALUE.  Any theoretically possible
!           correlations have very small chance of being observed.
!  level 4  (p=389): highest possible luxury, all 24 bits chaotic.

!!!! ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!!!!  Calling sequences for RANLUX:                                  ++
!!!!      CALCL RANLUX (RVEC, LEN)   returns a vector RVEC of LEN     ++
!!!!                   32-bit random floating point numbers between  ++
!!!!                   zero (not included) and one (also not incl.). ++
!!!!      CALCL RLUXGO(LUX,INT,K1,K2) initializes the generator from  ++
!!!!               one 32-bit integer INT and sets Luxury Level LUX  ++
!!!!               which is integer between zero and MAXLEV, or if   ++
!!!!               LUX .GT. 24, it sets p=LUX directly.  K1 and K2   ++
!!!!               should be set to zero unless restarting at a break++
!!!!               point given by output of RLUXAT (see RLUXAT).     ++
!!!!      CALCL RLUXAT(LUX,INT,K1,K2) gets the values of four integers++
!!!!               which can be used to restart the RANLUX generator ++
!!!!               at the current point by calling RLUXGO.  K1 and K2++
!!!!               specify how many numbers were generated since the ++
!!!!               initialization with LUX and INT.  The restarting  ++
!!!!               skips over  K1+K2*E9   numbers, so it can be long.++
!!!!   A more efficient but less convenient way of restarting is by: ++
!!!!      CALCL RLUXIN(ISVEC)    restarts the generator from vector   ++
!!!!                   ISVEC of 25 32-bit integers (see RLUXUT)      ++
!!!!      CALCL RLUXUT(ISVEC)    outputs the current values of the 25 ++
!!!!                 32-bit integer seeds, to be used for restarting ++
!!!!      ISVEC must be dimensioned 25 in the calling program        ++
!!!! ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

!-------------------------------------------------------------------------------
! Begin specification part
!-------------------------------------------------------------------------------

  ! -- Use association --

  use calc_type, only: k_i4, k_lg, k_r4

  ! -- Null mapping --

  implicit none

  ! -- Accessibility --

  private
  public :: notyet, i24, j24, carry, seeds, twom24, twom12, luxlev
  public :: nskip, ndskip, in24, next, kount, mkount, inseed

  ! -- Derived type definitions --

  ! -- Type declarations --

  integer(kind=k_i4)            :: isdext(25)
  integer(kind=k_i4), parameter :: maxlev = 4, lxdflt = 3, jsdflt = 314159265
  integer(kind=k_i4)            :: ndskip(0:maxlev) = (/ 0, 24, 73, 199, 365 /)
  integer(kind=k_i4)            :: igiga = 1000000000, i24 = 24, j24 = 10
  real   (kind=k_r4), parameter :: twop12 = 4096.0_k_r4
  integer(kind=k_i4), parameter :: itwo24 = 2**24, icons = 2147483563
  integer(kind=k_i4), save      :: next(24), luxlev = lxdflt
  integer(kind=k_i4), save      :: nskip, inseed, jseed
  logical(kind=k_lg), save      :: notyet = .true.
  integer(kind=k_i4), save      :: in24 = 0, kount = 0, mkount = 0
  real   (kind=k_r4), save      :: seeds(24), carry = 0., twom24, twom12

!                            default
!  Luxury Level     0   1   2  *3*    4
!    ndskip        /0, 24, 73, 199, 365/
! Corresponds to p=24  48  97  223  389
!     time factor   1   2   3    6   10   on slow workstation
!                   1 1.5   2    3    5   on fast mainframe
!                   1 1.5 2.5    5  8.5   on PC using LF90

!-------------------------------------------------------------------------------
! End specification part
!-------------------------------------------------------------------------------

!-------------------------------------------------------------------------------
! Begin subprogram part
!-------------------------------------------------------------------------------

contains

  subroutine ranlux(rvec, lenv)

  !-----------------------------------------------------------------------------
  ! Begin specification part
  !-----------------------------------------------------------------------------

    ! -- Use association --

    ! -- Null mapping --

    implicit none

    ! -- Dummy argument declarations --

    integer(kind=k_i4), intent(in)  :: lenv
    real   (kind=k_r4), intent(out) :: rvec(lenv)

    !     Local declarations

    integer(kind=k_i4) :: i, k, lp, ivec, iseeds(24), isk
    real   (kind=k_r4) :: uni

  !-----------------------------------------------------------------------------
  ! End specification part
  !-----------------------------------------------------------------------------

  !-----------------------------------------------------------------------------
  ! Begin execution part
  !-----------------------------------------------------------------------------

    !  notyet is .true. if no initialization has been performed yet.
    !  Default Initialization by multiplicative congruential

    if (notyet .eqv. .true.) then
      notyet = .false.
      jseed  = jsdflt
      inseed = jseed
      write (6, '(a, i12)') ' ranlux default initialization: ', jseed
      luxlev = lxdflt
      nskip  = ndskip(luxlev)
      lp     = nskip + 24
      in24   = 0
      kount  = 0
      mkount = 0
      write (6, '(a, i2, a, i4)') ' ranlux default luxury level =  ', &
                                  luxlev, ' p =', lp
      twom24 = 1.
      do i = 1, 24
        twom24 = twom24 * 0.5
        k      = jseed / 53668
        jseed  = 40014 * (jseed - k * 53668) - k * 12211
        if (jseed < 0) jseed = jseed + icons
        iseeds(i) = mod(jseed, itwo24)
      end do
      twom12 = twom24 * 4096.
      do i = 1, 24
        seeds(i) = real(iseeds(i)) * twom24
        next(i)  = i - 1
      end do
      next(1) = 24
      i24     = 24
      j24     = 10
      carry   = 0.
      if (seeds(24) == 0.) carry = twom24
    end if

    ! The Generator proper: "Subtract-with-borrow",
    ! as proposed by Marsaglia and Zaman,
    ! Florida State University, March, 1989

    do ivec = 1, lenv
      uni = seeds(j24) - seeds(i24) - carry
      if (uni < 0.) then
        uni   = uni + 1.0
        carry = twom24
      else
        carry = 0.
      end if
      seeds(i24) = uni
      i24        = next(i24)
      j24        = next(j24)
      rvec(ivec) = uni

      ! Small numbers (with less than 12 "significant" bits) are "padded".

      if (uni < twom12) then
        rvec(ivec) = rvec(ivec) + twom24 * seeds(j24)

      ! Zero is forbidden in case someone takes a logarithm.

        if (rvec(ivec) == 0.) rvec(ivec) = twom24 * twom24
      end if

      ! Skipping to luxury. As proposed by Martin Luscher.

      in24 = in24 + 1
      if (in24 == 24) then
        in24  = 0
        kount = kount + nskip
        do isk = 1, nskip
          uni = seeds(j24) - seeds(i24) - carry
          if (uni < 0.) then
            uni   = uni + 1.0
            carry = twom24
          else
            carry = 0.
          end if
          seeds(i24) = uni
          i24        = next(i24)
          j24        = next(j24)
        end do
      end if
    end do
    kount = kount + lenv
    if (kount >= igiga) then
      mkount = mkount + 1
      kount  = kount - igiga
    end if

    ! -- Last executable statement --

    return

  !-----------------------------------------------------------------------------
  ! End execution part
  !----------------------------------------------------------------------------- 
  !-----------------------------------------------------------------------------
  ! Begin subprogram part
  !----------------------------------------------------------------------------- 
  !-----------------------------------------------------------------------------
  ! End subprogram part
  !-----------------------------------------------------------------------------

  end subroutine ranlux

  subroutine rluxin()

  ! Subroutine to input and float integer seeds from previous run
  ! the following IF BLOCK added by Phillip Helbig, based on conversation
  ! with Fred James; an equivalent correction has been published by James.

  !-----------------------------------------------------------------------------
  ! Begin specification part
  !-----------------------------------------------------------------------------

    ! -- Use association --

    ! -- Null mapping --

    implicit none

    ! -- Dummy argument declarations --

    ! -- Local declarations --

    integer :: i, isd

  !-----------------------------------------------------------------------------
  ! End specification part
  !-----------------------------------------------------------------------------

  !-----------------------------------------------------------------------------
  ! Begin execution part
  !-----------------------------------------------------------------------------

    if (notyet .eqv. .true.) then
      write (6,'(a)') 'Proper results ONLY with initialisation from 25 ',  &
                      'integers obtained with rluxut'
      notyet = .false.
    end if
    twom24 = 1.
    do i = 1, 24
      next(i) = i - 1
      twom24  = twom24 * 0.5
    end do
    next(1) = 24
    twom12  = twom24 * 4096.
    write (6, '(a)') 'Full initialization of ranlux with 25 integers:'
    write (6, '(5x, 5i12)') isdext
    do i = 1, 24
      seeds(i) = real(isdext(i)) * twom24
    end do
    carry = 0.
    if (isdext(25) < 0) carry = twom24
    isd    = abs(isdext(25))
    i24    = mod(isd, 100)
    isd    = isd / 100
    j24    = mod(isd, 100)
    isd    = isd / 100
    in24   = mod(isd, 100)
    isd    = isd / 100
    luxlev = isd
    if (luxlev <= maxlev) then
      nskip = ndskip(luxlev)
      write (6, '(A, I2)') 'ranlux luxury level set by rluxin to:', luxlev
    else if (luxlev >= 24) then
      nskip = luxlev - 24
      write (6, '(A, I5)') 'ranlux p-values set by rluxin to:', luxlev
    else
      nskip = ndskip(maxlev)
      write (6, '(A, I5)') 'ranlux illegal luxury rluxin:', luxlev
      luxlev = maxlev
    end if
    inseed = -1

    ! -- Last executable statement --

    return

  !-----------------------------------------------------------------------------
  ! End execution part
  !-----------------------------------------------------------------------------

  !-----------------------------------------------------------------------------
  ! Begin subprogram part
  !----------------------------------------------------------------------------- 
  !-----------------------------------------------------------------------------
  ! End subprogram part
  !-----------------------------------------------------------------------------

  end subroutine rluxin

  subroutine rluxut

  ! Subroutine to ouput seeds as integers

  !-----------------------------------------------------------------------------
  ! Begin specification part
  !-----------------------------------------------------------------------------

    ! -- Use association --

    ! -- Null mapping --

    implicit none

    ! -- Dummy argument declarations --

    ! -- Local declarations --

    integer(kind=k_i4) :: i

  !-----------------------------------------------------------------------------
  ! End specification part
  !-----------------------------------------------------------------------------

  !-----------------------------------------------------------------------------
  ! Begin execution part
  !-----------------------------------------------------------------------------

    do i = 1, 24
      isdext(i) = int(seeds(i) * twop12 * twop12)
    end do
    isdext(25) = i24 + 100 * j24 + 10000 * in24 + 1000000 * luxlev
    if (carry > 0.) isdext(25) = -isdext(25)

    ! -- Last executable statement --

    return

  !-----------------------------------------------------------------------------
  ! End execution part
  !-----------------------------------------------------------------------------

  !-----------------------------------------------------------------------------
  ! Begin subprogram part
  !----------------------------------------------------------------------------- 
  !-----------------------------------------------------------------------------
  ! End subprogram part
  !-----------------------------------------------------------------------------

  end subroutine rluxut

  subroutine rluxat(lout, inout, k1, k2)

  ! Subroutine to output the "convenient" restart point

  !-----------------------------------------------------------------------------
  ! Begin specification part
  !-----------------------------------------------------------------------------

    ! -- Use association --

    ! -- Null mapping --

    implicit none

    ! -- Dummy argument declarations --

    integer(kind=k_i4), intent(out) :: lout, inout, k1, k2

    ! -- Local declarations --

  !-----------------------------------------------------------------------------
  ! End specification part
  !-----------------------------------------------------------------------------

  !-----------------------------------------------------------------------------
  ! Begin execution part
  !-----------------------------------------------------------------------------

    lout  = luxlev
    inout = inseed
    k1    = kount
    k2    = mkount

    ! -- Last executable statement --

    return

  !-----------------------------------------------------------------------------
  ! End execution part
  !-----------------------------------------------------------------------------

  !-----------------------------------------------------------------------------
  ! Begin subprogram part
  !----------------------------------------------------------------------------- 
  !-----------------------------------------------------------------------------
  ! End subprogram part
  !-----------------------------------------------------------------------------

  end subroutine rluxat

  subroutine rluxgo(lux, ins, k1, k2)

  ! Subroutine to initialize from one or three integers

  !-----------------------------------------------------------------------------
  ! Begin specification part
  !-----------------------------------------------------------------------------

    ! -- Use association --

    use calc_type, only: k_i4, k_r4

    ! -- Null mapping --

    implicit none

    ! -- Dummy argument declarations --

    integer(kind=k_i4), intent (in) :: lux, ins, k1, k2

    ! -- Local declarations --

    integer(kind=k_i4)                :: ilx, i, iouter, isk, inner
    integer(kind=k_i4)                :: izip, izip2, k
    integer(kind=k_i4), dimension(24) :: iseeds
    real   (kind=k_r4)                :: uni

  !-----------------------------------------------------------------------------
  ! End specification part
  !-----------------------------------------------------------------------------

  !-----------------------------------------------------------------------------
  ! Begin execution part
  !-----------------------------------------------------------------------------

    if (lux < 0) then
      luxlev = lxdflt
    else if (lux <= maxlev) then
      luxlev = lux
    else if (lux < 24 .or. lux > 2000) then
      luxlev = maxlev
      write (6, '(A, I7)') 'ranlux illegal luxury rluxgo:', lux
    else
      luxlev = lux
      do ilx = 0, maxlev
        if (lux == ndskip(ilx) + 24) luxlev = ilx
      end do
    end if
    if (luxlev <= maxlev) then
      nskip = ndskip(luxlev)
      write (6, '(A, I2, A, I4)') 'ranlux luxury level set by rluxgo:', &
                                  luxlev,  'p = ', nskip + 24
    else
      nskip = luxlev - 24
      write (6, '(A, I5)') 'ranlux p-value set by rluxgo to:', luxlev
    end if
    in24 = 0
    IF (ins.LT.0) WRITE (6,'(A)') &
              ' Illegal initialization by RLUXGO, negative input seed'
IF (ins.GT.0) THEN
  jseed = ins
  WRITE (6,'(A,3I12)') ' RANLUX INITIALIZED BY RLUXGO FROM SEEDS', jseed, k1, k2
ELSE
  jseed = jsdflt
  WRITE (6,'(A)') ' RANLUX INITIALIZED BY RLUXGO FROM DEFAULT SEED'
END IF
inseed = jseed
notyet = .false.
twom24 = 1.
DO i = 1, 24
  twom24 = twom24 * 0.5
  k = jseed / 53668
  jseed = 40014 * (jseed-k*53668) - k * 12211
  IF (jseed.LT.0) jseed = jseed + icons
  iseeds(i) = MOD(jseed,itwo24)
END DO
twom12 = twom24 * 4096.
DO i = 1, 24
  seeds(i) = REAL(iseeds(i)) * twom24
  next(i) = i - 1
END DO
next(1) = 24
i24 = 24
j24 = 10
carry = 0.
IF (seeds(24).EQ.0.) carry = twom24
!        If restarting at a break point, skip K1 + IGIGA*K2
!        Note that this is the number of numbers delivered to
!        the user PLUS the number skipped (if luxury .GT. 0).
kount = k1
mkount = k2
IF (k1+k2.NE.0) THEN
  DO iouter = 1, k2 + 1
    inner = igiga
    IF (iouter.EQ.k2+1) inner = k1
    DO isk = 1, inner
      uni = seeds(j24) - seeds(i24) - carry
      IF (uni.LT.0.) THEN
        uni = uni + 1.0
        carry = twom24
      ELSE
        carry = 0.
      END IF
      seeds(i24) = uni
      i24 = next(i24)
      j24 = next(j24)
    END DO
  END DO
!         Get the right value of IN24 by direct calculation
  in24 = MOD(kount,nskip+24)
  IF (mkount.GT.0) THEN
    izip = MOD(igiga, nskip+24)
    izip2 = mkount * izip + in24
    in24 = MOD(izip2, nskip+24)
  END IF
!       Now IN24 had better be between zero and 23 inclusive
  IF (in24.GT.23) THEN
    WRITE (6,'(A/A,3I11,A,I5)') &
               '  Error in RESTARTING with RLUXGO:', '  The values', ins, &
               k1, k2, ' cannot occur at luxury level', luxlev
        in24 = 0
      END IF
    END IF

    ! -- Last executable statement --

    return

  !-----------------------------------------------------------------------------
  ! End execution part
  !-----------------------------------------------------------------------------

  !-----------------------------------------------------------------------------
  ! Begin subprogram part
  !----------------------------------------------------------------------------- 
  !-----------------------------------------------------------------------------
  ! End subprogram part
  !-----------------------------------------------------------------------------

  end subroutine rluxgo

!-------------------------------------------------------------------------------
! End subprogram part
!-------------------------------------------------------------------------------

end module calc_rand_luxury
