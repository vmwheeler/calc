module calc_rand_lfsr258

!-------------------------------------------------------------------------------
! This file is part of the Computational Algorithm Library Collection (CALC).
!
! Copyright 2012 The CALC Team
! Copyright 2001 Alan Miller
! Copyright 1999 P. L'Ecuyer
!
! CALC is free software: you can redistribute it and/or modify
! it under the terms of the GNU Lesser General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! CALC is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU Lesser General Public License for more details.
!
! You should have received a copy of the GNU Lesser General Public License
! along with CALC.  If not, see <http://www.gnu.org/licenses/>.
!-------------------------------------------------------------------------------

!-------------------------------------------------------------------------------
! Begin specification part
!-------------------------------------------------------------------------------

  ! -- Use association --

  use calc_type, only: k_i8, k_r8

  ! -- Null mapping --

  implicit none

  ! -- Accessibility --

  private
  public :: init_lfsr258
  public :: lfsr258

  ! -- Derived type definitions --

  ! -- Type declarations --

  integer (kind = k_i8), save, private :: s1 = 153587801,  s2 = -759022222
  integer (kind = k_i8), save, private :: s3 = 1288503317, s4 = -1718083407
  integer (kind = k_i8), save, private :: s5 = -123456789

  ! -- Interface blocks --

  interface init_lfsr258
    module procedure init_lfsr258
  end interface init_lfsr258

  interface lfsr258
    module procedure lfsr258
  end interface lfsr258

!-------------------------------------------------------------------------------
! End specification part
!-------------------------------------------------------------------------------

!-------------------------------------------------------------------------------
! Begin subprogram part
!-------------------------------------------------------------------------------

contains

  subroutine init_lfsr258(i1, i2, i3, i4, i5)

  !-----------------------------------------------------------------------------
  ! Begin specification part
  !-----------------------------------------------------------------------------

    ! -- Use association --

    use calc_type, only: k_i8

    ! -- Null mapping --

    implicit none

    ! -- Dummy argument declarations --

    integer(kind = k_i8), intent (in) :: i1, i2, i3, i4, i5

    ! -- Local declarations --

  !-----------------------------------------------------------------------------
  ! End specification part
  !-----------------------------------------------------------------------------

  !-----------------------------------------------------------------------------
  ! Begin execution part
  !-----------------------------------------------------------------------------

    s1 = i1
    s2 = i2
    s3 = i3
    s4 = i4
    s5 = i5
    if (iand(s1,      -2_k_i8) == 0) s1 = i1 - 8388607_k_i8
    if (iand(s2,    -512_k_i8) == 0) s2 = i2 - 8388607_k_i8
    if (iand(s3,   -4096_k_i8) == 0) s3 = i3 - 8388607_k_i8
    if (iand(s4, -131072_k_i8) == 0) s4 = i4 - 8388607_k_i8
    if (iand(s5,-8388608_k_i8) == 0) s5 = i5 - 8388607_k_i8

    ! -- Last executable statement --

    return

  !-----------------------------------------------------------------------------
  ! End execution part
  !-----------------------------------------------------------------------------

  !-----------------------------------------------------------------------------
  ! Begin subprogram part
  !-----------------------------------------------------------------------------

  !-----------------------------------------------------------------------------
  ! End subprogram part
  !-----------------------------------------------------------------------------

  end subroutine init_lfsr258

  function lfsr258() result (rn)

  !-----------------------------------------------------------------------------
  ! Begin specification part
  !-----------------------------------------------------------------------------

    ! -- Use association --

    use calc_type, only: k_i8, k_r8

    ! -- Null mapping --

    implicit none

    ! -- Dummy argument declarations --

    real(kind = k_r8) :: rn

    ! -- Local declarations --

    integer (kind = k_i8) :: b

  !-----------------------------------------------------------------------------
  ! End specification part
  !-----------------------------------------------------------------------------

  !-----------------------------------------------------------------------------
  ! Begin execution part
  !-----------------------------------------------------------------------------

    ! N.B. ishft(i,j) is a bitwise (non-circular) shift operation;
    ! to the left if j > 0, otherwise to the right.

    b  = ishft(ieor (ishft(s1,        1_k_i8),      s1), -53_k_i8)
    s1 = ieor (ishft(iand (s1,       -2_k_i8), 10_k_i8),        b)
    b  = ishft(ieor (ishft(s2,       24_k_i8),      s2), -50_k_i8)
    s2 = ieor (ishft(iand (s2,     -512_k_i8),  5_k_i8),        b)
    b  = ishft(ieor (ishft(s3,        3_k_i8),      s3), -23_k_i8)
    s3 = ieor (ishft(iand (s3,    -4096_k_i8), 29_k_i8),        b)
    b  = ishft(ieor (ishft(s4,        5_k_i8),      s4), -24_k_i8)
    s4 = ieor (ishft(iand (s4,  -131072_k_i8), 23_k_i8),        b)
    b  = ishft(ieor (ishft(s5,        3_k_i8),      s5), -33_k_i8)
    s5 = ieor (ishft(iand (s5, -8388608_k_i8),  8_k_i8),        b)

    ! The constant below is the reciprocal of (2^64 - 1)

    rn = ieor(ieor(ieor(ieor(s1, s2), s3), s4), s5) * &
         5.4210108624275221E-20_k_r8 + 0.5_k_r8

    ! -- Last executable statement --

    return

  !-----------------------------------------------------------------------------
  ! End execution part
  !-----------------------------------------------------------------------------

  !-----------------------------------------------------------------------------
  ! Begin subprogram part
  !-----------------------------------------------------------------------------

  !-----------------------------------------------------------------------------
  ! End subprogram part
  !-----------------------------------------------------------------------------

   end function lfsr258

!-------------------------------------------------------------------------------
! End subprogram part
!-------------------------------------------------------------------------------

end module calc_rand_lfsr258
