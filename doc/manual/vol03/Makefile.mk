#-------------------------------------------------------------------------------
# This file is part of the Computational Algorithm Library Collection
# Reference Manual.
# 
# Copyright 2012 The CALCC Team
#
# Permission is granted to copy, distribute and/or modify this
# document under the terms of the GNU Free Documentation License,
# Version 1.3 or any later version published by the Free Software
# Foundation; with no Invariant Sections, no Front-Cover Texts and
# no Back-Cover Texts.  A copy of the license is included in the
# section entitled "GNU Free Documentation License".
#-------------------------------------------------------------------------------

#-------------------------------------------------------------------------------
# Begin variables
#-------------------------------------------------------------------------------

SUBNAME  := vol03
SUBDIRS  := rng search
PDF      := $(SUBNAME).pdf
TEX      := $(wildcard $(addsuffix /*.tex, $(SUBDIRS))) $(SUBNAME).tex
AUX      := $(patsubst %.tex, %.aux, $(TEX))

#-------------------------------------------------------------------------------
# End variables
#-------------------------------------------------------------------------------

#-------------------------------------------------------------------------------
# Begin rues
#-------------------------------------------------------------------------------

all      : $(PDF)

$(PDF)   : $(TEX) $(BIB)
	pdflatex $(SUBNAME).tex && \
	bibtex   $(SUBNAME)     && \
	pdflatex $(SUBNAME).tex && \
	pdflatex $(SUBNAME).tex
 
#-------------------------------------------------------------------------------
# Begin directory-specific rules
#-------------------------------------------------------------------------------

#-------------------------------------------------------------------------------
# End directory-specific rules
#-------------------------------------------------------------------------------

clean    :
	$(RM) *.toc *.bbl *.blg *.log *.bak x.tex *.out $(AUX)

.PHONY   : all clean

#-------------------------------------------------------------------------------
# End rules
#-------------------------------------------------------------------------------
