#-------------------------------------------------------------------------------
# This file is part of the Computational Algorithm Library Collection (CALC).
#
# Copyright 2012 The CALC Team
#
# CALC is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# CALC is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with CALC.  If not, see <http://www.gnu.org/licenses/>.
#-------------------------------------------------------------------------------

#-------------------------------------------------------------------------------
# Begin variables
#-------------------------------------------------------------------------------

SUBNAME   := $(NAME)
SUBDIRS   :=

BIN       := $(basename $(wildcard *.f90))
OBJ       := $(patsubst %.f90, %.o, $(wildcard *.f90))

#-------------------------------------------------------------------------------
# End variables
#-------------------------------------------------------------------------------

#-------------------------------------------------------------------------------
# Begin rules
#-------------------------------------------------------------------------------

all       : $(BIN) $(SUBDIRS)

$(BIN)    : $(OBJ)
	for file in $(BIN); do \
	  $(FC) $(FCFLAGS) -o $$file $(LDFLAGS) $$file.o $(LIBPATHS) $(LIBS);\
	done
 
$(SUBDIRS):
	$(MAKE) -C $@ -f $(SUBMAKEFILE)

#-------------------------------------------------------------------------------
# Begin directory-specific rules
#-------------------------------------------------------------------------------

#-------------------------------------------------------------------------------
# End directory-specific rules
#-------------------------------------------------------------------------------

%.o: %.f90
	$(FC) $(FCFLAGS) $(INCPATHS) -c $<

clean     :
	@for dir in $(SUBDIRS); do                    \
	   $(MAKE) -C $$dir -f $(SUBMAKEFILE) clean ; \
	done;                                         \
	$(RM) $(MOD) $(OBJ)
 
clean_all:
	@for dir in $(SUBDIRS); do                        \
	   $(MAKE) -C $$dir -f $(SUBMAKEFILE) clean_all ; \
	done;                                             \
	$(RM) $(BIN) $(MOD) $(OBJ)
 
.PHONY    : all $(SUBDIRS) clean clean_all

#-------------------------------------------------------------------------------
# End rules
#-------------------------------------------------------------------------------
