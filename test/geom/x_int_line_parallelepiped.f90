program x_int_line_parallelepiped

!-------------------------------------------------------------------------------
! This file is part of the Computational Algorithm Library Collection (CALC).
!
! Copyright 2012 The CALC Team
!
! CALC is free software: you can redistribute it and/or modify
! it under the terms of the GNU Lesser General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! CALC is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU Lesser General Public License for more details.
!
! You should have received a copy of the GNU Lesser General Public License
! along with CALC.  If not, see <http://www.gnu.org/licenses/>.
!-------------------------------------------------------------------------------

!-------------------------------------------------------------------------------
! Begin specification part
!-------------------------------------------------------------------------------

  ! -- Use association --

  use calc, only: k_i1, k_r8
  use calc, only: int_line_parallelepiped

  ! -- Null mapping --

  implicit none

  ! -- Declarations --

  real    (kind = k_r8), dimension (3)                :: r1, us
  real    (kind = k_r8), dimension (3)                :: r2, a, b, c
  real    (kind = k_r8), dimension (2, 3)             :: ri
  integer (kind = k_i1), dimension (2)                :: isurf
  integer (kind = k_i1)                               :: ierr

!-------------------------------------------------------------------------------
! End specification part
!-------------------------------------------------------------------------------

!-------------------------------------------------------------------------------
! Begin execution part
!-------------------------------------------------------------------------------

  call calc_header()

  print '(2x, ''Usage example for subroutine int_line_parallelepiped'')'
  print '()'
  
  r1 = (/0.5_k_r8, 0.5_k_r8,  0.5_k_r8/)
  us = (/0.0_k_r8, 0.0_k_r8, 1.0_k_r8/)

  r2 = (/1.0_k_r8,  0.0_k_r8, 0.0_k_r8/)
  a  = (/0.0_k_r8,  0.0_k_r8, 1.0_k_r8/)
  b  = (/0.0_k_r8,  1.0_k_r8, 0.0_k_r8/)
  c  = (/-1.0_k_r8, 0.0_k_r8, 0.0_k_r8/)

  call int_line_parallelepiped(r1, us, r2, a, b, c, ri, isurf, ierr)

  if (ierr == -1) then
    print *, 'No intersection'
  else if (ierr == -2) then
    print *, 'Invalid cuboid definition'
  else if (ierr == -4) then
    print *, 'Unknown error' 
  else
    print *, 'Intersection points and the intersected faces are:'
    print *, ri(1,:), isurf(1)
    print *, ri(2,:), isurf(2)
  end if

  call calc_footer()

  ! -- Last executable statement --

  stop

!-------------------------------------------------------------------------------
! End execution part
!-------------------------------------------------------------------------------

!-------------------------------------------------------------------------------
! Begin subprogram part
!-------------------------------------------------------------------------------

!-------------------------------------------------------------------------------
! End subprogram part
!-------------------------------------------------------------------------------
 
end program x_int_line_parallelepiped
